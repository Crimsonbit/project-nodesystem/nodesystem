{
  "selectAll": {
    "combination": {
      "code": "A",
      "shift": "UP",
      "control": "DOWN",
      "alt": "UP",
      "meta": "UP",
      "shortcut": "UP"
    }
  },
  "deselectAll": {
    "combination": {
      "code": "D",
      "shift": "UP",
      "control": "DOWN",
      "alt": "UP",
      "meta": "UP",
      "shortcut": "UP"
    }
  },
  "copy": {
    "combination": {
      "code": "C",
      "shift": "UP",
      "control": "DOWN",
      "alt": "UP",
      "meta": "UP",
      "shortcut": "UP"
    }
  },
  "paste": {
    "combination": {
      "code": "V",
      "shift": "UP",
      "control": "DOWN",
      "alt": "UP",
      "meta": "UP",
      "shortcut": "UP"
    }
  },
  "remove": {
    "combination": {
      "code": "DELETE",
      "shift": "UP",
      "control": "UP",
      "alt": "UP",
      "meta": "UP",
      "shortcut": "UP"
    }
  },
  "executeGraph": {
    "combination": {
      "code": "X",
      "shift": "UP",
      "control": "DOWN",
      "alt": "UP",
      "meta": "UP",
      "shortcut": "UP"
    }
  },
  "executeNode": {
    "combination": {
      "code": "F",
      "shift": "UP",
      "control": "DOWN",
      "alt": "UP",
      "meta": "UP",
      "shortcut": "UP"
    }
  },
  "openSearchBar": {
    "combination": {
      "code": "SPACE",
      "shift": "UP",
      "control": "DOWN",
      "alt": "UP",
      "meta": "UP",
      "shortcut": "UP"
    }
  },
  "moveToOrigin": {
    "combination": {
      "code": "SPACE",
      "shift": "UP",
      "control": "UP",
      "alt": "UP",
      "meta": "UP",
      "shortcut": "UP"
    }
  },
  "saveGraph": {
    "combination": {
      "code": "S",
      "shift": "UP",
      "control": "DOWN",
      "alt": "UP",
      "meta": "UP",
      "shortcut": "UP"
    }
  },
  "loadGraph": {
    "combination": {
      "code": "L",
      "shift": "UP",
      "control": "DOWN",
      "alt": "UP",
      "meta": "UP",
      "shortcut": "UP"
    }
  },
  "newGraph": {
    "combination": {
      "code": "N",
      "shift": "UP",
      "control": "DOWN",
      "alt": "UP",
      "meta": "UP",
      "shortcut": "UP"
    }
  },
  "mergeGraph": {
    "combination": {
      "code": "M",
      "shift": "UP",
      "control": "DOWN",
      "alt": "UP",
      "meta": "UP",
      "shortcut": "UP"
    }
  }
}