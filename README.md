# NodeSystem
<p> NodeSystem is a JavaFX based Node-Graph </p>
<h1 align="center">
    <img src="https://gitlab.com/HTL-GTM/Software/nodesystem/raw/master/doc/nodesystem.png">
</h1>
<p align="center">
<sup>
<b>.</b>
</sup>
</p>

# About

## What is NodeSystem?
NodeSystem a javaFX based Node-Graph. It can be used as a standalone program or be integrated in any kind of JavaFX application.

## How does it work?
The NodeSystem is very easy to use and to extend. New nodes can be made very easily and will be described below.

# Examples

## Create a NodeGraph

```java
public class GUITest extends Application {

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Node Editor");
		
		DrawGraph drawGraph = new GraphBuilder().registerAllModules("Modules").build();
		
		Scene scene = new Scene(drawGraph.getBaseLayer());
		primaryStage.setScene(scene);
		primaryStage.show();
		
		//has to be initialized AFTER it has been added to the scene!
		drawGraph.init();

	}

	public static void main(String[] args) {
		launch(args);
	}
}

```

## Create custom nodes
Creating custom nodes, and loading them may change in future updates! This is because we are currently working on making the NodeSystem modular!

To create a custom node you need at least 3 classes!
<ul>
  <li>1. A class which describes a NodeGroup this class needs to implement INodeType. Typically this is an Enum.</li>
  <li>2. A class which represents the node inside the graph. Needs to extend AbstractNode</li>
  <li>3. A class to register the nodes to the graph.</li>
</ul>
To those 3 classes you need 1 json file which has to be at the top of the src hierarchy.

### 1 Creating a node group
This example shows how to create a new NodeGroup.
Every Enum constant represents a Node.
```java
public enum Math implements INodeType {
	ADD("Addition Node"), 
	SUBTRACT("Subtraction Node"), 
	MULTIPLY("Multiplier Node"), 
	DIVIDE("Divider Node"),
	MODULO("Modulo Node");

	private String name;

	private Math(String s) {

		this.name = s;
	}

	@Override
	public String toString() {
		return this.name;
	}

}
```

### 2 Create node classes
A Node is described by a class which extends AbstractNode.

There are basically four annotations that describe the node further:
<ul>
  <li>@NodeField -> Creates a field element in the gui, this let's the user set a value in the GUI.</li>
  <li>@NodeInput -> Tells the NodeSystem that this is an input. The variable can be any Object. The port name is the variable name</li>
  <li>@NodeOutput("") -> Tells the NodeSystem that this is an output. The port name is the variable name</li>
</ul>

```java

public class AddNode extends AbstractNode {
	
	@NodeInput
	double in_1;

	@NodeInput
	double in_2;

	@NodeOutput("compute")
	double output;

	public AddNode() {}

	@Override
	public void compute() {
		output = in_1 + in_2;
	}

}

```
### 3 NodeModule - register the nodes 
The final class you need is the module itself. 
This class has to extend NodeModule. 
You can ignore the registerGUINodes, this is deprecated!

```java
public class MathNodeModule extends NodeModule {

	@Override
	public void registerNodes(NodeRegistry registry) {
		registry.registerDefaultFactory(Math.ADD, AddNode.class);
		registry.registerDefaultFactory(Math.DIVIDE, DivideNode.class);
		registry.registerDefaultFactory(Math.MODULO, ModuloNode.class);
		registry.registerDefaultFactory(Math.MULTIPLY, MultiplyNode.class);
		registry.registerDefaultFactory(Math.SUBTRACT, SubtractNode.class);
	}

	@Override
	public void registerGUINodes(GUINodeRegistry registry) {
	}
}
```

### 4 The json file
It needs to be name "moduledefs.json"
In case of our math module it will look like this:
```json
{
	"id": "math_nodes",
	"version": "1.0"
}

```


The GUI is automatically generated!

To view a complete example have a look at: 
https://gitlab.com/Crimsonbit/NodeSystem-MathNodes/tree/dev


# Mobile Support
The Nodesystem almost completely supports mobile devices that run on Android or IOS.
To view an example check out this repo here: https://gitlab.com/HTL-GTM/Software/nodesystem-mobile

# Help us
You are always welcome to work with us on this project! 
