package at.crimsonbit.nodesystem.plugin.internal;

import java.io.IOException;

import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.plugin.dataType.CommandData;
import at.crimsonbit.nodesystem.plugin.impl.PluginMaster;
import javafx.scene.input.KeyCodeCombination;

public class ReloadPluginsCommand extends CommandData {

	@Override
	public String getPluginID() {
		return "id:cmd:reloadplugins";
	}

	@Override
	public String getName() {
		return "Reload Plugins";
	}

	@Override
	public boolean load() {
		return true;
	}

	@Override
	public void unload() {
	}

	@Override
	public void postLoad() {
	}

	@Override
	public void execute(DrawGraph graph) {
		try {
			PluginMaster.get().reloadPlugins();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public KeyCodeCombination getShortcut() {
		return null;
	}

	@Override
	public String getMenu() {
		return "Plugins";
	}

}
