package at.crimsonbit.nodesystem.plugin.internal;

import java.io.File;
import java.io.IOException;

import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.plugin.dataType.CommandData;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class SaveCommand extends CommandData {
	private final String PLUGIN_ID = "id:cmd:internal:save";
	private final String NAME = "Save Graph";
	private FileChooser dirChooser = new FileChooser();
	private final String MENU = "File";

	@Override
	public String getMenu() {
		return MENU;
	}

	@Override
	public String getPluginID() {
		return PLUGIN_ID;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean load() {
		
		//dirChooser.getExtensionFilters().add(new ExtensionFilter("Zip File", ".zip"));
		return true;
	}

	@Override
	public void unload() {

	}

	@Override
	public void postLoad() {

	}

	@Override
	public void execute(DrawGraph graph) {
		dirChooser.setTitle("Save Nodesystem Folder");

		File saveLoc = dirChooser.showSaveDialog(graph.getBaseLayer().getScene().getWindow());
		if (saveLoc != null)
			try {
				graph.saveGraph(saveLoc.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	@Override
	public KeyCodeCombination getShortcut() {
		return new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN);
	}

	@Override
	public Image getIcon() {
		return new Image(getClass().getResourceAsStream("/icons/save.png"));
	}
}
