package at.crimsonbit.nodesystem.plugin.internal;

import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.plugin.dataType.CommandData;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyCombination.Modifier;

public class GroupCommand extends CommandData {

	private final String PLUGIN_ID = "id:cmd:internal:group";
	private final String NAME = "Group";
	private final String MENU = "Edit";

	@Override
	public String getPluginID() {
		return PLUGIN_ID;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean load() {
		return true;
	}

	@Override
	public void unload() {

	}

	@Override
	public void postLoad() {

	}

	@Override
	public void execute(DrawGraph graph) {
		graph.groupSelectedNodes();
	}

	@Override
	public KeyCodeCombination getShortcut() {
		return new KeyCodeCombination(KeyCode.G, KeyCombination.CONTROL_DOWN);
	}

	@Override
	public String getMenu() {
		return MENU;
	}

}
