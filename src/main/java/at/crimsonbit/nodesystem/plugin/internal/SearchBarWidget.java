/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.plugin.internal;

import java.util.Map;

import com.google.common.eventbus.EventBus;

import at.crimsonbit.nodesystem.event.BaseEvent;
import at.crimsonbit.nodesystem.event.WidgetCloseEvent;
import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.frontend.gui.GuiState;
import at.crimsonbit.nodesystem.frontend.gui.guilang.GLShadow;
import at.crimsonbit.nodesystem.nodebackend.INodeType;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import at.crimsonbit.nodesystem.plugin.dataType.WidgetData;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class SearchBarWidget extends WidgetData {

	private AnchorPane mainPane;
	private ToolBar searchToolBar;
	private TextField searchField;
	private Button addButton;
	private Button closeButton;
	private ListView<String> searchList;
	private EventBus eventBus;
	private double x = 0;
	private double y = 0;
	private double mousex = 0;
	private double mousey = 0;
	public boolean toggle = false;
	private NodeGraph graph;
	private DrawGraph drawGraph;

	private Map<String, INodeType> availableNodesMap;

	public void createLayout() {
		searchField = new TextField();
		addButton = new Button("Add");
		closeButton = new Button("X");
		searchList = new ListView<>();
		searchToolBar = new ToolBar(searchField, addButton, closeButton);
		searchList.getItems().addAll(availableNodesMap.keySet());
		searchList.setVisible(false);

		DropShadow shadowE = new DropShadow();
		shadowE.setBlurType(BlurType.GAUSSIAN);
		GLShadow shadow = new GLShadow();
		shadow.setShadow_color(Color.BLACK.brighter().desaturate());
		shadow.setShadow_offset_x(5D);
		shadow.setShadow_offset_y(5D);

		shadowE.setColor(shadow.getShadow_color());
		shadowE.setWidth(shadow.getShadow_width());
		shadowE.setHeight(shadow.getShadow_height());
		shadowE.setOffsetX(shadow.getShadow_offset_x());
		shadowE.setOffsetY(shadow.getShadow_offset_y());
		shadowE.setRadius(shadow.getShadow_radius());
		mainPane.setEffect(shadowE);

		mainPane.getChildren().add(searchToolBar);

		mainPane.getChildren().add(searchList);

		AnchorPane.setTopAnchor(searchToolBar, 0D);
		AnchorPane.setLeftAnchor(searchToolBar, 0D);
		AnchorPane.setRightAnchor(searchToolBar, 0D);
		AnchorPane.setTopAnchor(searchList, 40D);
		AnchorPane.setBottomAnchor(searchList, 0D);
		AnchorPane.setLeftAnchor(searchList, 0D);
		AnchorPane.setRightAnchor(searchList, 0D);

		closeButton.setOnAction(event -> {
			hide();
		});

		searchList.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
					// System.out.println(event.getTarget());
					if (event.getTarget() instanceof Text) {
						postAddEvent();
					}
				}
			}
		});

		searchToolBar.setOnMousePressed(event -> {
			if (!event.isPrimaryButtonDown())
				return;

			GuiState.setState(GuiState.SEARCH);

			mousex = event.getSceneX();
			mousey = event.getSceneY();

			x = mainPane.getLayoutX();
			y = mainPane.getLayoutY();

		});

		searchToolBar.setOnMouseDragged(event -> {
			if (!event.isPrimaryButtonDown())
				return;

			if (GuiState.getState().equals(GuiState.SEARCH)) {
				double offsetX = event.getSceneX() - mousex;
				double offsetY = event.getSceneY() - mousey;

				x += offsetX;
				y += offsetY;

				double scaledX = x;
				double scaledY = y;

				mainPane.setLayoutX(scaledX);
				mainPane.setLayoutY(scaledY);

				mousex = event.getSceneX();
				mousey = event.getSceneY();

				event.consume();
			}

		});

		searchToolBar.setOnMouseClicked(event -> {
			GuiState.setState(GuiState.SEARCH);
			if (event.getClickCount() >= 2) {
				toggle = !toggle;
				searchList.setVisible(toggle);
				event.consume();
			}

		});

		searchField.setOnKeyPressed(event -> {

			searchList.getItems().clear();

			for (String s : availableNodesMap.keySet()) {
				if (s.contains(searchField.getText()))
					searchList.getItems().add(s);
			}
			searchList.setVisible(true);
			if (searchList.getItems().size() > 0) {
				searchList.getSelectionModel().select(0);

			}

			if (event.getCode().equals(KeyCode.ESCAPE)) {
				GuiState.setState(GuiState.DEFAULT);
				mainPane.setVisible(false);
				eventBus.post(new WidgetCloseEvent(this));
				event.consume();

			} else if (event.getCode().equals(KeyCode.ENTER)) {
				postAddEvent();
				event.consume();
				GuiState.setState(GuiState.DEFAULT);
			}

		});

		addButton.setOnAction(event -> {
			postAddEvent();
			event.consume();
		});

		searchField.setFocusTraversable(true);
		searchList.setFocusTraversable(false);
		addButton.setFocusTraversable(false);
		closeButton.setFocusTraversable(false);
		Platform.runLater(() -> mainPane.requestFocus());
		Platform.runLater(() -> searchField.requestFocus());
	}

	private void postAddEvent() {
		if (searchList.getItems().size() > 0) {
			String selection = searchList.getSelectionModel().getSelectedItem();
			INodeType type = graph.getMaster().getStrToType().get(selection);
			if (type != null)
				graph.addNode(type);
		}
	}

	public void hide() {
		GuiState.setState(GuiState.DEFAULT);
		mainPane.setVisible(false);
		eventBus.post(new WidgetCloseEvent(this));
	}

	@Override
	public String getPluginID() {
		return "widget:id:searchbar";
	}

	@Override
	public String getName() {
		return "SearchBarWidget";
	}

	@Override
	public boolean load() {
		return true;
	}

	@Override
	public void unload() {

	}

	@Override
	public void postLoad() {

	}

	@Override
	public void initWidget(DrawGraph graph) {
		this.graph = graph.getNodeGraph();
		this.drawGraph = graph;
		eventBus = this.drawGraph.getGlobalEventBus();
		this.availableNodesMap = this.graph.getMaster().getStrToType();
	}

	@Override
	public void onMessage(BaseEvent event) {

	}

	@Override
	public void freeWidget() {
	}

	@Override
	public void open(double x, double y) {
	
		mainPane = new AnchorPane();
		mainPane.setPrefWidth(400D);
		mainPane.setPrefHeight(140D);
		mainPane.setPrefSize(400D, 140D);
		mainPane.setMaxHeight(Control.USE_PREF_SIZE);
		mainPane.setMaxWidth(Control.USE_PREF_SIZE);
		createLayout();
		GuiState.setState(GuiState.SEARCH);
		mainPane.setVisible(true);
		mainPane.setTranslateX(x);
		mainPane.setTranslateY(y);
		searchField.setFocusTraversable(true);
		searchList.setFocusTraversable(false);
		addButton.setFocusTraversable(false);
		closeButton.setFocusTraversable(false);
		Platform.runLater(() -> mainPane.requestFocus());
		Platform.runLater(() -> searchField.requestFocus());
	}

	@Override
	public Node getLayout() {
		return mainPane;
	}

	@Override
	public KeyCodeCombination getShortcut() {
		return new KeyCodeCombination(KeyCode.SPACE, KeyCodeCombination.CONTROL_DOWN);
	}

}
