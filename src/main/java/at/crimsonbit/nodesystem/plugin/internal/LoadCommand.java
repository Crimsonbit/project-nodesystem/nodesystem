package at.crimsonbit.nodesystem.plugin.internal;

import java.io.File;
import java.io.IOException;

import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.nodebackend.exception.NoSuchNode;
import at.crimsonbit.nodesystem.plugin.dataType.CommandData;
import htl.gtm.nodeLangParser.exception.NodeLangException;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class LoadCommand extends CommandData {

	private final String PLUGIN_ID = "id:cmd:internal:load";
	private final String NAME = "Load Graph";
	private DrawGraph graph;
	private FileChooser dirChooser = new FileChooser();
	private final String MENU = "File";

	@Override
	public String getMenu() {
		return MENU;
	}

	@Override
	public String getPluginID() {
		return PLUGIN_ID;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean load() {
	//	dirChooser.getExtensionFilters().add(new ExtensionFilter("Zip File", ".zip", ".ZIP"));
		return true;
	}

	@Override
	public void unload() {

	}

	@Override
	public void postLoad() {

	}

	@Override
	public void execute(DrawGraph graph) {
		this.graph = graph;
		dirChooser.setTitle("Load Node Graph");

		File saveLoc = dirChooser.showOpenDialog(this.graph.getBaseLayer().getScene().getWindow());
		if (saveLoc != null)
			if (saveLoc.exists())
				try {
					this.graph.loadGraphFromFolder(saveLoc.toPath());
				} catch (NoSuchNode | IOException | NodeLangException e) {
					e.printStackTrace();
				}
	}

	@Override
	public KeyCodeCombination getShortcut() {
		return new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN);
	}

	@Override
	public Image getIcon() {
		return new Image(getClass().getResourceAsStream("/icons/load.png"));
	}
}
