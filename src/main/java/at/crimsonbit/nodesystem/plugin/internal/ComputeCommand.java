package at.crimsonbit.nodesystem.plugin.internal;

import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.plugin.dataType.CommandData;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;

public class ComputeCommand extends CommandData {
	private final String PLUGIN_ID = "id:cmd:internal:compute";
	private final String NAME = "Compute Graph";
	private final String MENU = "Compute";
	
	@Override
	public String getMenu() {
		return MENU;
	}
	
	@Override
	public String getPluginID() {
		return PLUGIN_ID;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean load() {
		return true;
	}

	@Override
	public void unload() {

	}

	@Override
	public void postLoad() {

	}

	@Override
	public void execute(DrawGraph graph) {
		graph.executeGraph();
	}

	@Override
	public KeyCodeCombination getShortcut() {
		return new KeyCodeCombination(KeyCode.X, KeyCombination.CONTROL_DOWN);

	}

}
