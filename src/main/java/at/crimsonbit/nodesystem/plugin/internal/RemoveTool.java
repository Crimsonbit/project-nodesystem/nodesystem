/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.plugin.internal;

import at.crimsonbit.nodesystem.event.KeyboardBaseEvent;
import at.crimsonbit.nodesystem.event.MouseBaseEvent;
import at.crimsonbit.nodesystem.event.MouseNodeClickedEvent;
import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.plugin.dataType.ToolData;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class RemoveTool extends ToolData {

	private DrawGraph graph;

	@Override
	public String getPluginID() {
		return "tool:id:remove";
	}

	@Override
	public boolean load() {
		// System.out.println("Loading: " + getPluginID());
		return true;
	}

	@Override
	public void unload() {

	}

	@Override
	public void postLoad() {
		// System.out.println(getPluginID() + ": Post Load");
	}

	@Override
	public void initTool(DrawGraph graph) {
		this.graph = graph;
	}

	@Override
	public void freeTool() {
	}

	@Override
	public Node draw() {
		return null;
	}

	@Override
	public void onMouseInput(MouseBaseEvent mouseEvent) {
		if (mouseEvent instanceof MouseNodeClickedEvent) {
			graph.deleteSelectedNodes();
		}
	}

	@Override
	public void onKeyboardInput(KeyboardBaseEvent keyboardEvent) {

	}

	@Override
	public String getName() {
		return "Remove Tool";
	}

	@Override
	public Image getIcon() {
		return new Image(getClass().getResourceAsStream("/icons/removeTool.png"));
	}

	@Override
	public KeyCodeCombination getShortcut() {
		return new KeyCodeCombination(KeyCode.R, KeyCombination.SHIFT_DOWN);
	}

	@Override
	public String getToolTip() {
		return "Can remove every node in the scene";
	}
}
