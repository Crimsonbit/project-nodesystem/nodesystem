package at.crimsonbit.nodesystem.plugin.internal;

import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.plugin.dataType.CommandData;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;

public class NewCommand extends CommandData {
	private final String PLUGIN_ID = "id:cmd:internal:new";
	private final String NAME = "New Graph";
	private final String MENU = "File";

	@Override
	public String getMenu() {
		return MENU;
	}

	@Override
	public String getPluginID() {
		return PLUGIN_ID;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean load() {
		return true;
	}

	@Override
	public void unload() {

	}

	@Override
	public void postLoad() {

	}

	@Override
	public void execute(DrawGraph graph) {
		graph.createNewGraph();
	}

	@Override
	public KeyCodeCombination getShortcut() {
		return new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN);

	}
	@Override
	public Image getIcon() {
		return new Image(getClass().getResourceAsStream("/icons/fileNew.png"));
	}
}
