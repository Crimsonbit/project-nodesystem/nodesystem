package at.crimsonbit.nodesystem.plugin.internal;

import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.plugin.dataType.CommandData;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;

public class MoveToOriginCommand extends CommandData {
	private final String PLUGIN_ID = "id:cmd:internal:movetoorigin";
	private final String NAME = "Move To Origin";
	private final String MENU = "Edit";

	@Override
	public String getMenu() {
		return MENU;
	}

	@Override
	public String getPluginID() {
		return PLUGIN_ID;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean load() {
		return true;
	}

	@Override
	public void unload() {

	}

	@Override
	public void postLoad() {

	}

	@Override
	public void execute(DrawGraph graph) {
		graph.moveToOrigin();
	}

	@Override
	public KeyCodeCombination getShortcut() {
		return new KeyCodeCombination(KeyCode.SPACE);

	}
}
