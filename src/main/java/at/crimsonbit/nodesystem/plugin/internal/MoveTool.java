/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.plugin.internal;

import java.util.HashMap;
import java.util.Map;

import at.crimsonbit.nodesystem.event.KeyboardBaseEvent;
import at.crimsonbit.nodesystem.event.MouseBaseEvent;
import at.crimsonbit.nodesystem.event.MouseNodeClickedEvent;
import at.crimsonbit.nodesystem.event.MouseNodeDraggedEvent;
import at.crimsonbit.nodesystem.event.MouseNodePressedEvent;
import at.crimsonbit.nodesystem.event.MouseNodeReleasedEvent;
import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.frontend.gui.GuiState;
import at.crimsonbit.nodesystem.frontend.gui.drawable.DrawNode;
import at.crimsonbit.nodesystem.plugin.dataType.ToolData;
import at.crimsonbit.nodesystem.util.DragContext;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TouchEvent;

public class MoveTool extends ToolData {

	private DrawGraph nodeGraph;
	private Map<DrawNode, DragContext> dragMap = new HashMap<DrawNode, DragContext>();

	@Override
	public String getPluginID() {
		return "tool:id:move";
	}

	@Override
	public boolean load() {
		// System.out.println(getPluginID());
		return true;
	}

	@Override
	public void unload() {

	}

	@Override
	public void postLoad() {
		// System.out.println(getPluginID() + ": Post Load");
	}

	@Override
	public void initTool(DrawGraph graph) {
		this.nodeGraph = graph;
	}

	@Override
	public void freeTool() {
		System.out.println("Tool Freed");
		this.nodeGraph = null;
	}

	@Override
	public Node draw() {
		return null;
	}

	/**
	 * Populates the drag map with the drawcontext and the corresponding node. This
	 * is used internally for node dragging.
	 * 
	 * @param event the mouse event
	 */
	private void populateDragMap(MouseEvent event) {
		dragMap.clear();
		for (DrawNode node : nodeGraph.getSelectedNodes()) {
			DragContext drawContext = new DragContext();
			drawContext.mouseAnchorX = event.getSceneX();
			drawContext.mouseAnchorY = event.getSceneY();
			drawContext.translateAnchorX = node.getLayoutX();
			drawContext.translateAnchorY = node.getLayoutY();
			node.toFront();
			node.requestFocus();
			dragMap.put(node, drawContext);
		}
	}

	/**
	 * Populates the drag map with the drawcontext and the corresponding node. This
	 * is used internally for node dragging.
	 * 
	 * @param event the touch event
	 */
	private void populateDragMap(TouchEvent event) {
		dragMap.clear();
		for (DrawNode node : nodeGraph.getSelectedNodes()) {
			DragContext drawContext = new DragContext();
			drawContext.mouseAnchorX = event.getTouchPoint().getSceneX();
			drawContext.mouseAnchorY = event.getTouchPoint().getSceneY();
			drawContext.translateAnchorX = node.getLayoutX();
			drawContext.translateAnchorY = node.getLayoutY();
			node.toFront();
			nodeGraph.addToSelection(node);
			node.requestFocus();
			dragMap.put(node, drawContext);
		}
	}

	@Override
	public void onMouseInput(MouseBaseEvent mouseEvent) {
		if (mouseEvent instanceof MouseNodeClickedEvent) {
			MouseNodeClickedEvent e = (MouseNodeClickedEvent) mouseEvent;
			if (!GuiState.getState().equals(GuiState.RESIZE)) {
				if (e.event instanceof MouseEvent) {
					MouseEvent event = (MouseEvent) e.event;
					DrawNode node = nodeGraph.getNode(e.nodeID);
					if (event.getClickCount() >= 2) {
						node.toggle = !node.toggle;
						node.createLayout();
					}
				} else if (e.event instanceof TouchEvent) {
					TouchEvent event = (TouchEvent) e.event;
					DrawNode node = nodeGraph.getNode(e.nodeID);
					if (event.getTouchCount() >= 2) {
						node.toggle = !node.toggle;
						node.createLayout();
					}
					nodeGraph.selectNode(node);
				}
			}
		} else if (mouseEvent instanceof MouseNodeDraggedEvent) {
			MouseNodeDraggedEvent e = (MouseNodeDraggedEvent) mouseEvent;
			if (e.event instanceof MouseEvent) {
				MouseEvent event = (MouseEvent) e.event;
				if (!event.isPrimaryButtonDown())
					return;

				if (GuiState.getState().equals(GuiState.NODE_DRAGGING)) {
					if (nodeGraph.getNode(e.nodeID).isSelected()) {
						if (dragMap.size() > 0)
							for (DrawNode _node : dragMap.keySet()) {
								DrawNode node = nodeGraph.getNode(_node.getNode().getBackendID());
								DragContext context = dragMap.get(_node);
								double scale = node.getScaleProperty().get();
								node.setLayoutX(context.translateAnchorX
										+ ((event.getSceneX() - context.mouseAnchorX) / scale));
								node.setLayoutY(context.translateAnchorY
										+ ((event.getSceneY() - context.mouseAnchorY) / scale));

							}
					} else if (event.isShiftDown()) {
						nodeGraph.addToSelection(nodeGraph.getNode(e.nodeID));
						populateDragMap(event);
					} else {
						nodeGraph.selectNode(nodeGraph.getNode(e.nodeID));
						populateDragMap(event);
					}
					e.event.consume();
				}
			} else if (e.event instanceof TouchEvent) {

				TouchEvent event = (TouchEvent) e.event;
				if (GuiState.getState().equals(GuiState.NODE_DRAGGING)) {
					if (nodeGraph.getNode(e.nodeID).isSelected())
						if (dragMap.size() > 0)
							for (DrawNode _node : dragMap.keySet()) {
								DrawNode node = nodeGraph.getNode(_node.getNode().getBackendID());
								DragContext context = dragMap.get(_node);
								double scale = node.getScaleProperty().get();
								node.setLayoutX(context.translateAnchorX
										+ ((event.getTouchPoint().getSceneX() - context.mouseAnchorX) / scale));
								node.setLayoutY(context.translateAnchorY
										+ ((event.getTouchPoint().getSceneY() - context.mouseAnchorY) / scale));

							}
					e.event.consume();
				}
			}
		} else if (mouseEvent instanceof MouseNodePressedEvent) {
			MouseNodePressedEvent e = (MouseNodePressedEvent) mouseEvent;
			if (e.event instanceof MouseEvent) {
				MouseEvent event = (MouseEvent) e.event;
				if (!event.isPrimaryButtonDown())
					return;

				if (!(GuiState.getState().equals(GuiState.RESIZE)) && (GuiState.getState().equals(GuiState.DEFAULT)
						|| GuiState.getState().equals(GuiState.SEARCH))) {
					GuiState.setState(GuiState.NODE_DRAGGING);
				}

				// selectNode(nodeCache.getNode(e.nodeID));
				populateDragMap(event);
				e.event.consume();
			} else if (e.event instanceof TouchEvent) {

				TouchEvent event = (TouchEvent) e.event;
				if (!(GuiState.getState().equals(GuiState.RESIZE)) && GuiState.getState().equals(GuiState.DEFAULT)
						|| GuiState.getState().equals(GuiState.SEARCH)) {
					GuiState.setState(GuiState.NODE_DRAGGING);
				}
				// selectNode(nodeCache.getNode(e.nodeID));
				populateDragMap(event);
				e.event.consume();
			}
		} else if (mouseEvent instanceof MouseNodeReleasedEvent) {
			GuiState.setState(GuiState.DEFAULT);
			dragMap.clear();
		}
	}

	@Override
	public void onKeyboardInput(KeyboardBaseEvent keyboardEvent) {

	}

	@Override
	public String getName() {
		return "Move Tool";
	}

	@Override
	public Image getIcon() {
		return new Image(getClass().getResourceAsStream("/icons/moveTool.png"));
	}

	@Override
	public KeyCodeCombination getShortcut() {
		return new KeyCodeCombination(KeyCode.M, KeyCombination.SHIFT_DOWN);
	}

	@Override
	public String getToolTip() {
		return "Can move every node in the scene";
	}

}
