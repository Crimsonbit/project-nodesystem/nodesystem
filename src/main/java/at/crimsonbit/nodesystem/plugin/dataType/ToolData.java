/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.plugin.dataType;

import com.google.common.eventbus.Subscribe;

import at.crimsonbit.nodesystem.event.KeyboardBaseEvent;
import at.crimsonbit.nodesystem.event.MouseBaseEvent;
import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.plugin.impl.IBasePlugin;
import javafx.scene.Node;
import javafx.scene.input.KeyCodeCombination;

/**
 * <h1>ToolData</h1><br>
 * Base class for all tool data plugins in the node system.
 * 
 * @author Florian Wagner
 *
 */
public abstract class ToolData implements IBasePlugin {

	/**
	 * Called whenever the tool is selected.
	 * 
	 * @param graph the graph where the plugin is registered to
	 */
	public abstract void initTool(DrawGraph graph);

	/**
	 * Called bevore a new tool is selected. This method can be used to free
	 * resources.
	 */
	public abstract void freeTool();

	/**
	 * Called to do drawing operations in the viewport.
	 */
	public abstract Node draw();

	/**
	 * Called for every mouse input that is received
	 * 
	 * @param mouseEvent the mouse event
	 */
	@Subscribe
	public abstract void onMouseInput(MouseBaseEvent mouseEvent);

	/**
	 * This method returns the tooltip for this tool.
	 * 
	 * @return the tooltip for this tool
	 */
	public abstract String getToolTip();

	/**
	 * Called for every keyboard input that is received
	 * 
	 * @param keyboardEvent the keyboard event
	 */
	@Subscribe
	public abstract void onKeyboardInput(KeyboardBaseEvent keyboardEvent);

	/**
	 * This method is used to set a shortcut to this tooldata plugin.
	 * 
	 * @return the shortcut to use
	 */
	public abstract KeyCodeCombination getShortcut();
}
