/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.plugin.dataType;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import at.crimsonbit.nodesystem.event.BaseEvent;
import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.plugin.impl.IBasePlugin;

/**
 * <h1>MessageData</h1><br>
 * Base class for all message data plugins in the node system.
 * {@link MessageData} plugins will be registerd to the internal
 * {@link EventBus} as soon as they are loaded. Make sure to check for the right
 * event as a {@link MessageData} plugin will receive every event send by the
 * nodesystem.
 * 
 * @author Florian Wagner
 *
 */
public abstract class MessageData implements IBasePlugin {

	/**
	 * Called for every message (event) that is send by the nodesystem. Make sure to
	 * check for specific events!
	 * 
	 * @param event the event that has been send.
	 */
	@Subscribe
	public abstract void onMessage(BaseEvent event);

	/**
	 * This method is called as soon as the plugin is initialized by the graph.
	 * 
	 * @param graph the {@link DrawGraph} where this {@link MessageData} is
	 * registered to
	 */
	public abstract void initMessageData(DrawGraph graph);
}