/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.plugin.dataType;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import at.crimsonbit.nodesystem.event.BaseEvent;
import at.crimsonbit.nodesystem.event.WidgetCloseEvent;
import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.plugin.impl.IBasePlugin;
import javafx.scene.Node;
import javafx.scene.input.KeyCodeCombination;

/**
 * <h1>WidgetData</h1><br>
 * Base class for all widget data plugins in the node system.
 * 
 * @author Florian Wagner
 *
 */
public abstract class WidgetData implements IBasePlugin {

	/**
	 * Called whenever the widget is selected.
	 */
	public abstract void initWidget(DrawGraph graph);

	/**
	 * Called when this widget is being closed. This method can be used to free
	 * resources.
	 */
	public abstract void freeWidget();

	/**
	 * Called when this widget is opened. For closing you have to send a
	 * {@link WidgetCloseEvent} to the eventSystem. The {@link EventBus} can be
	 * optained trough the {@link DrawGraph#getGlobalEventBus()} in the
	 * {@link #initWidget(DrawGraph)} method.
	 * 
	 * @param x x position of the mouse in scene coordinates
	 * @param y y position of the mouse in scene coordinates
	 */
	public abstract void open(double x, double y);

	/**
	 * This should return the layout of the widget. Make sure to store the base
	 * {@link Node} inside of the widget as the returned node is used to add and
	 * remove it from the graph
	 * 
	 * @return the layout of the widget
	 */
	public abstract Node getLayout();

	/**
	 * Called for every message (event) that is send by the graph.
	 * 
	 * @param event the event that has been send
	 */
	@Subscribe
	public abstract void onMessage(BaseEvent event);

	/**
	 * This method is used to set a shortcut to this tooldata plugin.
	 * 
	 * @return the shortcut to use
	 */
	public abstract KeyCodeCombination getShortcut();
}
