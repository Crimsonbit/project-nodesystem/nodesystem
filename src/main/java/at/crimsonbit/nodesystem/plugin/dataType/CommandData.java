/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.plugin.dataType;

import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.plugin.impl.IBasePlugin;
import javafx.scene.input.KeyCodeCombination;

/**
 * <h1>CommandData</h1><br>
 * Base class for all command data plugins in the node system.
 * 
 * @author Florian Wagner
 *
 */
public abstract class CommandData implements IBasePlugin {

	/**
	 * Called when the plugin is selected by the user.
	 * 
	 * @param graph the graph where the plugin is registered to
	 */
	public abstract void execute(DrawGraph graph);

	/**
	 * This method is used to set a shortcut to this tooldata plugin.
	 * 
	 * @return the shortcut to use
	 */
	public abstract KeyCodeCombination getShortcut();

	/**
	 * Return the name of the menu where the command should be added to.
	 * 
	 * @return the menu name
	 */
	public abstract String getMenu();

}
