/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.plugin.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import com.google.common.eventbus.EventBus;

import at.crimsonbit.nodesystem.event.PluginLoadedEvent;
import at.crimsonbit.nodesystem.event.PluginUnloadedEvent;
import at.crimsonbit.nodesystem.event.PluginsFinishedLoadingEvent;
import at.crimsonbit.nodesystem.event.PluginsFinishedUnloadingEvent;
import at.crimsonbit.nodesystem.plugin.dataType.CommandData;
import at.crimsonbit.nodesystem.plugin.dataType.MessageData;
import at.crimsonbit.nodesystem.plugin.dataType.ToolData;
import at.crimsonbit.nodesystem.plugin.dataType.WidgetData;

/**
 * <h1>PluginMaster</h1><br>
 * The {@link PluginMaster} is the link between the plugins and the nodesystem.
 * It is responisble for loading and registering the plugins into the
 * nodesystem. To get the plugin master use the {@link #get()} method. The
 * {@link #create(EventBus)} method should only ever be used internally!
 * 
 * @author Florian Wagner
 * @see IBasePlugin
 *
 */
public class PluginMaster {

	private Map<String, IBasePlugin> pluginMap = new HashMap<String, IBasePlugin>();
	private ClassLoader classLoader = ClassLoader.getSystemClassLoader();
	private final EventBus eventBus;
	private static PluginMaster pluginMaster;
	private Path pluginPath;
	
	/**
	 * Creates a new PluginMaster object. This method is only meant to be used
	 * internally by the nodesystem.
	 * 
	 * @param bus the global {@link EventBus} of the nodesystem.
	 * @return the newly allocated {@link PluginMaster}
	 */
	public static PluginMaster create(EventBus bus) {
		if (pluginMaster == null)
			pluginMaster = new PluginMaster(bus);
		return pluginMaster;
	}

	/**
	 * Returns the {@link PluginMaster} object.
	 * 
	 * @return the plugin master
	 */
	public static PluginMaster get() {
		return pluginMaster;
	}

	private PluginMaster(EventBus bus) {
		this.eventBus = bus;
		this.eventBus.register(this);
	}

	/**
	 * Tries to load plugins from the folder or file given with pluginsPath.
	 * 
	 * @param pluginsPath the path to load the plugins from.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public void loadPlugins(Path pluginsPath)

			throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		pluginPath = pluginsPath;
		String[] paths = Files
				.find(pluginsPath, Integer.MAX_VALUE, (p, a) -> p.getFileName().toString().endsWith(".jar"))
				.map(p -> p.toString()).toArray(String[]::new);

		_loadPlugins(paths);
		eventBus.post(new PluginsFinishedLoadingEvent(pluginMap.values()));
	}

	/**
	 * Returns the plugin which corresponds to the id given or null if none was
	 * found.
	 * 
	 * @param id the id of the plugin
	 * @return the plugin with given id
	 */
	public IBasePlugin findPlugin(String id) {
		
		return pluginMap.get(id);
	}

	/**
	 * Unloads all plugins.
	 */
	public void unloadPlugins() {
		Iterator<Map.Entry<String, IBasePlugin>> iter = pluginMap.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<String, IBasePlugin> entry = iter.next();
			iter.remove();
			eventBus.post(new PluginUnloadedEvent(entry.getValue()));
		}
		eventBus.post(new PluginsFinishedUnloadingEvent(pluginMap.values()));
	}

	/**
	 * Reloads all plugins.
	 * 
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IOException
	 */
	public void reloadPlugins()
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
		if (pluginPath != null) {
			unloadPlugins();
			loadPlugins(pluginPath);
		}
	}

	/**
	 * Unloads the plugin given.
	 * 
	 * @param plugin the plugin to unload
	 */
	public void unloadPlugin(IBasePlugin plugin) {
		plugin.unload();
		pluginMap.remove(plugin.getPluginID());
		eventBus.post(new PluginUnloadedEvent(plugin));
		eventBus.post(new PluginsFinishedUnloadingEvent(pluginMap.values()));
	}

	/**
	 * Returns a list of registered {@link ToolData} plugins.
	 * 
	 * @return List<ToolData>
	 */
	public List<ToolData> getToolDataPlugins() {
		List<ToolData> retList = new ArrayList<>();
		for (IBasePlugin plugin : pluginMap.values())
			if (plugin instanceof ToolData)
				retList.add((ToolData) plugin);
		return retList;
	}

	/**
	 * Returns a list of registered {@link WidgetData} plugins.
	 * 
	 * @return List<WidgetData>
	 */
	public List<WidgetData> getWidgetDataPlugins() {
		List<WidgetData> retList = new ArrayList<>();
		for (IBasePlugin plugin : pluginMap.values())
			if (plugin instanceof WidgetData)
				retList.add((WidgetData) plugin);
		return retList;
	}

	/**
	 * Returns a list of registered {@link CommandData} plugins.
	 * 
	 * @return List<CommandData>
	 */
	public List<CommandData> getCommandDataPlugins() {
		List<CommandData> retList = new ArrayList<>();
		for (IBasePlugin plugin : pluginMap.values())
			if (plugin instanceof CommandData)
				retList.add((CommandData) plugin);
		return retList;
	}

	/**
	 * Returns a list of registered {@link MessageData} plugins.
	 * 
	 * @return List<MessageData>
	 */
	public List<MessageData> getMessageDataPlugins() {
		List<MessageData> retList = new ArrayList<>();
		for (IBasePlugin plugin : pluginMap.values())
			if (plugin instanceof MessageData)
				retList.add((MessageData) plugin);
		return retList;
	}

	/**
	 * Returns all loaded plugins as {@link Collection}
	 * 
	 * @return all loaded plugins
	 */
	public Collection<IBasePlugin> getLoadedPlugins() {
		return pluginMap.values();
	}

	private void _loadPlugins(String[] files)
			throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		unloadPlugins();

		try {
			for (int i = 0; i < files.length; i++) {
				URL url = new URL("jar:file:" + files[i] + "!/");
				addURL(url);
			}

		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("Malformed file name", e);
		}
		URLClassLoader cl = (URLClassLoader) classLoader;

		for (String f : files) {
			List<Class<?>> loadedClasses = new ArrayList<>();
			IBasePlugin p = null;

			JarFile jf = new JarFile(f);
			Enumeration<JarEntry> entries = jf.entries();
			while (entries.hasMoreElements()) {
				JarEntry je = entries.nextElement();
				if (je.isDirectory() || !je.getName().endsWith(".class")) {
					continue;
				}
				String className = je.getName().substring(0, je.getName().length() - ".class".length());
				className = className.replace('/', '.');
				Class<?> clazz = cl.loadClass(className);
				loadedClasses.add(clazz);
				System.out.println(clazz);
				Class<? extends IBasePlugin> clazzPlugin = null;
				try {
					clazzPlugin = clazz.asSubclass(IBasePlugin.class);
				} catch (Exception e) {
					continue;
				}
				if (clazzPlugin != null) {
					p = clazzPlugin.newInstance();
					_loadPlugin(p);
				}

			}
			jf.close();
		}
		this.classLoader = cl;

	}

	private void _loadPlugin(IBasePlugin plugin) {
		if (plugin.load()) {
			plugin.postLoad();
			pluginMap.put(plugin.getPluginID(), plugin);
			eventBus.post(new PluginLoadedEvent(plugin));
		}
	}

	private void addURL(URL url) {
		URLClassLoader loader = (URLClassLoader) ClassLoader.getSystemClassLoader();
		Method addURL;
		try {
			addURL = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
			addURL.setAccessible(true);
			addURL.invoke(loader, url);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
	}

}
