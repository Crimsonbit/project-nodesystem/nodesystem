/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.plugin.impl;

import javafx.scene.image.Image;

/**
 * <h1>IBasePlugin</h1> <br>
 * Represents a registered Nodesystem plugin.
 * 
 * @author Florian Wagner
 *
 */

public interface IBasePlugin {

	/**
	 * The id of the plugin. Make sure that plugin id is always unique!
	 * 
	 * @return the id of the plugin
	 */
	String getPluginID();

	/**
	 * The name of the plugin.
	 * 
	 * @return the plugin name
	 */
	String getName();

	/**
	 * Called when the plugin is loaded by the nodesystem. The nodesystem will only
	 * register the plugin if this method returns true.
	 * 
	 * @return return true to register the plugin, otherwise false
	 */
	boolean load();

	/**
	 * Called when the plugin is unloaded. This usually never happens
	 */
	void unload();

	/**
	 * This method is called after the plugin has already been registered!
	 */
	void postLoad();

	/**
	 * This method tells the nodesystem which icon to use for the plugin. The icon
	 * resolution should be adjusted accordingly.
	 * 
	 * @return the icon of the plugin
	 */
	default Image getIcon() { return null; }
	
}