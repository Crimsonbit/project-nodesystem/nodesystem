/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.util;
/**
 * 
 * @author Florian Wagner
 *
 */
public class RangeMapper {
	public static double mapValue(double value, double min_input, double max_input, int min_output, int max_output) {
		double inrange = max_input - min_input;
		/// (0.0))) value = 0.0; // Prevent DivByZero error
		value = (value - min_input) / inrange; // Map input range to [0.0 ... 1.0]
		if (value > max_output) {
			return max_output;
		}
		if (value < min_output) {
			return min_output;
		}
		return (min_output + (max_output - min_output) * value); // Map to output range and return result
	}
}
