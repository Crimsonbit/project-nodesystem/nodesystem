/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.util;

import java.io.File;

import com.google.common.primitives.Primitives;

import at.crimsonbit.nodesystem.frontend.gui.drawable.DrawNode;
import at.crimsonbit.nodesystem.frontend.gui.drawable.DrawPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodePort;
import at.crimsonbit.nodesystem.nodebackend.util.Tuple;
import javafx.scene.paint.Color;

public class Util {

	private static String OS = System.getProperty("os.name").toLowerCase();

	public static double map(double x, double in_min, double in_max, double out_min, double out_max) {
		return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}

	public static Object getDummyObject(INodePort inPortInterface) {
		Class<?> d = inPortInterface.getType();
		if (d.isPrimitive())
			d = Primitives.wrap(d);
		Object obj = null;
		if (String.class.isAssignableFrom(d)) {
			obj = "";
		} else if (Integer.class.isAssignableFrom(d)) {
			obj = new Integer(0);
		} else if (Double.class.isAssignableFrom(d)) {
			obj = new Double(0.0);
		} else if (Long.class.isAssignableFrom(d)) {
			obj = new Long(0);
		} else if (Boolean.class.isAssignableFrom(d)) {
			obj = new Boolean(true);
		} else if (Float.class.isAssignableFrom(d)) {
			obj = new Float(0.0);
		}
		return obj;
	}

	public static String getNameWithoutExtension(File file) {
		String name = file.getName();
		int pos = name.lastIndexOf(".");
		if (pos > 0) {
			name = name.substring(0, pos);
		}
		return name;
	}

	public static String toRGBCode(Color color) {
		String hex1;
		String hex2;

		hex1 = Integer.toHexString(color.hashCode()).toUpperCase();

		switch (hex1.length()) {
		case 2:
			hex2 = "000000";
			break;
		case 3:
			hex2 = String.format("00000%s", hex1.substring(0, 1));
			break;
		case 4:
			hex2 = String.format("0000%s", hex1.substring(0, 2));
			break;
		case 5:
			hex2 = String.format("000%s", hex1.substring(0, 3));
			break;
		case 6:
			hex2 = String.format("00%s", hex1.substring(0, 4));
			break;
		case 7:
			hex2 = String.format("0%s", hex1.substring(0, 5));
			break;
		default:
			hex2 = hex1.substring(0, 6);
		}
		return "#" + hex2;
	}

	public static Tuple<Double, Double> calculateSignalPositionBasedOnPort(DrawPort port, DrawNode node) {

		double x = node.getLayoutX();
		double y = node.getLayoutY();

		if (port.getPort().isInput()) {

			x += port.getLayoutX() + port.getPortCircle().getTranslateX()
					+ (port.getSettings().getPort().getInput().getSize() * 2D);
			y += port.getLayoutY();

		} else {
			x += port.getLayoutX() + port.getSettings().getNode().getWidth() - port.getPortCircle().getTranslateX()
					- (port.getSettings().getPort().getOutput().getSize() * 2D);
			y += port.getLayoutY();
		}
		Tuple<Double, Double> coords = new Tuple<Double, Double>(x, y);

		return coords;
	}

	public static String getOS() {
		return OS;
	}

	public static boolean isAndroid() {
		return (OS.indexOf("andr") >= 0);
	}

	public static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	public static boolean isMac() {

		return (OS.indexOf("mac") >= 0);
	}

	public static boolean isUnix() {

		return (OS.indexOf("uni") >= 0);
	}

	public static Tuple<Double, Double> calculateSignalPositionBasedOnPort(DrawPort port) {
		double x = 0;
		double y = 0;
		if (port.getPort().isInput()) {

			x += port.getLayoutX() + port.getPortCircle().getTranslateX()
					+ (port.getSettings().getPort().getInput().getSize() * 2D);
			y += port.getLayoutY();

		} else {
			x += port.getLayoutX() + port.getSettings().getNode().getWidth() - port.getPortCircle().getTranslateX()
					- (port.getSettings().getPort().getOutput().getSize() * 2D);
			y += port.getLayoutY();
		}
		Tuple<Double, Double> coords = new Tuple<Double, Double>(x, y);

		return coords;
	}
}
