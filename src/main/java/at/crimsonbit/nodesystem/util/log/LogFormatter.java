package at.crimsonbit.nodesystem.util.log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class LogFormatter extends Formatter {

	private final Date dat = new Date();
	private final SimpleDateFormat date_format = new SimpleDateFormat("yyyy MM dd HH:mm");

	@Override
	public String format(LogRecord record) {
		dat.setTime(record.getMillis());
		String message = formatMessage(record);
		String throwable = "";
		if (record.getThrown() != null) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			pw.println();
			record.getThrown().printStackTrace(pw);
			pw.close();
			throwable = sw.toString();
		}
		if (record.getLevel().intValue() == 700) {
			return new String(message + throwable + System.lineSeparator());
		} else
			return new String("[" + date_format.format(dat) + "][" + record.getLevel() + "] :	" + message + throwable
					+ System.lineSeparator());
	}

}
