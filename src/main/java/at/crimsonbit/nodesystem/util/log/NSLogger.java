package at.crimsonbit.nodesystem.util.log;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class NSLogger {

	private static Logger logger = Logger.getLogger("nodesystem");
	static {
		try {
			FileHandler fh = new FileHandler("nodesystem.log");
			logger.addHandler(fh);
			LogFormatter formatter = new LogFormatter();
			fh.setFormatter(formatter);
			logger.setUseParentHandlers(false);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Logger getLogger() {
		return logger;
	}

}
