/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.google.common.eventbus.EventBus;

import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.frontend.gui.NodeSystemUI;
import at.crimsonbit.nodesystem.frontend.gui.Theme;
import at.crimsonbit.nodesystem.frontend.gui.layer.Layer;
import at.crimsonbit.nodesystem.frontend.gui.widget.QuickMenu;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import at.crimsonbit.nodesystem.nodebackend.master.NodeMaster;
import at.crimsonbit.nodesystem.plugin.impl.PluginMaster;

/**
 * <h1>GraphBuilder</h1>
 * <p>
 * The GraphBuilder is the main entry-point of the NodeSystem library. Using the
 * {@link #build()} method one can obtain a {@link DrawGraph}. The
 * {@link DrawGraph} holds the drawing layers of the graph.
 * 
 * The whole DrawGraph is structured in {@link Layer}s. Through this method it
 * is very easy to draw widgets on top of the graph or below if needed. The main
 * layer which should be used is {@link DrawGraph#getBaseLayer()}.
 * 
 * The Graph uses node-modules which have to be loaded through
 * {@link #registerAllModules(String)}
 * 
 * </p>
 * 
 * @author Florian Wagner
 * @see DrawGraph
 */
public class GraphBuilder {
	private List<String> searchPaths = new ArrayList<>();
	private Path pluginSearchPath;
	private NodeMaster master ;
	private boolean enableTouch = false;
	private static GraphBuilder graphBuilder;
	private final EventBus eventBus;
	private NodeSystemUI nodesystem;
	private GraphConfig graphConfig;
	private PluginMaster pluginMaster;
	private Theme theme = Theme.DARK;

	/**
	 * Creates a new {@link GraphBuilder} object. Said object can be used to build
	 * the {@link DrawGraph}.
	 * 
	 * @return a new GraphBuilder
	 * @see #registerAllModules(String)
	 * @see #useTouch(boolean)
	 * @see #createDummyNode()
	 * @see #build()
	 */
	public static GraphBuilder create() {
		graphBuilder = new GraphBuilder();
		return graphBuilder;
	}

	private GraphBuilder() {
		eventBus = new EventBus();
		graphConfig = new GraphConfig();
		master = new NodeMaster(eventBus);
	}

	/**
	 * Tells the graph whether it runs in mobile mode or pc mode.
	 * 
	 * @param runOnMobile
	 *            true for mobile mode otherwise pc mode
	 * @return this GraphBuilder object
	 */
	public GraphBuilder setRunOnMobile(boolean runOnMobile) {
		graphConfig.setRunOnMobile(runOnMobile);
		return this;
	}

	/**
	 * Enables (or disables) the touch support for this node graph.
	 * 
	 * @param touchEnabled
	 *            true for touch enabled otherwise false
	 * @return this GraphBuilder object
	 */
	public GraphBuilder setTouchEnabled(boolean touchEnabled) {
		graphConfig.setTouchEnabled(touchEnabled);
		return this;
	}

	/**
	 * Registers a new path to look for node-modules.<br>
	 * This method can be called multiple times to load from more than one path.
	 * <br>
	 * Call this method before {@link #build()}!
	 * 
	 * @param path
	 *            the path where to search for node-modules
	 * @return this GraphBuilder object
	 */
	public GraphBuilder registerAllModules(String path) {
		searchPaths.add(path);
		return this;
	}

	/**
	 * Registers a new path to look for node-modules.<br>
	 * This method can be called multiple times to load from more than one path.
	 * <br>
	 * Call this method before {@link #build()}!
	 * 
	 * @param path
	 *            the path where to search for node-modules
	 * @return this GraphBuilder object
	 */
	public GraphBuilder registerAllModules(Path path) {
		searchPaths.add(path.toString());
		return this;
	}

	/**
	 * Registers a new path to look for node-modules.<br>
	 * This method can be called multiple times to load from more than one path.
	 * <br>
	 * Call this method before {@link #build()}!
	 * 
	 * @param paths
	 *            the paths where to search for node-modules
	 * @return this GraphBuilder object
	 */
	public GraphBuilder registerAllModules(Path... paths) {
		for (Path p : paths)
			searchPaths.add(p.toString());
		return this;
	}

	public GraphBuilder registerAllPlugins(String path) {
		pluginSearchPath = Paths.get(path);
		return this;
	}

	public GraphBuilder registerAllPlugins(Path absolutePath) {
		pluginSearchPath = absolutePath;
		return this;
	}

	public GraphBuilder setTheme(Theme th) {
		this.theme = th;
		return this;
	}

	/**
	 * Enables support for touch gestures. Touch gestures are used on touch screens
	 * or on mobile devices.
	 * 
	 * @param touch
	 *            true if touch support should be enabled, otherwise false
	 * @return this GraphBuilder object
	 */
	public GraphBuilder useTouch(boolean touch) {
		this.enableTouch = touch;
		return this;
	}

	/**
	 * Create dummy nodes in the backend
	 * 
	 * @return
	 */
	public GraphBuilder createDummyNode() {
		master.createDummyNode();
		return this;
	}

	/**
	 * Builds a new {@link DrawGraph} and loads the modules found.
	 * 
	 * @return a newly allocated {@link DrawGraph}
	 */
	public NodeSystemUI build() {
		
		searchPaths.forEach(master::addModuleFolder);
		master.loadAllNodesFromModules();
		pluginMaster = PluginMaster.create(eventBus);
		NodeGraph graph = master.createNewGraph("root");
		DrawGraph drawGraph = new DrawGraph(graph, eventBus, graphConfig);
		nodesystem = new NodeSystemUI(drawGraph, new QuickMenu(eventBus, drawGraph), eventBus, theme);

		if (pluginSearchPath != null)
			try {
				pluginMaster.loadPlugins(pluginSearchPath);
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IOException e) {
				e.printStackTrace();
			}
		return nodesystem;
	}

	/**
	 * Returns the search paths which have been added in the GraphBuilder.
	 * 
	 * @return the search paths
	 */
	public List<String> getSearchPaths() {
		return searchPaths;
	}

	/**
	 * Returns the node master of this GraphBuilder.
	 * 
	 * @return the node master
	 */
	public NodeMaster getMaster() {
		return master;
	}

	/**
	 * Returns whether touch is enabled or not.
	 * 
	 * @return touch enabled property
	 */
	public boolean isEnableTouch() {
		return enableTouch;
	}

	/**
	 * Returns the global {@link EventBus} object which the graph uses.
	 * 
	 * @return the global event bus
	 */
	public EventBus getEventBus() {
		return eventBus;
	}

}