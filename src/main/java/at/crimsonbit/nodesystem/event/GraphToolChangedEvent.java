package at.crimsonbit.nodesystem.event;

import at.crimsonbit.nodesystem.plugin.dataType.ToolData;

public class GraphToolChangedEvent extends GraphBaseEvent {

	public ToolData tool;

	public GraphToolChangedEvent(ToolData tool) {
		super();
		this.tool = tool;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tool == null) ? 0 : tool.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GraphToolChangedEvent other = (GraphToolChangedEvent) obj;
		if (tool == null) {
			if (other.tool != null)
				return false;
		} else if (!tool.equals(other.tool))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GraphToolChangedEvent [tool=" + tool + "]";
	}

}
