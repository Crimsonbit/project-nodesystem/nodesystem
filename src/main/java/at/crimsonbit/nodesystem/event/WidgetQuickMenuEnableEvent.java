package at.crimsonbit.nodesystem.event;

public class WidgetQuickMenuEnableEvent extends WidgetBaseEvent{

	public boolean enable;

	public WidgetQuickMenuEnableEvent(boolean enable) {
		super();
		this.enable = enable;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (enable ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WidgetQuickMenuEnableEvent other = (WidgetQuickMenuEnableEvent) obj;
		if (enable != other.enable)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WidgetQuickMenuEnableEvent [enable=" + enable + "]";
	}

}
