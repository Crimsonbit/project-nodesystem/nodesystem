package at.crimsonbit.nodesystem.event;

import at.crimsonbit.nodesystem.frontend.gui.drawable.DrawNode;

public class WidgetQuickMenuSelectionEvent extends WidgetBaseEvent{
	
	public DrawNode node;

	public WidgetQuickMenuSelectionEvent(DrawNode node) {
		super();
		this.node = node;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((node == null) ? 0 : node.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WidgetQuickMenuSelectionEvent other = (WidgetQuickMenuSelectionEvent) obj;
		if (node == null) {
			if (other.node != null)
				return false;
		} else if (!node.equals(other.node))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WidgetQuickMenuSelectionEvent [node=" + node + "]";
	}
	
	
	
}
