package at.crimsonbit.nodesystem.event;

import at.crimsonbit.nodesystem.plugin.dataType.WidgetData;

public class WidgetOpenEvent extends WidgetBaseEvent {

	public WidgetData widget;

	public WidgetOpenEvent(WidgetData widget) {
		super();
		this.widget = widget;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((widget == null) ? 0 : widget.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WidgetOpenEvent other = (WidgetOpenEvent) obj;
		if (widget == null) {
			if (other.widget != null)
				return false;
		} else if (!widget.equals(other.widget))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WidgetOpenEvent [widget=" + widget + "]";
	}

	
}
