/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.event;

public class NodeDisconnectedEvent extends NodeConnectionBaseEvent {

	public NodeDisconnectedEvent(String inPortID, String outPortID) {
		super(inPortID, outPortID);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((inPortID == null) ? 0 : inPortID.hashCode());
		result = prime * result + ((outPortID == null) ? 0 : outPortID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NodeConnectionBaseEvent other = (NodeConnectionBaseEvent) obj;
		if (inPortID == null) {
			if (other.inPortID != null)
				return false;
		} else if (!inPortID.equals(other.inPortID))
			return false;
		if (outPortID == null) {
			if (other.outPortID != null)
				return false;
		} else if (!outPortID.equals(other.outPortID))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Removed " + super.toString();
	}
}
