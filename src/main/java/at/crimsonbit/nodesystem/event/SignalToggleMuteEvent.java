package at.crimsonbit.nodesystem.event;

public class SignalToggleMuteEvent extends SignalBaseEvent{

	public boolean mute;
	public String inPortID;

	public SignalToggleMuteEvent(boolean mute, String inPort) {
		super();
		this.mute = mute;
		inPortID = inPort;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((inPortID == null) ? 0 : inPortID.hashCode());
		result = prime * result + (mute ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SignalToggleMuteEvent other = (SignalToggleMuteEvent) obj;
		if (inPortID == null) {
			if (other.inPortID != null)
				return false;
		} else if (!inPortID.equals(other.inPortID))
			return false;
		if (mute != other.mute)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SignalToggleMuteEvent [mute=" + mute + ", inPortID=" + inPortID + "]";
	}

	
}
