package at.crimsonbit.nodesystem.event;

import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;

public class NodeGraphCreatedEvent extends BackendBaseEvent {

	public final NodeGraph graph;

	public NodeGraphCreatedEvent(NodeGraph graph) {
		super();
		this.graph = graph;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((graph == null) ? 0 : graph.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NodeGraphCreatedEvent other = (NodeGraphCreatedEvent) obj;
		if (graph == null) {
			if (other.graph != null)
				return false;
		} else if (!graph.equals(other.graph))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NodeGraphCreatedEvent [graph=" + graph + "]";
	}
}
