package at.crimsonbit.nodesystem.event;

import java.util.Collection;

import at.crimsonbit.nodesystem.plugin.impl.IBasePlugin;

public class PluginsFinishedLoadingEvent extends PluginBaseEvent {
	
	public Collection<IBasePlugin> plugins;

	public PluginsFinishedLoadingEvent(Collection<IBasePlugin> collection) {
		super();
		this.plugins = collection;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((plugins == null) ? 0 : plugins.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PluginsFinishedLoadingEvent other = (PluginsFinishedLoadingEvent) obj;
		if (plugins == null) {
			if (other.plugins != null)
				return false;
		} else if (!plugins.equals(other.plugins))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PluginsFinishedLoadingEvent [plugins=" + plugins + "]";
	}

}
