package at.crimsonbit.nodesystem.event;

public class GlobalInputAddedEvent extends BackendBaseEvent {

	public String portID;

	public GlobalInputAddedEvent(String portID) {
		super();
		this.portID = portID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((portID == null) ? 0 : portID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GlobalInputAddedEvent other = (GlobalInputAddedEvent) obj;
		if (portID == null) {
			if (other.portID != null)
				return false;
		} else if (!portID.equals(other.portID))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GlobalInputAddedEvent [portID=" + portID + "]";
	}

}
