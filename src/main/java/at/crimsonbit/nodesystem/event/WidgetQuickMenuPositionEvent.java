package at.crimsonbit.nodesystem.event;

public class WidgetQuickMenuPositionEvent extends WidgetBaseEvent{

	public Number position;

	public WidgetQuickMenuPositionEvent(Number position) {
		super();
		this.position = position;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WidgetQuickMenuPositionEvent other = (WidgetQuickMenuPositionEvent) obj;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WidgetQuickMenuPositionEvent [position=" + position + "]";
	}

}
