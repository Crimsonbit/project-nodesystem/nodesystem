/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.event;

import at.crimsonbit.nodesystem.frontend.gui.base.BasePort;
import javafx.scene.paint.Color;

public class SignalTryConnectionEven extends SignalBaseEvent {

	public boolean wouldWork;
	public BasePort from;
	public BasePort to;
	public Color fail;
	public Color succ;

	public SignalTryConnectionEven(boolean wouldWork, BasePort from, BasePort to, Color fail, Color succ) {
		this.wouldWork = wouldWork;
		this.from = from;
		this.to = to;
		this.fail = fail;
		this.succ = succ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fail == null) ? 0 : fail.hashCode());
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + ((succ == null) ? 0 : succ.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		result = prime * result + (wouldWork ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SignalTryConnectionEven other = (SignalTryConnectionEven) obj;
		if (fail == null) {
			if (other.fail != null)
				return false;
		} else if (!fail.equals(other.fail))
			return false;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (succ == null) {
			if (other.succ != null)
				return false;
		} else if (!succ.equals(other.succ))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		if (wouldWork != other.wouldWork)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SignalTryConnectionEven [wouldWork=" + wouldWork + ", from=" + from + ", to=" + to + ", fail=" + fail
				+ ", succ=" + succ + "]";
	}

}
