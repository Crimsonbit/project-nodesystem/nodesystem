/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.event;

import at.crimsonbit.nodesystem.frontend.gui.base.BaseSignal;
import javafx.geometry.Bounds;
import javafx.scene.input.ContextMenuEvent;

public class SignalOpenNodeMenuEvent extends SignalBaseEvent {

	public BaseSignal signal;
	public Bounds bounds;
	public ContextMenuEvent event;

	public SignalOpenNodeMenuEvent(BaseSignal signal, Bounds bounds, ContextMenuEvent event) {
		super();
		this.signal = signal;
		this.bounds = bounds;
		this.event = event;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bounds == null) ? 0 : bounds.hashCode());
		result = prime * result + ((event == null) ? 0 : event.hashCode());
		result = prime * result + ((signal == null) ? 0 : signal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SignalOpenNodeMenuEvent other = (SignalOpenNodeMenuEvent) obj;
		if (bounds == null) {
			if (other.bounds != null)
				return false;
		} else if (!bounds.equals(other.bounds))
			return false;
		if (event == null) {
			if (other.event != null)
				return false;
		} else if (!event.equals(other.event))
			return false;
		if (signal == null) {
			if (other.signal != null)
				return false;
		} else if (!signal.equals(other.signal))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SignalOpenNodeMenuEvent [signal=" + signal + ", bounds=" + bounds + ", event=" + event + "]";
	}

}
