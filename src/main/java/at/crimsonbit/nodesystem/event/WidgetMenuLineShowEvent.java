package at.crimsonbit.nodesystem.event;

public class WidgetMenuLineShowEvent extends WidgetBaseEvent{

	public boolean show;

	public WidgetMenuLineShowEvent(boolean show) {
		super();
		this.show = show;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (show ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WidgetMenuLineShowEvent other = (WidgetMenuLineShowEvent) obj;
		if (show != other.show)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WidgetMenuLineShowEvent [show=" + show + "]";
	}

}
