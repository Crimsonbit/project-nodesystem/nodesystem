package at.crimsonbit.nodesystem.event;

import at.crimsonbit.nodesystem.plugin.impl.IBasePlugin;

public class PluginLoadedEvent extends PluginBaseEvent{

	public IBasePlugin plugin;

	public PluginLoadedEvent(IBasePlugin plugin) {
		super();
		this.plugin = plugin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((plugin == null) ? 0 : plugin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PluginLoadedEvent other = (PluginLoadedEvent) obj;
		if (plugin == null) {
			if (other.plugin != null)
				return false;
		} else if (!plugin.equals(other.plugin))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PluginLoadedEvent [plugin=" + plugin + "]";
	}

}
