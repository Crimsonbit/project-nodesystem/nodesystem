package at.crimsonbit.nodesystem.event;

/**
 * The base class for all events that are being send in the nodesystem. This
 * class can not be intatiated.
 * 
 * @author Florian Wagner
 *
 */
public abstract class BaseEvent {

}
