/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.midlayer;

import java.util.HashMap;
import java.util.Map;

import at.crimsonbit.nodesystem.frontend.gui.drawable.DrawNode;
import at.crimsonbit.nodesystem.frontend.gui.drawable.DrawPort;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Node;

public class FrontBackCache {

	private Map<String, DrawNode> nodeMap = new HashMap<>();
	private Map<String, DrawPort> portMap = new HashMap<>();

	private class Listener implements ListChangeListener<Node> {
		@Override
		public void onChanged(Change<? extends Node> c) {
			while (c.next()) {
				if (c.wasAdded()) {
					for (Node n : c.getAddedSubList()) {
						if (n instanceof DrawNode) {
							nodeMap.put(((DrawNode) n).getBackendID(), (DrawNode) n);
							((DrawNode) n).getChildren().addListener(new Listener());
							for (Node n1 : ((DrawNode) n).getChildren()) {
								if (n1 instanceof DrawPort) {
									portMap.put(((DrawPort) n1).getPort().getBackendID(), (DrawPort) n1);
								}
							}
						} else if (n instanceof DrawPort) {
							portMap.put(((DrawPort) n).getPort().getBackendID(), (DrawPort) n);
						}
					}
				}
				if (c.wasRemoved()) {
					for (Node n : c.getRemoved()) {
						if (n instanceof DrawNode) {
							nodeMap.remove(((DrawNode) n).getBackendID());
						} else if (n instanceof DrawPort) {
							portMap.remove(((DrawPort) n).getPort().getBackendID());
						}
					}
				}
			}
		}
	}

	public FrontBackCache() {
	}

	public void watch(ObservableList<Node> list) {
		list.addListener(new Listener());
	}

	public DrawNode getNode(String backendID) {
		return nodeMap.get(backendID);
	}

	public DrawPort getPort(String backendID) {
		return portMap.get(backendID);
	}
}
