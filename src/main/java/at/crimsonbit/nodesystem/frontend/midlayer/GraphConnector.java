/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.midlayer;

import java.util.Optional;

import at.crimsonbit.nodesystem.event.NodeConnectionStartEvent;
import at.crimsonbit.nodesystem.frontend.gui.base.BasePort;
import at.crimsonbit.nodesystem.nodebackend.exception.InvalidPortException;
import at.crimsonbit.nodesystem.nodebackend.graph.ConnectionResult;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeOutputPort;

public class GraphConnector {
	private NodeGraph graph;
	private BasePort first;

	public GraphConnector(NodeGraph backendGraph) {
		graph = backendGraph;
	}

	/**
	 * Select the first Port for connection. This may only be called if no
	 * Connection is being made currently. Call
	 * {@link GraphConnector#abortConnection()} or
	 * {@link GraphConnector#completeConnection(BasePort)} before calling this a
	 * second time.
	 * 
	 * @param begin
	 */
	public void beginConnection(BasePort begin) {
		if (first != null)
			throw new IllegalStateException("There is already a first port set");
		first = begin;
		graph.eventBus.post(new NodeConnectionStartEvent(begin.getBackendID()));

	}

	/**
	 * Abort connecting Ports
	 */
	public void abortConnection() {
		first = null;

	}

	public void disconnectFromInput(BasePort input) {
		Optional<? extends INodeInputPort> inPort = graph.getInputPort(input.getBackendID());
		if (inPort.isPresent()) {
			graph.removeConnectionTo(inPort.get());
		}
	}

	public void disconnectFromOutput(BasePort output) {
		Optional<? extends INodeOutputPort> outPort = graph.getOutputPort(output.getBackendID());
		if (outPort.isPresent()) {
			graph.removeConnectionsFrom(outPort.get());
		}
	}

	/**
	 * Connect two Ports. The first Port has to be set by calling
	 * {@link GraphConnector#beginConnection(BasePort)} first. If the first Port was
	 * an InputPort, this Port should be an output port. Otherwise this should be an
	 * input Port
	 * 
	 * @param end The Second Port to connect.
	 * @return
	 * @throws InvalidPortException
	 */
	public ConnectionResult completeConnection(BasePort end, boolean dry_run) throws InvalidPortException {
		if (first == null) {
			return ConnectionResult.PORT_NULL;
		}
		if (first.equals(end)) {
			return ConnectionResult.IO_SAME;
		}

		Optional<? extends INodeInputPort> inPort = graph.getInputPort(first.getBackendID());
		Optional<? extends INodeOutputPort> outPort = null;

		INodeInputPort in;
		INodeOutputPort out;

		if (inPort.isPresent()) {
			outPort = graph.getOutputPort(end.getBackendID());
			if (!outPort.isPresent())
				return ConnectionResult.PORT_NULL;
			in = inPort.get();
			out = outPort.get();
		} else {
			inPort = graph.getInputPort(end.getBackendID());
			outPort = graph.getOutputPort(first.getBackendID());
			if (!(inPort.isPresent() && outPort.isPresent()))
				return ConnectionResult.PORT_NULL;
			in = inPort.get();
			out = outPort.get();
		}

		if (dry_run)
			return graph.checkConnection(in, out);

		ConnectionResult res = graph.addConnection(in, out);
		if (res == ConnectionResult.SUCCESS)
			first = null;
		return res;
	}

	public BasePort getFirst() {
		return first;
	}

	public void setFirst(BasePort first) {
		this.first = first;
	}

	public boolean isInConnection() {

		return first != null;
	}

}
