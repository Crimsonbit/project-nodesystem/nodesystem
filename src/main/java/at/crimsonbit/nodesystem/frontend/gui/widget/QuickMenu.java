package at.crimsonbit.nodesystem.frontend.gui.widget;

import java.math.BigDecimal;
import java.util.Map;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import at.crimsonbit.nodesystem.event.GraphChangedEvent;
import at.crimsonbit.nodesystem.event.WidgetQuickMenuSelectionEvent;
import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.frontend.gui.component.ColorSlider;
import at.crimsonbit.nodesystem.frontend.gui.drawable.DrawNode;
import at.crimsonbit.nodesystem.frontend.gui.drawable.DrawPort;
import at.crimsonbit.nodesystem.frontend.gui.fields.BigDecimalField;
import at.crimsonbit.nodesystem.frontend.gui.fields.DoubleField;
import at.crimsonbit.nodesystem.frontend.gui.fields.FloatField;
import at.crimsonbit.nodesystem.frontend.gui.fields.IntegerField;
import at.crimsonbit.nodesystem.frontend.gui.fields.LongField;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeConnection;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeField;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeOutputPort;
import at.crimsonbit.nodesystem.nodebackend.util.NodePortId;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

/**
 * <h1>QuickMenu</h1><br>
 * The {@link QuickMenu} is used to access the nodes parameters outside of code.
 * It shows the Basic settsing, Fields and more of the currently selected node.
 * The user can then change those settings very easily.
 * 
 * @author Florian Wagner
 *
 */
public class QuickMenu extends AnchorPane {

	private final EventBus eventBus;
	private NodeGraph graph;
	private DrawGraph drawGraph;
	private TabPane tabPane = new TabPane();
	private Tab defaultTap = new Tab();
	private Tab lastSelected = new Tab();
	private Pane defaultPane = new Pane();
	private Tab fieldTab = new Tab("Fields");
	private Tab portsTab = new Tab("Ports");
	private Pane portsPane = new Pane();
	private AnchorPane fieldPane = new AnchorPane();

	public QuickMenu(EventBus bus, DrawGraph dgraph) {
		this.eventBus = bus;
		this.graph = dgraph.getNodeGraph();
		this.drawGraph = dgraph;
		this.eventBus.register(this);

		getStyleClass().add("quickmenu");
		tabPane.getStyleClass().add("quickmenu-tabpane");
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		getChildren().add(tabPane);

		setLeftAnchor(tabPane, 0D);
		setRightAnchor(tabPane, 0D);
		setTopAnchor(tabPane, 0D);
		setBottomAnchor(tabPane, 0D);

		tabPane.getSelectionModel().selectedItemProperty().addListener((obs, ov, nv) -> {
			if (ov != null)
				lastSelected = nv;

		});
	}

	@Subscribe
	private void onGraphChanged(GraphChangedEvent ev) {
		drawGraph = ev.graph;
		graph = drawGraph.getNodeGraph();

	}

	@Subscribe
	private void onSelectionChanged(WidgetQuickMenuSelectionEvent event) {
		DrawNode node = event.node;
		populateTabs(node);
	}

	private void addDefaultField(Pane defaultPane, String name, String name2, int c, double size, boolean bind,
			DrawNode node, boolean editable, double width, double space, boolean indend) {
		Text fieldName = new Text(name);
		fieldName.setFill(Color.WHITE);
		TextField nameField = new TextField(name2);
		nameField.getStyleClass().add("nodesystem-field");
		nameField.setTranslateY(size * c);
		nameField.setPrefHeight(size - space);
		nameField.setMinHeight(size - space);
		defaultPane.getChildren().add(nameField);
		fieldName.setTranslateY(size * c + (fieldName.getBoundsInLocal().getMaxY() / 2) + (size / 2));
		nameField.setEditable(editable);

		if (!indend) {
			nameField.prefWidthProperty().bind(widthProperty());
			nameField.setTranslateX(width + 10D);
		} else {
			Button select = new Button("Go To");
			defaultPane.getChildren().add(select);
			double selectWidth = 100D;

			select.setPrefWidth(selectWidth);
			select.setOnAction(ev -> {
				NodePortId npi = new NodePortId(name2);
				DrawNode _node = drawGraph.getNode(npi.node);
				drawGraph.deselectAll();
				drawGraph.selectNode(_node);
			});

			select.translateXProperty().bind(widthProperty().subtract(selectWidth + 10D));
			select.setPrefHeight(size - space);
			select.setTranslateY(size * c - ((size - space) / 8D));
			nameField.prefWidthProperty().bind(widthProperty().subtract(selectWidth * 2.5D));
			nameField.setTranslateX(width + 10D + 25D);

		}
		defaultPane.getChildren().add(fieldName);

		if (bind)
			node.getNode().getNameProperty().bind(nameField.textProperty());

	}

	private double getLargestTextWidth(String... texts) {
		double size = 0;
		for (String te : texts) {
			Text t = new Text(te);
			if (t.getBoundsInLocal().getMaxX() > size) {
				size = t.getBoundsInLocal().getMaxX();
			}
		}
		return size;
	}

	private void addHeaderText(String txt, int cc, double spacing, Pane pane) {
		Text inPortsText = new Text(txt);
		pane.getChildren().add(inPortsText);
		inPortsText.setFill(Color.WHITE);
		inPortsText.setTranslateY((spacing * cc));
		inPortsText.setStyle("-fx-font: 20 arial;");
	}

	private void addColorField(double size, double width, int _c, ObjectProperty<Paint> property, Pane pane) {
		Text fieldName = new Text("Node Color: ");
		fieldName.setFill(Color.WHITE);
		fieldName.setTranslateY(size * _c + (size / 2));

		ColorPicker nodeColor = new ColorPicker((Color) property.get());
		nodeColor.setTranslateY(size * _c);
		nodeColor.setTranslateX(width + 10D);
		property.bind(nodeColor.valueProperty());
		pane.getChildren().add(fieldName);
		pane.getChildren().add(nodeColor);
	}

	private void populateTabs(DrawNode node) {
		fieldPane.getChildren().clear();
		defaultPane.getChildren().clear();
		portsPane.getChildren().clear();

		if (node != null) {
			double cc = 15D;

			defaultTap.setText("Basic");
			defaultTap.getStyleClass().add("quickmenu-tab");
			int _c = 1;
			double size = 30D;
			double spacing = size;
			double space = 5D;
			addHeaderText("Basic", _c, spacing, defaultPane);
			_c++;
			double width = getLargestTextWidth("Node Name: ", "Type: ", "Backend ID: ", "Input Ports: ",
					"Output Ports: ", "Node Color: ");
			addDefaultField(defaultPane, "Node Name: ", node.getNode().getName(), _c++, size, true, node, true, width,
					space, false);
			addDefaultField(defaultPane, "Type: ", node.getNode().getType().regName(), _c++, size, false, node, false,
					width, space, false);
			addDefaultField(defaultPane, "Backend ID: ", node.getNode().getBackendID(), _c++, size, false, node, false,
					width, space, false);
			addDefaultField(defaultPane, "Input Ports: ", String.valueOf(node.getNode().getInputPortCount()), _c++,
					size, false, node, false, width, space, false);
			addDefaultField(defaultPane, "Output Ports: ", String.valueOf(node.getNode().getOutputPortCount()), _c++,
					size, false, node, false, width, space, false);

			addColorField(size, width, _c, node.getColorPorperty(), defaultPane);

			defaultTap.setContent(getScrollPaneForTab(defaultPane, _c));

			Map<String, INodeField> fields = graph.getFieldsOfNode(node.getNode().getBackendID());
			Map<String, INodeInputPort> inputPorts = graph.getInputsOfNode(node.getNode().getBackendID());
			Map<String, INodeOutputPort> outputPorts = graph.getOutputsOfNode(node.getNode().getBackendID());
			Map<INodeInputPort, NodeConnection> connections = graph.getConnections();

			fieldTab.getStyleClass().add("quickmenu-tab");
			portsTab.getStyleClass().add("quickmenu-tab");

			_c = 1;
			addHeaderText("Fields", _c, spacing, fieldPane);
			_c++;
			tabPane.getTabs().add(defaultTap);

			if (fields.values().size() > 0) {
				fieldTab.setContent(getScrollPaneForTab(fieldPane, _c));
				tabPane.getTabs().add(fieldTab);
			}
			for (INodeField f : fields.values()) {
				addField(spacing, fieldPane, f, node, _c++, space);
			}

			tabPane.getTabs().add(portsTab);

			_c = 1;

			addHeaderText("Input Ports", _c++, spacing, portsPane);
			for (INodeInputPort inPort : inputPorts.values()) {
				addDefaultField(portsPane, "Port Name: ", inPort.getName(), _c++, size, false, node, false, width,
						space, false);
				addDefaultField(portsPane, "Port ID: ", inPort.getID(), _c++, size, false, node, false, width, space,
						false);
				addDefaultField(portsPane, "Port Type: ", inPort.getType().getSimpleName(), _c++, size, false, node,
						false, width, space, false);

				for (INodeInputPort connIPorts : connections.keySet()) {
					if (inPort.equals(connIPorts)) {
						NodeConnection conn = connections.get(connIPorts);
						addDefaultField(portsPane, "Connected to: ", conn.driver.getID(), _c++, size, false, node,
								false, width, space, true);
					}
				}

			}

			_c++;
			addHeaderText("Output Ports", _c++, spacing, portsPane);

			for (INodeOutputPort outPort : outputPorts.values()) {
				addDefaultField(portsPane, "Port Name: ", outPort.getName(), _c++, size, false, node, false, width,
						space, false);
				addDefaultField(portsPane, "Port ID: ", outPort.getID(), _c++, size, false, node, false, width, space,
						false);
				addDefaultField(portsPane, "Port Type: ", outPort.getType().getSimpleName(), _c++, size, false, node,
						false, width, space, false);
			}
			portsTab.setContent(getScrollPaneForTab(portsPane, _c));

			for (Tab t : tabPane.getTabs()) {
				if (lastSelected != null)
					if (t.getText().equals(lastSelected.getText())) {
						tabPane.getSelectionModel().select(t);
					}
			}
		}
	}

	private ScrollPane getScrollPaneForTab(Pane pane, int _c) {
		double minHeight = 0.0;
		for (Node n : pane.getChildren()) {
			minHeight += n.getBoundsInLocal().getMaxY();
		}

		minHeight = minHeight * _c;
		ScrollPane scPortsTab = new ScrollPane();
		pane.setMinHeight(minHeight);

		scPortsTab.getStyleClass().add("nodesystem-scrollpane");
		scPortsTab.setHbarPolicy(ScrollBarPolicy.NEVER);
		scPortsTab.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);

		scPortsTab.setContent(pane);
		return scPortsTab;
	}

	private void addField(double size, Pane fieldPane, INodeField portt, DrawNode node, int c, double space) {
		TextField field = null;
		final ColorSlider sliderField = new ColorSlider();

		Text fieldName = new Text(portt.getName());
		fieldName.setFill(Color.WHITE);

		double nameLength = fieldName.getBoundsInLocal().getMaxX() + 10D;
		DrawPort port = drawGraph.getPort(portt.getID());

		if (String.class.isAssignableFrom(portt.getType())) {
			field = new TextField();
			if (port.getField() != null)
				field.setText(port.getField().getText());
			field.editableProperty().bind(port.connectedProperty().not());
			field.setTranslateY(size * c);
			field.setTranslateX(nameLength);
			field.setPrefHeight(size - space);
			field.setMinHeight(size - space);
			field.getStyleClass().add("nodesystem-field");
			if (port.getField() != null)
				port.getField().textProperty().bindBidirectional(field.textProperty());
			field.prefWidthProperty().bind(fieldPane.widthProperty());
			field.requestFocus();
			fieldPane.getChildren().add(field);

		} else if (Integer.class.isAssignableFrom(portt.getType())) {
			field = new IntegerField();
			if (port.getField() != null)
				((IntegerField) field).setNumber(((IntegerField) port.getField()).getNumber());
			field.editableProperty().bind(port.connectedProperty().not());
			field.setTranslateX(nameLength);
			field.setTranslateY(size * c);
			field.setPrefHeight(size - space);
			field.setMinHeight(size - space);
			field.getStyleClass().add("nodesystem-field");
			if (port.getField() != null)
				((IntegerField) port.getField()).numberProperty()
						.bindBidirectional(((IntegerField) field).numberProperty());
			field.prefWidthProperty().bind(fieldPane.widthProperty());
			field.requestFocus();
			fieldPane.getChildren().add(field);

		} else if (Double.class.isAssignableFrom(portt.getType())) {
			field = new DoubleField();
			field.setTranslateX(nameLength);
			if (port.getField() != null)
				((DoubleField) field).setNumber(((DoubleField) port.getField()).getNumber());
			field.editableProperty().bind(port.connectedProperty().not());
			field.setTranslateY(size * c);
			field.setPrefHeight(size - space);
			field.setMinHeight(size - space);
			field.getStyleClass().add("nodesystem-field");
			if (port.getField() != null)
				((DoubleField) port.getField()).numberProperty()
						.bindBidirectional(((DoubleField) field).numberProperty());
			field.requestFocus();

			field.prefWidthProperty().bind(fieldPane.widthProperty());
			fieldPane.getChildren().add(field);

		} else if (Long.class.isAssignableFrom(portt.getType())) {
			field = new LongField();
			field.setTranslateX(nameLength);
			if (port.getField() != null)
				((LongField) field).setNumber(((LongField) port.getField()).getNumber());
			field.editableProperty().bind(port.connectedProperty().not());
			field.setTranslateY(size * c);
			field.setPrefHeight(size - space);
			field.setMinHeight(size - space);
			field.getStyleClass().add("nodesystem-field");
			if (port.getField() != null)
				((LongField) port.getField()).numberProperty().bindBidirectional(((LongField) field).numberProperty());
			field.requestFocus();
			field.prefWidthProperty().bind(fieldPane.widthProperty());
			fieldPane.getChildren().add(field);

		} else if (Boolean.class.isAssignableFrom(portt.getType())) {
			CheckBox checkFieldControl = new CheckBox();

			final double SIZE = size; // size due to JavaFX being stupid

			checkFieldControl.setPrefSize(SIZE, SIZE);
			checkFieldControl.selectedProperty().set(port.getBooleanField().selectedProperty().get());
			port.getBooleanField().selectedProperty()
					.bind(Bindings.when(checkFieldControl.selectedProperty()).then(true).otherwise(false));
			checkFieldControl.setLayoutY(size * c);

			checkFieldControl.requestFocus();
			fieldPane.getChildren().add(checkFieldControl);

		} else if (Float.class.isAssignableFrom(portt.getType())) {
			field = new FloatField();
			field.setTranslateX(nameLength);
			if (port.getField() != null)
				((FloatField) field).setNumber(((FloatField) port.getField()).getNumber());
			field.editableProperty().bind(port.connectedProperty().not());

			field.setTranslateY(size * c);
			field.setPrefHeight(size - space);
			field.setMinHeight(size - space);
			field.getStyleClass().add("nodesystem-field");
			if (port.getField() != null)
				((FloatField) port.getField()).numberProperty()
						.bindBidirectional(((FloatField) field).numberProperty());
			field.requestFocus();
			field.prefWidthProperty().bind(fieldPane.widthProperty());
			fieldPane.getChildren().add(field);

		} else if (BigDecimal.class.isAssignableFrom(portt.getType())) {
			double minMaxFieldSize = 50D;
			DoubleField minField = new DoubleField(0.0);
			DoubleField maxField = new DoubleField(100.0);
			minField.getStyleClass().add("nodesystem-field");
			maxField.getStyleClass().add("nodesystem-field");
			minField.setPrefWidth(minMaxFieldSize);
			maxField.setPrefWidth(minMaxFieldSize);

			sliderField.getStyleClass().add("nodesystem-slider");
			sliderField.setPrefHeight(size - space);
			sliderField.setMinHeight(size - space);

			minField.setPrefHeight(size - space);
			minField.setMinHeight(size - space);

			maxField.setPrefHeight(size - space);
			maxField.setMinHeight(size - space);

			minField.setTranslateY(size * c);
			maxField.setTranslateY(size * c);
			// maxField.setTranslateX(getWidth() - minMaxFieldSize);
			maxField.translateXProperty().bind(widthProperty().subtract(minMaxFieldSize));
			sliderField.getRectangle().widthProperty().bind((widthProperty().subtract(minMaxFieldSize * 2D)));
			sliderField.getSlider().prefWidthProperty().bind((widthProperty().subtract(minMaxFieldSize * 2D)));

			minField.setTranslateX(nameLength);

			sliderField.setTranslateX(nameLength + minMaxFieldSize);

			if (port.getField() != null) {
				double value = ((BigDecimalField) port.getField()).getNumber().doubleValue();
				if (value > maxField.getNumber())
					maxField.setNumber(value);
				if (value < minField.getNumber())
					minField.setNumber(value);

				sliderField.getSlider().setValue(value);

			}
			sliderField.setTranslateY(size * c);
			sliderField.getSlider().minProperty().bind(minField.numberProperty());
			sliderField.getSlider().maxProperty().bind(maxField.numberProperty());

			sliderField.disableProperty().bind(port.connectedProperty());
			minField.editableProperty().bind(port.connectedProperty().not());
			maxField.editableProperty().bind(port.connectedProperty().not());

			sliderField.getSlider().valueProperty().addListener((ovs, ov, nv) -> {
				((BigDecimalField) port.getField()).numberProperty().set(BigDecimal.valueOf(nv.doubleValue()));
			});
			((BigDecimalField) port.getField()).numberProperty().addListener((ovs, ov, nv) -> {
				if (nv.doubleValue() > maxField.getNumber())
					maxField.setNumber(nv.doubleValue());
				if (nv.doubleValue() < minField.getNumber())
					minField.setNumber(nv.doubleValue());
				sliderField.getSlider().valueProperty().set(nv.doubleValue());
			});

			sliderField.requestFocus();

			// 10 = offset right of text
			sliderField.prefWidthProperty().bind(fieldPane.widthProperty().subtract(minMaxFieldSize * 2D));
			fieldPane.getChildren().addAll(sliderField, minField, maxField);

			/*
			 * field = new BigDecimalField(); field.setTranslateX(nameLength); if
			 * (port.getField() != null) ((BigDecimalField)
			 * field).setNumber(((BigDecimalField) port.getField()).getNumber());
			 * field.setTranslateY(size * c);
			 * field.editableProperty().bind(port.connectedProperty().not());
			 * 
			 * field.getStyleClass().add("nodesystem-field"); if (port.getField() != null)
			 * ((BigDecimalField) port.getField()).numberProperty()
			 * .bindBidirectional(((BigDecimalField) field).numberProperty());
			 * field.requestFocus();
			 * field.prefWidthProperty().bind(fieldPane.widthProperty());
			 * fieldPane.getChildren().add(field);
			 */
		}
		if (field != null || sliderField != null) {
			fieldName.setTranslateY(size * c + (size / 2));
			fieldPane.getChildren().add(fieldName);
		}
	}

}
