/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.guilang;

public class GLContainer {

	private GLNode node = new GLNode();
	private GLPort port = new GLPort();
	private GLSignal signal = new GLSignal();
	
	public GLContainer() {

	}

	public GLContainer(GLNode node, GLPort port, GLSignal signal) {
		this.node = node;
		this.port = port;
		this.signal = signal;
	}

	public GLSignal getSignal() {
		return signal;
	}
	
	public void setSignal(GLSignal s) {
		this.signal = s;
	}
	
	public GLNode getNode() {
		return node;
	}

	public void setNode(GLNode node) {
		this.node = node;
	}

	public GLPort getPort() {
		return port;
	}

	public void setPort(GLPort port) {
		this.port = port;
	}

}
