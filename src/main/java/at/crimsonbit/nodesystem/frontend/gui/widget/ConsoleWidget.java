package at.crimsonbit.nodesystem.frontend.gui.widget;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import at.crimsonbit.nodesystem.event.MessageEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;

public class ConsoleWidget {
	private AnchorPane mainPane;
	private TextFlow consoleText;
	private final EventBus globalBus;
	private TextField inputField;
	private Button sendButton;
	private ScrollPane scrollPane;
	private Scene scene;
	private Stage stage;

	public ConsoleWidget(EventBus eventBus) {
		this.globalBus = eventBus;
		this.globalBus.register(this);

		mainPane = new AnchorPane();
		consoleText = new TextFlow();
		scrollPane = new ScrollPane();
		scrollPane.setVvalue(1.0);
		scrollPane.setContent(consoleText);
		mainPane.getChildren().add(scrollPane);
		inputField = new TextField();
		sendButton = new Button("Send");
		sendButton.setPrefWidth(50D);
		sendButton.setPrefHeight(30D);
		inputField.setPrefHeight(30D);
		mainPane.getChildren().add(inputField);
		mainPane.getChildren().add(sendButton);
		sendButton.setOnAction(ev->{
			onSend(inputField.getText());
		});
		AnchorPane.setBottomAnchor(inputField, 0D);
		AnchorPane.setBottomAnchor(sendButton, 0D);
		AnchorPane.setRightAnchor(sendButton, 0D);
		AnchorPane.setRightAnchor(inputField, 50D);
		AnchorPane.setLeftAnchor(inputField, 0D);
		AnchorPane.setTopAnchor(scrollPane, 0D);
		AnchorPane.setBottomAnchor(scrollPane, 30D);
		AnchorPane.setLeftAnchor(scrollPane, 0D);
		AnchorPane.setRightAnchor(scrollPane, 0D);

		ErrorConsoleHandler ech = new ErrorConsoleHandler(consoleText);
		OutConsoleHandler och = new OutConsoleHandler(consoleText);
		PrintStream ops = new PrintStream(och, true);
		System.setOut(ops);

		PrintStream eps = new PrintStream(ech, true);
		System.setErr(eps);

		scene = new Scene(mainPane);
		stage = new Stage();
		stage.setScene(scene);
		stage.setTitle("Console");
		stage.setOnCloseRequest(ev -> {
			stage.hide();
		});
	}
	
	private void onSend(String input) {
		
	}
	
	
	
	public void open() {
		stage.show();
	}

	@Subscribe
	private void onMessageRecevied(MessageEvent event) {
		System.out.println(event.message);
	}

	public AnchorPane getConsoleWindow() {
		return mainPane;
	}

	private class ErrorConsoleHandler extends OutputStream {

		private TextFlow output;
		private Text text;

		public ErrorConsoleHandler(TextFlow ta) {
			this.output = ta;
			text = new Text();
			text.setStyle("-fx-fill: #f11515;-fx-font-weight:bold;");

		}

		@Override
		public void write(int i) throws IOException {

			if (((char) i) == '\n') {
				output.getChildren().add(text);
				text.setText(text.getText() + String.valueOf((char) i));
				text = new Text();
				text.setStyle("-fx-fill: f11515;-fx-font-weight:bold;");
			} else
				text.setText(text.getText() + String.valueOf((char) i));
		}

	}

	private class OutConsoleHandler extends OutputStream {

		private TextFlow output;
		private Text text;

		public OutConsoleHandler(TextFlow ta) {
			this.output = ta;
			text = new Text();
			text.setStyle("-fx-fill: white;-fx-font-weight:bold;");
		}

		@Override
		public void write(int i) throws IOException {
			if (((char) i) == '\n') {
				output.getChildren().add(text);
				text.setText(text.getText() + String.valueOf((char) i));
				text = new Text();
				text.setStyle("-fx-fill: white;-fx-font-weight:bold;");
			} else
				text.setText(text.getText() + String.valueOf((char) i));
		}
	}
}
