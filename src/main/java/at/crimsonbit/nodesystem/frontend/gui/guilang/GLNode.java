/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.guilang;

import javafx.scene.paint.Color;

public class GLNode {

	private double width = 200d;
	private double height = 200d;
	private GLEdge edge = new GLEdge();

	private Color text_color = Color.WHITE;
	private double top_height = 35d;

	private Color top_color = Color.rgb(232, 142, 70);
	private Color bottom_color = Color.rgb(90, 90, 90);
	private Color port_side_color = Color.rgb(70, 70, 70);
	private Color selected_color = Color.rgb(244, 172, 49);
	private Color outline_color = Color.rgb(42, 42, 42);

	private GLCollapsed collapsed = new GLCollapsed();
	private GLShadow node_shadow = new GLShadow();

	public GLNode() {

	}

	public GLNode(double width, double height, GLEdge edge, Color text_color, double top_height, Color top_color,
			Color bottom_color, Color port_side_color, Color selected_color, Color outline_color, GLCollapsed collapsed,
			GLShadow node_shadow) {
		super();
		this.width = width;
		this.height = height;
		this.edge = edge;
		this.text_color = text_color;
		this.top_height = top_height;
		this.top_color = top_color;
		this.bottom_color = bottom_color;
		this.port_side_color = port_side_color;
		this.selected_color = selected_color;
		this.outline_color = outline_color;
		this.collapsed = collapsed;
		this.node_shadow = node_shadow;
	}

	public Color getOutline_color() {
		return outline_color;
	}

	public void setOutline_color(Color outline_color) {
		this.outline_color = outline_color;
	}

	public Color getPortSideColor() {
		return port_side_color;
	}

	public void setPortSideColor(Color port_side_color) {
		this.port_side_color = port_side_color;
	}

	public GLShadow getNodeShadow() {
		return node_shadow;
	}

	public void setNodeShadow(GLShadow node_shadow) {
		this.node_shadow = node_shadow;
	}

	public Color getSelectedColor() {
		return selected_color;
	}

	public void setSelectedColor(Color selected_color) {
		this.selected_color = selected_color;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public GLEdge getEdge() {
		return edge;
	}

	public void setEdge(GLEdge edge) {
		this.edge = edge;
	}

	public Color getTextColor() {
		return text_color;
	}

	public void setTextColor(Color text_color) {
		this.text_color = text_color;
	}

	public Color getTopColor() {
		return top_color;
	}

	public void setTopColor(Color top_color) {
		this.top_color = top_color;
	}

	public Color getBottomColor() {
		return bottom_color;
	}

	public void setBottomColor(Color bottom_color) {
		this.bottom_color = bottom_color;
	}

	public GLCollapsed getCollapsed() {
		return collapsed;
	}

	public void setCollapsed(GLCollapsed collapsed) {
		this.collapsed = collapsed;
	}

	public double getTopHeight() {
		return top_height;
	}

	public void setTopHeight(double top_heigh) {
		this.top_height = top_heigh;
	}

}
