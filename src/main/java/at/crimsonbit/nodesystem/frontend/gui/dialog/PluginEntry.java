package at.crimsonbit.nodesystem.frontend.gui.dialog;

import javafx.scene.control.MenuItem;

public class PluginEntry extends MenuItem {

	private String id;
	private String name;

	public PluginEntry(String id, String name) {
		super(name);
		setId(String.valueOf(id));
		this.id = id;
		this.name = name;

	}

	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		setText(name);
		this.name = name;
	}

	@Override
	public String toString() {
		return "PluginEntry [id=" + id + ", name=" + name + "]";
	}
}