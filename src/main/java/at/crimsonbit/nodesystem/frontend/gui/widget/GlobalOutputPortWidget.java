package at.crimsonbit.nodesystem.frontend.gui.widget;

import at.crimsonbit.nodesystem.frontend.gui.drawable.DrawPort;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class GlobalOutputPortWidget extends AnchorPane {

	private Rectangle background;
	private Timeline timeline = new Timeline();
	private final long ANIMATION_DURATION = 150;
	private Color fill;
	private int c = 1;

	public GlobalOutputPortWidget() {
		background = new Rectangle();
		background.heightProperty().bind(heightProperty());
		background.setWidth(10D);
		background.getStyleClass().add("nodesystem-global-port");

		getChildren().add(background);
		initMouseGestures();
		setMouseTransparent(false);

	}

	private void initMouseGestures() {

		background.setOnMouseEntered(event -> {
			timeline.stop();
			timeline.getKeyFrames().clear();
			fill = (Color) background.getFill();
			final KeyValue kv = new KeyValue(background.strokeProperty(), Color.WHITE, Interpolator.EASE_BOTH);
			final KeyFrame kf = new KeyFrame(Duration.millis(ANIMATION_DURATION), kv);
			timeline.getKeyFrames().addAll(kf);
			timeline.play();
		});
		background.setOnMouseExited(event -> {
			timeline.stop();
			timeline.getKeyFrames().clear();
			final KeyValue kv = new KeyValue(background.strokeProperty(), fill, Interpolator.EASE_BOTH);
			final KeyFrame kf = new KeyFrame(Duration.millis(ANIMATION_DURATION), kv);
			timeline.getKeyFrames().addAll(kf);
			timeline.play();

		});
	}

	public double getRectangelWidth() {
		return background.getWidth();
	}

	public Rectangle getRectangle() {
		return background;
	}

	public void addPort(DrawPort drawPort) {
		drawPort.setLayoutY(drawPort.getBoundsInLocal().getMaxY() * c++);
		getChildren().add(drawPort);
		background.setWidth(drawPort.getBoundsInLocal().getMaxX());

	}

}
