/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.layer;

import javafx.animation.FadeTransition;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.geometry.Bounds;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.FontSmoothingType;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class MessageLayer extends Layer {

	private Text message;
	private Text status;
	private Rectangle statusBar;
	private final double STATUS_BAR_HEIGHT = 20D;

	public MessageLayer() {
		message = new Text();
		status = new Text();
		message.setFontSmoothingType(FontSmoothingType.LCD);
		status.setFontSmoothingType(FontSmoothingType.LCD);

		message.getStyleClass().add("nodesystem-messagelayer-text");
		status.getStyleClass().add("nodesystem-messagelayer-text");
		message.setFill(Color.WHITE);
		status.setFill(Color.WHITE);
		
		statusBar = new Rectangle();
		// statusBar.setFill(new Color(.1, .1, .1, .9));
		statusBar.setHeight(STATUS_BAR_HEIGHT);
		// setBackground(new Background(new BackgroundFill(Color.TRANSPARENT,
		// CornerRadii.EMPTY, Insets.EMPTY)));
		getChildren().add(statusBar);
		getChildren().add(message);
		getChildren().add(status);
		setMouseTransparent(true);
		statusBar.getStyleClass().add("nodesystem-messagelayer");
	}

	public void bindStatus(ReadOnlyDoubleProperty readOnlyDoubleProperty,
			ReadOnlyDoubleProperty readOnlyDoubleProperty2) {
		status.yProperty().bind(readOnlyDoubleProperty2);
		status.xProperty().bind(readOnlyDoubleProperty);
		statusBar.widthProperty().bind(readOnlyDoubleProperty);
		statusBar.translateYProperty().bind(readOnlyDoubleProperty2.subtract(STATUS_BAR_HEIGHT));
		message.yProperty().bind(readOnlyDoubleProperty2);

	}

	public void postStatus(String msg) {
		status.setText(msg);
		Bounds b = status.getBoundsInLocal();
		status.setTranslateX(-b.getWidth());
		status.setTranslateY(-STATUS_BAR_HEIGHT / 2D + (b.getHeight() / 4D));
	}

	public void postMessage(String msg) {
		message.setText(msg);
		FadeTransition ft = new FadeTransition(Duration.millis(2500), message);
		ft.setFromValue(1.0);
		ft.setToValue(0.0);
		ft.setAutoReverse(true);
		ft.play();
		Bounds b = message.getBoundsInLocal();
		message.setTranslateY(-STATUS_BAR_HEIGHT / 2D + (b.getHeight() / 4D));
	}

	public Double getMessageBarHeight() {
		return STATUS_BAR_HEIGHT;
	}
}
