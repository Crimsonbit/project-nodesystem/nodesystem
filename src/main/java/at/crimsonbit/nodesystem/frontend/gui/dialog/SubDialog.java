/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.dialog;

import javafx.scene.control.Menu;

/**
 * <h1>GSubMenu</h1>
 * <p>
 * The GSubMenu represents a menu. It can be used to add custom menus in for
 * e.g. the graph or in the nodes.
 * </p>
 * 
 * @author Florian Wagner
 *
 */

public class SubDialog extends Menu {
	private int id;
	private String name;

	public SubDialog(int id, String name) {
		super(name);
		setId(String.valueOf(id));
		this.id = id;
		this.name = name;
	}

	public SubDialog(int id, String name, boolean enabled) {
		super(name);
		setId(String.valueOf(id));
		this.id = id;
		this.name = name;
		setDisable(enabled);
	}

	public void addMenu(SubDialog menu) {
		getItems().add(menu);
	}

	public void addItem(Entry item) {
		getItems().add(item);
	}

	public void addItem(int id, String name) {
		getItems().add(new Entry(id, name));

	}

	public void addItem(String id, String name) {
		getItems().add(new PluginEntry(id, name));
	}

	public void addItem(PluginEntry entry) {
		getItems().add(entry);
	}

	public void addItem(int id, String name, boolean enabled) {
		getItems().add(new Entry(id, name, enabled));

	}

	public void addSeparator(int id) {
		getItems().add(new Separator(id));
	}

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		setText(name);
		this.name = name;
	}

	@Override
	public String toString() {
		return "GSubMenu [id=" + id + ", name=" + name + "]";
	}
}
