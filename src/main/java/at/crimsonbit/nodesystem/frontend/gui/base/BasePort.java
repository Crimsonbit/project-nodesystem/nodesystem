/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.base;

import com.google.common.primitives.Primitives;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * <h1>BasePort</h1>
 * 
 * <p>
 * The BasePort is the base class for all ports of a node.
 * </p>
 * 
 * @author Florian Wagner
 *
 * 
 */
public abstract class BasePort {

	private ObjectProperty<Object> valueProperty = new SimpleObjectProperty<>(new Object());
	private StringProperty nameProperty = new SimpleStringProperty();
	private StringProperty backendIdProperty = new SimpleStringProperty();
	private ObjectProperty<BaseNode> parentNodeProperty = new SimpleObjectProperty<>();
	private BooleanProperty isFieldProperty = new SimpleBooleanProperty();
	private BooleanProperty isInputProperty = new SimpleBooleanProperty();
	private ObjectProperty<Class<?>> typeProperty = new SimpleObjectProperty<>();

	/**
	 * Creates a new BasePort object.
	 * 
	 * @param backendID the backend id
	 * @param name      the name
	 * @param parent    the parent node
	 * @param type      the type of the port value
	 * @param isField   whether it is a field
	 * @param isInput   whether it is an input port
	 */
	public BasePort(String backendID, String name, BaseNode parent, Class<?> type, boolean isField, boolean isInput) {
		setBackendID(backendID);
		setValue(null);
		setName(name);
		setParentNode(parent);
		setField(isField);
		setInput(isInput);
		typeProperty.set(type);
	}

	/**
	 * Creates a new BasePort object. Copy constructor.
	 * 
	 * @param copy
	 */
	public BasePort(BasePort copy) {
		setBackendID(copy.getBackendID());
		setValue(null);
		setName(copy.getName());
		setParentNode(copy.getParentNode());
		setField(copy.isField());
		setInput(copy.isInput());
		typeProperty.set(copy.getType());
	}

	public BasePort() {

	}

	/**
	 * Dummy constructor!
	 * 
	 * @param backendID the backend id
	 * @param name      the name
	 * @param parent    the parent node
	 * @param isInput   whether it is an input port
	 */
	public BasePort(String backendID, String name, BaseNode parent, boolean isInput) {
		setBackendID(backendID);
		setValue(1.0D);
		setName(name);
		setParentNode(parent);
		setField(true);
		setInput(isInput);
		typeProperty.set(Double.class);

	}

	/**
	 * Sets this port as an input port.
	 * 
	 * @param input is input
	 */
	private void setInput(boolean input) {
		isInputProperty.set(input);
	}

	/**
	 * Returns true if this port is an input port. Otherwise false
	 * 
	 * @return true if input port
	 */
	public boolean isInput() {
		return isInputProperty.get();
	}

	/**
	 * Sets the field property to the given value.
	 * 
	 * @param b the value
	 */
	public void setField(boolean b) {
		isFieldProperty.set(b);

	}

	/**
	 * Returns true if the given port is a field, or should be treated as one.
	 * 
	 * @return whether this is a field
	 */
	public boolean isField() {
		return isFieldProperty.get();
	}

	/**
	 * This property is set when the port is a field or when it should be treated as
	 * one.
	 * 
	 * @return the isFieldProperty
	 */
	public BooleanProperty isFieldProperty() {
		return isFieldProperty;
	}

	/**
	 * Sets the parent node of this port.
	 * 
	 * @param node
	 */
	public void setParentNode(BaseNode node) {
		parentNodeProperty.set(node);
	}

	/**
	 * Returns the parent node of this port.
	 * 
	 * @return the parent node
	 */
	public BaseNode getParentNode() {
		return parentNodeProperty.get();
	}

	/**
	 * Returns the parent node as {@link ObjectProperty}
	 * 
	 * @return the parent node
	 */
	public ObjectProperty<BaseNode> getParentNodeProperty() {
		return parentNodeProperty;
	}

	/**
	 * Sets the backend id
	 * 
	 * @param id the id
	 */
	private void setBackendID(String id) {
		backendIdProperty.set(id);
	}

	/**
	 * Returns the backend id of this port.
	 * 
	 * @return the id
	 */
	public String getBackendID() {
		return backendIdProperty.get();
	}

	/**
	 * Returns the name of this port.
	 * 
	 * @return the name
	 */
	public String getName() {
		return nameProperty.get();
	}

	/**
	 * Sets the name of this port
	 * 
	 * @param value the name
	 */
	public void setName(String value) {
		nameProperty.set(value);
	}

	/**
	 * Returns the name of this port as {@link StringProperty}
	 * 
	 * @return the name property
	 */
	public StringProperty getNamePropert() {
		return nameProperty;
	}

	/**
	 * Returns the value of this port.
	 * 
	 * @return the value
	 */
	public Object getValue() {
		return valueProperty.get();
	}

	/**
	 * Sets the value of this port.
	 * 
	 * @param value the value
	 */
	public void setValue(Object value) {
		valueProperty.set(value);
	}

	/**
	 * Returns the value of this port as {@link ObjectProperty}
	 * 
	 * @return the value property
	 */
	public ObjectProperty<Object> getValueProperty() {
		return valueProperty;
	}

	/**
	 * Returns the type of the value of this port. Used for field identification.
	 * 
	 * @return the type of the port value
	 */
	public Class<?> getType() {
		Class<?> type = typeProperty.get();
		if (type.isPrimitive())
			return Primitives.wrap(type);
		return type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((backendIdProperty == null) ? 0 : backendIdProperty.get().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasePort other = (BasePort) obj;
		if (backendIdProperty == null) {
			if (other.backendIdProperty != null)
				return false;
		} else if (!backendIdProperty.get().equals(other.backendIdProperty.get()))
			return false;
		return true;
	}

}
