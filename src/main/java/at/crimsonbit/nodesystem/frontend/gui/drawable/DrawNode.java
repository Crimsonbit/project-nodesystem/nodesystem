/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.drawable;

import java.util.ArrayList;
import java.util.List;

import com.google.common.eventbus.EventBus;

import at.crimsonbit.nodesystem.event.GraphCalculateEvent;
import at.crimsonbit.nodesystem.event.GraphSelectionEvent;
import at.crimsonbit.nodesystem.event.MouseNodeClickedEvent;
import at.crimsonbit.nodesystem.event.MouseNodeDraggedEvent;
import at.crimsonbit.nodesystem.event.MouseNodePressedEvent;
import at.crimsonbit.nodesystem.event.MouseNodeReleasedEvent;
import at.crimsonbit.nodesystem.event.NodeCopiedEvent;
import at.crimsonbit.nodesystem.event.NodeMenuRemoveEvent;
import at.crimsonbit.nodesystem.event.NodeRenameEvent;
import at.crimsonbit.nodesystem.event.WidgetQuickMenuSelectionEvent;
import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.frontend.gui.base.BaseNode;
import at.crimsonbit.nodesystem.frontend.gui.base.BasePort;
import at.crimsonbit.nodesystem.frontend.gui.guilang.GLContainer;
import at.crimsonbit.nodesystem.frontend.gui.guilang.GLNode;
import at.crimsonbit.nodesystem.frontend.gui.guilang.GLPort;
import at.crimsonbit.nodesystem.frontend.gui.guilang.GLShadow;
import at.crimsonbit.nodesystem.frontend.gui.menu.NodeMenu;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Control;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * <h1>DrawNode</h1> The {@link DrawNode} is the graphical representation of any
 * type of node. It uses {@link BasePort}s and {@link BaseNode}s to create it's
 * layout. <br>
 * The {@link DrawNode} can have a {@link GLContainer} which holds more
 * information on how the node should look.
 * 
 * @author Florian Wagner
 * 
 * @see DrawGraph
 * @see GLContainer
 * @see BaseNode
 *
 */
public class DrawNode extends AnchorPane implements IDrawable {

	private BaseNode node;

	public boolean toggle = false;
	private boolean firstDraw = true;
	private boolean selected = false;
	private GLContainer settings;
	private Region top = new Region();
	private Text nodeText = new Text();
	private Rectangle background = new Rectangle();
	private Rectangle selectionRectangle = new Rectangle();
	private Region inPortBackground = new Region();
	private Region outPortBackground = new Region();
	private ProgressBar computationProgress = new ProgressBar();
	private final EventBus bus;
	private final EventBus localBus;
	private Timeline timeline = new Timeline();
	private final long ANIMATION_DURATION = 150;
	private boolean touch = false;
	private NodeMenu nodeMenu;
	private DoubleProperty scaleProperty = new SimpleDoubleProperty(1.0);
	private DoubleProperty BASE_WIDTH = new SimpleDoubleProperty(0.0);
	private DoubleProperty BASE_HEIGHT = new SimpleDoubleProperty(0.0);
	private ObjectProperty<Paint> colorPorperty = new SimpleObjectProperty<Paint>(null);
	private ObjectProperty<Paint> outlineColorProperty = new SimpleObjectProperty<Paint>(null);
	private double baseCalcSize = 0.0D;
	private final double CIRCLE_SIZE = 5D;
	private Circle resizeCircle = new Circle(CIRCLE_SIZE);

	/**
	 * Creates a new DrawNode object.
	 * 
	 * @param bus the event bus to use. The {@link NodeGraph#getEventBus()} should
	 * be used
	 * @param node the associated {@link BaseNode}
	 * @param settings the settings container
	 * @param touchEnabled whether touch is enabled
	 */
	public DrawNode(EventBus bus, EventBus localBus, BaseNode node, GLContainer settings, boolean touchEnabled) {
		this.node = node;
		this.bus = bus;
		this.localBus = localBus;
		this.settings = settings;
		this.touch = touchEnabled;
		createLayout();
		setMouseTransparent(false);

		setOnMouseEntered(event -> {
			if (!selected) {
				timeline.stop();
				timeline.getKeyFrames().clear();
				final KeyValue kv = new KeyValue(selectionRectangle.strokeProperty(), colorPorperty.get(),
						Interpolator.EASE_BOTH);
				final KeyFrame kf = new KeyFrame(Duration.millis(ANIMATION_DURATION), kv);
				final KeyValue kv2 = new KeyValue(selectionRectangle.fillProperty(), colorPorperty.get(),
						Interpolator.EASE_BOTH);
				final KeyFrame kf2 = new KeyFrame(Duration.millis(ANIMATION_DURATION), kv2);
				timeline.getKeyFrames().addAll(kf, kf2);
				timeline.play();

			} else {
				selectionRectangle.setStroke(colorPorperty.get());
				selectionRectangle.setFill(colorPorperty.get());

			}
		});

		setOnMouseClicked(event -> {
			toFront();
			requestFocus();
			bus.post(new GraphSelectionEvent(getNode().getBackendID(), event));
			bus.post(new WidgetQuickMenuSelectionEvent(this));
		});

		setOnMouseExited(event -> {
			if (selected == false) {
				timeline.stop();
				timeline.getKeyFrames().clear();
				final KeyValue kv = new KeyValue(selectionRectangle.strokeProperty(), outlineColorProperty.get(),
						Interpolator.EASE_BOTH);
				final KeyFrame kf = new KeyFrame(Duration.millis(ANIMATION_DURATION), kv);
				final KeyValue kv2 = new KeyValue(selectionRectangle.fillProperty(), outlineColorProperty.get(),
						Interpolator.EASE_BOTH);
				final KeyFrame kf2 = new KeyFrame(Duration.millis(ANIMATION_DURATION), kv2);
				timeline.getKeyFrames().addAll(kf, kf2);
				timeline.play();
			}
		});

		if (getSettings().getNode().getNodeShadow().isUse_shadow()) {
			DropShadow shadowE = new DropShadow();
			shadowE.setBlurType(BlurType.GAUSSIAN);
			GLShadow shadow = getSettings().getNode().getNodeShadow();
			shadowE.setColor(shadow.getShadow_color());
			shadowE.setWidth(shadow.getShadow_width());
			shadowE.setHeight(shadow.getShadow_height());
			shadowE.setOffsetX(shadow.getShadow_offset_x());
			shadowE.setOffsetY(shadow.getShadow_offset_y());
			shadowE.setRadius(shadow.getShadow_radius());
			setEffect(shadowE);
		}
		nodeText.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				BASE_WIDTH.set(calculateWidthBasedOnText(nodeText, BASE_WIDTH));
			}
		});
		// DragResizeMod.makeResizable(this);

		BASE_WIDTH.set(getSettings().getNode().getWidth());

	}

	/**
	 * Returns the background of the node
	 * 
	 * @return the background rectangle
	 */
	public Rectangle getNodeBackground() {
		return background;
	}

	/**
	 * Returns the top part of the node.
	 * 
	 * @return the top region
	 */
	public Region getNodeTop() {
		return top;
	}

	/**
	 * Returns true if this node is selected, otherwise false
	 * 
	 * @return whether this node is selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Returns the calculated height of this node.
	 * 
	 * @return the height
	 */
	public double getCalculatedHeight() {
		return baseCalcSize;
	}

	/**
	 * Sets the selected value of the node.
	 * 
	 * @param selected true if the node has been selected otherwise false to
	 * deselect it
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
		if (selected == true) {
			selectionRectangle.setStroke(colorPorperty.get());
			selectionRectangle.setFill(colorPorperty.get());

		} else {
			selectionRectangle.setStroke(outlineColorProperty.get());
			selectionRectangle.setFill(outlineColorProperty.get());
		}
	}

	/**
	 * Returns the {@link BaseNode} of this DrawNode
	 * 
	 * @return the base node
	 */
	public BaseNode getNode() {
		return node;
	}

	/**
	 * Returns whether this node is toggled or not
	 * 
	 * @return the toggled value of the node
	 */
	public boolean isToggled() {
		return toggle;
	}

	/**
	 * Returns the backend id of this node.
	 * 
	 * @return
	 */
	public String getBackendID() {
		return node.getBackendID();
	}

	/**
	 * Sets the node's width
	 * 
	 * @param width the width to set
	 */
	public void setNodeWidth(double width) {
		setWidth(width);
	}

	/**
	 * Returns the width property of this DrawNode.
	 * 
	 * @return the width property
	 */
	public DoubleProperty getWidthProperty() {
		return BASE_WIDTH;
	}

	/**
	 * Returns the height property of this DrawNode.
	 * 
	 * @return the height property
	 */
	public DoubleProperty getHeightProperty() {
		return BASE_HEIGHT;
	}

	private double calculateWidthBasedOnText(Text text, DoubleProperty base_width) {

		double txWidth = text.getBoundsInLocal().getWidth();
		if ((txWidth + 15D) >= base_width.get())
			return (txWidth + 15D);
		else if ((txWidth + 15D) < base_width.get())
			base_width.set(txWidth + 15D);
		if ((txWidth + 15D) < getSettings().getNode().getWidth()) {
			base_width.set(getSettings().getNode().getWidth());
		}
		return base_width.get();
	}

	private void makeDragable(javafx.scene.Node top) {

		top.setOnMousePressed(event -> {
			bus.post(new MouseNodePressedEvent(getNode().getBackendID(), event));
		});

		top.setOnMouseReleased(event -> {
			bus.post(new MouseNodeReleasedEvent(getNode().getBackendID(), event));
		});

		top.setOnMouseDragged(event -> {
			bus.post(new MouseNodeDraggedEvent(getNode().getBackendID(), event));
		});

		top.setOnMouseClicked(event -> {
			bus.post(new MouseNodeClickedEvent(getNode().getBackendID(), event));
		});

		if (touch) {
			top.setOnTouchPressed(event -> {
				bus.post(new MouseNodeClickedEvent(getNode().getBackendID(), event));
				bus.post(new MouseNodePressedEvent(getNode().getBackendID(), event));
			});

			top.setOnTouchReleased(event -> {
				bus.post(new MouseNodeReleasedEvent(getNode().getBackendID(), event));
			});

			top.setOnTouchMoved(event -> {
				bus.post(new MouseNodeDraggedEvent(getNode().getBackendID(), event));
			});
		}
	}

	/**
	 * Sets the name of this node
	 * 
	 * @param name the name
	 */
	public void setName(String name) {
		getNode().setName(name);
	}

	public ObjectProperty<Paint> getColorPorperty() {
		return colorPorperty;
	}

	public void setColorPorperty(ObjectProperty<Paint> colorPorperty) {
		this.colorPorperty = colorPorperty;
	}

	/**
	 * Creates the whole layout of the node. Also used to update the node.
	 */
	@Override
	public void createLayout() {

		GLNode nodeSettings = getSettings().getNode();
		GLPort portSettings = getSettings().getPort();

		double BASE_PREF_HEIGHT = nodeSettings.getHeight();
		double BASE_OFFSET_START = nodeSettings.getTopHeight();
		double IN_OFFSET_X = portSettings.getInput().getOffsetX();
		double IN_OFFSET_Y = portSettings.getInput().getOffsetY();
		double OUT_OFFSET_X = portSettings.getOutput().getOffsetX();
		double OUT_OFFSET_Y = portSettings.getOutput().getOffsetY();
		double BASE_PORT_OFFSET = portSettings.getPortOffsetY();
		double PORT_OFFSET = portSettings.getPortOffsetY();

		double BASE_TOP_HEIGHT = nodeSettings.getTopHeight();
		double ARC_WIDTH = nodeSettings.getEdge().getWidth();
		double ARC_HEIGHT = nodeSettings.getEdge().getHeight();

		final int inPortCount = getNode().getInputPortCount();
		final int outPortCount = getNode().getOutputPortCount();

		final double SELECTION_SIZE = 2D;
		Color COLOR_PORT_SEIDE = nodeSettings.getPortSideColor();
		if (colorPorperty.get() == null)
			colorPorperty.set(nodeSettings.getTopColor());

		nodeSettings.setTopColor((Color) colorPorperty.get());
		Color COLOR_SELECTION = getSettings().getNode().getOutline_color();
		Color COLOR_NODE = nodeSettings.getBottomColor();

		if (outlineColorProperty.get() == null)
			outlineColorProperty.set(COLOR_SELECTION);

		Color TEXT_COLOR = nodeSettings.getTextColor();
		BASE_HEIGHT.set(BASE_PREF_HEIGHT);

		// setWidth(BASE_WIDTH.get());
		// setPrefWidth(BASE_WIDTH.get());
		prefWidthProperty().bind(BASE_WIDTH);
		prefHeightProperty().bind(BASE_HEIGHT);

		if (!toggle) {

			resizeCircle.setFill(colorPorperty.get());

			// ================TEXT======================
			nodeText.textProperty().bind(node.getNameProperty());

			nodeText.setFill(TEXT_COLOR);
			// getNode().getNameProperty().bind(nodeText.textProperty());
			setLeftAnchor(nodeText, 15d);
			setTopAnchor(nodeText, 5d);
			BASE_WIDTH.set(calculateWidthBasedOnText(nodeText, BASE_WIDTH));

			// ===============TOP=======================
			BackgroundFill topBackground = new BackgroundFill(colorPorperty.get(),
					new CornerRadii(ARC_WIDTH / 2D, ARC_WIDTH / 2D, 0, 0, false), new Insets(0, 0, 0, 0));
			top.setBackground(new Background(topBackground));

			colorPorperty.addListener(new ChangeListener<Paint>() {

				@Override
				public void changed(ObservableValue<? extends Paint> observable, Paint oldValue, Paint newValue) {
					BackgroundFill topBackground = new BackgroundFill(colorPorperty.get(),
							new CornerRadii(ARC_WIDTH / 2D, ARC_WIDTH / 2D, 0, 0, false), new Insets(0, 0, 0, 0));
					top.setBackground(new Background(topBackground));

				}
			});

			top.setPrefHeight(BASE_TOP_HEIGHT);
			top.prefWidthProperty().bind(BASE_WIDTH.add(SELECTION_SIZE / 2D));
			// top.setMinWidth(Double.MIN_VALUE);
			setLeftAnchor(top, 0d);
			setTopAnchor(top, 0d);

			makeDragable(top);
			makeDragable(nodeText);

			nodeMenu = new NodeMenu(top);
			// nodeMenu.addItemDisabled(getBackendID());
			nodeMenu.addItem("Calculate", new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					bus.post(new GraphCalculateEvent(getNode().getBackendID()));
				}
			});

			nodeMenu.addItem("Copy", new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					bus.post(new NodeCopiedEvent(getNode().getBackendID()));
				}
			});
			nodeMenu.addItem("Rename", new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					bus.post(new NodeRenameEvent(getNode().getBackendID()));
				}
			});
			nodeMenu.addSeparator();
			nodeMenu.addItem("Remove", new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					bus.post(new NodeMenuRemoveEvent(getNode().getBackendID()));
				}
			});

			// ===============BACKGROUND=======================

			background.setStroke(COLOR_NODE);
			background.setFill(COLOR_NODE);
			setLeftAnchor(background, 0d);
			setRightAnchor(background, 0d);
			setTopAnchor(background, 0d);
			setBottomAnchor(background, 0d);
			background.widthProperty().bind(BASE_WIDTH);
			background.setArcHeight(ARC_WIDTH);
			background.setArcWidth(ARC_HEIGHT);
			background.setMouseTransparent(false);
			background.setVisible(true);

			makeDragable(background);

			background.setOnMouseClicked(event -> {

				if (event.getClickCount() >= 2) {

					toggle = !toggle;
					createLayout();
				}
			});

			background.toBack();

			// ================SELECTION RECT======================

			selectionRectangle.setArcHeight(ARC_WIDTH);
			selectionRectangle.setArcWidth(ARC_HEIGHT);
			selectionRectangle.setStroke(COLOR_SELECTION);
			selectionRectangle.setFill(COLOR_SELECTION);

			selectionRectangle.toBack();
			selectionRectangle.widthProperty().bind(BASE_WIDTH.add(SELECTION_SIZE));
			selectionRectangle.setTranslateX(-((SELECTION_SIZE - 1) / 2D));
			// selectionRectangle.setTranslateY(-(SELECTION_SIZE / 2D));
			// selectionRectangle.setVisible(false);

			// ================PORT BG======================

			double inBGSize = portSettings.getInput().getSize() + 15;

			inPortBackground.setBackground(new Background(new BackgroundFill(COLOR_PORT_SEIDE,
					new CornerRadii(0, 0, 0, ARC_WIDTH / 2D, false), new Insets(BASE_TOP_HEIGHT, 0, 0, 0))));

			inPortBackground.setPrefSize(inBGSize, BASE_PREF_HEIGHT);
			inPortBackground.setMaxWidth(Control.USE_PREF_SIZE);
			inPortBackground.setMinWidth(Double.MIN_VALUE);

			double outBGSize = portSettings.getOutput().getSize() + 15;

			outPortBackground.setBackground(new Background(new BackgroundFill(COLOR_PORT_SEIDE,
					new CornerRadii(0, 0, ARC_WIDTH / 2D, 0, false), new Insets(BASE_TOP_HEIGHT, 0, 0, 0))));

			outPortBackground.setPrefSize(outBGSize, BASE_PREF_HEIGHT);
			// outPortBackground.setTranslateX(BASE_WIDTH.get() - outBGSize + 1D);
			outPortBackground.translateXProperty().bind(BASE_WIDTH.subtract(outBGSize).add(1D));
			outPortBackground.setMaxWidth(Control.USE_PREF_SIZE);
			outPortBackground.setMinWidth(Double.MIN_VALUE);

			// computationProgress.setMaxWidth(BASE_TOP_HEIGHT - 5D);
			// computationProgress.setMaxHeight(BASE_TOP_HEIGHT - 5D);
			// computationProgress.setPrefSize(BASE_TOP_HEIGHT - 5D, BASE_TOP_HEIGHT - 5D);
			// computationProgress.setBackground(new Background(
			// new BackgroundFill(Color.TRANSPARENT, new CornerRadii(0, 0, 0, 0, false), new
			// Insets(0, 0, 0, 0))));
			setLeftAnchor(computationProgress, 0D);
			// setRightAnchor(computationProgress, 0D);
			// computationProgress.setPrefWidth(BASE_WIDTH.get() + (SELECTION_SIZE / 2D));
			computationProgress.prefWidthProperty().bind(BASE_WIDTH.add(SELECTION_SIZE / 2D));
			computationProgress.getStylesheets().add(getClass().getResource("prbar.css").toExternalForm());
			setTopAnchor(computationProgress, BASE_TOP_HEIGHT - 1D);

			// ================ADDING======================
			if (firstDraw) {
				getChildren().add(selectionRectangle);
				getChildren().add(background);
				getChildren().add(inPortBackground);
				getChildren().add(outPortBackground);
				getChildren().add(top);
				getChildren().add(nodeText);
				getChildren().add(resizeCircle);

			}

			// ================PORTS======================
			BASE_HEIGHT.addListener(new ChangeListener<Number>() {

				@Override
				public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
					resizeCircle.setLayoutY(newValue.doubleValue() - (CIRCLE_SIZE * 1.5D));
					resizeCircle.setLayoutX(CIRCLE_SIZE * 1.5D);
				}
			});
			BASE_WIDTH.addListener(new ChangeListener<Number>() {

				@Override
				public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
					for (javafx.scene.Node n : getChildren()) {
						if (n instanceof DrawPort) {
							DrawPort dPort = (DrawPort) n;
							if (!dPort.isInputPort()) {

								dPort.setLayoutX(newValue.doubleValue() - dPort.getBoundsInLocal().getMaxX()
										- OUT_OFFSET_X + 15D);
							}
						}
					}
				}
			});
			if (firstDraw) {
				for (BasePort port : getNode().getOutputPorts()) {
					int index = getNode().getOutputPortIndex(port);
					DrawPort drawPort = new DrawPort(port, getSettings(), localBus, BASE_WIDTH);
					double layoutY = BASE_TOP_HEIGHT + (PORT_OFFSET * index);

					if (index != 0) {

						layoutY = (layoutY + OUT_OFFSET_Y);
					} else {
						layoutY = (layoutY + portSettings.getOutput().getSize());
					}
					drawPort.setLayoutY(layoutY + 5D);
					drawPort.setLayoutX(OUT_OFFSET_X);

					drawPort.setPosY(drawPort.getLayoutY());

					getChildren().add(drawPort);
				}

				for (BasePort port : getNode().getInputPorts()) {
					int index = getNode().getInpnutPortIndex(port);
					DrawPort drawPort = new DrawPort(port, getSettings(), localBus, BASE_WIDTH);
					double layoutY = BASE_OFFSET_START
							+ ((BASE_PORT_OFFSET * inPortCount) + (BASE_PORT_OFFSET * index));
					drawPort.setLayoutY(layoutY);
					drawPort.setLayoutX(IN_OFFSET_X);
					drawPort.setLayoutY(drawPort.getLayoutY() + IN_OFFSET_Y + 5D);

					drawPort.setPosY(drawPort.getLayoutY());

					getChildren().add(drawPort);
				}

			} else

			{
				List<DrawPort> pfusch = new ArrayList<>();
				for (javafx.scene.Node n : getChildren()) {
					if (n instanceof DrawPort) {
						pfusch.add((DrawPort) n);

					}
				}

				for (DrawPort node : pfusch) {
					node.setLayoutY(node.getPosY());
					node.toFront();
					node.setVisible(true);
				}
			}

			BASE_HEIGHT.set(BASE_TOP_HEIGHT + portSettings.getInput().getSize()
					+ ((BASE_PORT_OFFSET * (inPortCount + outPortCount + 1D))));

			baseCalcSize = BASE_HEIGHT.get();
			background.heightProperty().bind(BASE_HEIGHT);
			selectionRectangle.heightProperty().bind(BASE_HEIGHT.add(SELECTION_SIZE / 2D));
			inPortBackground.maxHeightProperty().bind(BASE_HEIGHT.add(1D));
			inPortBackground.prefHeightProperty().bind(BASE_HEIGHT.add(1D));
			outPortBackground.maxHeightProperty().bind(BASE_HEIGHT.add(1D));
			outPortBackground.prefHeightProperty().bind(BASE_HEIGHT.add(1D));
			selectionRectangle.setVisible(true);
			resizeCircle.setVisible(true);

			// selectionRectangle.setTranslateY(-.1D);
			// selectionRectangle.setVisible(true);

		}

		else {

			List<DrawPort> pfusch = new ArrayList<>();
			for (javafx.scene.Node n :

			getChildren()) {
				if (n instanceof DrawPort) {
					pfusch.add((DrawPort) n);
				}
			}
			for (DrawPort node : pfusch) {
				node.setLayoutY((getSettings().getNode().getTopHeight() / 2D));
				node.toBack();
				node.setVisible(false);

			}

			selectionRectangle.setVisible(false);
			selectionRectangle.heightProperty().unbind();
			background.heightProperty().unbind();
			selectionRectangle.heightProperty().unbind();
			inPortBackground.maxHeightProperty().unbind();
			inPortBackground.prefHeightProperty().unbind();
			outPortBackground.maxHeightProperty().unbind();
			outPortBackground.prefHeightProperty().unbind();
			resizeCircle.setVisible(false);
			background.setHeight(BASE_TOP_HEIGHT - 1D);
			inPortBackground.setMaxHeight(BASE_TOP_HEIGHT - 1D);
			inPortBackground.setPrefHeight(BASE_TOP_HEIGHT - 1D);
			outPortBackground.setMaxHeight(BASE_TOP_HEIGHT - 1D);
			outPortBackground.setPrefHeight(BASE_TOP_HEIGHT - 1D);
		}
		firstDraw = false;

	}

	/**
	 * Starts the progress indicator of the node. This is used to show which node is
	 * being computed.
	 */
	public void startProgress() {
		Platform.runLater(() -> {
			// if (!getChildren().contains(computationProgress))
			// getChildren().add(computationProgress);
			computationProgress.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
			selectionRectangle.setStroke(getSettings().getNode().getOutline_color());
			selectionRectangle.setFill(getSettings().getNode().getOutline_color());
		});

	}

	/**
	 * Marks the node. Called when an error occurs during computation
	 */
	public void errorProgress() {
		// selectionRectangle.setStroke(Color.RED);
		// selectionRectangle.setFill(Color.RED);
		Platform.runLater(() -> {
			computationProgress.getStylesheets().add(getClass().getResource("prbar_err.css").toExternalForm());
			computationProgress.setProgress(1.0D);
		});
	}

	/**
	 * Stops the progress indicator of the node. This is used to show which node is
	 * finished with his computation.
	 */
	public void finishProgress() {
		Platform.runLater(() -> {
			computationProgress.setProgress(1.0D);
		});

	}

	/**
	 * Binds the scale property of this node
	 * 
	 * @param prop the property to bind
	 */
	public void bindScale(DoubleProperty prop) {
		scaleProperty.bind(prop);
	}

	/**
	 * Returns the settings container of this node
	 * 
	 * @return the settings
	 */
	public GLContainer getSettings() {
		return settings;
	}

	public DoubleProperty getScaleProperty() {
		return scaleProperty;
	}

}
