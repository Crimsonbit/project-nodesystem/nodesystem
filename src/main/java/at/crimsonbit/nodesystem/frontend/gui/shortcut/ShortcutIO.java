/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.shortcut;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

/**
 * <h1>ShortcutReader</h1>
 * <p>
 * Reader to import the shortcut table
 * </p>
 * 
 * @author Florian Wagner
 *
 */
@Deprecated
public class ShortcutIO {

	private final static String LOCATION = "shortcutTable.res";
	private final static GsonBuilder gsonB = new GsonBuilder();
	private static final Gson gson;
	static {
		gsonB.setPrettyPrinting();
		gson = gsonB.create();
	}

	public static void writeShortcutTable(ShortcutTable table) {

		String jsonOut = gson.toJson(table);
		Path p = null;
		p = new File(LOCATION).toPath();

		try {
			BufferedWriter writer = Files.newBufferedWriter(p, StandardCharsets.UTF_8);
			writer.append(jsonOut);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static ShortcutTable readShortcutTable() {
		JsonReader reader = null;
		try {
			reader = new JsonReader(new FileReader(LOCATION));
		} catch (IOException e) {
			ShortcutTable data = new ShortcutTable();

			return data;
		}
		ShortcutTable data = null;
		if (reader != null) {
			data = gson.fromJson(reader, ShortcutTable.class);
		}

		if (data == null) {
			data = new ShortcutTable();

		}
		return data;
	}

}
