/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.drawable;

import java.math.BigDecimal;

import com.google.common.eventbus.EventBus;

import at.crimsonbit.nodesystem.event.MousePortEnteredEvent;
import at.crimsonbit.nodesystem.event.MousePortExitedEvent;
import at.crimsonbit.nodesystem.event.MouseSnapEvent;
import at.crimsonbit.nodesystem.event.MouseUnsnapEvent;
import at.crimsonbit.nodesystem.event.NodeChangeConnectionEvent;
import at.crimsonbit.nodesystem.event.PortValueChangedEvent;
import at.crimsonbit.nodesystem.event.SignalResetColorEvent;
import at.crimsonbit.nodesystem.event.SignalTryConnectionEven;
import at.crimsonbit.nodesystem.frontend.gui.GuiState;
import at.crimsonbit.nodesystem.frontend.gui.base.BaseInputPort;
import at.crimsonbit.nodesystem.frontend.gui.base.BaseOutputPort;
import at.crimsonbit.nodesystem.frontend.gui.base.BasePort;
import at.crimsonbit.nodesystem.frontend.gui.fields.BigDecimalField;
import at.crimsonbit.nodesystem.frontend.gui.fields.DoubleField;
import at.crimsonbit.nodesystem.frontend.gui.fields.FloatField;
import at.crimsonbit.nodesystem.frontend.gui.fields.IntegerField;
import at.crimsonbit.nodesystem.frontend.gui.fields.LongField;
import at.crimsonbit.nodesystem.frontend.gui.guilang.GLContainer;
import at.crimsonbit.nodesystem.frontend.gui.guilang.GLNode;
import at.crimsonbit.nodesystem.frontend.gui.guilang.GLPort;
import at.crimsonbit.nodesystem.nodebackend.exception.InvalidPortException;
import at.crimsonbit.nodesystem.nodebackend.graph.ConnectionResult;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import at.crimsonbit.nodesystem.util.Util;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

/**
 * <h1>DrawPort</h1> <br>
 * The graphical representation of a port.
 * 
 * @author Florian Wagner
 * @see BasePort
 * @see GLContainer
 */
public class DrawPort extends Group implements IDrawable {

	public final BasePort port;
	private GLContainer settings;
	private double posY;
	private Circle cPort;
	private Circle snapCircle;
	private Text portName;
	private final EventBus eventBus;
	private TextField field;
	private CheckBox checkFieldControl;
	private BooleanProperty connectedProperty = new SimpleBooleanProperty(false);
	private DoubleProperty nodeWidthProperty = new SimpleDoubleProperty(0.0D);
	private boolean drawOutputNoOffset = false;

	/**
	 * Creates a new DrawPort object.
	 * 
	 * @param port     the base port object
	 * @param settings the settings container
	 * @param bus      the event bus to use. The {@link NodeGraph#getEventBus()}
	 *                 should be used
	 */
	public DrawPort(BasePort port, GLContainer settings, EventBus bus, DoubleProperty prop) {
		this.port = port;
		this.settings = settings;
		this.eventBus = bus;
		this.nodeWidthProperty = prop;
		createLayout();
		addConnectivitySupport();
		disconnectPort();

	}

	public DrawPort(BasePort port, GLContainer settings, EventBus bus, DoubleProperty prop, boolean field,
			boolean drawOutputNoOffset) {
		this.port = port;
		this.settings = settings;
		this.eventBus = bus;
		this.nodeWidthProperty = prop;
		this.drawOutputNoOffset = drawOutputNoOffset;
		port.setField(field);
		createLayout();
		// addConnectivitySupport();
		// disconnectPort();

	}

	private void disconnectPort() {
		setOnContextMenuRequested(event -> {
			if (getPort().isInput()) {
				port.getParentNode().getGraphConnector().disconnectFromInput(port);
			} else {
				port.getParentNode().getGraphConnector().disconnectFromOutput(port);

			}
		});
	}

	/**
	 * Returns the {@link BasePort} object.
	 * 
	 * @return the base port
	 */
	public BasePort getPort() {
		return port;
	}

	/**
	 * Returns whether this port is an input port.
	 * 
	 * @return true if it is an input port otherwise false
	 */
	public boolean isInputPort() {
		return port instanceof BaseInputPort;
	}

	public void turnOffField() {
		port.setField(false);
	}

	/**
	 * Returns whether this port is an output port.
	 * 
	 * @return true if it is an output port otherwise false
	 */
	public boolean isOutputPort() {
		return port instanceof BaseOutputPort;
	}

	private void addConnectivitySupport() {

		setOnMouseEntered(event -> {
			eventBus.post(
					new MousePortEnteredEvent(getPort().getBackendID(), getPort().getParentNode().getBackendID()));

		});

		setOnMouseExited(event -> {
			eventBus.post(new MousePortExitedEvent(getPort().getBackendID(), getPort().getParentNode().getBackendID()));
		});
		getSnapCircle().setOnMouseDragExited(event -> {
			eventBus.post(new MouseUnsnapEvent(this));
			eventBus.post(new SignalResetColorEvent(getSettings().getSignal().getColorDefault()));

		});
		getSnapCircle().setOnMouseDragReleased(event -> {
			if (GuiState.getState().equals(GuiState.CONNECTION)) {
				if (port.getParentNode().getGraphConnector().isInConnection()) {
					try {
						if (port.getParentNode().getGraphConnector().completeConnection(port,
								true) == ConnectionResult.SUCCESS) {
							port.getParentNode().getGraphConnector().completeConnection(port, false);
							GuiState.setState(GuiState.DEFAULT);
						} else {
							port.getParentNode().getGraphConnector().abortConnection();
						}
					} catch (InvalidPortException e) {
						e.printStackTrace();
					}

				}

			}
		});
		getSnapCircle().setOnMouseDragEntered(event -> {
			if (port.getParentNode().getGraphConnector().isInConnection()) {
				if (port.getParentNode().getGraphConnector().getFirst() != port)
					try {
						if (port.getParentNode().getGraphConnector().completeConnection(port,
								true) == ConnectionResult.SUCCESS) {
							eventBus.post(new MouseSnapEvent(this));
							eventBus.post(new SignalTryConnectionEven(true,
									port.getParentNode().getGraphConnector().getFirst(), port,
									getSettings().getSignal().getColor_con_fail(),
									getSettings().getSignal().getColor_con_success()));

						} else {
							eventBus.post(new SignalTryConnectionEven(false,
									port.getParentNode().getGraphConnector().getFirst(), port,
									getSettings().getSignal().getColor_con_fail(),
									getSettings().getSignal().getColor_con_success()));

						}
					} catch (InvalidPortException e) {
						e.printStackTrace();
					}
			}
		});

		getPortCircle().setOnMouseDragExited(event -> {
			eventBus.post(new SignalResetColorEvent(getSettings().getSignal().getColorDefault()));
		});

		getPortCircle().setOnMouseDragReleased(event -> {
			if (GuiState.getState().equals(GuiState.CONNECTION)) {
				if (port.getParentNode().getGraphConnector().isInConnection()) {
					try {
						if (port.getParentNode().getGraphConnector().completeConnection(port,
								true) == ConnectionResult.SUCCESS) {
							port.getParentNode().getGraphConnector().completeConnection(port, false);
							GuiState.setState(GuiState.DEFAULT);
						} else {
							port.getParentNode().getGraphConnector().abortConnection();
						}
					} catch (InvalidPortException e) {
						e.printStackTrace();
					}

				}

			}
		});

		getPortCircle().setOnMousePressed(mousePressed -> {
			GuiState.setState(GuiState.CONNECTION);
			if (mousePressed.getButton().equals(MouseButton.PRIMARY)) {
				if (port.getParentNode().getGraphConnector().getFirst() == null) {
					if (port.isInput()) {
						if (!connectedProperty.get()) {
							port.getParentNode().getGraphConnector().beginConnection(port);
						} else {
							port.getParentNode().getGraphConnector().abortConnection();
						}
					} else
						port.getParentNode().getGraphConnector().beginConnection(port);
				} else
					port.getParentNode().getGraphConnector().abortConnection();

			}
		});

		getPortCircle().setOnDragDetected(dragDetected -> {
			startFullDrag();
			if (connectedProperty.get())
				if (port.isInput()) {
					GuiState.setState(GuiState.CONNECTION);
					eventBus.post(new NodeChangeConnectionEvent(this));
				}

		});

		getPortCircle().setOnMouseReleased(mouseReleased -> {
			port.getParentNode().getGraphConnector().abortConnection();
			GuiState.setState(GuiState.DEFAULT);
		});

		getPortCircle().setOnMouseDragEntered(mouseEntered -> {

			if (GuiState.getState().equals(GuiState.CONNECTION)) {
				if (port.getParentNode().getGraphConnector().isInConnection()) {

					try {
						if (port.getParentNode().getGraphConnector().completeConnection(port,
								true) == ConnectionResult.SUCCESS) {
							eventBus.post(new SignalTryConnectionEven(true,
									port.getParentNode().getGraphConnector().getFirst(), port,
									getSettings().getSignal().getColor_con_fail(),
									getSettings().getSignal().getColor_con_success()));

						} else {
							eventBus.post(new SignalTryConnectionEven(false,
									port.getParentNode().getGraphConnector().getFirst(), port,
									getSettings().getSignal().getColor_con_fail(),
									getSettings().getSignal().getColor_con_success()));

						}
					} catch (InvalidPortException e) {
						e.printStackTrace();
					}

				}

			}

		});
	}

	/**
	 * Returns the port circle.
	 * 
	 * @return the port cirlse
	 */
	public Circle getPortCircle() {
		return cPort;
	}

	/**
	 * Returns the circle which is used to snap the mouse.
	 * 
	 * @return the snap circle
	 */
	public Circle getSnapCircle() {
		return snapCircle;
	}

	/**
	 * Returns the port text.
	 * 
	 * @return the port text
	 */
	public Text getPortText() {
		return portName;
	}

	/**
	 * Returns the settings container.
	 * 
	 * @return the settings container
	 */
	public GLContainer getSettings() {
		return settings;
	}

	private GLPort getPortSettings() {
		return settings.getPort();
	}

	private GLNode getNodeSettings() {
		return settings.getNode();
	}

	@Override
	public void createLayout() {
		final double LAYOUT_X_OUT = getNodeSettings().getWidth();
		final double LAYOUT_X_IN = 15;
		final double LAYOUT_X_PORT_OUT = getNodeSettings().getWidth();
		final double SNAPCIRCLE_SIZE = 20D;
		if (port instanceof BaseOutputPort) {
			if (!drawOutputNoOffset) {
				portName = new Text();
				portName.textProperty().bind(port.getNamePropert());
				portName.setLayoutX(
						LAYOUT_X_OUT - getPortSettings().getOutput().getSize() - portName.getBoundsInLocal().getWidth()
								- LAYOUT_X_IN - (portName.getBoundsInLocal().getWidth() / 4D));
				portName.setLayoutY((portName.getBoundsInLocal().getHeight() / 4d));
				portName.setFill(getPortSettings().getOutput().getTextColor());

				cPort = new Circle(getPortSettings().getOutput().getSize());
				cPort.setLayoutX(LAYOUT_X_PORT_OUT - LAYOUT_X_IN + 1 + (getPortSettings().getOutput().getSize()));
				cPort.setFill(getPortSettings().getOutput().getColor().darker().darker().desaturate().desaturate());
				cPort.setStroke(getPortSettings().getOutput().getColor());

				snapCircle = new Circle(getPortSettings().getOutput().getSize() + SNAPCIRCLE_SIZE);
				snapCircle.setLayoutX(LAYOUT_X_PORT_OUT - LAYOUT_X_IN + 1 + (getPortSettings().getOutput().getSize()));
				snapCircle.setFill(Color.TRANSPARENT);
				snapCircle.setStroke(Color.TRANSPARENT);
				getChildren().add(snapCircle);
				getChildren().add(cPort);
				getChildren().add(portName);
			} else {
				portName = new Text();
				portName.textProperty().bind(port.getNamePropert());
				portName.setLayoutY((portName.getBoundsInLocal().getHeight() / 4d));
				portName.setFill(getPortSettings().getOutput().getTextColor());
				snapCircle = new Circle(getPortSettings().getOutput().getSize() + SNAPCIRCLE_SIZE);
				cPort = new Circle(getPortSettings().getOutput().getSize());
				cPort.setLayoutX(LAYOUT_X_IN + 1 + (getPortSettings().getOutput().getSize())
						+ portName.getBoundsInLocal().getWidth());
				cPort.setFill(getPortSettings().getOutput().getColor().darker().darker().desaturate().desaturate());
				cPort.setStroke(getPortSettings().getOutput().getColor());
				getChildren().add(cPort);
				getChildren().add(portName);
			}
			if (port.isField())
				addField(LAYOUT_X_IN);

		} else if (port instanceof BaseInputPort) {

			portName = new Text();
			portName.textProperty().bind(port.getNamePropert());
			portName.setLayoutX(LAYOUT_X_IN + getPortSettings().getInput().getSize());
			portName.setLayoutY((portName.getBoundsInLocal().getHeight() / 4d));
			portName.setFill(getPortSettings().getInput().getTextColor());
			cPort = new Circle(getPortSettings().getInput().getSize());
			cPort.setLayoutX((getPortSettings().getOutput().getSize()) + 0.5 + (LAYOUT_X_IN / 4D));
			cPort.setFill(getPortSettings().getInput().getColor().darker().darker().desaturate().desaturate());
			cPort.setStroke(getPortSettings().getInput().getColor());
			snapCircle = new Circle(getPortSettings().getOutput().getSize() + SNAPCIRCLE_SIZE);
			snapCircle.setLayoutX((getPortSettings().getOutput().getSize()) + 0.5 + (LAYOUT_X_IN / 4D));
			snapCircle.setFill(Color.TRANSPARENT);
			snapCircle.setStroke(Color.TRANSPARENT);
			getChildren().add(snapCircle);
			getChildren().add(cPort);
			getChildren().add(portName);

			if (port.isField())
				addField(LAYOUT_X_IN);

			// Tooltip.install(cPort, new Tooltip("Port: " + port.getName() + "\nType:" +
			// port.getValue().toString()));

		}
//		port.getValueProperty().addListener(new ChangeListener<Object>() {
//
//			@Override
//			public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
//				Tooltip tp = new Tooltip("Port: " + port.getName() + "\nType: " + String.valueOf(newValue));
//				
//				
//				
//			}
//		});
	}

	private void addField(double size) {
		final double TEXT_HEIGHT = 1d;

		// System.out.println(port.getType());

		if (String.class.isAssignableFrom(port.getType())) {
			field = new TextField();
			field.setPrefSize(nodeWidthProperty.get() - 39D, TEXT_HEIGHT);
			field.prefWidthProperty().bind(nodeWidthProperty.subtract(39D));
			field.setTranslateX(20D);
			field.setTranslateY(portName.getBoundsInLocal().getMaxY() + TEXT_HEIGHT);
			field.setBackground(new Background(new BackgroundFill(getSettings().getNode().getBottomColor().brighter(),
					new CornerRadii(0, 0, 0, 0, false), new Insets(0, 0, 0, 0))));
			field.setStyle("-fx-text-fill: white; -fx-background-color: "
					+ Util.toRGBCode(getSettings().getNode().getBottomColor().brighter())
					+ "; -fx-border: transparent; -fx-background-insets: 0, 0 0 1 0 ;  -fx-background-radius: 0 ;");
			port.getValueProperty().bind(field.textProperty());
			field.requestFocus();
			getChildren().add(field);

		} else if (Integer.class.isAssignableFrom(port.getType())) {
			field = new IntegerField();

			field.prefWidthProperty().bind(nodeWidthProperty.subtract(39D));
			field.setTranslateX(20D);
			field.setTranslateY(portName.getBoundsInLocal().getMaxY()+ TEXT_HEIGHT);
			field.setBackground(new Background(new BackgroundFill(getSettings().getNode().getBottomColor().brighter(),
					new CornerRadii(0, 0, 0, 0, false), new Insets(0, 0, 0, 0))));
			field.setStyle("-fx-text-fill: white; -fx-background-color: "
					+ Util.toRGBCode(getSettings().getNode().getBottomColor().brighter())
					+ "; -fx-border: transparent; -fx-background-insets: 0, 0 0 1 0 ;  -fx-background-radius: 0 ;");
			port.getValueProperty().bind(((IntegerField) field).numberProperty());
			field.requestFocus();
			getChildren().add(field);

		} else if (Double.class.isAssignableFrom(port.getType())) {
			field = new DoubleField();
			field.prefWidthProperty().bind(nodeWidthProperty.subtract(39D));
			field.setTranslateX(20D);
			field.setTranslateY(portName.getBoundsInLocal().getMaxY()+ TEXT_HEIGHT);
			field.setBackground(new Background(new BackgroundFill(getSettings().getNode().getBottomColor().brighter(),
					new CornerRadii(0, 0, 0, 0, false), new Insets(0, 0, 0, 0))));
			field.setStyle("-fx-text-fill: white; -fx-background-color: "
					+ Util.toRGBCode(getSettings().getNode().getBottomColor().brighter())
					+ "; -fx-border: transparent; -fx-background-insets: 0, 0 0 1 0 ;  -fx-background-radius: 0 ;");
			port.getValueProperty().bind(((DoubleField) field).numberProperty());
			field.requestFocus();
			getChildren().add(field);

		} else if (Long.class.isAssignableFrom(port.getType())) {
			field = new LongField();
			field.prefWidthProperty().bind(nodeWidthProperty.subtract(39D));
			field.setTranslateX(20D);
			field.setTranslateY(portName.getBoundsInLocal().getMaxY()+ TEXT_HEIGHT);
			field.setBackground(new Background(new BackgroundFill(getSettings().getNode().getBottomColor().brighter(),
					new CornerRadii(0, 0, 0, 0, false), new Insets(0, 0, 0, 0))));
			field.setStyle("-fx-text-fill: white; -fx-background-color: "
					+ Util.toRGBCode(getSettings().getNode().getBottomColor().brighter())
					+ "; -fx-border: transparent; -fx-background-insets: 0, 0 0 1 0 ;  -fx-background-radius: 0 ;");
			port.getValueProperty().bind(((LongField) field).numberProperty());
			field.requestFocus();
			getChildren().add(field);

		} else if (Boolean.class.isAssignableFrom(port.getType())) {
			checkFieldControl = new CheckBox();

			final double SIZE = 16D; // size due to JavaFX being stupid

			checkFieldControl.setPrefSize(SIZE, SIZE);

			port.getValueProperty()
					.bind(Bindings.when(checkFieldControl.selectedProperty()).then(true).otherwise(false));
			checkFieldControl.setLayoutY(getPortSettings().getOutput().getSize() + TEXT_HEIGHT);
			checkFieldControl.setLayoutX((getNodeSettings().getWidth() / 2D) - (SIZE / 2D));
			checkFieldControl.requestFocus();
			getChildren().add(checkFieldControl);

		} else if (Float.class.isAssignableFrom(port.getType())) {
			field = new FloatField();
			field.prefWidthProperty().bind(nodeWidthProperty.subtract(39D));
			field.setTranslateX(20D);
			field.setTranslateY(portName.getBoundsInLocal().getMaxY()+ TEXT_HEIGHT);
			field.setBackground(new Background(new BackgroundFill(getSettings().getNode().getBottomColor().brighter(),
					new CornerRadii(0, 0, 0, 0, false), new Insets(0, 0, 0, 0))));
			field.setStyle("-fx-text-fill: white; -fx-background-color: "
					+ Util.toRGBCode(getSettings().getNode().getBottomColor().brighter())
					+ "; -fx-border: transparent; -fx-background-insets: 0, 0 0 1 0 ;  -fx-background-radius: 0 ;");
			port.getValueProperty().bind(((FloatField) field).numberProperty());
			field.requestFocus();
			getChildren().add(field);

		} else if (BigDecimal.class.isAssignableFrom(port.getType())) {
			field = new BigDecimalField();
			field.prefWidthProperty().bind(nodeWidthProperty.subtract(39D));
			field.setTranslateX(20D);
			field.setTranslateY(portName.getBoundsInLocal().getMaxY()+ TEXT_HEIGHT);
			field.setBackground(new Background(new BackgroundFill(getSettings().getNode().getBottomColor().brighter(),
					new CornerRadii(0, 0, 0, 0, false), new Insets(0, 0, 0, 0))));
			field.setStyle("-fx-text-fill: white; -fx-background-color: "
					+ Util.toRGBCode(getSettings().getNode().getBottomColor().brighter())
					+ "; -fx-border: transparent; -fx-background-insets: 0, 0 0 1 0 ;  -fx-background-radius: 0 ;");
			port.getValueProperty().bind(((BigDecimalField) field).numberProperty());
			field.requestFocus();
			getChildren().add(field);
		}

		port.getValueProperty().addListener(new ChangeListener<Object>() {
			@Override
			public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {

				eventBus.post(new PortValueChangedEvent(port.getBackendID(), newValue));

			}
		});
	}

	/**
	 * Returns a field as {@link TextField} or null if this port does not have one.
	 * 
	 * @return the field
	 */
	public TextField getField() {
		return field;
	}

	/**
	 * Returns the boolean field of this port as {@link CheckBox} or null if this
	 * port does not have one.
	 * 
	 * @return the field
	 */
	public CheckBox getBooleanField() {
		return checkFieldControl;
	}

	/**
	 * Shows the field object
	 */
	public void onPortFieldShow() {
		if (field != null) {
			if (!getChildren().contains(field)) {
				getChildren().add(field);
			}
		} else if (checkFieldControl != null) {
			if (!getChildren().contains(checkFieldControl)) {
				getChildren().add(checkFieldControl);
			}
		}
	}

	/**
	 * Hides the field object
	 */
	public void onPortFieldHide() {
		if (field != null) {
			if (getChildren().contains(field)) {
				getChildren().remove(field);
			}
		} else if (checkFieldControl != null) {
			if (getChildren().contains(checkFieldControl)) {
				getChildren().remove(checkFieldControl);
			}
		}
	}

	/**
	 * Returns the y position of this port
	 * 
	 * @return
	 */
	public double getPosY() {
		return posY;
	}

	/**
	 * Sets the y position of this port
	 * 
	 * @param posY the y position
	 */
	public void setPosY(double posY) {
		this.posY = posY;
	}

	public BooleanProperty connectedProperty() {
		return connectedProperty;
	}
}
