package at.crimsonbit.nodesystem.frontend.gui.component;

import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import javafx.scene.control.Tab;

public class GraphTab extends Tab {

	private DrawGraph graph;

	public GraphTab(String name, DrawGraph graph) {
		super(name);
		this.graph = graph;
	}

	public DrawGraph getGraph() {
		return graph;
	}

	public void setGraph(DrawGraph graph) {
		this.graph = graph;
	}

}
