package at.crimsonbit.nodesystem.frontend.gui;

public enum Theme {

	DARK, LIGHT;

}
