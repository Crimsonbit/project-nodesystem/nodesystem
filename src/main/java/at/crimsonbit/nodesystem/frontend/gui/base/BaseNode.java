/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.base;

import java.util.stream.Stream;

import at.crimsonbit.nodesystem.frontend.midlayer.GraphConnector;
import at.crimsonbit.nodesystem.nodebackend.INodeType;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * <h1>BaseNode</h1>
 * <p>
 * The BaseNode class is the base for all nodes. It stores all necessary values
 * that are used for the front and backend.
 * </p>
 * 
 * @author Florian Wagner
 *
 */
public class BaseNode {

	private ObservableList<BasePort> inputPorts = FXCollections.observableArrayList();
	private ObservableList<BasePort> outputPorts = FXCollections.observableArrayList();
	private StringProperty backendIdProperty = new SimpleStringProperty();
	private ObjectProperty<INodeType> typeProperty = new SimpleObjectProperty<INodeType>();
	private StringProperty nameProperty = new SimpleStringProperty();
	private ObjectProperty<GraphConnector> graphConnectorProperty = new SimpleObjectProperty<>();
	
	/**
	 * Creates a new BaseNode object.
	 * 
	 * @param type type of the node
	 * @param id   id of the node
	 * @param name the name of the node
	 * @param conn the {@link GraphConnector}
	 */
	public BaseNode(INodeType type, String id, String name, GraphConnector conn) {
		setName(name);
		setBackendID(id);
		setType(type);
		setGraphConnector(conn);
	}

	/**
	 * Simple constructor.<br>
	 * Creates a new BaseNode object
	 * 
	 * @param id   the id of the node
	 * @param name the name of the node
	 */
	public BaseNode(String id, String name) {
		setName(name);
		setBackendID(id);
	}

	/**
	 * Returns the {@link GraphConnector} object.
	 * 
	 * @return graphconnector
	 */
	public GraphConnector getGraphConnector() {
		return this.graphConnectorProperty.get();
	}

	/**
	 * Sets the {@link GraphConnector} object.
	 * 
	 * @param conn the connector
	 */
	public void setGraphConnector(GraphConnector conn) {
		graphConnectorProperty.set(conn);
	}

	/**
	 * Returns the type of this node.
	 * 
	 * @return the type
	 */
	public INodeType getType() {
		return typeProperty.get();
	}

	private void setType(INodeType type) {
		typeProperty.set(type);
	}

	protected void setBackendID(String id) {
		backendIdProperty.set(id);
	}

	/**
	 * Returns the string-id of this node.
	 * 
	 * @return the id of the node
	 */
	public String getBackendID() {
		return backendIdProperty.get();
	}

	/**
	 * Sets the name of this node.
	 * 
	 * @param name the name
	 */
	public void setName(String name) {
		nameProperty.set(name);
	}

	/**
	 * Returns the name of this node.
	 * 
	 * @return the name
	 */
	public String getName() {
		return nameProperty.get();
	}

	/**
	 * Returns the name as {@link StringProperty}.
	 * 
	 * @return the name
	 */
	public StringProperty getNameProperty() {
		return nameProperty;
	}

	/**
	 * Adds a new port to the node. This method uses {@link #addInputPort(BasePort)}
	 * and {@link #addOutputPort(BasePort)} internally.
	 * 
	 * @param port the port to add
	 * @return true if successful, otherwise false
	 */
	public boolean addPort(BasePort port) {

		if (addInputPort(port))
			return true;
		else if (addOutputPort(port))
			return true;
		else
			return false;

	}

	/**
	 * Adds a port as input port.
	 * 
	 * @param inputPort the port to add
	 * @return true if successful, otherwise false
	 */
	public boolean addInputPort(BasePort inputPort) {
		if (inputPort instanceof BaseInputPort) {
			inputPorts.add(inputPort);
			return true;
		}
		return false;
	}

	/**
	 * Adds a port as output port.
	 * 
	 * @param outputPort the port to add
	 * @return true if successful, otherwise false
	 */
	public boolean addOutputPort(BasePort outputPort) {
		if (outputPort instanceof BaseOutputPort) {
			outputPorts.add(outputPort);
			return true;
		}
		return false;
	}

	/**
	 * Removes a port from the node. This method uses
	 * {@link #removeInputPort(BasePort)} and {@link #removeOutputPort(BasePort)}
	 * internally.
	 * 
	 * @param port the port to remove
	 * @return true if successful, otherwise false
	 */
	public boolean removePort(BasePort port) {
		if (removeInputPort(port))
			return true;
		else if (removeOutputPort(port))
			return true;
		else
			return false;
	}

	/**
	 * Removes an input port from the node.
	 * 
	 * @param port the input port to remove
	 * @return true if successful, otherwise false
	 */
	public boolean removeInputPort(BasePort port) {
		return inputPorts.remove(port);
	}

	/**
	 * Removes an output port from this node.
	 * 
	 * @param port the port to remove
	 * @return true if successful, otherwise false
	 */
	public boolean removeOutputPort(BasePort port) {
		return outputPorts.remove(port);
	}

	/**
	 * Returns the index of the given input port.
	 * 
	 * @param port the port to get the index from
	 * @return the index or -1 if the port is not added to this node
	 */
	public int getInpnutPortIndex(BasePort port) {
		for (int i = 0; i < inputPorts.size(); i++) {
			if (inputPorts.get(i).equals(port))
				return i;
		}
		return -1;
	}

	/**
	 * Returns the index of the given output port.
	 * 
	 * @param port the port to get the index from
	 * @return the index or -1 if the port is not added to this node
	 */
	public int getOutputPortIndex(BasePort port) {
		for (int i = 0; i < outputPorts.size(); i++) {
			if (outputPorts.get(i).equals(port))
				return i;
		}
		return -1;
	}

	/**
	 * Returns the number of ports added as input ports
	 * 
	 * @return the number of ports
	 */
	public int getInputPortCount() {
		return inputPorts.size();
	}

	/**
	 * Returns the number of ports added as output ports
	 * 
	 * @return the number of ports
	 */
	public int getOutputPortCount() {
		return outputPorts.size();
	}

	/**
	 * Returns the port associated with the given index.
	 * 
	 * @param index the index of the port
	 * @return the port or null
	 * @see #getInpnutPortIndex(BasePort)
	 */
	public BasePort getInputPortByIndex(int index) {
		if (index > inputPorts.size() && index >= 0)
			return null;
		return inputPorts.get(index);
	}

	/**
	 * Returns the port associated with the given index.
	 * 
	 * @param index the index of the port
	 * @return the port or null
	 * @see #getOutputPortIndex(BasePort)
	 */
	public BasePort getOutputPortByIndex(int index) {
		if (index > outputPorts.size() && index >= 0)
			return null;
		return outputPorts.get(index);
	}

	/**
	 * Checks whether the given port is already added or not. This method uses
	 * {@link #isInputPortAdded(BasePort)} and {@link #isOutputPortAdded(BasePort)}
	 * internally.
	 * 
	 * @param port the port to check
	 * @return true if the port is already added, otherwise false
	 */
	public boolean isPortAdded(BasePort port) {
		if (isInputPortAdded(port))
			return true;
		else if (isOutputPortAdded(port))
			return true;
		else
			return false;
	}

	/**
	 * Checks whether the port is already an added input port.
	 * 
	 * @param port the port to check
	 * @return true if already added, otherwise false
	 */
	public boolean isInputPortAdded(BasePort port) {
		return inputPorts.contains(port);
	}

	/**
	 * Checks whether the port is already an added output port.
	 * 
	 * @param port the port to check
	 * @return true if already added, otherwise false
	 */
	public boolean isOutputPortAdded(BasePort port) {
		return outputPorts.contains(port);
	}

	/**
	 * Returns all input ports as {@link ObservableList}
	 * 
	 * @return the input ports of this node
	 */
	public ObservableList<BasePort> getInputPorts() {
		return inputPorts;
	}

	/**
	 * Returns all output ports as {@link ObservableList}
	 * 
	 * @return the output ports of this node
	 */
	public ObservableList<BasePort> getOutputPorts() {
		return outputPorts;
	}

	public Stream<BasePort> portStream() {
		return Stream.concat(inputPorts.stream(), outputPorts.stream());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((backendIdProperty == null) ? 0 : backendIdProperty.hashCode());
		result = prime * result + ((graphConnectorProperty == null) ? 0 : graphConnectorProperty.hashCode());
		result = prime * result + ((inputPorts == null) ? 0 : inputPorts.hashCode());
		result = prime * result + ((nameProperty == null) ? 0 : nameProperty.hashCode());
		result = prime * result + ((outputPorts == null) ? 0 : outputPorts.hashCode());
		result = prime * result + ((typeProperty == null) ? 0 : typeProperty.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseNode other = (BaseNode) obj;
		if (backendIdProperty == null) {
			if (other.backendIdProperty != null)
				return false;
		} else if (!backendIdProperty.get().equals(other.backendIdProperty.get()))
			return false;
		if (graphConnectorProperty == null) {
			if (other.graphConnectorProperty != null)
				return false;
		} else if (!graphConnectorProperty.get().equals(other.graphConnectorProperty.get()))
			return false;
		if (inputPorts == null) {
			if (other.inputPorts != null)
				return false;
		} else if (!inputPorts.equals(other.inputPorts))
			return false;
		if (nameProperty == null) {
			if (other.nameProperty != null)
				return false;
		} else if (!nameProperty.get().equals(other.nameProperty.get()))
			return false;
		if (outputPorts == null) {
			if (other.outputPorts != null)
				return false;
		} else if (!outputPorts.equals(other.outputPorts))
			return false;
		if (typeProperty == null) {
			if (other.typeProperty != null)
				return false;
		} else if (!typeProperty.get().equals(other.typeProperty.get()))
			return false;
		return true;
	}

}
