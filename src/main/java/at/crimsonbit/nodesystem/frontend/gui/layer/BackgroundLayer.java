/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.layer;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;

public class BackgroundLayer extends Layer {
	private Canvas grid = new Canvas();
	private GraphicsContext gc;

	private DoubleProperty scaleProperty = new SimpleDoubleProperty(1.0);
	private DoubleProperty translateX = new SimpleDoubleProperty(0.0);
	private DoubleProperty translateY = new SimpleDoubleProperty(0.0);

	public BackgroundLayer() {

		getChildren().add(grid);
		gc = grid.getGraphicsContext2D();

		draw();

		// That would be quite the "pfusch", but it would work
//		Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.01), e -> {
//			addGrid();
//		}));
//		timeline.setCycleCount(Timeline.INDEFINITE);
//		timeline.play();
	}

	@Override
	public void layoutChildren() {
		draw();
	}

	/**
	 * Add a grid to the canvas, send it to back
	 */
	public void draw() {
		getStyleClass().add("nodesystem-background");
		final int top = (int) ((int) snappedTopInset());
		// final int right = (int) ((int) snappedRightInset());
		// final int bottom = (int) ((int) snappedBottomInset());
		final int left = (int) ((int) snappedLeftInset());
		double width = (int) ((getWidth()));
		double height = (int) ((getHeight()));

		double spacing = 50 * (scaleProperty.get());
		double strokeValue = 1;
		Color lineColor = Color.rgb(40, 40, 40);
		
		//setBackground(new Background(new BackgroundFill(Color.rgb(48, 48, 48), CornerRadii.EMPTY, Insets.EMPTY)));

		grid.setLayoutX(left);
		grid.setLayoutY(top);
		// if (width != grid.getWidth() || height != grid.getHeight()) {
		final int hLineCount = (int) Math.floor((height + 1 - translateY.get()) / spacing);
		final int vLineCount = (int) Math.floor((width + 1 - translateX.get()) / spacing);

		grid.setWidth(width);
		grid.setHeight(height);
		gc.clearRect(0, 0, width, height);

		gc.setStroke(lineColor);
		gc.setLineWidth(strokeValue);

		for (int i = 0; i < hLineCount; i++) {

			gc.strokeLine(0, snap((i + 1) * spacing) + translateY.get(), width,
					snap((i + 1) * spacing) + translateY.get());

		}

		for (int i = 0; i < vLineCount; i++) {

			gc.strokeLine(snap((i + 1) * spacing) + translateX.get(), 0, snap((i + 1) * spacing) + translateX.get(),
					height);
		}
		// }

	}

	public void bindTranslation(DoubleProperty trX, DoubleProperty trY) {
		translateX.bind(trX);
		translateY.bind(trY);
	}

	public void bindScale(DoubleProperty prop) {
		scaleProperty.bind(prop);
	}

	private double snap(double y) {
		return ((int) y) + 0.5;
	}
}
