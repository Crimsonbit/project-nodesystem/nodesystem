/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.guilang;

import javafx.scene.paint.Color;

public class GLSignal {

	private Color color_default = Color.WHITE;
	private Color color_con_fail = Color.RED;
	private Color color_con_success = Color.GREEN;
	private double signal_width = 1D;
	private GLShadow signal_shadow = new GLShadow();

	public GLShadow getSignalShadow() {
		return signal_shadow;
	}

	public void setSignalShadow(GLShadow signal_shadow) {
		this.signal_shadow = signal_shadow;
	}

	public Color getColorDefault() {
		return color_default;
	}

	public void setColorDefault(Color color_default) {
		this.color_default = color_default;
	}

	public Color getColor_con_fail() {
		return color_con_fail;
	}

	public void setColor_con_fail(Color color_con_fail) {
		this.color_con_fail = color_con_fail;
	}

	public Color getColor_con_success() {
		return color_con_success;
	}

	public void setColor_con_success(Color color_con_success) {
		this.color_con_success = color_con_success;
	}

	public double getSignalWidth() {
		return signal_width;
	}

	public void setSignalWidth(double signal_width) {
		this.signal_width = signal_width;
	}

	public GLSignal(Color color_default, Color color_con_fail, Color color_con_success, double signal_width,
			GLShadow signal_shadow) {
		super();
		this.color_default = color_default;
		this.color_con_fail = color_con_fail;
		this.color_con_success = color_con_success;
		this.signal_width = signal_width;
		this.signal_shadow = signal_shadow;
	}

	public GLSignal() {
		super();
	}

}
