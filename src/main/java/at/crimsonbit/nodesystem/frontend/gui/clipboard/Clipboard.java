/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.clipboard;

import java.util.ArrayList;
import java.util.List;

import at.crimsonbit.nodesystem.frontend.gui.DrawGraph;
import at.crimsonbit.nodesystem.frontend.gui.drawable.DrawNode;

/**
 * <h1>Clipboard</h1> The clipboard object holds an {@link List} of
 * {@link DrawNode}s. This is used to copy and paste selected nodes over.
 * 
 * @author Florian Wagner
 * 
 * @see DrawGraph
 *
 */
public class Clipboard {

	private List<DrawNode> nodeClipboard = new ArrayList<>();

	/**
	 * Creates a new clipboard.
	 */
	public Clipboard() {

	}

	/**
	 * Copies the children of the given list to the clipboard's internal list
	 * 
	 * @param toCopy the {@link List} to copy
	 */
	public void copy(List<DrawNode> toCopy) {
		nodeClipboard.clear();
		for (DrawNode node : toCopy) {
			nodeClipboard.add(node);

		}
	}

	/**
	 * Returns the internal copy list
	 * 
	 * @return the list
	 */
	public List<DrawNode> paste() {
		return nodeClipboard;

	}

}
