/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.widget;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import at.crimsonbit.nodesystem.nodebackend.INodeType;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import javafx.event.EventHandler;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.util.Callback;

public class NodeTreeViewWidget extends TreeView<String> {
	private static TreeItem<String> rootNode = new TreeItem<String>("Available Nodes");
	
	public NodeTreeViewWidget(NodeGraph graph) {
		super(rootNode);
		Set<INodeType> map = graph.getMaster().getRegisteredNodes();
		Map<String, TreeItem<String>> cache = new HashMap<>();
		rootNode.getChildren().clear();
		for (INodeType type : map) {

			String name = type.getClass().getSimpleName();
			TreeItem<String> menu = cache.get(name);
			if (menu == null) {
				menu = new TreeItem<String>(name);
				cache.put(name, menu);
			}

			menu.getChildren().add(new TreeItem<String>(type.regName()));
		}
		for (TreeItem<String> menu : cache.values()) {
			rootNode.getChildren().add(menu);
		}

		
		setCellFactory(new Callback<TreeView<String>, TreeCell<String>>() {

			@Override
			public TreeCell<String> call(TreeView<String> stringTreeView) {
				TreeCell<String> treeCell = new TreeCell<String>() {
					protected void updateItem(String item, boolean empty) {
						super.updateItem(item, empty);
						if (!empty && item != null) {
							setText(item);
							setGraphic(getTreeItem().getGraphic());
						} else {
							setText(null);
							setGraphic(null);
						}
					}
				};

				treeCell.setOnDragDetected(new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent mouseEvent) {
						Dragboard db = treeCell.startDragAndDrop(TransferMode.ANY);

						/* put a string on dragboard */
						ClipboardContent content = new ClipboardContent();
						content.putString(treeCell.getText());
						db.setContent(content);
					}
				});

				return treeCell;
			}
		});
		setOnMouseEntered(event -> {

		});

		setOnMouseExited(event -> {

		});

	}

}
