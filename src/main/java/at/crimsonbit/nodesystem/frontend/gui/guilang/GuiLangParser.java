/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.guilang;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import at.crimsonbit.nodesystem.frontend.gui.drawable.DrawNode;
import at.crimsonbit.nodesystem.frontend.gui.drawable.DrawPort;

/**
 * <h1>GuiLangParser</h1> The {@link GuiLangParser} is responsible for loading
 * and saving the gui settings for the {@link DrawNode}s and {@link DrawPort}s
 * aswell as the corresponding signals. <br>
 * The GuiLang is saved in the JSON file format. This file uses Gson for its
 * serialization
 * 
 * @author Florian Wagner
 *
 */
public class GuiLangParser {
	private final static GsonBuilder gsonB = new GsonBuilder();
	private static final Gson gson;

	static {
		gsonB.setPrettyPrinting();
		gson = gsonB.create();
	}

	/**
	 * Writes the {@link GLContainer}, which represents the whole Json file, to the
	 * path given.
	 * 
	 * @param files the GLContainer to save
	 * @param path the path to save the container to
	 */
	public static void writeContainer(GLContainer files, String path) {

		String jsonOut = gson.toJson(files);
		Path p = null;
		p = new File(path).toPath();

		try {
			BufferedWriter writer = Files.newBufferedWriter(p, StandardCharsets.UTF_8);
			writer.append(jsonOut);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Reads a GLContainer with the given reader. If the reader is invalid, or no
	 * file could be read a default {@link GLContainer} will be created.
	 * 
	 * @param reader the reader to read the json file.
	 * @return the read GLContainer.
	 */
	public static GLContainer readContainer(Reader reader) {
		JsonReader jReader = new JsonReader(reader);
		GLContainer data = null;
		if (reader != null) {
			data = gson.fromJson(jReader, GLContainer.class);

		}

		if (data == null)
			data = new GLContainer();
		return data;
	}

	/**
	 * Reads a GLContainer with the from the given path. If the path is invalid, or
	 * no file could be read a default {@link GLContainer} will be created.
	 * 
	 * @param path the path where to read the GLContainer from
	 * @return the read GLContainer.
	 */
	public static GLContainer readContainer(String path) {
		JsonReader reader = null;
		try {
			reader = new JsonReader(new FileReader(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		GLContainer data = null;
		if (reader != null) {
			data = gson.fromJson(reader, GLContainer.class);

		}
		if (data == null)
			data = new GLContainer();
		return data;
	}
}
