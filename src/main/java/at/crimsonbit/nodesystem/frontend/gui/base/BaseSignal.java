/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.base;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * <h1>BaseSignal</h1>
 * 
 * <p>
 * The BaseSignal is the base class for all signals of a node.
 * </p>
 * 
 * @author Florian Wagner
 *
 * 
 */
public class BaseSignal {

	private ObjectProperty<BasePort> inputPortProperty = new SimpleObjectProperty<>();
	private ObjectProperty<BasePort> outputPortProperty = new SimpleObjectProperty<>();
	private BooleanProperty inputPortOnConnectionStartProperty = new SimpleBooleanProperty(true);

	/**
	 * Creates a new BaseSignal object.
	 * 
	 * @param port    the initial port where the signal is started from
	 * @param isInput whether this port is an input pot
	 */
	public BaseSignal(BasePort port, boolean isInput) {
		if (isInput)
			setInputPort(port);
		else
			setOutputPort(port);

		setInputPortOnConnectionStart(isInput);
	}

	/**
	 * Creates a new BaseSignal object.
	 * 
	 * @param inPort  input port
	 * @param outPort output port
	 */
	public BaseSignal(BasePort inPort, BasePort outPort) {
		setInputPort(inPort);
		setOutputPort(outPort);
	}

	/**
	 * Returns true if the connection (the signal) was started from an input port.
	 * 
	 * @return true if started from an input port
	 */
	public boolean isInputPortOnConnectionStart() {
		return inputPortOnConnectionStartProperty.get();
	}

	/**
	 * Sets the inputPortOnConnectionStartProperty value.
	 * 
	 * @param isInput the value
	 */
	private void setInputPortOnConnectionStart(boolean isInput) {
		inputPortOnConnectionStartProperty.set(isInput);
	}

	/**
	 * Sets the input port of the signal
	 * 
	 * @param port the input port to set
	 */
	public void setInputPort(BasePort port) {
		getInputPortProperty().set(port);
	}

	/**
	 * Sets the output port of the signal
	 * 
	 * @param port the output port to set
	 */
	public void setOutputPort(BasePort port) {
		getOutputPortProperty().set(port);
	}

	/**
	 * Returns the input port property
	 * 
	 * @return the property
	 */
	public ObjectProperty<BasePort> getInputPortProperty() {
		return inputPortProperty;
	}

	/**
	 * Sets the input port property.
	 * 
	 * @param inputPortProperty the property to set
	 */
	public void setInputPortProperty(ObjectProperty<BasePort> inputPortProperty) {
		this.inputPortProperty = inputPortProperty;
	}

	/**
	 * Returns the output port property
	 * 
	 * @return the property
	 */
	public ObjectProperty<BasePort> getOutputPortProperty() {
		return outputPortProperty;
	}

	/**
	 * Sets the output port property.
	 * 
	 * @param outputPortProperty the property to set
	 */
	public void setOutputPortProperty(ObjectProperty<BasePort> outputPortProperty) {
		this.outputPortProperty = outputPortProperty;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((inputPortOnConnectionStartProperty == null) ? 0 : inputPortOnConnectionStartProperty.hashCode());
		result = prime * result + ((inputPortProperty == null) ? 0 : inputPortProperty.hashCode());
		result = prime * result + ((outputPortProperty == null) ? 0 : outputPortProperty.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseSignal other = (BaseSignal) obj;
		if (inputPortOnConnectionStartProperty == null) {
			if (other.inputPortOnConnectionStartProperty != null)
				return false;
		} else if (!inputPortOnConnectionStartProperty.get() == (other.inputPortOnConnectionStartProperty.get()))
			return false;
		if (inputPortProperty == null) {
			if (other.inputPortProperty != null)
				return false;
		} else if (!inputPortProperty.get().equals(other.inputPortProperty.get()))
			return false;
		if (outputPortProperty == null) {
			if (other.outputPortProperty != null)
				return false;
		} else if (!outputPortProperty.get().equals(other.outputPortProperty.get()))
			return false;
		return true;
	}

}
