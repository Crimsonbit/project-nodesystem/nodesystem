/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.guilang;

import javafx.scene.paint.Color;

public class GLInput {

	private Color color = Color.rgb(102, 127, 204);
	private Color text_color = Color.WHITE;
	private double offset_x = 0d;
	private double offset_y = 0d;
	private double size = 5d;

	public GLInput() {
		super();
	}

	public GLInput(Color color, Color text_color, double offset_x, double offset_y, double size) {
		super();
		this.color = color;
		this.text_color = text_color;
		this.offset_x = offset_x;
		this.offset_y = offset_y;
		this.size = size;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Color getTextColor() {
		return text_color;
	}

	public void setTextColor(Color text_color) {
		this.text_color = text_color;
	}

	public double getOffsetX() {
		return offset_x;
	}

	public void setOffsetX(double offset_x) {
		this.offset_x = offset_x;
	}

	public double getOffsetY() {
		return offset_y;
	}

	public void setOffsetY(double offset_y) {
		this.offset_y = offset_y;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

}
