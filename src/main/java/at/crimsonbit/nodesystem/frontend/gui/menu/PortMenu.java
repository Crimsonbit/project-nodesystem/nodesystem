/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.menu;

import at.crimsonbit.nodesystem.frontend.gui.drawable.DrawPort;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;

public class PortMenu extends ContextMenu {

	private DrawPort port;

	public PortMenu(DrawPort port) {
		this.port = port;
		port.setOnContextMenuRequested(event -> {
			show(port, event.getScreenX(), event.getScreenY());
		});
	}

	public void addSeparator() {
		SeparatorMenuItem separator = new SeparatorMenuItem();
		getItems().add(separator);
	}

	public void addItem(String name, EventHandler<ActionEvent> event) {
		MenuItem item = new MenuItem(name);
		item.setOnAction(event);
		getItems().add(item);
	}

	public DrawPort getPort() {
		return port;
	}

}
