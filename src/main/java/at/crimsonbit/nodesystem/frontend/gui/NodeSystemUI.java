/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import at.crimsonbit.nodesystem.GraphConfig;
import at.crimsonbit.nodesystem.event.GraphChangedEvent;
import at.crimsonbit.nodesystem.event.GraphToolChangedEvent;
import at.crimsonbit.nodesystem.event.NodeGraphCreatedEvent;
import at.crimsonbit.nodesystem.event.PluginsFinishedLoadingEvent;
import at.crimsonbit.nodesystem.event.PluginsFinishedUnloadingEvent;
import at.crimsonbit.nodesystem.event.WidgetMenuLineShowEvent;
import at.crimsonbit.nodesystem.event.WidgetOpenEvent;
import at.crimsonbit.nodesystem.event.WidgetQuickMenuPositionEvent;
import at.crimsonbit.nodesystem.frontend.gui.component.GraphTab;
import at.crimsonbit.nodesystem.frontend.gui.component.ImageButton;
import at.crimsonbit.nodesystem.frontend.gui.dialog.Entry;
import at.crimsonbit.nodesystem.frontend.gui.dialog.PluginEntry;
import at.crimsonbit.nodesystem.frontend.gui.dialog.SubDialog;
import at.crimsonbit.nodesystem.frontend.gui.widget.ConsoleWidget;
import at.crimsonbit.nodesystem.frontend.gui.widget.QuickMenu;
import at.crimsonbit.nodesystem.nodebackend.INodeType;
import at.crimsonbit.nodesystem.plugin.dataType.CommandData;
import at.crimsonbit.nodesystem.plugin.dataType.MessageData;
import at.crimsonbit.nodesystem.plugin.dataType.ToolData;
import at.crimsonbit.nodesystem.plugin.dataType.WidgetData;
import at.crimsonbit.nodesystem.plugin.impl.IBasePlugin;
import at.crimsonbit.nodesystem.plugin.impl.PluginMaster;
import javafx.scene.control.MenuBar;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

/**
 * 
 * @author Florian Wagner
 *
 */
public class NodeSystemUI extends AnchorPane {

	private List<DrawGraph> drawGraphs;
	private DrawGraph currentGraph;

	private QuickMenu quickMenu;
	private MenuBar menuBar = new MenuBar();
	private ToolBar toolBar = new ToolBar();
	private TabPane nodeGraphPanes = new TabPane();
	private ConsoleWidget consoleWidget;

	// private ShortcutTable shortcutTable;
	private final EventBus eventBus;
	private int subMenuCount = 0;
	private int idCount = 0;
	private SubDialog pluginsMenu = new SubDialog(0, "Plugins");
	private SubDialog nodeMenu = new SubDialog(0, "Add");
	private SubDialog toolMenu = new SubDialog(2, "Tools");
	private SubDialog widgetMenu = new SubDialog(3, "Widgets");
	private SubDialog windowMenu = new SubDialog(4, "Window");

	private SplitPane splitPane = new SplitPane();
	private Map<String, SubDialog> dialogMap = new TreeMap<String, SubDialog>();
	private final double IMAGE_SIZE = 16D;
	private final Image nodeImage = new Image(getClass().getResourceAsStream("/icons/nodes.png"));
	private Theme theme;

	private ToolData currentTool;

	public NodeSystemUI(DrawGraph graph, QuickMenu menu, EventBus bus, Theme th) {
		this.drawGraphs = new ArrayList<DrawGraph>();
		this.drawGraphs.add(graph);
		this.quickMenu = menu;
		this.eventBus = bus;
		this.theme = th;
		consoleWidget = new ConsoleWidget(eventBus);		
		// shortcutTable = ShortcutIO.readShortcutTable();
		populateNodesMenu();
		attachItems(true);
		
		if (theme.equals(Theme.LIGHT)) {
			getStylesheets().add(getClass().getResource("systemUILight.css").toExternalForm());
			consoleWidget.getConsoleWindow().getStylesheets().add(getClass().getResource("systemUILight.css").toExternalForm());
		}else {
			getStylesheets().add(getClass().getResource("systemUIDark.css").toExternalForm());
			consoleWidget.getConsoleWindow().getStylesheets().add(getClass().getResource("systemUIDark.css").toExternalForm());
		}
		this.eventBus.register(this);
		nodeGraphPanes.getStyleClass().add("quickmenu-tabpane");
		toolBar.getStyleClass().add("nodesystem-toolbar");
		menuBar.getStyleClass().add("nodesystem-menubar");
		nodeGraphPanes.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		nodeGraphPanes.getSelectionModel().selectedItemProperty().addListener((obs, ov, nv) -> {
			if (nv instanceof GraphTab) {
				currentGraph = ((GraphTab) nv).getGraph();
				if (currentGraph.getTool() != null)
					currentGraph.getTool().initTool(currentGraph);
				eventBus.post(new GraphChangedEvent(currentGraph));
			}
		});
		
		
	}

	/**
	 * Called whenever the user selects a new {@link ToolData} from the menu bar.
	 */
	@Subscribe
	private void onToolChanged(GraphToolChangedEvent event) {
		setTool(event.tool);
	}

	/**
	 * Sets the given tool as active tool in the nodesystem
	 * 
	 * @param tool the tool to set active
	 */
	public void setTool(ToolData tool) {
		if (currentTool != null) {
			currentTool.freeTool();
			eventBus.unregister(currentTool);
		}
		currentTool = tool;
		currentTool.initTool(currentGraph);
		eventBus.register(currentTool);
	}

	public SplitPane getSplitPane() {
		return splitPane;
	}

	public MenuBar getMenuBar() {
		return menuBar;
	}

	// public ShortcutTable getShortcutTable() {
	// return shortcutTable;
	// }

	/**
	 * Loads and fills the menus. The shortcuts are loaded here aswell!
	 */
	private void populateNodesMenu() {
		nodeMenu.getItems().clear();
		Set<INodeType> map = this.drawGraphs.get(0).getNodeGraph().getMaster().getRegisteredNodes();
		Map<String, SubDialog> cache = new HashMap<>();
		for (INodeType type : map) {

			String name = type.getClass().getSimpleName();
			SubDialog menu = cache.get(name);
			if (menu == null) {
				menu = new SubDialog(subMenuCount++, name);
				cache.put(name, menu);
			}
			Entry ent = new Entry(idCount++, type.regName(), type.name(), false);
			ent.setOnAction(event -> {
				if (currentGraph != null)
					currentGraph.addNodeFromEntry(ent);
				event.consume();
			});
			ImageView view = new ImageView(nodeImage);
			view.setFitHeight(IMAGE_SIZE);
			view.setFitWidth(IMAGE_SIZE);
			ent.setGraphic(view);
			menu.addItem(ent);
		}
		for (SubDialog menu : cache.values()) {
			nodeMenu.addMenu(menu);
		}
		menuBar.prefWidthProperty().bind(widthProperty());

	}

	@Subscribe
	private void onGraphCreated(NodeGraphCreatedEvent ev) {

		DrawGraph graph = new DrawGraph(ev.graph, eventBus, new GraphConfig());
		addGraphTab(graph);
	}

	@Subscribe
	private void QuickMenuPositionEvent(WidgetQuickMenuPositionEvent e) {
		splitPane.setDividerPosition(0, e.position.doubleValue());
	}

	public void populateMenus(Collection<IBasePlugin> plugins) {
		toolMenu.getItems().clear();
		widgetMenu.getItems().clear();
		pluginsMenu.getItems().clear();
		toolBar.getItems().clear();
		menuBar.getMenus().clear();
		windowMenu.getItems().clear();
		for (SubDialog dia : dialogMap.values()) {
			dia.getItems().clear();
		}
		dialogMap.clear();
		populateNodesMenu();

		for (IBasePlugin plugin : plugins) {
			if (plugin instanceof ToolData) {
				PluginEntry entry = new PluginEntry(plugin.getPluginID(), plugin.getName());
				toolMenu.addItem(entry);
				ImageButton btn = null;
				if (((ToolData) plugin).getShortcut() != null)
					entry.setAccelerator(((ToolData) plugin).getShortcut());

				entry.setOnAction(ev -> {
					eventBus.post(
							new GraphToolChangedEvent((ToolData) PluginMaster.get().findPlugin(plugin.getPluginID())));
				});
				if (plugin.getIcon() != null) {
					ImageView view = new ImageView(plugin.getIcon());
					view.setFitHeight(IMAGE_SIZE);
					view.setFitWidth(IMAGE_SIZE);
					entry.setGraphic(view);
					btn = new ImageButton(plugin.getIcon());
					toolBar.getItems().add(btn);
					btn.setOnAction(ev -> {
						eventBus.post(new GraphToolChangedEvent(
								(ToolData) PluginMaster.get().findPlugin(plugin.getPluginID())));
					});
				} else {
					btn = new ImageButton(plugin.getName());

					if (toolBar.getItems().size() < 15)
						toolBar.getItems().add(btn);
					btn.setOnAction(ev -> {
						eventBus.post(new GraphToolChangedEvent(
								(ToolData) PluginMaster.get().findPlugin(plugin.getPluginID())));
					});
				}
				if (btn != null)
					if (((ToolData) plugin).getToolTip() != null) {
						if (currentGraph != null) {
							currentGraph.postMessage(((ToolData) plugin).getToolTip());
							btn.setOnMouseEntered(ev -> {

								currentGraph.postMessage(((ToolData) plugin).getToolTip());
							});
						}
						Tooltip tp = new Tooltip(((ToolData) plugin).getToolTip());
						btn.setTooltip(tp);
					}
			} else if (plugin instanceof WidgetData) {
				PluginEntry entry = new PluginEntry(plugin.getPluginID(), plugin.getName());
				widgetMenu.addItem(entry);
				if (((WidgetData) plugin).getShortcut() != null)
					entry.setAccelerator(((WidgetData) plugin).getShortcut());
				entry.setOnAction(ev -> {

					eventBus.post(
							new WidgetOpenEvent((WidgetData) PluginMaster.get().findPlugin(plugin.getPluginID())));
				});
				if (plugin.getIcon() != null) {
					ImageView view = new ImageView(plugin.getIcon());
					view.setFitHeight(IMAGE_SIZE);
					view.setFitWidth(IMAGE_SIZE);
					entry.setGraphic(view);
				}
			} else if (plugin instanceof MessageData) {
				MessageData msg = (MessageData) plugin;
				eventBus.register(msg);

			} else if (plugin instanceof CommandData) {
				CommandData data = (CommandData) plugin;
				PluginEntry entry = new PluginEntry(plugin.getPluginID(), plugin.getName());
				entry.setOnAction(ev -> {
					if (currentGraph != null)
						data.execute(currentGraph);
				});
				if (plugin.getIcon() != null) {
					ImageView view = new ImageView(plugin.getIcon());
					view.setFitHeight(IMAGE_SIZE);
					view.setFitWidth(IMAGE_SIZE);
					entry.setGraphic(view);
				}
				if (!data.getMenu().equals("Plugins"))
					if (dialogMap.get(data.getMenu()) == null) {
						dialogMap.put(data.getMenu(), new SubDialog(0, data.getMenu()));
						dialogMap.get(data.getMenu()).addItem(entry);
					} else {
						dialogMap.get(data.getMenu()).addItem(entry);
					}
				else {
					pluginsMenu.getItems().add(entry);
				}
				if (data.getShortcut() != null)
					entry.setAccelerator(data.getShortcut());

			}
		}

		List<SubDialog> list = new ArrayList<>(dialogMap.values());
		Collections.reverse(list);

		for (SubDialog dialog : list) {
			menuBar.getMenus().add(dialog);
		}

		menuBar.getMenus().add(nodeMenu);

		if (toolMenu.getItems().size() > 0)
			pluginsMenu.getItems().add(toolMenu);

		if (widgetMenu.getItems().size() > 0)
			pluginsMenu.getItems().add(widgetMenu);
		if (pluginsMenu.getItems().size() > 0)
			menuBar.getMenus().add(pluginsMenu);

		setTopAnchor(toolBar, 20D);
		eventBus.post(new GraphToolChangedEvent((ToolData) PluginMaster.get().findPlugin("tool:id:move")));
		Entry ConsoleEnt = new Entry(0, "Console");
		ConsoleEnt.setOnAction(ev -> {
			consoleWidget.open();
		});
		windowMenu.getItems().add(ConsoleEnt);
		menuBar.getMenus().add(windowMenu);
	}

	@Subscribe
	private void onPluginsUnloaded(PluginsFinishedUnloadingEvent event) {
		if (this.getScene() != null)
			if (this.getScene().getWindow().isShowing())
				populateMenus(event.plugins);
	}

	@Subscribe
	private void onPluginsLoaded(PluginsFinishedLoadingEvent event) {
		populateMenus(event.plugins);
	}

	private void attachItems(boolean enable) {

		if (enable) {
			splitPane.getItems().add(nodeGraphPanes);
			splitPane.getItems().add(quickMenu);
			splitPane.setDividerPosition(0, 0.7D);

			getChildren().add(splitPane);
			setLeftAnchor(splitPane, 0D);
			setRightAnchor(splitPane, 0D);

			setBottomAnchor(splitPane, 0D);
			setLeftAnchor(toolBar, 0D);
			setRightAnchor(toolBar, 0D);
			setTopAnchor(quickMenu, 0D);
			if (!getChildren().contains(toolBar))
				getChildren().add(toolBar);
			if (!getChildren().contains(menuBar))
				getChildren().add(menuBar);

		} else {
			getChildren().add(nodeGraphPanes);
			setLeftAnchor(nodeGraphPanes, 0D);
			setRightAnchor(nodeGraphPanes, 0D);
			setTopAnchor(nodeGraphPanes, 0D);
			setBottomAnchor(nodeGraphPanes, 0D);
			if (!getChildren().contains(toolBar))
				getChildren().add(toolBar);
			if (!getChildren().contains(menuBar))
				getChildren().add(menuBar);
		}

		// System.out.println(menuBar.getHeight() + toolBar.getHeight());
		setTopAnchor(splitPane, 52D);
		addGraphTab(this.drawGraphs.get(0));

	}

	public void addGraphTab(DrawGraph graph) {
		GraphTab graphTab = new GraphTab(graph.nameProperty().get(), graph);
		graphTab.getStyleClass().add("quickmenu-tab");
		graphTab.textProperty().bind(graph.nameProperty());
		graphTab.setContent(graph.getBaseLayer());
		nodeGraphPanes.getTabs().add(graphTab);
		this.drawGraphs.add(graph);
		currentGraph = graph;
		if (PluginMaster.get().findPlugin("tool:id:move") != null)
			currentGraph.setTool((ToolData) PluginMaster.get().findPlugin("tool:id:move"));
	}

	@Subscribe
	private void ShowMenuLineEvent(WidgetMenuLineShowEvent e) {
		if (e.show) {
			if (!getChildren().contains(menuBar)) {
				getChildren().add(menuBar);
			}
		} else {
			if (getChildren().contains(menuBar)) {
				getChildren().remove(menuBar);
			}
		}

	}

	public QuickMenu getQuickMenu() {
		return quickMenu;
	}

	public void setQuickMenu(QuickMenu quickMenu) {
		this.quickMenu = quickMenu;
	}

	public DrawGraph getRootGraph() {
		return this.drawGraphs.get(0);
	}

}
