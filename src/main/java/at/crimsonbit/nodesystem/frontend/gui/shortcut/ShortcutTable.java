/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.shortcut;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
@Deprecated
public class ShortcutTable {

	private Shortcut selectAll = new Shortcut(new KeyCodeCombination(KeyCode.A, KeyCombination.CONTROL_DOWN));
	private Shortcut deselectAll = new Shortcut(new KeyCodeCombination(KeyCode.D, KeyCombination.CONTROL_DOWN));
	private Shortcut copy = new Shortcut(new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN));
	private Shortcut paste = new Shortcut(new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_DOWN));
	private Shortcut remove = new Shortcut(new KeyCodeCombination(KeyCode.DELETE));
	private Shortcut executeGraph = new Shortcut(new KeyCodeCombination(KeyCode.X, KeyCombination.CONTROL_DOWN));
	private Shortcut executeNode = new Shortcut(new KeyCodeCombination(KeyCode.F, KeyCombination.CONTROL_DOWN));
	private Shortcut openSearchBar = new Shortcut(new KeyCodeCombination(KeyCode.SPACE, KeyCombination.CONTROL_DOWN));
	private Shortcut moveToOrigin = new Shortcut(new KeyCodeCombination(KeyCode.SPACE));
	private Shortcut saveGraph = new Shortcut(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN));
	private Shortcut loadGraph = new Shortcut(new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN));
	private Shortcut newGraph = new Shortcut(new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN));
	private Shortcut mergeGraph = new Shortcut(new KeyCodeCombination(KeyCode.M, KeyCombination.CONTROL_DOWN));

	public ShortcutTable() {

	}

	public ShortcutTable(Shortcut selectAll, Shortcut deselectAll, Shortcut copy, Shortcut paste, Shortcut remove,
			Shortcut executeGraph, Shortcut executeNode, Shortcut openSearchBar, Shortcut moveToOrigin,
			Shortcut saveGraph, Shortcut loadGraph, Shortcut newGraph, Shortcut mergeGraph) {
		super();
		this.selectAll = selectAll;
		this.deselectAll = deselectAll;
		this.copy = copy;
		this.paste = paste;
		this.remove = remove;
		this.executeGraph = executeGraph;
		this.executeNode = executeNode;
		this.openSearchBar = openSearchBar;
		this.moveToOrigin = moveToOrigin;
		this.saveGraph = saveGraph;
		this.loadGraph = loadGraph;
		this.newGraph = newGraph;
		this.mergeGraph = mergeGraph;
	}

	public Shortcut getMergeGraph() {
		return mergeGraph;
	}

	public void setMergeGraph(Shortcut mergeGraph) {
		this.mergeGraph = mergeGraph;
	}

	public Shortcut getSaveGraph() {
		return saveGraph;
	}

	public void setSaveGraph(Shortcut saveGraph) {
		this.saveGraph = saveGraph;
	}

	public Shortcut getLoadGraph() {
		return loadGraph;
	}

	public void setLoadGraph(Shortcut loadGraph) {
		this.loadGraph = loadGraph;
	}

	public Shortcut getNewGraph() {
		return newGraph;
	}

	public void setNewGraph(Shortcut newGraph) {
		this.newGraph = newGraph;
	}

	public Shortcut getSelectAll() {
		return selectAll;
	}

	public void setSelectAll(Shortcut selectAll) {
		this.selectAll = selectAll;
	}

	public Shortcut getDeselectAll() {
		return deselectAll;
	}

	public void setDeselectAll(Shortcut deselectAll) {
		this.deselectAll = deselectAll;
	}

	public Shortcut getCopy() {
		return copy;
	}

	public void setCopy(Shortcut copy) {
		this.copy = copy;
	}

	public Shortcut getPaste() {
		return paste;
	}

	public void setPaste(Shortcut paste) {
		this.paste = paste;
	}

	public Shortcut getRemove() {
		return remove;
	}

	public void setRemove(Shortcut remove) {
		this.remove = remove;
	}

	public Shortcut getExecuteGraph() {
		return executeGraph;
	}

	public void setExecuteGraph(Shortcut executeGraph) {
		this.executeGraph = executeGraph;
	}

	public Shortcut getExecuteNode() {
		return executeNode;
	}

	public void setExecuteNode(Shortcut executeNode) {
		this.executeNode = executeNode;
	}

	public Shortcut getOpenSearchBar() {
		return openSearchBar;
	}

	public void setOpenSearchBar(Shortcut openSearchBar) {
		this.openSearchBar = openSearchBar;
	}

	public Shortcut getMoveToOrigin() {
		return moveToOrigin;
	}

	public void setMoveToOrigin(Shortcut moveToOrigin) {
		this.moveToOrigin = moveToOrigin;
	}

	/*
	 * private Map<String, List<KeyCode>> shortcuts = new HashMap<>();
	 * 
	 * public ShortcutTable() { }
	 * 
	 * public Map<String, List<KeyCode>> getShortcutTable() { return shortcuts; }
	 * 
	 * public void addShortcut(String shortcut, List<KeyCode> keys) {
	 * shortcuts.put(shortcut, keys); }
	 * 
	 * public List<KeyCode> getKeys(String shortcut) { return
	 * shortcuts.get(shortcut); }
	 * 
	 * public void addShortcut(String shortcut, KeyCode code) {
	 * shortcuts.putIfAbsent(shortcut, new ArrayList<KeyCode>());
	 * shortcuts.get(shortcut).add(code); }
	 * 
	 * public Iterator getIterator() { return shortcuts.entrySet().iterator(); }
	 * 
	 * public void populateDefault() { List<KeyCode> key = new ArrayList<>();
	 * 
	 * // SAVE key.add(KeyCode.CONTROL); key.add(KeyCode.S); shortcuts.put("save",
	 * key);
	 * 
	 * // LOAD key = new ArrayList<>(); key.add(KeyCode.CONTROL);
	 * key.add(KeyCode.L); shortcuts.put("load", key);
	 * 
	 * // MOVE TO DEFAULT LOCATION key = new ArrayList<>(); key.add(KeyCode.SPACE);
	 * shortcuts.put("move_to_default_location", key);
	 * 
	 * // SEARCH BAR key = new ArrayList<>(); key.add(KeyCode.CONTROL);
	 * key.add(KeyCode.SPACE); shortcuts.put("open_search_bar", key);
	 * 
	 * // COPY key = new ArrayList<>(); key.add(KeyCode.CONTROL);
	 * key.add(KeyCode.C); shortcuts.put("copy", key);
	 * 
	 * // PASTE key = new ArrayList<>(); key.add(KeyCode.CONTROL);
	 * key.add(KeyCode.V); shortcuts.put("paste", key);
	 * 
	 * // REMOVE NODE key = new ArrayList<>(); key.add(KeyCode.DELETE);
	 * shortcuts.put("remove_node", key);
	 * 
	 * }
	 */
}
