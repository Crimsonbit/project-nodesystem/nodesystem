package at.crimsonbit.nodesystem.frontend.gui.component;

import java.awt.image.BufferedImage;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * <h1>ImageButton</h1> The {@link ImageButton} is a normal button but instead
 * of text it displays an {@link Image}. This Object is mainly used for the
 * toolbar of the nodesystem.
 * 
 * @author Florian Wagner
 *
 */
public class ImageButton extends Button {

	private ImageView imageView;
	private final double IMAGE_SIZE = 20D;

	public ImageButton(BufferedImage image) {
		if (image != null) {
			imageView = new ImageView(SwingFXUtils.toFXImage(image, null));
			imageView.setFitHeight(IMAGE_SIZE);
			imageView.setFitWidth(IMAGE_SIZE);
			setGraphic(imageView);
			getStyleClass().add("nodesystem-image-button");
		}
	}

	public ImageButton(Image image) {
		if (image != null) {
			imageView = new ImageView(image);
			imageView.setFitHeight(IMAGE_SIZE);
			imageView.setFitWidth(IMAGE_SIZE);
			setGraphic(imageView);
			getStyleClass().add("nodesystem-image-button");
		}
	}

	public ImageButton(ImageView view) {
		if (view != null) {
			imageView = view;
			imageView.setFitHeight(IMAGE_SIZE);
			imageView.setFitWidth(IMAGE_SIZE);
			setGraphic(imageView);
			getStyleClass().add("nodesystem-image-button");
		}
	}

	public ImageButton(String txt) {
		super(txt);
		getStyleClass().add("nodesystem-image-button");
	}

}
