package at.crimsonbit.nodesystem.frontend.gui.component;

import at.crimsonbit.nodesystem.util.Util;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Slider;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;

public class ColorSlider extends StackPane {
	private Slider slider;
	private Rectangle progressRec;

	public ColorSlider() {
		slider = new Slider();
		slider.getStyleClass().add("nodesystem-slider");
		// progressRec.setStyle("-fx-fill: -fx-dark");
		progressRec = new Rectangle();
		progressRec.heightProperty().bind(prefHeightProperty());
		slider.prefHeightProperty().bind(prefHeightProperty());
		
		//progressRec.widthProperty().bind(prefWidthProperty());
		//slider.prefWidthProperty().bind(prefWidthProperty());
		// slider.setShowTickLabels(true);
		// slider.setShowTickMarks(true);

		slider.valueProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
				int value = (int) Util.map(new_val.intValue(), slider.getMin(), slider.getMax(), 0, 100);
				String style = String.format("-fx-fill: linear-gradient(to right, -fx-base %d%%, -fx-dark %d%%);",
						value, value);
				progressRec.setStyle(style);
			}
		});
		slider.setValue(0.0);
		getChildren().addAll(progressRec, slider);
	}

	public Rectangle getRectangle() {
		return progressRec;
	}

	public Slider getSlider() {
		return slider;
	}

}
