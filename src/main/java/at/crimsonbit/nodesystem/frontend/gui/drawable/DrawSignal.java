/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.drawable;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import at.crimsonbit.nodesystem.event.NodeRemoveSignalEvent;
import at.crimsonbit.nodesystem.event.SignalOpenNodeMenuEvent;
import at.crimsonbit.nodesystem.event.SignalResetColorEvent;
import at.crimsonbit.nodesystem.event.SignalTryConnectionEven;
import at.crimsonbit.nodesystem.frontend.gui.base.BaseSignal;
import at.crimsonbit.nodesystem.frontend.gui.guilang.GLContainer;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.StrokeLineCap;
import javafx.util.Duration;

/**
 * <h1>DrawSignal</h1> Is the graphical representation of a signal connection
 * between node ports.
 * 
 * @author Florian Wagner
 *
 * @see BaseSignal
 * @See {@link GLContainer}
 */
public class DrawSignal extends CubicCurve implements IDrawable {

	private BaseSignal signal;
	private double startX = 0;
	private double startY = 0;
	private GLContainer settings;
	private  EventBus bus;
	private Timeline timeline = new Timeline();
	private final long ANIMATION_DURATION = 10;
	private final long ANIMATION_FADE_DURATION = 500;

	private ObjectProperty<Paint> outColor = new SimpleObjectProperty<Paint>();
	private BooleanProperty selectedProperty = new SimpleBooleanProperty(false);
	private BooleanProperty snappedProperty = new SimpleBooleanProperty(false);
	private BooleanProperty muteProperty = new SimpleBooleanProperty(false);

	/**
	 * Creates a new DrawSignal object.
	 * 
	 * @param signal the base signal
	 * @param set    the settings container
	 * @param bus    the event bus to use. The {@link NodeGraph#getEventBus()}
	 *               should be used
	 */
	public DrawSignal(BaseSignal signal, ObjectProperty<Paint> outColor, GLContainer set, EventBus bus) {
		this.signal = signal;
		this.settings = set;
		this.bus = bus;
		this.outColor.bind(outColor);

		createLayout();
		setOnContextMenuRequested(event -> {
			bus.post(new SignalOpenNodeMenuEvent(signal, getBoundsInLocal(), event));
		});
		initAnimation();
	}

	/**
	 * Part of the event system. Called for every {@link SignalResetColorEvent}.
	 * Called when a node connection exits a port to reset the color of the signal
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void resetSignalColor(SignalResetColorEvent e) {
		timeline.stop();
		timeline.getKeyFrames().clear();
		final KeyValue kv = new KeyValue(strokeProperty(), e.color, Interpolator.EASE_BOTH);
		final KeyFrame kf = new KeyFrame(Duration.millis(ANIMATION_DURATION), kv);

		timeline.getKeyFrames().addAll(kf);
		timeline.play();
		getStrokeDashArray().clear();
	}

	/**
	 * Part of the event system. Called for every {@link SignalTryConnectionEven}.
	 * Called when a node connection enters a port to change the color of the signal
	 * accordingly
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void trySignalColor(SignalTryConnectionEven e) {
		if (e.wouldWork) {
			timeline.stop();
			timeline.getKeyFrames().clear();
			final KeyValue kv = new KeyValue(strokeProperty(), e.succ, Interpolator.EASE_BOTH);
			final KeyFrame kf = new KeyFrame(Duration.millis(ANIMATION_DURATION), kv);
			timeline.getKeyFrames().addAll(kf);
			timeline.play();
			getStrokeDashArray().clear();
		} else {
			timeline.stop();
			timeline.getKeyFrames().clear();
			final KeyValue kv = new KeyValue(strokeProperty(), e.fail, Interpolator.EASE_BOTH);
			final KeyFrame kf = new KeyFrame(Duration.millis(ANIMATION_DURATION), kv);
			timeline.getKeyFrames().addAll(kf);
			timeline.play();
			getStrokeDashArray().add(10.0);
		}
	}

	private void initAnimation() {
		setOnMouseEntered(event -> {
			selectedProperty.set(true);
			if (event.isControlDown()) {
				bus.post(new NodeRemoveSignalEvent(this));
			}

		});

		setOnMouseExited(event -> {
			selectedProperty.set(false);

		});

		setOnMouseDragEntered(event -> {

		});

		muteProperty.addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (newValue == false) {
					getStrokeDashArray().clear();
				} else {
					getStrokeDashArray().add(10.0);
				}
			}
		});

		selectedProperty.addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (newValue == false) {
					timeline.stop();
					timeline.getKeyFrames().clear();

					KeyValue kv = new KeyValue(strokeProperty(), settings.getSignal().getColorDefault(),
							Interpolator.EASE_BOTH);
					KeyFrame kf = new KeyFrame(Duration.millis(ANIMATION_FADE_DURATION), kv);

					KeyValue kv2 = new KeyValue(strokeWidthProperty(), settings.getSignal().getSignalWidth(),
							Interpolator.EASE_BOTH);
					KeyFrame kf2 = new KeyFrame(Duration.millis(ANIMATION_FADE_DURATION), kv2);

					timeline.getKeyFrames().addAll(kf, kf2);
					timeline.play();
				} else {
					timeline.stop();
//					timeline.getKeyFrames().clear();
//					final KeyValue kv = new KeyValue(strokeProperty(), outColor, Interpolator.EASE_BOTH);
//					final KeyFrame kf = new KeyFrame(Duration.millis(ANIMATION_DURATION), kv);
//					final KeyValue kv2 = new KeyValue(strokeWidthProperty(), settings.getSignal().getSignalWidth() + 2D,
//							Interpolator.EASE_BOTH);
//					final KeyFrame kf2 = new KeyFrame(Duration.millis(ANIMATION_DURATION), kv2);
//					timeline.getKeyFrames().addAll(kf, kf2);
//					timeline.play();
					strokeProperty().set(outColor.get());
					strokeWidthProperty().set(settings.getSignal().getSignalWidth() + 2D);
				}
			}
		});

	}

	public BooleanProperty selectedProperty() {
		return selectedProperty;
	}

	/**
	 * Creates a new DrawSignal object.
	 * 
	 * @param signal the base signal
	 * @param set    the settigns container
	 * @param x      the start x coordinate of this signal
	 * @param y      the start y coordinate of this signal
	 * @param bus    the event bus to use. The {@link NodeGraph#getEventBus()}
	 *               should be used
	 */
	public DrawSignal(BaseSignal signal, GLContainer set, double x, double y, EventBus bus) {
		this.signal = signal;
		this.startX = x;
		this.startY = y;
		this.settings = set;
		this.bus = bus;
		bus.register(this);
		createLayout();
		initAnimation();
	}
	
	/**
	 * Creates a new DrawSignal object.
	 * 
	 * @param signal the base signal
	 * @param set    the settigns container
	 * @param x      the start x coordinate of this signal
	 * @param y      the start y coordinate of this signal
	 */
	public DrawSignal(BaseSignal signal, GLContainer set, double x, double y) {
		this.signal = signal;
		this.startX = x;
		this.startY = y;
		this.settings = set;
		createLayout();
		initAnimation();
	}
	
	/**
	 * creates the layout of this signal.
	 */
	@Override
	public void createLayout() {
		startXProperty().set(this.startX);
		startYProperty().set(this.startY);
		setStroke(settings.getSignal().getColorDefault());
		setStrokeWidth(settings.getSignal().getSignalWidth());
		setStrokeLineCap(StrokeLineCap.ROUND);
		setFill(Color.TRANSPARENT);

	}

	/**
	 * Returns the {@link BaseSignal} object of this signal.
	 * 
	 * @return the base signal
	 */
	public BaseSignal getSignal() {
		return signal;
	}

	/**
	 * Returns the settings container of this signal.
	 * 
	 * @return the settings contaienr
	 */
	public GLContainer getSettings() {

		return settings;
	}

	/**
	 * Returns the snapped property of this signal. Used for the temp signal while
	 * connecting nodes
	 * 
	 * @return boolean property
	 */
	public BooleanProperty snappedProperty() {
		return snappedProperty;
	}

	/**
	 * Returns the mute property of this signal. It is used to mute, but not delete,
	 * a signal connection.
	 * 
	 * @return boolean property
	 */
	public BooleanProperty muteProperty() {
		return muteProperty;
	}

	/**
	 * Toggles the mute property of the signal.
	 */
	public void toggleMute() {
		muteProperty().set(!muteProperty().get());

	}

}
