/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package at.crimsonbit.nodesystem.frontend.gui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import at.crimsonbit.nodesystem.GraphConfig;
import at.crimsonbit.nodesystem.event.GlobalInputAddedEvent;
import at.crimsonbit.nodesystem.event.GlobalOutputAddedEvent;
import at.crimsonbit.nodesystem.event.GraphCalculateEvent;
import at.crimsonbit.nodesystem.event.GraphDeselectAllEvent;
import at.crimsonbit.nodesystem.event.GraphRectangleSelectionEvent;
import at.crimsonbit.nodesystem.event.GraphSelectionEvent;
import at.crimsonbit.nodesystem.event.GraphToolChangedEvent;
import at.crimsonbit.nodesystem.event.KeyPressedEvent;
import at.crimsonbit.nodesystem.event.KeyoardTypedEvent;
import at.crimsonbit.nodesystem.event.LoadEvent;
import at.crimsonbit.nodesystem.event.MessageEvent;
import at.crimsonbit.nodesystem.event.MouseClickEvent;
import at.crimsonbit.nodesystem.event.MouseMovedEvent;
import at.crimsonbit.nodesystem.event.MousePortEnteredEvent;
import at.crimsonbit.nodesystem.event.MousePortExitedEvent;
import at.crimsonbit.nodesystem.event.MouseSnapEvent;
import at.crimsonbit.nodesystem.event.MouseUnsnapEvent;
import at.crimsonbit.nodesystem.event.NodeAddedEvent;
import at.crimsonbit.nodesystem.event.NodeChangeConnectionEvent;
import at.crimsonbit.nodesystem.event.NodeComputationErrorEvent;
import at.crimsonbit.nodesystem.event.NodeComputationFinishedEvent;
import at.crimsonbit.nodesystem.event.NodeComputationStartEvent;
import at.crimsonbit.nodesystem.event.NodeConnectedEvent;
import at.crimsonbit.nodesystem.event.NodeConnectionStartEvent;
import at.crimsonbit.nodesystem.event.NodeCopiedEvent;
import at.crimsonbit.nodesystem.event.NodeDeletedEvent;
import at.crimsonbit.nodesystem.event.NodeDisconnectedEvent;
import at.crimsonbit.nodesystem.event.NodeMenuRemoveEvent;
import at.crimsonbit.nodesystem.event.NodeRemoveSignalEvent;
import at.crimsonbit.nodesystem.event.NodeRenameEvent;
import at.crimsonbit.nodesystem.event.PluginLoadedEvent;
import at.crimsonbit.nodesystem.event.PluginUnloadedEvent;
import at.crimsonbit.nodesystem.event.PluginsFinishedLoadingEvent;
import at.crimsonbit.nodesystem.event.SaveEvent;
import at.crimsonbit.nodesystem.event.SceneChangedEvent;
import at.crimsonbit.nodesystem.event.SignalOpenNodeMenuEvent;
import at.crimsonbit.nodesystem.event.SignalToggleMuteEvent;
import at.crimsonbit.nodesystem.event.SignalTryConnectionEven;
import at.crimsonbit.nodesystem.event.ValueLoadedEvent;
import at.crimsonbit.nodesystem.event.WidgetCloseEvent;
import at.crimsonbit.nodesystem.event.WidgetMenuLineShowEvent;
import at.crimsonbit.nodesystem.event.WidgetOpenEvent;
import at.crimsonbit.nodesystem.event.WidgetQuickMenuEnableEvent;
import at.crimsonbit.nodesystem.event.WidgetQuickMenuPositionEvent;
import at.crimsonbit.nodesystem.event.WidgetQuickMenuSelectionEvent;
import at.crimsonbit.nodesystem.frontend.gui.base.BaseInputPort;
import at.crimsonbit.nodesystem.frontend.gui.base.BaseNode;
import at.crimsonbit.nodesystem.frontend.gui.base.BaseOutputPort;
import at.crimsonbit.nodesystem.frontend.gui.base.BasePort;
import at.crimsonbit.nodesystem.frontend.gui.base.BaseSignal;
import at.crimsonbit.nodesystem.frontend.gui.clipboard.Clipboard;
import at.crimsonbit.nodesystem.frontend.gui.dialog.Entry;
import at.crimsonbit.nodesystem.frontend.gui.dialog.PopupDialog;
import at.crimsonbit.nodesystem.frontend.gui.dialog.SubDialog;
import at.crimsonbit.nodesystem.frontend.gui.drawable.DrawNode;
import at.crimsonbit.nodesystem.frontend.gui.drawable.DrawPort;
import at.crimsonbit.nodesystem.frontend.gui.drawable.DrawSignal;
import at.crimsonbit.nodesystem.frontend.gui.fields.BigDecimalField;
import at.crimsonbit.nodesystem.frontend.gui.fields.DoubleField;
import at.crimsonbit.nodesystem.frontend.gui.fields.FloatField;
import at.crimsonbit.nodesystem.frontend.gui.fields.IntegerField;
import at.crimsonbit.nodesystem.frontend.gui.fields.LongField;
import at.crimsonbit.nodesystem.frontend.gui.guilang.GLContainer;
import at.crimsonbit.nodesystem.frontend.gui.guilang.GLShadow;
import at.crimsonbit.nodesystem.frontend.gui.guilang.GuiLangParser;
import at.crimsonbit.nodesystem.frontend.gui.layer.BackgroundLayer;
import at.crimsonbit.nodesystem.frontend.gui.layer.BaseLayer;
import at.crimsonbit.nodesystem.frontend.gui.layer.DrawLayer;
import at.crimsonbit.nodesystem.frontend.gui.layer.GraphLayer;
import at.crimsonbit.nodesystem.frontend.gui.layer.Layer;
import at.crimsonbit.nodesystem.frontend.gui.layer.MessageLayer;
import at.crimsonbit.nodesystem.frontend.gui.layer.NodeLayer;
import at.crimsonbit.nodesystem.frontend.gui.layer.SignalLayer;
import at.crimsonbit.nodesystem.frontend.gui.widget.GlobalInputPortWidget;
import at.crimsonbit.nodesystem.frontend.gui.widget.GlobalOutputPortWidget;
import at.crimsonbit.nodesystem.frontend.gui.widget.NodeAreaWidget;
import at.crimsonbit.nodesystem.frontend.gui.widget.SelectionWidget;
import at.crimsonbit.nodesystem.frontend.midlayer.FrontBackCache;
import at.crimsonbit.nodesystem.frontend.midlayer.GraphConnector;
import at.crimsonbit.nodesystem.nodebackend.INodeType;
import at.crimsonbit.nodesystem.nodebackend.exception.InvalidPortException;
import at.crimsonbit.nodesystem.nodebackend.exception.NoSuchNode;
import at.crimsonbit.nodesystem.nodebackend.graph.ConnectionResult;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeConnection;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeOutputPort;
import at.crimsonbit.nodesystem.nodebackend.util.NodePortId;
import at.crimsonbit.nodesystem.nodebackend.util.Tuple;
import at.crimsonbit.nodesystem.plugin.dataType.ToolData;
import at.crimsonbit.nodesystem.plugin.dataType.WidgetData;
import at.crimsonbit.nodesystem.plugin.impl.IBasePlugin;
import at.crimsonbit.nodesystem.plugin.impl.PluginMaster;
import at.crimsonbit.nodesystem.util.DragContext;
import at.crimsonbit.nodesystem.util.Util;
import htl.gtm.nodeLangParser.exception.NodeLangException;
import javafx.animation.FadeTransition;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Cursor;
import javafx.scene.ImageCursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.TextInputDialog;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Light.Point;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.input.ZoomEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

/**
 * <h1>DrawGraph</h1>
 * <p>
 * The DrawGraph is responsible for everything GUI related. The whole Nodesystem
 * uses an {@link EventBus} to communicate with all elements. This allows the
 * graph to be very versatile and to react to both events from the front-end and
 * the backend in a very similar way. The whole node system uses only one
 * {@link EventBus} which resides in the {@link NodeGraph}. <br>
 * The whole GUI is based on so called {@link Layer}s. This allows the graph to
 * be very flexible when it comes to adding new widgets or drawing extra
 * information. <br>
 * The layer to use when adding the graph into a scene is the {@link BaseLayer}
 * which can be optained with {@link #getBaseLayer()}. The {@link BaseLayer}
 * holds every other layer. <br>
 * The Layers are structured as follows:
 * <ul>
 * <li>BaseLayer</li>
 * <ul>
 * <li>GraphLayer</li>
 * <ul>
 * <li>BackgroundLayer</li>
 * <li>DrawLayer</li>
 * <ul>
 * <li>SignalLayer -> Obsolete! Signals now reside in the NodeLayer</li>
 * <li>NodeLayer</li>
 * <li>TempSignalLayer</li>
 * </ul>
 * </ul>
 * <li>SelectionLayer</li>
 * <li>MessageLayer</li>
 * <li>TopLayer</li>
 * </ul>
 * </ul>
 * </p>
 * 
 * @author Florian Wagner
 * @see DrawNode
 * @see DrawPort
 * @see DrawSignal
 */
public class DrawGraph {

	private BaseLayer baseLayer = new BaseLayer();
	private DrawLayer drawLayer = new DrawLayer();
	private BackgroundLayer backgroundLayer = new BackgroundLayer();
	private MessageLayer messageLayer = new MessageLayer();
	private Layer widgetLayer = new Layer();
	private Layer selectionLayer = new Layer();
	private NodeLayer nodeLayer = new NodeLayer();
	// Change: 06.02.2019
	// DrawSignal now reside in the nodelayer for more functionality
	// private SignalLayer signalLayer = new SignalLayer();
	private SignalLayer tempSignalLayer = new SignalLayer();
	private GraphLayer graphLayer = new GraphLayer();

	private SceneGestures sceneGestures;
	private DrawSignal tempSignal;

	private NodeGraph backendNodeGraph;
	private GraphConnector graphConnector;

	private FrontBackCache nodeCache = new FrontBackCache();
	private DragContext nodeDragContext = new DragContext();

	private Clipboard clipboard = new Clipboard();
	private GLContainer settingsContainer;

	private NodeAreaWidget nodeAreaWidget = new NodeAreaWidget();
	private SelectionWidget selectionWidget;
	private GlobalInputPortWidget globalInputPortWidget = new GlobalInputPortWidget();
	private GlobalOutputPortWidget globalOutputPortWidget = new GlobalOutputPortWidget();

	// private SearchBarWidget searchBar;

	private GraphConfig graphConfig;
	@SuppressWarnings("unused")
	private PluginMaster pluginMaster = PluginMaster.get();

	// =============================================

	private SnapshotParameters snP = new SnapshotParameters();

	private ObservableList<DrawNode> selected = FXCollections.observableArrayList();

	@SuppressWarnings("unused")
	private SubDialog nodeMenu = new SubDialog(0, "Add");
	private SubDialog nodeSignalMenu = new SubDialog(0, "Nodes");
	private PopupDialog signalPopUp = new PopupDialog();

	@SuppressWarnings("unused")
	private Polyline signalDisconnectPolygon = new Polyline();

	private DoubleProperty mouseX = new SimpleDoubleProperty(20D);
	private DoubleProperty mouseY = new SimpleDoubleProperty(100D);
	private BooleanProperty runOnMobileProperty = new SimpleBooleanProperty(false);
	private BooleanProperty touchEnabledProperty = new SimpleBooleanProperty(false);
	private BooleanProperty menuLineEnabledProperty = new SimpleBooleanProperty(true);
	private BooleanProperty enableQuickMenuProperty = new SimpleBooleanProperty(true);
	private DoubleProperty quickMenuPositionProperty = new SimpleDoubleProperty(0.7D);
	private BooleanProperty useNodeAreaWidgetProperty = new SimpleBooleanProperty(false);
	private BooleanProperty useSelectionWidgetProperty = new SimpleBooleanProperty(false);
	private BooleanProperty useSearchBarWidgetProperty = new SimpleBooleanProperty(true);
	private DoubleProperty screenMX = new SimpleDoubleProperty(0.0D);
	private DoubleProperty screenMY = new SimpleDoubleProperty(0.0D);
	private StringProperty nameProperty = new SimpleStringProperty("Unnamed Graph");
	private final Point anchor = new Point();

	private ToolData currentTool;
	private Node currentToolNode;

	private final String CONTAINER_PATH = "/settings.json";
	private int copyCounter = 1;
	private boolean initialized = false;
	private final EventBus globalBus;

	public DrawGraph(NodeGraph graph, EventBus globalEventBus, GraphConfig config) {
		if (config != null)
			this.graphConfig = config;
		else
			this.graphConfig = new GraphConfig();
		this.backendNodeGraph = graph;
		globalBus = globalEventBus;
		nameProperty.set(backendNodeGraph.id);

		initBindings();
		setupEventBusSystem();
		initLayers();
		initSelection();
		init();
		Runtime.getRuntime().addShutdownHook(new Thread(() -> PluginMaster.get().unloadPlugins()));

	}

	@Subscribe
	private void onPluginUnloaded(PluginUnloadedEvent ev) {
		System.out.println("Unloading Plugin: " + ev.plugin);
	}

	/**
	 * Initializes the graph. This method is very important and needs to be called
	 * After the graph has been added to a scene!
	 */
	public void init() {
		if (!initialized) {
			backgroundLayer.prefWidthProperty().bind(baseLayer.widthProperty());
			backgroundLayer.prefHeightProperty().bind(baseLayer.heightProperty());
			backgroundLayer.maxWidthProperty().bind((baseLayer.widthProperty()));
			backgroundLayer.maxHeightProperty().bind((baseLayer.heightProperty()));

			backgroundLayer.bindScale(drawLayer.getScaleProperty());
			backgroundLayer.bindTranslation(drawLayer.translateXProperty(), drawLayer.translateYProperty());
			messageLayer.bindStatus(baseLayer.widthProperty(), baseLayer.heightProperty());

			// signalDisconnectPolygon.setFill(Color.TRANSPARENT);
			// signalDisconnectPolygon.setStroke(Color.ORANGE);
			// signalDisconnectPolygon.setMouseTransparent(true);
			// signalDisconnectPolygon.setSmooth(true);

			attachTempSignalToMouse();

			selected.addListener(new ListChangeListener<DrawNode>() {

				@Override
				public void onChanged(Change<? extends DrawNode> c) {

					if (selected.isEmpty()) {
						globalBus.post(new WidgetQuickMenuSelectionEvent(null));
					}

				}
			});

			// drawLayer.getScaleProperty().addListener(new ChangeListener<Number>() {
			//
			// @Override
			// public void changed(ObservableValue<? extends Number> observable, Number
			// oldValue, Number newValue) {
			// attachTempSignalToMouse();
			// }
			// });

			/*
			 * baseLayer.setOnDragEntered(new EventHandler<DragEvent>() { public void
			 * handle(DragEvent event) { System.out.println("onDragEntered"); if
			 * (event.getGestureSource() != nodeLayer && event.getDragboard().hasString()) {
			 * String backendID =
			 * backendNodeGraph.addNode(event.getDragboard().getString()); DrawNode node =
			 * nodeCache.getNode(backendID); _node = node;
			 * node.translateXProperty().bind(mouseX);
			 * node.translateYProperty().bind(mouseY); } event.consume(); } });
			 * 
			 * baseLayer.setOnDragExited(new EventHandler<DragEvent>() { public void
			 * handle(DragEvent event) { String backendID =
			 * event.getDragboard().getString(); _node = null;
			 * deleteNode(nodeCache.getNode(backendID)); event.consume(); } });
			 * 
			 * baseLayer.setOnDragDropped(new EventHandler<DragEvent>() { public void
			 * handle(DragEvent event) { System.out.println("onDragDropped"); Dragboard db =
			 * event.getDragboard(); boolean success = false; if (db.hasString()) { if
			 * (_node != null) { DrawNode n =
			 * nodeCache.getNode(_node.getNode().getBackendID());
			 * n.translateXProperty().unbind(); n.translateYProperty().unbind(); success =
			 * true; } }
			 * 
			 * event.setDropCompleted(success);
			 * 
			 * event.consume(); } });
			 */

			// if (getDrawLayer().getScene() != null) {
			initGestures();

			initialized = true;
		}
		// }
	}

	private void initBindings() {
		if (graphConfig != null) {
			enableQuickMenuProperty.bind(graphConfig.enableQuickMenuProperty());
			menuLineEnabledProperty.bind(graphConfig.menuLineEnabledProperty());
			quickMenuPositionProperty.bind(graphConfig.quickMenuPositionProperty());
			runOnMobileProperty.bind(graphConfig.runOnMobileProperty());
			touchEnabledProperty.bind(graphConfig.touchEnabledProperty());
			useNodeAreaWidgetProperty.bind(graphConfig.useNodeAreaWidgetProperty());
			useSearchBarWidgetProperty.bind(graphConfig.useSearchBarWidgetProperty());
			useSelectionWidgetProperty.bind(graphConfig.useSelectionWidgetProperty());

			useSelectionWidgetProperty.addListener(new ChangeListener<Boolean>() {

				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					initSelection();
				}
			});

			useNodeAreaWidgetProperty.addListener(new ChangeListener<Boolean>() {

				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					// addNodeAreaWidget();
				}
			});

			quickMenuPositionProperty.addListener(new ChangeListener<Number>() {

				@Override
				public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
					getGlobalEventBus().post(new WidgetQuickMenuPositionEvent(newValue));
				}
			});

			enableQuickMenuProperty.addListener(new ChangeListener<Boolean>() {

				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					getGlobalEventBus().post(new WidgetQuickMenuEnableEvent(newValue));
				}
			});

			menuLineEnabledProperty.addListener(new ChangeListener<Boolean>() {
				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					getGlobalEventBus().post(new WidgetMenuLineShowEvent(newValue));
				}
			});

			getGlobalEventBus().post(new WidgetQuickMenuEnableEvent(enableQuickMenu()));
			getGlobalEventBus().post(new WidgetMenuLineShowEvent(menuLineEnabled()));
		}
	}

	/**
	 * Initializes the gestures. The gestures are set with eventFilters on the scene
	 */
	private void initGestures() {

		getDrawLayer().sceneProperty().addListener(new ChangeListener<Scene>() {
			@Override
			public void changed(ObservableValue<? extends Scene> observable, Scene oldValue, Scene newValue) {
				if (newValue != null) {
					getBaseLayer().addEventFilter(MouseEvent.MOUSE_MOVED, sceneGestures.getOnMouseMoved());
					newValue.addEventFilter(MouseEvent.MOUSE_CLICKED, sceneGestures.getOnMouseClicked());
					getBaseLayer().addEventFilter(MouseEvent.MOUSE_DRAGGED,
							sceneGestures.getOnMouseDraggedEventHandler());
					newValue.addEventFilter(MouseEvent.MOUSE_PRESSED, sceneGestures.getOnMousePressedEventHandler());
					newValue.addEventFilter(MouseEvent.MOUSE_RELEASED, sceneGestures.getOnMouseReleased());
					getBaseLayer().addEventFilter(KeyEvent.KEY_PRESSED, sceneGestures.getOnKeyPressed());
					getBaseLayer().addEventFilter(KeyEvent.KEY_TYPED, sceneGestures.getOnKeyTyped());
					if (!graphConfig.touchEnabledProperty().get()) {
						getBaseLayer().addEventFilter(ScrollEvent.ANY, sceneGestures.getOnScrollEventHandler());
						postMessage("Started on PC");
					}
				}
				if (graphConfig.touchEnabledProperty().get()) {
					getBaseLayer().addEventFilter(TouchEvent.TOUCH_PRESSED,
							sceneGestures.getOnTouchPressedEventHandler());
					getBaseLayer().addEventFilter(TouchEvent.TOUCH_MOVED,
							sceneGestures.getOnTouchDraggedEventHandler());
					getBaseLayer().addEventFilter(TouchEvent.TOUCH_STATIONARY,
							sceneGestures.getOnTouchClickedEventHandler());
					// getDrawLayer().getScene().addEventFilter(MouseEvent.MOUSE_MOVED,
					// sceneGestures.getOnMouseMoved());
					getBaseLayer().addEventFilter(TouchEvent.TOUCH_RELEASED,
							sceneGestures.getOnTouchReleasedEventHandler());
					getBaseLayer().addEventFilter(ZoomEvent.ANY, sceneGestures.getOnZoomEventHandler());
					postMessage("Started on Mobile");
				}
			}
		});

	}

	/**
	 * Initializes the layer - setup of the graph
	 */
	private void initLayers() {
		graphLayer.getChildren().add(backgroundLayer);
		graphLayer.getChildren().add(drawLayer);

		drawLayer.getChildren().add(tempSignalLayer);
		drawLayer.getChildren().add(nodeLayer);

		baseLayer.getChildren().add(graphLayer);
		baseLayer.getChildren().add(selectionLayer);
		baseLayer.getChildren().add(messageLayer);
		baseLayer.getChildren().add(widgetLayer);

		selectionLayer.setMouseTransparent(true);

		nodeLayer.toFront();
		tempSignalLayer.toFront();
		// topLayer.getChildren().add(menuBar);
		// addNodeAreaWidget();
		graphLayer.getChildren().add(globalInputPortWidget);
		graphLayer.getChildren().add(globalOutputPortWidget);

		globalInputPortWidget.prefHeightProperty()
				.bind(baseLayer.heightProperty().subtract(messageLayer.getMessageBarHeight() + 1D));

		globalOutputPortWidget.translateXProperty()
				.bind(baseLayer.widthProperty().subtract(globalOutputPortWidget.getRectangle().widthProperty()));

		globalOutputPortWidget.prefHeightProperty()
				.bind(baseLayer.heightProperty().subtract(messageLayer.getMessageBarHeight() + 1D));

		// globalInputPortWidget.setMouseTransparent(true);
		// globalOutputPortWidget.setMouseTransparent(true);
		nodeCache.watch(nodeLayer.getChildren());
		getTopLayer().setMouseTransparent(false);
	}

	/**
	 * Initializes the selection
	 * 
	 * @param use whether to initialize it or not (for debug)
	 */
	private void initSelection() {
		if (useSelectionWidget()) {
			if (!selectionLayer.getChildren().contains(selectionWidget))
				selectionLayer.getChildren().add(selectionWidget);
		} else {
			if (selectionLayer.getChildren().contains(selectionWidget))
				selectionLayer.getChildren().remove(selectionWidget);
		}

	}

	/**
	 * Clears everything from the nodeLayer.
	 * 
	 * This should not be called just like that! It should only be called after the
	 * backend has been cleared the same way
	 */
	private void clearLayers() {
		// signalLayer.getChildren().clear();
		nodeLayer.getChildren().clear();
	}

	/**
	 * Setup every member variable that needs the event bus to work Called in
	 * constructor only!
	 */
	private void setupEventBusSystem() {
		backendNodeGraph.subscribeToEventBus(this);
		globalBus.register(this);
		graphConnector = new GraphConnector(backendNodeGraph);
		sceneGestures = new SceneGestures(getDrawLayer(), selectionWidget, backendNodeGraph.getEventBus());

		// searchBar = new SearchBarWidget(backendNodeGraph.getEventBus(),
		// backendNodeGraph);
		selectionWidget = new SelectionWidget(backendNodeGraph.getEventBus());
		// nodeTreeWidget = new NodeTreeViewWidget(backendNodeGraph);
		// nodeTreeWidget.setLayoutX(-nodeTreeWidget.getWidth() / 6D);
	}

	/**
	 * This method is called to place a node inbetween to already connected nodes.
	 * It removes the current signal and places said node inbetween
	 * 
	 * @param sig the signal
	 * @param b the bounds of the signal
	 * @param e the entry of the menu (node selection)
	 */
	private void onPlaceNodeIntoSignal(BaseSignal sig, Bounds b, Entry e) {
		String id = backendNodeGraph.addNode(e.getName());
		BasePort inSPort = sig.getInputPortProperty().get();
		BasePort outSPort = sig.getOutputPortProperty().get();
		DrawNode node = getNode(id);

		graphConnector.disconnectFromInput(inSPort);
		graphConnector.beginConnection(outSPort);
		boolean ConSucc = false;
		loop: for (BasePort inNPort : node.getNode().getInputPorts()) {
			try {
				if (graphConnector.completeConnection(inNPort, true) == ConnectionResult.SUCCESS) {
					graphConnector.completeConnection(inNPort, false);
					ConSucc = true;
					break loop;
				}
			} catch (InvalidPortException e1) {
				continue;
			}
		}

		if (!ConSucc)
			graphConnector.abortConnection();

		graphConnector.beginConnection(inSPort);

		ConSucc = false;
		loop: for (BasePort outNPort : node.getNode().getOutputPorts()) {
			try {
				if (graphConnector.completeConnection(outNPort, true) == ConnectionResult.SUCCESS) {
					graphConnector.completeConnection(outNPort, false);
					ConSucc = true;
					break loop;
				}
			} catch (InvalidPortException e1) {
				continue;

			}
		}

		if (!ConSucc)
			graphConnector.abortConnection();

		DrawNode leftNode = getNode(inSPort.getParentNode().getBackendID());
		DrawNode rightNode = getNode(outSPort.getParentNode().getBackendID());

		double x_dist = rightNode.getLayoutX() - leftNode.getLayoutX();
		double y_dist = rightNode.getLayoutY() - leftNode.getLayoutY();

		double pos_x = leftNode.getLayoutX() + (x_dist / 2D);
		double pos_y = leftNode.getLayoutY() + (y_dist / 2D);

		node.setLayoutX(pos_x);
		node.setLayoutY(pos_y);
		selectNode(node);
	}

	/**
	 * Called when a global input is added to a group node
	 * 
	 * @param ev
	 */
	@Subscribe
	private void onGlobalInputAdded(GlobalInputAddedEvent ev) {
		String portID = ev.portID;

		INodeInputPort port = backendNodeGraph.getInputPort(portID).get();
		DrawPort drawPort = getPort(port.getID());

		BasePort bPort = new BaseOutputPort(drawPort.getPort());
		DrawPort dPort = new DrawPort(bPort, drawPort.getSettings(), getNodeGraph().getEventBus(),
				new SimpleDoubleProperty(0.0), false, true);
		BaseSignal tempSignal = new BaseSignal(drawPort.getPort(), drawPort.getPort().isInput());
		DrawSignal virtualSignal = new DrawSignal(tempSignal, drawPort.getSettings(),
				dPort.getPortCircle().getLayoutX(), dPort.getPortCircle().getLayoutY());
		virtualSignal.endXProperty().bind(drawPort.getPortCircle().layoutXProperty());
		virtualSignal.endYProperty().bind(drawPort.getPortCircle().layoutYProperty());
		dPort.turnOffField();
		globalInputPortWidget.addPort(dPort, virtualSignal);
	}

	/**
	 * Called when a global input is added to a group node
	 * 
	 * @param ev
	 */
	@Subscribe
	private void onGlobalOutputAdded(GlobalOutputAddedEvent ev) {
		String portID = ev.portID;

		INodeOutputPort port = backendNodeGraph.getOutputPort(portID).get();
		DrawPort drawPort = getPort(port.getID());
		BasePort bPort = new BaseInputPort(drawPort.getPort());
		DrawPort dPort = new DrawPort(bPort, drawPort.getSettings(), getNodeGraph().getEventBus(),
				new SimpleDoubleProperty(0.0), false, true);
		dPort.turnOffField();
		globalOutputPortWidget.addPort(dPort);
	}

	/**
	 * Part of the event system. Called for every {@link SignalOpenNodeMenuEvent}.
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onSignalOpenNodeMenu(SignalOpenNodeMenuEvent e) {
		int mCount = 0;
		int iCount = 0;

		Set<INodeType> map = backendNodeGraph.getMaster().getRegisteredNodes();
		Map<String, SubDialog> cache = new HashMap<>();
		nodeSignalMenu.getItems().clear();
		for (INodeType type : map) {

			String name = type.getClass().getSimpleName();
			SubDialog menu = cache.get(name);
			if (menu == null) {
				menu = new SubDialog(mCount++, name);
				cache.put(name, menu);
			}
			Entry ent = new Entry(iCount++, type.regName(), type.name(), false);
			ent.setOnAction(event -> {
				onPlaceNodeIntoSignal(e.signal, e.bounds, ent);
				event.consume();
			});

			menu.addItem(ent);
		}
		for (SubDialog menu : cache.values()) {
			nodeSignalMenu.addMenu(menu);
		}
		Entry mute;
		if (getSignal(e.signal).muteProperty().get()) {
			mute = new Entry(iCount++, "UnMute", "UnMute", false);
		} else {
			mute = new Entry(iCount++, "Mute", "Mute", false);
		}
		if (mute != null) {
			mute.setOnAction(event -> {
				toggleSignalMute(getSignal(e.signal));
				event.consume();
			});

		}

		signalPopUp = new PopupDialog();
		signalPopUp.addItem(nodeSignalMenu);
		signalPopUp.addItem(mute);
		signalPopUp.show(getBaseLayer().getScene().getWindow(), e.event.getScreenX(), e.event.getScreenY());

	}

	/**
	 * Toggles the signal mute property and changes the internal connection.
	 * 
	 * @param signal the signal to toggle the mute
	 */
	private void toggleSignalMute(DrawSignal signal) {
		if (!signal.muteProperty().get()) {
			backendNodeGraph.getEventBus().post(
					new SignalToggleMuteEvent(true, signal.getSignal().getInputPortProperty().get().getBackendID()));

			getPort(signal.getSignal().getInputPortProperty().get().getBackendID()).onPortFieldShow();
			getPort(signal.getSignal().getInputPortProperty().get().getBackendID()).connectedProperty().set(false);

		} else {
			backendNodeGraph.getEventBus().post(
					new SignalToggleMuteEvent(false, signal.getSignal().getInputPortProperty().get().getBackendID()));
			getPort(signal.getSignal().getInputPortProperty().get().getBackendID()).onPortFieldHide();
			getPort(signal.getSignal().getInputPortProperty().get().getBackendID()).connectedProperty().set(true);

		}
		signal.toggleMute();

	}

	/**
	 * Used to get the port of a node with the ports id.
	 * 
	 * @param backendID the backend id of the port
	 * @return the corresponding {@link DrawPort}
	 */
	public DrawPort getPort(String backendID) {
		return nodeCache.getPort(backendID);

	}

	/**
	 * Adds the given node to the selection without clearing the selection list
	 * beforehand.
	 * 
	 * @param node
	 */

	public void addToSelection(DrawNode node) {
		if (!node.isSelected()) {
			node.setSelected(true);
			selected.add(node);
			node.toFront();
		}
	}

	/**
	 * Selects the node given. Every other node will be deselected and the
	 * {@link #selected} array will be cleared!
	 * 
	 * @param node the node to select
	 */
	public void selectNode(DrawNode node) {
		for (Node n : nodeLayer.getChildren()) {
			if (n instanceof DrawNode) {
				((DrawNode) n).setSelected(false);
				deselectSignals((DrawNode) n);
			}
		}
		selected.clear();
		node.setSelected(true);
		selected.add(node);
		node.toFront();
		//globalBus.post(new GraphSelectionEvent(node.getNode().getBackendID(), null));
		globalBus.post(new WidgetQuickMenuSelectionEvent(node));
	}

	/**
	 * Selects the node given. Every other node will be deselected and the
	 * {@link #selected} array will be cleared!
	 * 
	 * @param nodeID the node id to select
	 */
	public void selectNode(String nodeId) {
		DrawNode node = getNode(nodeId);
		for (Node n : nodeLayer.getChildren()) {
			if (n instanceof DrawNode) {
				((DrawNode) n).setSelected(false);
				deselectSignals((DrawNode) n);
			}
		}
		selected.clear();
		node.setSelected(true);
		selected.add(node);
		node.toFront();
	}

	/**
	 * Checks the {@link DrawNode} for connected signals and pushes them to the
	 * front. Called when a node selection occured.
	 * 
	 * @param node the node to push the signal to the front
	 */
	private void pushSelectedNodeSignalToFront(DrawNode node) {
		if (node != null) {
			for (BasePort inPort : node.getNode().getInputPorts()) {

				for (DrawSignal sig : getSignalFromPort(inPort.getBackendID())) {
					if (sig != null)
						sig.toFront();
				}
			}
			for (BasePort outPort : node.getNode().getOutputPorts()) {
				for (DrawSignal sig : getSignalFromPort(outPort.getBackendID())) {
					if (sig != null)
						sig.toFront();
				}
			}
		}
	}

	/**
	 * Checks the {@link DrawNode} for connected signals and pushes them to the
	 * back. Called when a node selection occured.
	 * 
	 * @param node the node to push the signal to the back
	 */
	private void pushSelectedNodeSignalToBack(DrawNode node) {
		if (node != null) {
			for (BasePort inPort : node.getNode().getInputPorts()) {
				for (DrawSignal sig : getSignalFromPort(inPort.getBackendID())) {
					if (sig != null)
						sig.toBack();
				}
			}
			for (BasePort outPort : node.getNode().getOutputPorts()) {
				for (DrawSignal sig : getSignalFromPort(outPort.getBackendID())) {
					if (sig != null)
						sig.toBack();
				}
			}
		}
	}

	/**
	 * This method is still called on every (scene) mouse move. Part of the event
	 * system. Called for every {@link MouseMovedEvent}.
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onMouseMoved(MouseMovedEvent e) {

		if (e.event instanceof MouseEvent) {
			MouseEvent event = (MouseEvent) e.event;
			mouseX.set(event.getSceneX()); // *drawLayer.getScale();
			mouseY.set(event.getSceneY()); // *drawLayer.getScale();
			if (event.isControlDown()) {
				baseLayer.getScene().setCursor(javafx.scene.Cursor.CROSSHAIR);
				// if (!widgetLayer.getChildren().contains(signalDisconnectPolygon))
				// widgetLayer.getChildren().add(signalDisconnectPolygon);
				// signalDisconnectPolygon.getPoints().addAll(new Double[] { event.getX(),
				// event.getY() });
				// if (signalDisconnectPolygon.getPoints().size() > 500) {
				// signalDisconnectPolygon.getPoints().clear();
				/// }

			} else {
				// if (widgetLayer.getChildren().contains(signalDisconnectPolygon)) {
				// widgetLayer.getChildren().remove(signalDisconnectPolygon);
				// signalDisconnectPolygon.getPoints().clear();
				baseLayer.getScene().setCursor(javafx.scene.Cursor.DEFAULT);
				// }
			}
			screenMX.set(event.getScreenX());
			screenMY.set(event.getScreenY());
		} else if (e.event instanceof TouchEvent) {
			TouchEvent event = (TouchEvent) e.event;
			screenMX.set(event.getTouchPoint().getScreenX());
			screenMY.set(event.getTouchPoint().getScreenY());
		}

	}

	/**
	 * Called when the mouse should be snapped to a port.
	 */
	@Subscribe
	private void onSignalSnapping(MouseSnapEvent e) {

		DrawPort port = e.port;
		if (tempSignal != null) {
			DrawNode node = getNode(port.getPort().getParentNode().getBackendID());
			tempSignal.endXProperty().bind(node.layoutXProperty().add(port.layoutXProperty())
					.add(port.getSettings().getPort().getInput().getSize()));
			if (!port.getPort().isInput()) {
				tempSignal.endXProperty().bind(
						node.layoutXProperty().add(port.layoutXProperty().add(port.getSettings().getNode().getWidth())
								.subtract(port.getSettings().getPort().getOutput().getSize())));
			}
			tempSignal.endYProperty().bind(node.layoutYProperty().add(port.layoutYProperty()));
			tempSignal.snappedProperty().set(true);
		}

	}

	/**
	 * Called when the mouse should stop to be snapped to a port.
	 */
	@Subscribe
	private void onSignalStopSnapping(MouseUnsnapEvent e) {
		if (tempSignal != null) {
			tempSignal.snappedProperty().set(false);
			// attachTempSignalToMouse();
		}
	}

	/**
	 * Called when the user pressed control and enters a signal. Then this signal
	 * gets removed
	 * 
	 * @param e
	 */
	@Subscribe
	private void onRemoveSignal(NodeRemoveSignalEvent e) {
		DrawSignal signal = e.signal;
		BasePort input = signal.getSignal().getInputPortProperty().get();
		graphConnector.disconnectFromInput(input);

	}

	/**
	 * Part of the event system. Called for every {@link ValueLoadedEvent}. Called
	 * when node fields are loaded
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onValuesLoaded(ValueLoadedEvent e) {
		DrawPort port = nodeCache.getPort(e.portID);

		if (port.getBooleanField() != null) {
			port.getBooleanField().setSelected((boolean) e.val);
		} else {
			if (port.getField() != null) {
				if (port.getField() instanceof IntegerField) {
					((IntegerField) port.getField()).numberProperty().set((Integer) e.val);
				} else if (port.getField() instanceof DoubleField) {
					((DoubleField) port.getField()).numberProperty().set((Double) e.val);
				} else if (port.getField() instanceof FloatField) {
					((FloatField) port.getField()).numberProperty().set((Float) e.val);
				} else if (port.getField() instanceof LongField) {
					((LongField) port.getField()).numberProperty().set((Long) e.val);
				} else if (port.getField() instanceof BigDecimalField) {
					((BigDecimalField) port.getField()).numberProperty().set((BigDecimal) e.val);
				} else {
					port.getField().textProperty().set((String) e.val);
				}
			}
		}

	}

//	/**
//	 * Called when the user clicks on a node
//	 * 
//	 * @param e the corresponding event
//	 */
//	@Subscribe
//	private void onMouseNodeClicked(MouseNodeClickedEvent e) {
//
//	}
//
//	/**
//	 * Called when the user draggs a node
//	 * 
//	 * @param e the corresponding event
//	 */
//	@Subscribe
//	private void onMouseNodeDragged(MouseNodeDraggedEvent e) {
//		
//	}
//
//	/**
//	 * Called when the user realeases a node
//	 * 
//	 * @param e the corresponding event
//	 */
//	@Subscribe
//	private void onMouseNodeReleased(MouseNodeReleasedEvent e) {
//		
//	}
//
//	/**
//	 * Called when the user presses on a node
//	 * 
//	 * @param e the corresponding event
//	 */
//	@Subscribe
//	private void onMouseNodePressed(MouseNodePressedEvent e) {
//		
//	}

	/**
	 * Called when the mouse enters a port.
	 * 
	 * @param e the corresponding event
	 */
	@Subscribe
	private void onMousePortEntered(MousePortEnteredEvent e) {
		DrawPort port = getPort(e.portID);
		if (port != null)
			for (DrawSignal sig : getSignalFromPort(port.getPort().getBackendID())) {
				if (sig != null)
					sig.selectedProperty().set(true);
			}

	}

	/**
	 * Called when the mouse exits a port.
	 * 
	 * @param e the corresponding event
	 */
	@Subscribe
	private void onMousePortExited(MousePortExitedEvent e) {
		DrawPort port = getPort(e.portID);
		if (port != null)
			for (DrawSignal sig : getSignalFromPort(port.getPort().getBackendID())) {
				if (sig != null)
					sig.selectedProperty().set(false);
			}

	}

	/**
	 * Part of the event system. Called for every {@link GraphDeselectAllEvent}.
	 * Called when the given shortcut is executed
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onDeselectAll(GraphDeselectAllEvent e) {
		deselectAll();
	}

	/**
	 * Part of the event system. Called for every
	 * {@link GraphRectangleSelectionEvent}. Called when a rectangle selection has
	 * occured
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onRectangleSelection(GraphRectangleSelectionEvent e) {
		Rectangle selection = e.selection;
		if (useSelectionWidget())
			for (Node node : nodeLayer.getChildren()) {
				if (node instanceof DrawNode) {
					DrawNode dNode = (DrawNode) node;

					Bounds nodeBounds = dNode.getBoundsInParent();
					// Bounds selectionBounds =
					// selection.localToScreen(selection.getBoundsInLocal());
					// double x = dNode.getScene().getWindow().getX();
					// double biny = dNode.getScene().getWindow().getY();

					double minX = (nodeBounds.getMinX() + drawLayer.getTranslateX()) / drawLayer.getScale();
					double minY = (nodeBounds.getMinY() + drawLayer.getTranslateY()) / drawLayer.getScale();
					double maxX = (nodeBounds.getMaxX() + drawLayer.getTranslateX()) / drawLayer.getScale();
					double maxY = (nodeBounds.getMaxY() + drawLayer.getTranslateY()) / drawLayer.getScale();
					double selMinX = selection.getX() / drawLayer.getScale();
					double selMinY = selection.getY() / drawLayer.getScale();
					double selMaxX = (selection.getX() + selection.getWidth()) / drawLayer.getScale();
					double selMaxY = (selection.getY() + selection.getHeight()) / drawLayer.getScale();

					// System.out.prin tln(nodeBounds);
					// System.out.print(selection.getX());
					// System.out.print(selection.getY());
					// System.out.print(selection.getWidth());
					// System.out.println(selection.getHeight());

					if (selMinX < (minX) && selMinY < (minY) && selMaxX > (maxX) && selMaxY > (maxY)) {
						dNode.setSelected(true);
						selected.add(dNode);

					} else {
						dNode.setSelected(false);
						selected.remove(dNode);
					}
				}
			}

	}

	/**
	 * Part of the event system. Called for every {@link GraphCalculateEvent}.
	 * Called when the graph should calculate a node.
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onCalculateGraph(GraphCalculateEvent e) {
		executeNode(e.nodeID);

	}

	/**
	 * Part of the event system. Called for every {@link MouseClickEvent}. Called
	 * when a {@link MouseEvent} -> setOnMouseClicked on the scene is detected.
	 * Currently an empty method call.
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onMouseClicked(MouseClickEvent e) {
		// deselectAll();
	}

	/**
	 * Part of the event system. Called for every {@link KeyoardTypedEvent}. Called
	 * when a {@link KeyEvent} -> setOnKeyTyped on the scene is detected. Currently
	 * an empty method
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onKeyTyped(KeyoardTypedEvent e) {
	}

	/**
	 * Part of the event system. Called for every {@link WidgetCloseEvent}. Called
	 * when the searchbar is closed
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onWidgetClose(WidgetCloseEvent e) {
		if (widgetLayer.getChildren().contains(e.widget.getLayout())) {
			widgetLayer.getChildren().remove(e.widget.getLayout());
			e.widget.freeWidget();
			backendNodeGraph.getEventBus().unregister(e.widget);
		}
	}

	/**
	 * Part of the event system. Called for every {@link GraphSelectionEvent}.
	 * Called when a (node) selection has occured
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onSelectionChanged(GraphSelectionEvent e) {

		if (!e.event.isShiftDown()) {
			deselectAll();
		}

		DrawNode n = getNode(e.nodeId);
		if (n != null) {
			if (n.isSelected()) {
				pushSelectedNodeSignalToBack(n);
				n.setSelected(false);
				deselectSignals(n);
				selected.remove(n);
			} else {
				pushSelectedNodeSignalToFront(n);
				n.setSelected(true);
				n.toFront();
				selectSignals(n);
				selected.add(n);
			}

			e.event.consume();
		}
	}

	/**
	 * Deselects all Signals which are connected to the given node.
	 * 
	 * @param node the DrawNode
	 */
	private void deselectSignals(DrawNode node) {
		deselectSignalsForPort(node.getNode().getInputPorts());
		deselectSignalsForPort(node.getNode().getOutputPorts());
	}

	/**
	 * Selects all Signals which are connected to the given node.
	 * 
	 * @param node the DrawNode
	 */
	private void selectSignals(DrawNode node) {
		selectSignalsForPort(node.getNode().getInputPorts());
		selectSignalsForPort(node.getNode().getOutputPorts());

	}

	/**
	 * Deselects all Signals which are connected to the given port.
	 * 
	 * @param ports the ports
	 */
	private void deselectSignalsForPort(List<BasePort> ports) {
		for (BasePort port : ports) {
			DrawPort dPort = nodeCache.getPort(port.getBackendID());
			List<DrawSignal> signals = getSignalFromPort(dPort);
			for (DrawSignal signal : signals)
				signal.selectedProperty().set(false);
		}
	}

	/**
	 * Selects all Signals which are connected to the given port.
	 * 
	 * @param ports the ports
	 */
	private void selectSignalsForPort(List<BasePort> ports) {
		for (BasePort port : ports) {
			DrawPort dPort = nodeCache.getPort(port.getBackendID());
			List<DrawSignal> signals = getSignalFromPort(dPort);
			for (DrawSignal signal : signals)
				signal.selectedProperty().set(true);
		}
	}

	/**
	 * Part of the event system. Called for every {@link NodeCopiedEvent}. Called
	 * when a {@link DrawNode} is copied.
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onNodeCopied(NodeCopiedEvent e) {
		copyNodes();
	}

	/**
	 * Part of the event system. Called for every {@link KeyPressedEvent}. Called
	 * when a Key is pressed. Currently only calls {@link #postMessage(String)}
	 * 
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onKeyPressed(KeyPressedEvent keyEvent) {
		postMessage("KeyPress received: " + keyEvent.event.getCode());
	}

	/**
	 * This method is called for every plugin that is beeing registered into the
	 * Nodesystem.
	 * 
	 * @param event the {@link PluginLoadedEvent}
	 */
	@Subscribe
	private void onPluginLoaded(PluginLoadedEvent event) {
		// System.out.println("Plugin Loaded: " + event.plugin);
	}

	@Subscribe
	private void onPluginsLoaded(PluginsFinishedLoadingEvent event) {
		// System.out.println("Done loading plugins!");
		for (IBasePlugin plugin : event.plugins)
			if (plugin instanceof ToolData) {
				if (plugin.getPluginID().equals("tool:id:move")) {
					setTool((ToolData) plugin);
				}
			}
	}

	/**
	 * Called whenever the user selects a new {@link ToolData} from the menu bar.
	 */
	@Subscribe
	private void onToolChanged(GraphToolChangedEvent event) {
		setTool(event.tool);
	}

	/**
	 * Sets the given tool as active tool in the nodesystem
	 * 
	 * @param tool the tool to set active
	 */
	public void setTool(ToolData tool) {
		if (currentTool != null) {
			// currentTool.freeTool();
			// getNodeGraph().getEventBus().unregister(currentTool);
			getBaseLayer().setCursor(Cursor.DEFAULT);
			if (currentToolNode != null)
				widgetLayer.getChildren().remove(currentToolNode);
			currentToolNode = null;
		}

		currentTool = tool;
		// currentTool.initTool(this);
		// getNodeGraph().getEventBus().register(currentTool);
		currentToolNode = currentTool.draw();
		if (currentToolNode != null) {
			currentToolNode.setId("_current_tool_");
			currentToolNode.layoutXProperty().bind(mouseX);
			currentToolNode.layoutYProperty().bind(mouseY);
			currentToolNode.setMouseTransparent(true);
			widgetLayer.getChildren().add(currentToolNode);
		}

		if (currentTool.getIcon() != null) {
			ImageCursor cursor = new ImageCursor(currentTool.getIcon(), 30, 30);
			getBaseLayer().setCursor(cursor);
		} else {
			getBaseLayer().setCursor(Cursor.DEFAULT);
		}
		widgetLayer.setMouseTransparent(true);
	}

	/**
	 * Returns the currently active tool.
	 * 
	 * @return the active tool
	 */
	public ToolData getTool() {
		return currentTool;
	}

	/**
	 * Part of the event system. Called for every {@link SceneChangedEvent}. Called
	 * when the scene has changed (zoom/moved). Used to redraw the background.
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onSceneChanged(SceneChangedEvent event) {
		backgroundLayer.draw();
	}

	/**
	 * UPDATE: 08.02.2018 Moved into DrawSignal! <br>
	 * Part of the event system. Called for every {@link ResetSignalColorEvent}.
	 * Called when a node connection exits a port to reset the color of the signal
	 * 
	 * @param e the event
	 * 
	 * @Subscribe private void onResetSignalColor(ResetSignalColorEvent e) { if
	 * (tempSignal != null) tempSignal.setStroke(e.color); }
	 */

	/**
	 * UPDATE: 08.02.2018 Copied into DrawSignal! <br>
	 * Part of the event system. Called for every {@link SignalTryConnectionEven}.
	 * Called when a node connection enters a port to change the color of the signal
	 * accordingly
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onTrySignalConnection(SignalTryConnectionEven e) {
		if (tempSignal != null)
			if (e.wouldWork) {
				postMessage("Connection: " + e.from.getBackendID() + " to " + e.to.getBackendID() + " possible!");
			} else {
				postMessage("Connection: " + e.from.getBackendID() + " to " + e.to.getBackendID() + " NOT possible!");
			}
	}

	/**
	 * Part of the event system. Called for every {@link NodeAddedEvent}. Called
	 * when a node has been added to the backend.
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onNodeAdded(NodeAddedEvent e) {
		postStatus("Adding new Node: " + e.nodeID);
		AbstractNode bNode = backendNodeGraph.getNodeByID(e.nodeID).orElseThrow(() -> new NoSuchNode(e.nodeID));
		Map<String, INodeInputPort> inPorts = bNode.getInputs();
		Map<String, INodeOutputPort> outPorts = bNode.getOutputs();
		INodeType type = bNode.getType();
		String id = bNode.getId();

		BaseNode node = new BaseNode(type, id, id, graphConnector);
		for (String stringID : inPorts.keySet()) {
			INodeInputPort inPortInterface = inPorts.get(stringID);
			boolean isField = backendNodeGraph.getField(inPortInterface.getID()).isPresent();
			BaseInputPort inPort = new BaseInputPort(inPortInterface.getID(), inPortInterface.getName(), node,
					inPortInterface.getType(), isField);
			node.addPort(inPort);
		}
		for (String stringID : outPorts.keySet()) {
			INodeOutputPort inPortInterface = outPorts.get(stringID);
			boolean isField = backendNodeGraph.getField(inPortInterface.getID()).isPresent();
			BaseOutputPort inPort = new BaseOutputPort(inPortInterface.getID(), inPortInterface.getName(), node,
					inPortInterface.getType(), isField);
			node.addPort(inPort);
		}
		boolean loadPossible = true;
		try {
			backendNodeGraph.getMaster().getModulePathOfNode(type);
		} catch (Exception a2) {
			loadPossible = false;
		}
		if (loadPossible) {
			try (FileSystem fs = FileSystems.newFileSystem(backendNodeGraph.getMaster().getModulePathOfNode(type),
					null)) {
				Path moduledefs = fs.getPath(CONTAINER_PATH);
				try (BufferedReader in = Files.newBufferedReader(moduledefs)) {
					settingsContainer = GuiLangParser.readContainer(in);
					setupNewNodeAndAdd(settingsContainer, node);

				} catch (IOException a) {
					settingsContainer = new GLContainer();
					setupNewNodeAndAdd(settingsContainer, node);

				}
			} catch (IOException e1) {
				settingsContainer = new GLContainer();
				setupNewNodeAndAdd(settingsContainer, node);

			}
			// TODO: Android = Linux ~ Linux != Android
		} else {
			settingsContainer = new GLContainer();
			setupNewNodeAndAdd(settingsContainer, node);
		}

		postMessage("Added Node: Type: " + type.name() + ", ID: " + id);
		postStatus("Number of Nodes: " + backendNodeGraph.getNodeByIDMap().size());

	}

	@Subscribe
	private void onNodeConnectionChange(NodeChangeConnectionEvent e) {
		DrawPort port = nodeCache.getPort(e.port.getPort().getBackendID());
		List<DrawSignal> signals = getSignalFromPort(port);

		if (signals.size() > 0) {
			DrawSignal signal = signals.get(0);
			graphConnector.disconnectFromInput(e.port.getPort());
			if (graphConnector.getFirst() == null) {
				graphConnector.beginConnection(signal.getSignal().getOutputPortProperty().get());
			} else {
				graphConnector.abortConnection();
			}
		}

	}

	/**
	 * Part of the event system. Called for every {@link NodeConnectionStartEvent}.
	 * Called when a node connection has been started.
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onNodeConnectionStart(NodeConnectionStartEvent e) {
		@SuppressWarnings("unused")
		NodePortId npid = new NodePortId(e.portID);
		DrawPort port = nodeCache.getPort(e.portID);
		DrawNode node = getNode(port.getPort().getParentNode().getBackendID());

		BaseSignal signal = new BaseSignal(port.getPort(), port.isInputPort());

		Tuple<Double, Double> coords = Util.calculateSignalPositionBasedOnPort(port, node);

		tempSignal = new DrawSignal(signal, node.getSettings(), coords.a, coords.b, backendNodeGraph.getEventBus());
		tempSignal.endXProperty().set(coords.a);
		tempSignal.endYProperty().set(coords.b);
		tempSignal.controlX1Property().set(coords.a + 10D);
		tempSignal.controlY1Property().set(coords.b);
		tempSignal.controlX2Property().set(coords.a - 10D);
		tempSignal.controlY2Property().set(coords.b);
		tempSignalLayer.getChildren().add(tempSignal);
		// attachTempSignalToMouse();

		postMessage("Started Connection: " + port.getPort().getBackendID());
	}

	/**
	 * Part of the event system. Called for every {@link NodeConnectedEvent}. Called
	 * when a node has been connected to another node
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onNodeConnection(NodeConnectedEvent e) {
		removeTempSignal();
		GuiState.setState(GuiState.DEFAULT);
		Tuple<Optional<DrawNode>, Optional<DrawPort>> _inNodePort = getNodePortById(e.inPortID);
		DrawNode inNode = _inNodePort.a.get();
		DrawPort inPort = _inNodePort.b.get();

		Tuple<Optional<DrawNode>, Optional<DrawPort>> _outNodePort = getNodePortById(e.outPortID);
		DrawNode outNode = _outNodePort.a.get();
		DrawPort outPort = _outNodePort.b.get();

		BaseSignal signal = new BaseSignal(inPort.getPort(), outPort.getPort());
		DrawSignal drawSignal = new DrawSignal(signal, outNode.getColorPorperty(), settingsContainer,
				backendNodeGraph.getEventBus());

		drawSignal.startXProperty().bind(inNode.layoutXProperty().add(inPort.layoutXProperty()));
		drawSignal.startYProperty().bind(inNode.layoutYProperty().add(inPort.layoutYProperty()));
		drawSignal.controlX1Property().bind(inNode.layoutXProperty().add(inPort.layoutXProperty().subtract(100d)));
		drawSignal.controlY1Property().bind(inNode.layoutYProperty().add(inPort.layoutYProperty()));

		drawSignal.controlX2Property().bind(outNode.layoutXProperty()
				.add(outPort.layoutXProperty().add(outPort.getSettings().getNode().getWidth()).add(100d)));
		drawSignal.controlY2Property().bind(outNode.layoutYProperty().add(outPort.layoutYProperty()));
		drawSignal.endXProperty().bind(outNode.layoutXProperty()
				.add(outPort.layoutXProperty().add(outPort.getSettings().getNode().getWidth())));
		drawSignal.endYProperty().bind(outNode.layoutYProperty().add(outPort.layoutYProperty()));

		GLShadow shadow = drawSignal.getSettings().getSignal().getSignalShadow();
		if (shadow.isUse_shadow()) {
			DropShadow shadowE = new DropShadow();
			shadowE.setBlurType(BlurType.GAUSSIAN);
			shadowE.setColor(shadow.getShadow_color());
			shadowE.setWidth(shadow.getShadow_width());
			shadowE.setHeight(shadow.getShadow_height());
			shadowE.setOffsetX(shadow.getShadow_offset_x());
			shadowE.setOffsetY(shadow.getShadow_offset_y());
			shadowE.setRadius(shadow.getShadow_radius());
			drawSignal.setEffect(shadowE);
		}
		nodeLayer.getChildren().add(drawSignal);
		fillCircleOnSucessfullConnection(inPort);
		inPort.onPortFieldHide();
		inPort.connectedProperty().set(true);
		outPort.connectedProperty().set(true);
		postMessage("Connected Nodes: " + inNode.getNode().getName() + " and " + outNode.getNode().getName());

	}

	/**
	 * Part of the event system. Called for every {@link NodeRenameEvent}. Called
	 * when a node should be renamed
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onNodeRename(NodeRenameEvent e) {

		DrawNode node = getNode(e.nodeID);
		TextInputDialog dialog = new TextInputDialog(node.getNode().getName());
		dialog.setTitle("Rename Node: " + node.getNode().getName());
		dialog.setHeaderText("Rename");
		dialog.setContentText("Please enter your name:");
		Optional<String> result = dialog.showAndWait();
		result.ifPresent(name -> node.setName(name));

	}

	/**
	 * Part of the event system. Called for every {@link NodeDisconnectedEvent}.
	 * Called when a signal should be disconnected.
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onNodeDisconnected(NodeDisconnectedEvent e) {

		Tuple<Optional<DrawNode>, Optional<DrawPort>> _inNodePort = getNodePortById(e.inPortID);
		DrawPort inPort = _inNodePort.b.get();

		Tuple<Optional<DrawNode>, Optional<DrawPort>> _outNodePort = getNodePortById(e.outPortID);
		DrawPort outPort = _outNodePort.b.get();
		DrawSignal tmp = null;

		for (Node signal : nodeLayer.getChildren()) {
			if (signal instanceof DrawSignal) {
				DrawSignal _signal = (DrawSignal) signal;
				if (_signal.getSignal().getInputPortProperty().get().equals(inPort.getPort())
						&& (_signal.getSignal().getOutputPortProperty().get().equals(outPort.getPort()))) {
					tmp = _signal;
				}
			}
		}

		if (tmp != null) {

			FadeTransition ft = new FadeTransition(Duration.millis(250), tmp);
			ft.setFromValue(1.0);
			ft.setToValue(0.0);
			ft.setAutoReverse(true);
			ft.play();
			final DrawSignal lambda_tmp = tmp;
			ft.setOnFinished(event -> {

				nodeLayer.getChildren().remove(lambda_tmp);
			});

			fillCircleOnDisconnect(inPort);
		}
		inPort.connectedProperty().set(false);
		outPort.connectedProperty().set(false);
		postMessage(
				"Disconnected Ports: " + inPort.getPort().getBackendID() + " and " + outPort.getPort().getBackendID());
		inPort.onPortFieldShow();

	}

	/**
	 * Part of the event system. Called for every {@link NodeMenuRemoveEvent}.
	 * Called when a node should be removed through the menu on the node itself
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onNodeRemovedByNodeMenu(NodeMenuRemoveEvent e) {
		deleteNode(getNode(e.nodeID));
	}

	/**
	 * Part of the event system. Called for every {@link NodeDeletedEvent}. Called
	 * when a node was deleted in the back end.
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onNodeDeleted(NodeDeletedEvent e) {
		DrawNode n = getNode(e.nodeID);
		nodeLayer.getChildren().remove(n);
		postMessage("Removed Node: " + n.getNode().getName());
		postStatus("Number of Nodes: " + backendNodeGraph.getNodeByIDMap().size());
	}

	/**
	 * Called when a node calculation is being started.
	 * 
	 * @param e
	 */
	@Subscribe
	private void onNodeComputationStart(NodeComputationStartEvent e) {
		DrawNode node = getNode(e.nodeID);
		node.startProgress();
	}

	/**
	 * Called when the backend encountered a problem while node computation
	 * 
	 * @param e
	 */
	@Subscribe
	private void onNodeComputationError(NodeComputationErrorEvent e) {
		DrawNode node = getNode(e.nodeID);
		System.out.println(e.nodeID);
		// selectNode(node);
		node.errorProgress();

	}

	/**
	 * Called when the node computation has been finished
	 * 
	 * @param e
	 */
	@Subscribe
	private void onNodeComputationFinished(NodeComputationFinishedEvent e) {
		DrawNode node = getNode(e.nodeID);
		node.finishProgress();
	}

	/**
	 * Part of the event system. Called for every {@link LoadEvent}. Called when a
	 * graph is loaded. This method is used to read the .pos file which holds the
	 * position information of the nodes
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onLoad(LoadEvent e) {
		Path posFile = e.folder.resolve(".pos");
		BufferedReader reader = null;
		try {
			reader = Files.newBufferedReader(posFile);
			readPosFile(reader);
		} catch (IOException e1) {
			System.err.println("Error Loading File: " + posFile);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
					System.err.println("Error Loading File: " + posFile);
				}
			}
		}
		Path colFile = e.folder.resolve(".col");
		try {
			reader = Files.newBufferedReader(colFile);
			readColorFile(reader);
		} catch (IOException e1) {
			System.err.println("Error Loading File: " + colFile);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
					System.err.println("Error Loading File: " + colFile);
				}
			}
		}

		Path grpFile = e.folder.resolve(".grp");
		try {
			reader = Files.newBufferedReader(grpFile);
			readGrPFile(reader);
		} catch (IOException e1) {
			System.err.println("Error Loading File: " + grpFile);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
					System.err.println("Error Loading File: " + grpFile);
				}
			}
		}
		Path nmeFile = e.folder.resolve(".nme");
		try {
			reader = Files.newBufferedReader(nmeFile);
			readNMEFile(reader);
		} catch (IOException e1) {
			System.err.println("Error Loading File: " + nmeFile);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
					System.err.println("Error Loading File: " + nmeFile);
				}
			}
		}
	}

	private void deleteForSaving(Path p) {
		try {
			Files.deleteIfExists(p);
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}

	/**
	 * Part of the event system. Called for every {@link SaveEvent}. Called when a
	 * graph is loaded. This method is used to write the .pos file which holds the
	 * position information of the nodes
	 * 
	 * @param e the event
	 */
	@Subscribe
	private void onSave(SaveEvent e) {
		Path posFile = e.saveFolder.resolve(".pos");
		Path colFile = e.saveFolder.resolve(".col");
		Path grpFile = e.saveFolder.resolve(".grp");
		Path nmeFile = e.saveFolder.resolve(".nme");
		deleteForSaving(nmeFile);
		deleteForSaving(grpFile);
		deleteForSaving(posFile);
		deleteForSaving(colFile);

		BufferedWriter writer = null;
		try {
			writer = Files.newBufferedWriter(posFile);
			writePosFile(writer);
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
			if (writer != null)
				try {
					writer.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		}
		try {
			writer = Files.newBufferedWriter(colFile);
			writeColorFile(writer);
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
			if (writer != null)
				try {
					writer.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		}
		try {
			writer = Files.newBufferedWriter(grpFile);
			writeGrPFile(writer);
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
			if (writer != null)
				try {
					writer.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		}

		try {
			writer = Files.newBufferedWriter(nmeFile);
			writeNMEFile(writer);
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
			if (writer != null)
				try {
					writer.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		}
	}

	/**
	 * Reads a .nme file.
	 * 
	 * @param reader the already initialized reader
	 * @throws IOException
	 */
	private void readNMEFile(BufferedReader reader) throws IOException {
		if (reader != null) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				String[] _read = line.split(";");
				if (!(_read.length <= 1)) {
					String backendID = _read[0];
					String name = _read[1];

					DrawNode node = getNode(backendID);
					if (node != null) {
						if (node.getNode().getNameProperty().isBound())
							node.getNode().getNameProperty().unbind();
						node.getNode().setName(name);
					}
				}
			}
		}
	}

	/**
	 * Reads a .col file.
	 * 
	 * @param reader the already initialized reader
	 * @throws IOException
	 */
	private void readColorFile(BufferedReader reader) throws IOException {
		if (reader != null) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				String[] _read = line.split(";");
				if (!(_read.length <= 1)) {
					String backendID = _read[0];
					Color c = Color.web(_read[1]);

					DrawNode node = getNode(backendID);
					if (node != null) {
						node.getColorPorperty().set(c);
					}
				}
			}
		}
	}

	/**
	 * Reads a .pos file.
	 * 
	 * @param reader the already initialized reader
	 * @throws IOException
	 */
	private void readPosFile(BufferedReader reader) throws IOException {
		if (reader != null) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				String[] _read = line.split(";");
				if (!(_read.length <= 2)) {
					String backendID = _read[0];
					double posX = Double.valueOf(_read[1]);
					double posY = Double.valueOf(_read[2]);

					DrawNode node = getNode(backendID);
					if (node != null) {
						node.setLayoutX(posX);
						node.setLayoutY(posY);
					}
				}
			}
		}
	}

	/**
	 * Reads a .grp file.
	 * 
	 * @param reader the already initialized reader
	 * @throws IOException
	 */
	private void readGrPFile(BufferedReader reader) throws IOException {
		if (reader != null) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				String[] _read = line.split(";");
				if (!(_read.length <= 2)) {
					double scale = Double.valueOf(_read[0]);
					double posX = Double.valueOf(_read[1]);
					double posY = Double.valueOf(_read[2]);

					drawLayer.setScale(scale);
					drawLayer.setTranslateX(posX);
					drawLayer.setTranslateY(posY);
				}
			}
		}
	}

	/**
	 * Writes a .grp file
	 * 
	 * @param writer the already initialized writer
	 * @throws IOException
	 */
	private void writeGrPFile(BufferedWriter writer) throws IOException {
		if (writer != null) {
			double scale = drawLayer.getScale();
			double x = drawLayer.getTranslateX();
			double y = drawLayer.getTranslateY();
			writer.write(scale + ";" + x + ";" + y + System.lineSeparator());

		}
	}

	/**
	 * Writes a .nme file
	 * 
	 * @param writer the already initialized writer
	 * @throws IOException
	 */
	private void writeNMEFile(BufferedWriter writer) throws IOException {
		if (writer != null) {
			for (Node node : nodeLayer.getChildren()) {
				if (node instanceof DrawNode) {
					DrawNode drawNode = (DrawNode) node;
					String name = drawNode.getNode().getName();
					String id = drawNode.getNode().getBackendID();
					writer.write(id + ";" + name + System.lineSeparator());
				}
			}
		}
	}

	/**
	 * Writes a .col file
	 * 
	 * @param writer the already initialized writer
	 * @throws IOException
	 */
	private void writeColorFile(BufferedWriter writer) throws IOException {
		if (writer != null) {
			for (Node node : nodeLayer.getChildren()) {
				if (node instanceof DrawNode) {
					DrawNode drawNode = (DrawNode) node;
					Color c = (Color) drawNode.getColorPorperty().get();
					String id = drawNode.getNode().getBackendID();
					writer.write(id + ";" + c + System.lineSeparator());
				}
			}
		}
	}

	/**
	 * Writes a .pos file
	 * 
	 * @param writer the already initialized writer
	 * @throws IOException
	 */
	private void writePosFile(BufferedWriter writer) throws IOException {
		if (writer != null) {
			for (Node node : nodeLayer.getChildren()) {
				if (node instanceof DrawNode) {
					DrawNode drawNode = (DrawNode) node;
					double posX = drawNode.getLayoutX();
					double posY = drawNode.getLayoutY();
					String id = drawNode.getNode().getBackendID();
					writer.write(id + ";" + posX + ";" + posY + System.lineSeparator());
				}
			}
		}
	}

	/**
	 * Called when a node should be added from an entry
	 * 
	 * @param ent the entry which holds the node id
	 */
	void addNodeFromEntry(Entry ent) {
		backendNodeGraph.addNode(ent.getName());
	}

	/**
	 * Called in {@link #onNodeAdded(NodeAddedEvent)}. Used to setup and position
	 * the node.
	 * 
	 * @param settingsContainer the settings of the node
	 * @param node the base node
	 */
	private void setupNewNodeAndAdd(GLContainer settingsContainer, BaseNode node) {
		DrawNode drawNode = new DrawNode(globalBus, backendNodeGraph.getEventBus(), node, settingsContainer,
				graphConfig.touchEnabledProperty().get());

		// drawNode.setLayoutX(nodeDragContext.translateAnchorX + (10D * copyCounter));
		// drawNode.setLayoutY(nodeDragContext.translateAnchorY + (10D * copyCounter));
		drawNode.setLayoutX(mouseX.get() + (10D * copyCounter));
		drawNode.setLayoutY(mouseY.get() + (10D * copyCounter));

		drawNode.bindScale(drawLayer.getScaleProperty());
		nodeLayer.getChildren().add(drawNode);
		postMessage("Created DrawNode");
	}

	/**
	 * Returns the draw node associated with the given backend id.
	 * 
	 * @param nodeID the backend id of the node
	 * @return the associated {@link DrawNode}
	 */
	public DrawNode getNode(String nodeID) {
		return nodeCache.getNode(nodeID);
	}

	/**
	 * Attaches the temp signal, which is visible during connections, to the current
	 * mouse.
	 */
	private void attachTempSignalToMouse() {

		baseLayer.setOnMousePressed(event -> {
			if (GuiState.getState().equals(GuiState.DEFAULT)) {
				if (event.isPrimaryButtonDown()) {
					if (event.getTarget() instanceof Canvas || event.getTarget() instanceof DrawLayer
							|| event.getTarget() instanceof NodeLayer) {

						selectionWidget.setVisible(true);
						anchor.setX(event.getX());
						anchor.setY(event.getY());
						selectionWidget.setX(event.getX());
						selectionWidget.setY(event.getY());
						selectionWidget.toFront();
						backendNodeGraph.getEventBus().post(new GraphDeselectAllEvent());
					}

				}
			}
			if (tempSignal != null) {
				nodeDragContext.mouseAnchorX = event.getX();
				nodeDragContext.mouseAnchorY = event.getY();

				nodeDragContext.translateAnchorX = tempSignal.getStartX();
				nodeDragContext.translateAnchorY = tempSignal.getStartY();
			}

		});

		baseLayer.setOnMouseDragged(event -> {

			if (event.isPrimaryButtonDown()) {
				if (event.getTarget() instanceof Canvas || event.getTarget() instanceof DrawLayer
						|| event.getTarget() instanceof NodeLayer) {
					selectionWidget.setWidth(Math.abs(event.getX() - anchor.getX()));
					selectionWidget.setHeight(Math.abs(event.getY() - anchor.getY()));
					selectionWidget.setX(Math.min(anchor.getX(), event.getX()));
					selectionWidget.setY(Math.min(anchor.getY(), event.getY()));
					selectionWidget.toFront();
					backendNodeGraph.getEventBus().post(new GraphRectangleSelectionEvent(selectionWidget));

				}
			}

			mouseX.set(nodeDragContext.translateAnchorX
					+ ((event.getX() - nodeDragContext.mouseAnchorX) / drawLayer.getScale()));

			mouseY.set(nodeDragContext.translateAnchorY
					+ ((event.getY() - nodeDragContext.mouseAnchorY) / drawLayer.getScale()));

			if (tempSignal != null) {
				if (tempSignal.getSignal().isInputPortOnConnectionStart()) {
					tempSignal.controlX1Property().set(tempSignal.getStartX() - 10D);
					tempSignal.controlY1Property().set(tempSignal.getStartY());
					tempSignal.controlX2Property().set(mouseX.get() + 10D);
					tempSignal.controlY2Property().set(mouseY.get());
				} else {
					tempSignal.controlX2Property().set(mouseX.get() - 10D);
					tempSignal.controlY2Property().set(mouseY.get());
					tempSignal.controlX1Property().set(tempSignal.getStartX() + 10D);
					tempSignal.controlY1Property().set(tempSignal.getStartY());
				}
				if (!tempSignal.snappedProperty().get()) {

					tempSignal.endXProperty().bind(mouseX);
					tempSignal.endYProperty().bind(mouseY);
				}
			}
		});

		baseLayer.setOnMouseReleased(event -> {
			removeTempSignal();
			graphConnector.abortConnection();

		});

		baseLayer.setOnMouseDragReleased(event -> {
			removeTempSignal();
			graphConnector.abortConnection();

		});
	}

	/**
	 * Removes the temp signal. Called after the connection has been sucessfull or
	 * aborted
	 */
	private void removeTempSignal() {
		tempSignalLayer.getChildren().clear();
		tempSignal = null;

	}

	/**
	 * Called when a conenction was successfull
	 * 
	 * @param inPort
	 */
	private void fillCircleOnSucessfullConnection(DrawPort inPort) {
		inPort.getPortCircle().setFill(inPort.getSettings().getPort().getInput().getColor());
	}

	/**
	 * Called when a connection was removed
	 * 
	 * @param inPort
	 */
	private void fillCircleOnDisconnect(DrawPort inPort) {
		inPort.getPortCircle().setFill(
				inPort.getSettings().getPort().getInput().getColor().darker().darker().desaturate().desaturate());
	}

	/**
	 * Deletes the given node. This should be called after it has been removed from
	 * the backend!
	 * 
	 * @param _node the node to remove
	 */
	private void deleteNode(DrawNode _node) {
		DrawNode node = getNode(_node.getNode().getBackendID());
		if (nodeLayer.getChildren().contains(node)) {

			for (BasePort port : node.getNode().getInputPorts())
				graphConnector.disconnectFromInput(port);

			for (BasePort port : node.getNode().getOutputPorts())
				graphConnector.disconnectFromOutput(port);

			FadeTransition ft = new FadeTransition(Duration.millis(100), node);
			ft.setFromValue(1.0);
			ft.setToValue(0.0);
			ft.setAutoReverse(true);
			ft.play();
			ft.setOnFinished(event -> {
				backendNodeGraph.removeNode(_node.getNode().getBackendID());
			});
		}
	}

	/**
	 * Deslectes a single node from the selction list.
	 * 
	 * @param node the node do deselect
	 */
	public void deselectNode(DrawNode node) {
		node.setSelected(false);
		deselectSignals(node);
		selected.remove(node);

	}

	/**
	 * Gets the node and port ids.
	 * 
	 * @param id the id
	 * @return
	 */
	private Tuple<Optional<DrawNode>, Optional<DrawPort>> getNodePortById(String id) {
		NodePortId npid = new NodePortId(id);
		Optional<DrawNode> _inNode = Optional.ofNullable(getNode(npid.node));
		Optional<DrawPort> _inPort = Optional.ofNullable(getPort(id));

		return new Tuple<Optional<DrawNode>, Optional<DrawPort>>(_inNode, _inPort);
	}

	/**
	 * Returns a {@link DrawSignal} which is associated with the given port id or
	 * null if none was found!
	 * 
	 * @param portID the port id
	 * @return the associated signal
	 */
	private List<DrawSignal> getSignalFromPort(String portID) {
		Tuple<Optional<DrawNode>, Optional<DrawPort>> _inNodePort = getNodePortById(portID);
		if (_inNodePort.b != null) {
			DrawPort port = _inNodePort.b.get();
			List<DrawSignal> signalList = new ArrayList<DrawSignal>();
			for (Node signal : nodeLayer.getChildren()) {
				if (signal instanceof DrawSignal) {
					DrawSignal _signal = (DrawSignal) signal;
					if (_signal.getSignal().getInputPortProperty().get().equals(port.getPort())
							|| (_signal.getSignal().getOutputPortProperty().get().equals(port.getPort()))) {
						signalList.add(_signal);
					}
				}
			}

			return signalList;
		}
		return null;
	}

	/**
	 * Returns the signal corresponding to the {@link BaseSignal} given.
	 * 
	 * @param signal the base signal
	 * @return the corresponding {@link DrawSignal}
	 */
	private DrawSignal getSignal(BaseSignal signal) {

		for (Node n : nodeLayer.getChildren()) {
			if (n instanceof DrawSignal) {
				DrawSignal sig = (DrawSignal) n;
				if (sig.getSignal().equals(signal)) {
					return sig;
				}
			}
		}
		return null;

	}

	/**
	 * Returns a {@link DrawSignal} which is associated with the given port id or
	 * null if none was found!
	 * 
	 * @param portID the port id
	 * @return the associated signal
	 */

	private List<DrawSignal> getSignalFromPort(DrawPort port) {
		List<DrawSignal> signalList = new ArrayList<DrawSignal>();
		for (Node signal : nodeLayer.getChildren()) {
			if (signal instanceof DrawSignal) {
				DrawSignal _signal = (DrawSignal) signal;
				if (_signal.getSignal().getInputPortProperty().get().equals(port.getPort())
						|| (_signal.getSignal().getOutputPortProperty().get().equals(port.getPort()))) {
					signalList.add(_signal);

				}
			}
		}
		return signalList;
	}

	/**
	 * Returns the {@link DrawLayer} of the graph. The DrawLayer has the zoom/scale
	 * information stored.
	 * 
	 * @return the drawLayer
	 */
	private DrawLayer getDrawLayer() {
		return drawLayer;
	}

	// ========== PUBLIC METHODS ==========

	/**
	 * Opens a widget at the current mouse position.
	 */
	@Subscribe
	private void onWidgetOpen(WidgetOpenEvent event) {
		WidgetData widget = event.widget;
		if (!widgetLayer.getChildren().contains(widget.getLayout())) {
			widget.initWidget(this);
			backendNodeGraph.getEventBus().register(widget);
			widget.open(mouseX.get(), mouseY.get());
			widgetLayer.getChildren().add(widget.getLayout());
			widgetLayer.setMouseTransparent(false);
		}
	}

	/**
	 * Moves to the origin of the graph. This can be helpful if the user moved too
	 * far away and cant find back.
	 */
	public void moveToOrigin() {
		drawLayer.setTranslateX(0);
		drawLayer.setTranslateY(0);
		drawLayer.setScale(1.0D);
		backgroundLayer.draw();
	}

	/**
	 * Selects all nodes inside the nodegraph.
	 * 
	 */
	public void selectAll() {
		for (Node n : nodeLayer.getChildren()) {
			if (n instanceof DrawNode) {
				((DrawNode) n).setSelected(true);
				selected.add((DrawNode) n);
				selectSignals((DrawNode) n);
			}
		}
	}

	/**
	 * Creates a completely new graph. All data, nodes and signals from the previous
	 * graph are lost. <br>
	 * <b>Note: Make sure to save the graph before creating a new one.</b>
	 */
	public void createNewGraph() {
		backendNodeGraph.getEventBus().unregister(this);
		deselectAll();
		clearLayers();
		backendNodeGraph.delete();
		backendNodeGraph = backendNodeGraph.getMaster().createNewGraph("root");
		setupEventBusSystem();
		initGestures();
		initialized = false;
		init();
	}

	/**
	 * Deselects all nodes inside the nodegraph.
	 */
	public void deselectAll() {
		for (DrawNode n : selected) {
			n.setSelected(false);
			deselectSignals(n);
		}
		selected.clear();
	}

	/**
	 * Copies all currently selected nodes.
	 */
	public void copyNodes() {
		if (selected.size() > 0) {
			clipboard.copy(selected);
			nodeDragContext.translateAnchorX = selected.get(0).getLayoutX();
			nodeDragContext.translateAnchorY = selected.get(0).getLayoutY();
			copyCounter = 1;
		}
	}

	/**
	 * Pastes all nodes that are stored within the given list into the nodegraph.
	 */
	public void pasteNodes(List<DrawNode> nodes) {
		for (DrawNode node : nodes) {
			backendNodeGraph.addNode(node.getNode().getType());

		}
	}

	/**
	 * Pastes all nodes that are stored within the clipboard into the nodegraph.
	 */
	public void pasteNodes() {
		pasteNodes(clipboard.paste());
		copyCounter++;
	}

	/**
	 * Removes all currently selected nodes.
	 */
	public void deleteSelectedNodes() {
		if (selected.size() > 0)
			for (DrawNode node : selected) {
				deleteNode(getNode(node.getBackendID()));
			}
		selected.clear();
	}

	/**
	 * Can be used to send Events to the nodesystem.
	 * 
	 * @param event the event class to send
	 */
	public void postEvent(Object event) {
		getGlobalEventBus().post(event);
	}

	/**
	 * Posts a message to the {@link MessageLayer}. The message is displayed on the
	 * bottom left for 2.5 seconds and then fades away. This is intended to give the
	 * user short feedback.
	 * 
	 * @param message the message to post
	 */
	public void postMessage(String message) {
		messageLayer.postMessage(message);
		globalBus.post(new MessageEvent(message));
	}

	/**
	 * Posts a status message to the {@link MessageLayer}. The status appears on the
	 * bottom right of the {@link MessageLayer}. Unless a new status message is
	 * posted the old one will remain indefinitely.
	 * 
	 * @param status the status
	 */
	public void postStatus(String status) {
		messageLayer.postStatus(status);
		globalBus.post(new MessageEvent(status));
	}

	/**
	 * Saves the current graph into a folder.
	 * 
	 * @param p the folder to save the graph into
	 * @throws IOException
	 */
	public void saveGraph(Path p) throws IOException {
		backendNodeGraph.getMaster().saveGraphToFile(backendNodeGraph, p);
		postMessage("Saved graph to: " + p);
		nameProperty.set(Util.getNameWithoutExtension(p.toFile()));
	}

	/**
	 * Loads a FOLDER where a graph has been stored to. The folder needs all
	 * necessary files (.ng, .nl, .pos).
	 * 
	 * @param p the path to the folder
	 * @throws NoSuchNode
	 * @throws IOException
	 * @throws NodeLangException
	 */
	public void loadGraphFromFolder(Path p) throws NoSuchNode, IOException, NodeLangException {
		backendNodeGraph.getMaster().loadGraphFromFile(p, this.backendNodeGraph);
		postMessage("Loaded graph from: " + p);
		nameProperty.set(Util.getNameWithoutExtension(p.toFile()));
	}

	/**
	 * Sets the backend node-graph. This allows to open different already loaded
	 * graphs.
	 * 
	 * @param graph the graph to set
	 */
	public void setBackendGraph(NodeGraph graph) {
		backendNodeGraph.getEventBus().unregister(this);
		deselectAll();
		clearLayers();
		backendNodeGraph = graph;
		setupEventBusSystem();
		initGestures();
		initialized = false;
		init();
	}

	/**
	 * Loads a .nl (NodeLang) file into the graph. Position will be ignored.
	 * 
	 * @param p the path to the NodeLang file.
	 * @throws NoSuchNode
	 * @throws IOException
	 * @throws NodeLangException
	 */
	public void importGraphFromNodelang(Path p) throws NoSuchNode, IOException, NodeLangException {
		backendNodeGraph.getMaster().importGraphFromNodelang(p, backendNodeGraph);
		postMessage("Loaded graph from: " + p);
	}

	/**
	 * Returns the backend node-graph.
	 * 
	 * @see NodeGraph
	 * @return the backend node-graph
	 */
	public NodeGraph getNodeGraph() {
		return backendNodeGraph;
	}

	/**
	 * Returns the {@link BaseLayer} of the graph. Use this {@link Layer} to add the
	 * graph into your program.
	 * 
	 * @return the base layer of the graph
	 */
	public Layer getBaseLayer() {
		return baseLayer;
	}

	/**
	 * Returns the top layer of the graph. This is used eg. for the graph's menu
	 * bar. You can display/add any widget you want! But the top layer is NOT mouse
	 * transparent!
	 * 
	 * @return the top most layer of the graph
	 */
	public Layer getTopLayer() {
		return widgetLayer;
	}

	/**
	 * Returns the {@link NodeLayer}. This layer holds every {@link DrawNode} of the
	 * graph. The {@link DrawNode} is the graphical representation of the
	 * backend-nodes.
	 * 
	 * @return the node layer
	 */
	public NodeLayer getNodeLayer() {
		return nodeLayer;
	}

	/**
	 * Returns the {@link SignalLayer} of the graph. The {@link SignalLayer} has
	 * every {@link DrawSignal}. A {@link DrawSignal} is the graphical
	 * representation of the connection between the nodes.
	 * 
	 * @return the signal layer
	 */
	// public SignalLayer getSignalLayer() {
	// return signalLayer;
	// }

	/**
	 * Returns an <b>unmodifiable</b> {@link ObservableList} filled with all
	 * {@link DrawNode}s of the current graph. <br>
	 * <b> Note: The list has {@link Node} as generic type, this method does not
	 * check whether the returned list only contains {@link DrawNode}s or not. </b>
	 * 
	 * @return list with all {@link DrawNode}s
	 */
	public ObservableList<Node> getNodeList() {
		return FXCollections.unmodifiableObservableList(nodeLayer.getChildren());
	}

	/**
	 * Used internal to calculate a specific node inside of the graph. The given id
	 * has to be available to the backend. Meaning it must be a valid node id.
	 * 
	 * @param id the backend id of the node
	 * @return a map of the calculated node and its result
	 */
	public Map<String, Object> executeNode(String id) {
		Map<String, Object> mp = null;
		try {
			mp = backendNodeGraph.computeNode(id);
			if (mp != null) {
				messageLayer.postStatus("Last Calculation " + mp.entrySet());
				System.out.println(mp.entrySet());
			}
		} catch (Exception e) {
			messageLayer.postStatus("Error while calculation!");

		}
		return mp;
	}

	/**
	 * Calculates the whole graph and returns the calculated result.
	 * 
	 * @return a map of the calculated node and its result
	 */
	public Map<String, Object> executeGraph() {
		Map<String, Object> mp = null;
		try {
			mp = backendNodeGraph.executeGraph();
			if (mp != null) {
				messageLayer.postStatus("Last Calculation " + mp.entrySet());
				System.out.println(mp.entrySet());
			}
		} catch (Exception e) {
			messageLayer.postStatus("Error while calculation!");

		}
		return mp;
	}

	/**
	 * Used to calculate all currently selected nodes inside of the graph.
	 * 
	 * @return a set containing all results
	 */
	public Set<Map<String, Object>> executeSelectedNodes() {

		Set<Map<String, Object>> result = new HashSet<>();
		try {
			if (selected.size() > 0) {
				for (DrawNode n : selected)
					result.add(executeNode(n.getNode().getBackendID()));
			}
			System.out.println(result);
		} catch (Exception e) {
			messageLayer.postStatus("Error while calculation!");
		}

		return result;

	}

	public void groupSelectedNodes() {

		List<String> ids = selected.stream().map(e -> e.getNode().getBackendID()).collect(Collectors.toList());

		backendNodeGraph.createNewGraphNode(ids, "Group");

	}

	public void ungroupNodes() {

	}

	/**
	 * Returns the global event bus which this graph is using.
	 * 
	 * @return the event bus
	 */
	public EventBus getGlobalEventBus() {
		return globalBus;
	}

	/**
	 * Creates a preview image of the current node-graph. The preview image does not
	 * include any top layers. Onle the nodes and the signals will be saved. The
	 * zoom level will always be set to 50%.
	 * 
	 * @return the preview Image
	 */
	public Image getPreviewImage() {
		Image img = null;

		double tmp = drawLayer.getScale();
		drawLayer.setScale(0.5d);
		backgroundLayer.draw();
		snP.setFill(Color.rgb(23, 23, 23));
		img = drawLayer.snapshot(snP, null);
		drawLayer.setScale(tmp);

		if (useNodeAreaWidget()) {
			nodeAreaWidget.setImage(img);

		}
		return img;
	}

	/**
	 * Returns an Image of a node.
	 * 
	 * @param node the node to take a snapshot of.
	 * @return the image of the node
	 */
	public Image getNodePreviewImage(DrawNode node) {
		Image img = null;
		img = node.snapshot(snP, null);
		return img;
	}

	/**
	 * Returns an Image of a port.
	 * 
	 * @param port the port to take a snapshot of.
	 * @return the image of the port
	 */
	public Image getPortPreviewImage(DrawPort port) {
		Image img = null;
		img = port.snapshot(snP, null);
		return img;
	}

	/**
	 * Returns the name property of this node graph.
	 * 
	 * @return the name property
	 */
	public StringProperty nameProperty() {
		return nameProperty;
	}

	/**
	 * Returns the name of this node graph.
	 * 
	 * @return the name
	 */
	public String getGraphName() {
		return nameProperty.get();
	}

	/**
	 * This method can be used to merge graphs with each other.
	 * 
	 * @param p the NodeLang (.nl) file to merge
	 * @throws NoSuchNode
	 * @throws IOException
	 * @throws NodeLangException
	 */
	public void mergeGraph(Path p) throws NoSuchNode, IOException, NodeLangException {
		NodeGraph graph = backendNodeGraph.getMaster().loadGraphFromFile(p);

		for (AbstractNode node : graph.getNodeByIDMap().values()) {
			backendNodeGraph.addNode(node.getType());
		}

		Map<INodeInputPort, NodeConnection> conn = graph.getConnections();

		Iterator<Map.Entry<INodeInputPort, NodeConnection>> it = conn.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<INodeInputPort, NodeConnection> pair = it.next();
			backendNodeGraph.addConnection(pair.getKey(), pair.getValue().driver);
		}

	}

	/**
	 * Returns the internally used clipboard of this nodegraph. The
	 * {@link Clipboard} is used eg. for copy and pasting nodes using
	 * {@link #copyNodes()} and {@link #pasteNodes()}.
	 * 
	 * @return teh clipboard of this graph
	 */
	public Clipboard getClipboard() {
		return clipboard;
	}

	/**
	 * Adds a new Node to the graph with auto generated id. The id is in the form
	 * "type[autoincrement_number]".
	 * 
	 * @param type The type of the node to be registered
	 * @return The id of the instance
	 */
	public String addNode(INodeType type) {
		return backendNodeGraph.addNode(type);
	}

	/**
	 * Adds a new Node to the graph with auto generated id. The id is in the form
	 * "type[autoincrement_number]".
	 * 
	 * @param type The type of the node to be registered as a String. The string is
	 * typically the node type name in all lower case letters.
	 * @return The id of the instance
	 */
	public String addNode(String type) {
		return backendNodeGraph.addNode(type);
	}

	/**
	 * Adds a new Node to the graph with specified id. id must not be in the form
	 * "type[some_number]", else the auto generated Nodes will have id conflicts and
	 * the program will not work
	 * 
	 * @param type The Type of Node to be registered
	 * @param id The id (name) of the node instance
	 * @return The id of the instance (the same as the parameter id)
	 */
	public String addNode(INodeType type, String id) {
		return backendNodeGraph.addNode(type, id);
	}

	/**
	 * Returns the zoom/scale value of the node graph. This is needed for
	 * calculating the right mouse positions inside of the {@link DrawLayer}.
	 * 
	 * @return the zoom value
	 */
	public double getZoomValue() {
		return drawLayer.getScale();
	}

	/**
	 * Sets the zoom/scale value of the node graph. The zoomValue is needed for
	 * calculating the right mouse positions inside of the {@link DrawLayer}.
	 * 
	 * @param zoomValue the zoom value to set
	 */
	public void setZoomValue(double zoomValue) {
		drawLayer.setScale(zoomValue);
	}

	/**
	 * Checks whether the graph has been initialized or not.
	 * 
	 * @return true if the graph has been initialized otherwise false
	 */
	public boolean isInitialized() {
		return initialized;
	}

	/**
	 * Returns a {@link List} with all selected {@link DrawNode}s of this nodegraph.
	 * 
	 * @return all selected nodes.
	 */
	public List<DrawNode> getSelectedNodes() {
		return selected;
	}

	/**
	 * Creates a dummy node. Should only be used for testing purposes.
	 */
	public void createDummyNode() {
		backendNodeGraph.addNode("dummy_node");
	}

// TODO java Doc.

	public BooleanProperty runOnMobileProperty() {
		return graphConfig.runOnMobileProperty();
	}

	public boolean runOnMobile() {
		return graphConfig.runOnMobile();
	}

	public BooleanProperty touchEnabledProperty() {
		return graphConfig.touchEnabledProperty();
	}

	public boolean touchEnabled() {
		return graphConfig.touchEnabled();
	}

	public BooleanProperty menuLineEnabledProperty() {
		return graphConfig.menuLineEnabledProperty();
	}

	public boolean menuLineEnabled() {
		return graphConfig.menuLineEnabled();
	}

	public BooleanProperty enableQuickMenuProperty() {
		return graphConfig.enableQuickMenuProperty();
	}

	public boolean enableQuickMenu() {
		return graphConfig.enableQuickMenu();
	}

	public DoubleProperty quickMenuPositionProperty() {
		return graphConfig.quickMenuPositionProperty();
	}

	public double quickMenuPosition() {
		return graphConfig.quickMenuPosition();
	}

	public BooleanProperty useNodeAreaWidgetProperty() {
		return graphConfig.useNodeAreaWidgetProperty();
	}

	public boolean useNodeAreaWidget() {
		return graphConfig.useNodeAreaWidget();
	}

	public BooleanProperty useSelectionWidgetProperty() {
		return graphConfig.useSelectionWidgetProperty();
	}

	public boolean useSelectionWidget() {
		return graphConfig.useSelectionWidget();
	}

	public BooleanProperty useSearchBarWidgetProperty() {
		return graphConfig.useSearchBarWidgetProperty();
	}

	public boolean useSearchBarWidget() {
		return graphConfig.useSearchBarWidget();
	}

	public GraphConfig getGraphConfig() {
		return this.graphConfig;
	}

	/**
	 * The SceneGestures class holds the scene gestures that are being called. These
	 * methods get initialized with {@link DrawGraph#init()}
	 * 
	 * @author Florian Wagner
	 *
	 */
	private class SceneGestures {

		private static final double MAX_SCALE = 10.0d;
		private static final double MIN_SCALE = .01d;
		public final EventBus eventBus;
		private SelectionWidget selection;
		private DragContext sceneDragContext = new DragContext();

		DrawLayer canvas;

		public SceneGestures(Layer layer, SelectionWidget selection, EventBus bus) {
			this.canvas = (DrawLayer) layer;
			this.selection = selection;
			eventBus = bus;

		}

		public EventHandler<TouchEvent> getOnTouchPressedEventHandler() {
			return onTouchPressedEventHandler;
		}

		public EventHandler<TouchEvent> getOnTouchClickedEventHandler() {
			return onTouchClicked;
		}

		public EventHandler<TouchEvent> getOnTouchDraggedEventHandler() {
			return onTouchDraggedEventHandler;
		}

		public EventHandler<TouchEvent> getOnTouchReleasedEventHandler() {
			return onTouchReleased;
		}

		public EventHandler<ZoomEvent> getOnZoomEventHandler() {
			return onZoomEventHandler;
		}

		public EventHandler<MouseEvent> getOnMousePressedEventHandler() {
			return onMousePressedEventHandler;
		}

		public EventHandler<MouseEvent> getOnMouseDraggedEventHandler() {
			return onMouseDraggedEventHandler;
		}

		public EventHandler<MouseEvent> getOnMouseReleased() {
			return onMouseReleased;
		}

		public EventHandler<MouseEvent> getOnMouseMoved() {
			return onMouseMoved;
		}

		public EventHandler<ScrollEvent> getOnScrollEventHandler() {
			return onScrollEventHandler;
		}

		public EventHandler<KeyEvent> getOnKeyPressed() {
			return onKeyPressedEventHandler;
		}

		public EventHandler<KeyEvent> getOnKeyTyped() {
			return onKeyTypedEventHandler;
		}

		public EventHandler<MouseEvent> getOnMouseClicked() {
			return onMouseClickEvent;
		}

		private EventHandler<TouchEvent> onTouchClicked = new EventHandler<TouchEvent>() {

			public void handle(TouchEvent event) {
				if (GuiState.getState().equals(GuiState.DEFAULT)) {
					eventBus.post(new MouseClickEvent(event));
				}
			}

		};

		private EventHandler<MouseEvent> onMouseClickEvent = new EventHandler<MouseEvent>() {

			public void handle(MouseEvent event) {

				if (GuiState.getState().equals(GuiState.DEFAULT)) {
					eventBus.post(new MouseClickEvent(event));

				}

			}

		};

		private EventHandler<MouseEvent> onMouseMoved = new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				eventBus.post(new MouseMovedEvent(event));
			}

		};

		private EventHandler<MouseEvent> onMousePressedEventHandler = new EventHandler<MouseEvent>() {

			public void handle(MouseEvent event) {

				if (event.isSecondaryButtonDown()) {

					// right mouse button => panning
					sceneDragContext.mouseAnchorX = event.getSceneX();
					sceneDragContext.mouseAnchorY = event.getSceneY();

					sceneDragContext.translateAnchorX = canvas.getTranslateX();
					sceneDragContext.translateAnchorY = canvas.getTranslateY();
					eventBus.post(new SceneChangedEvent());

				}
			}

		};

		private EventHandler<TouchEvent> onTouchPressedEventHandler = new EventHandler<TouchEvent>() {

			public void handle(TouchEvent event) {
				// right mouse button => panning
				if (event.getTouchCount() < 2) {
					sceneDragContext.mouseAnchorX = event.getTouchPoint().getSceneX();
					sceneDragContext.mouseAnchorY = event.getTouchPoint().getSceneY();

					sceneDragContext.translateAnchorX = canvas.getTranslateX();
					sceneDragContext.translateAnchorY = canvas.getTranslateY();
					eventBus.post(new SceneChangedEvent());
				}
			}

		};

		private EventHandler<KeyEvent> onKeyPressedEventHandler = new EventHandler<KeyEvent>() {
			public void handle(KeyEvent event) {

				eventBus.post(new KeyPressedEvent(event));

			}
		};
		private EventHandler<KeyEvent> onKeyTypedEventHandler = new EventHandler<KeyEvent>() {
			public void handle(KeyEvent event) {

				eventBus.post(new KeyoardTypedEvent(event));

			}
		};

		private EventHandler<TouchEvent> onTouchReleased = new EventHandler<TouchEvent>() {
			public void handle(TouchEvent event) {
				if (selection != null) {
					selection.setVisible(false);
					selection.setWidth(0D);
					selection.setHeight(0D);
					// GuiState.setState(GuiState.DEFAULT);
				}
			}
		};

		private EventHandler<MouseEvent> onMouseReleased = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				if (selection != null) {
					selection.setVisible(false);
					selection.setWidth(0D);
					selection.setHeight(0D);
				}
				// GuiState.setState(GuiState.DEFAULT);
			}
		};

		private EventHandler<TouchEvent> onTouchDraggedEventHandler = new EventHandler<TouchEvent>() {
			public void handle(TouchEvent event) {

				if (GuiState.getState().equals(GuiState.DEFAULT) || GuiState.getState().equals(GuiState.SELECTION)) {
					// right mouse button => panning
					if (event.getTouchCount() < 2) {
						if (sceneDragContext.translateAnchorX + event.getTouchPoint().getSceneX()
								- sceneDragContext.mouseAnchorX <= 0)
							canvas.setTranslateX(sceneDragContext.translateAnchorX + event.getTouchPoint().getSceneX()
									- sceneDragContext.mouseAnchorX);
						if (sceneDragContext.translateAnchorY + event.getTouchPoint().getSceneY()
								- sceneDragContext.mouseAnchorY <= 0)
							canvas.setTranslateY(sceneDragContext.translateAnchorY + event.getTouchPoint().getSceneY()
									- sceneDragContext.mouseAnchorY);
						eventBus.post(new SceneChangedEvent());
						event.consume();
					}
				}
			}
		};

		private EventHandler<MouseEvent> onMouseDraggedEventHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {

				if (GuiState.getState().equals(GuiState.DEFAULT) || GuiState.getState().equals(GuiState.SELECTION)) {
					// right mouse button => panning

					if (event.isSecondaryButtonDown()) {
						if (sceneDragContext.translateAnchorX + event.getSceneX() - sceneDragContext.mouseAnchorX <= 0)
							canvas.setTranslateX(sceneDragContext.translateAnchorX + event.getSceneX()
									- sceneDragContext.mouseAnchorX);
						if (sceneDragContext.translateAnchorY + event.getSceneY() - sceneDragContext.mouseAnchorY <= 0)
							canvas.setTranslateY(sceneDragContext.translateAnchorY + event.getSceneY()
									- sceneDragContext.mouseAnchorY);
						eventBus.post(new SceneChangedEvent());
						eventBus.post(new MouseMovedEvent(event));
						event.consume();
					}
				}
			}
		};

		private EventHandler<ZoomEvent> onZoomEventHandler = new EventHandler<ZoomEvent>() {

			@Override
			public void handle(ZoomEvent event) {
				if (GuiState.getState().equals(GuiState.DEFAULT)) {
					// double delta = 1.2;

					double scale = canvas.getScale(); // currently we only use Y, same value is used for X
					double oldScale = scale;

					// if (event.getZoomFactor() < 0)
					// scale /= delta;
					// else
					scale *= event.getZoomFactor();

					scale = clamp(scale, MIN_SCALE, MAX_SCALE);

					double f = (scale / oldScale) - 1;

					double dx = (event.getSceneX()
							- (canvas.getBoundsInParent().getWidth() / 2 + canvas.getBoundsInParent().getMinX()));
					double dy = (event.getSceneY()
							- (canvas.getBoundsInParent().getHeight() / 2 + canvas.getBoundsInParent().getMinY()));

					canvas.setScale(scale);

					// note: pivot value must be untransformed, i. e. without scaling
					canvas.setPivot(f * dx, f * dy);
					eventBus.post(new SceneChangedEvent());
					event.consume();
				}
			}

		};

		/**
		 * Mouse wheel handler: zoom to pivot point
		 */
		private EventHandler<ScrollEvent> onScrollEventHandler = new EventHandler<ScrollEvent>() {

			@Override
			public void handle(ScrollEvent event) {
				if (GuiState.getState().equals(GuiState.DEFAULT)) {
					double delta = 1.2;

					double scale = canvas.getScale(); // currently we only use Y, same value is used for X
					double oldScale = scale;

					if (event.getDeltaY() < 0)
						scale /= delta;
					else
						scale *= delta;

					scale = clamp(scale, MIN_SCALE, MAX_SCALE);

					double f = (scale / oldScale) - 1;

					double dx = (event.getSceneX()
							- (canvas.getBoundsInParent().getWidth() / 2 + canvas.getBoundsInParent().getMinX()));
					double dy = (event.getSceneY()
							- (canvas.getBoundsInParent().getHeight() / 2 + canvas.getBoundsInParent().getMinY()));

					canvas.setScale(scale);

					// note: pivot value must be untransformed, i. e. without scaling
					canvas.setPivot(f * dx, f * dy);
					eventBus.post(new SceneChangedEvent());

					event.consume();
				}
			}

		};

		public double clamp(double value, double min, double max) {

			if (Double.compare(value, min) < 0)
				return min;

			if (Double.compare(value, max) > 0)
				return max;

			return value;
		}

	}

}