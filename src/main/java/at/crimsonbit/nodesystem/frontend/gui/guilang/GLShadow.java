/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.guilang;

import javafx.scene.paint.Color;

public class GLShadow {

	private boolean use_shadow = false;
	private Color shadow_color = Color.color(0.1D, 0.1D, 0.1D);
	private double shadow_width = 1D;
	private double shadow_height = 1D;
	private double shadow_offset_x = 1D;
	private double shadow_offset_y = 1D;
	private double shadow_radius = 10D;

	public GLShadow() {

	}

	public GLShadow(boolean use_shadow, Color shadow_color, double shadow_width, double shadow_height,
			double shadow_offset_x, double shadow_offset_y, double shadow_radius) {
		super();
		this.use_shadow = use_shadow;
		this.shadow_color = shadow_color;
		this.shadow_width = shadow_width;
		this.shadow_height = shadow_height;
		this.shadow_offset_x = shadow_offset_x;
		this.shadow_offset_y = shadow_offset_y;
		this.shadow_radius = shadow_radius;
	}

	public boolean isUse_shadow() {
		return use_shadow;
	}

	public void setUse_shadow(boolean use_shadow) {
		this.use_shadow = use_shadow;
	}

	public Color getShadow_color() {
		return shadow_color;
	}

	public void setShadow_color(Color shadow_color) {
		this.shadow_color = shadow_color;
	}

	public double getShadow_width() {
		return shadow_width;
	}

	public void setShadow_width(double shadow_width) {
		this.shadow_width = shadow_width;
	}

	public double getShadow_height() {
		return shadow_height;
	}

	public void setShadow_height(double shadow_height) {
		this.shadow_height = shadow_height;
	}

	public double getShadow_offset_x() {
		return shadow_offset_x;
	}

	public void setShadow_offset_x(double shadow_offset_x) {
		this.shadow_offset_x = shadow_offset_x;
	}

	public double getShadow_offset_y() {
		return shadow_offset_y;
	}

	public void setShadow_offset_y(double shadow_offset_y) {
		this.shadow_offset_y = shadow_offset_y;
	}

	public double getShadow_radius() {
		return shadow_radius;
	}

	public void setShadow_radius(double shadow_radius) {
		this.shadow_radius = shadow_radius;
	}

}
