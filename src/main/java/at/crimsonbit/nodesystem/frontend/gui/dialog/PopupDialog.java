/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.dialog;

import javafx.scene.control.ContextMenu;
import javafx.scene.input.KeyCodeCombination;

/**
 * 
 * @author Florian Wagner
 *
 */

public class PopupDialog extends ContextMenu {

	public PopupDialog() {
		super();
	}

	public void addItem(SubDialog menu) {
		getItems().add(menu);
	}

	public void addItem(Entry item) {
		getItems().add(item);
	}

	public void addItem(int id, String name, KeyCodeCombination kombi) {
		getItems().add(new Entry(id, name, kombi));
	}

	public void addItem(int id, String name) {
		getItems().add(new Entry(id, name));

	}

	public void addItem(int id, String name, boolean enabled, KeyCodeCombination kombi) {
		getItems().add(new Entry(id, name, enabled, kombi));
	}

	public void addItem(int id, String name, boolean enabled) {
		getItems().add(new Entry(id, name, enabled));

	}

	public void addSeparator(int id) {
		getItems().add(new Separator(id));
	}
}
