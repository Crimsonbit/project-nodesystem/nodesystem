/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.widget;

import com.google.common.eventbus.EventBus;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class SelectionWidget extends Rectangle {
	public final EventBus eventBus;

	public SelectionWidget(EventBus bus) {
		this.eventBus = bus;
		setStroke(Color.DARKORANGE);
		setArcWidth(21.0);
		setArcHeight(21.0);
		setStrokeWidth(1);
		Color c = Color.DARKORANGE;
		setFill(new Color(c.getRed(), c.getGreen(), c.getBlue(), 0.3d));
		getStrokeDashArray().add(10.0);
		init();
	}

	private void init() {

	}

}
