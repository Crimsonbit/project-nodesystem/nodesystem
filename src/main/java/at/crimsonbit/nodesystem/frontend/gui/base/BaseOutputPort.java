/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.frontend.gui.base;

/**
 * 
 * @author Florian Wagner
 *
 */
public class BaseOutputPort extends BasePort {

	public BaseOutputPort(String backendID, String name, BaseNode parent, Class<?> type, boolean isField) {
		super(backendID, name, parent, type, isField, false);

	}

	public BaseOutputPort(BasePort port) {
		super(port);
	}

	public BaseOutputPort(String backendID, String name, BaseNode parent, boolean isInput) {
		super(backendID, name, parent, isInput);
	}

	public BaseOutputPort() {
		super();
	}

}
