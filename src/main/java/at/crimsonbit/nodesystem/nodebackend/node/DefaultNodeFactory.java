/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.node;

import java.lang.reflect.InvocationTargetException;

import at.crimsonbit.nodesystem.nodebackend.INodeType;
import at.crimsonbit.nodesystem.nodebackend.exception.RuntimeNodeException;

public class DefaultNodeFactory extends NodeFactory {

	private final Class<? extends AbstractNode> clazz;
	private final INodeType type;

	public DefaultNodeFactory(INodeType type, Class<? extends AbstractNode> clazz) {
		this.clazz = clazz;
		this.type = type;
	}

	@Override
	public AbstractNode create(String id) {
		try {
			AbstractNode newNode = clazz.newInstance();
			newNode.setId(id);
			newNode.setType(type);
			newNode.createAnnotatedPorts();
			return newNode;
		} catch (InstantiationException | IllegalAccessException | SecurityException | InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeNodeException(e);
		}
	}

	@Override
	public INodeType getType() {
		return type;
	}

}
