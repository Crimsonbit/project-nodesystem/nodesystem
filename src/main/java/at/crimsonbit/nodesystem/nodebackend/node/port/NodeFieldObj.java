/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.node.port;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.logging.Logger;

import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.util.BackendUtils;

public class NodeFieldObj implements INodeField {

	public final AbstractNode node;
	public final Field nodeField;
	public final String name;
	public final Class<?> fieldType;

	public NodeFieldObj(AbstractNode node, Field nodeField, String name, Class<?> fieldType) {
		super();
		this.node = node;
		this.nodeField = nodeField;
		nodeField.setAccessible(true);
		this.name = name;
		this.fieldType = fieldType;
	}

	@Override
	public void accept(Object t) {
		if (BackendUtils.isClassCompatible(fieldType, t)) {
			t = BackendUtils.tryToConvert(t, fieldType);
			try {
				nodeField.set(node, t);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		} else {
			throw new IllegalArgumentException("Object of Class " + t.getClass() + " cannot be converted to "
					+ fieldType + "\nWhen trying to set Field of Node " + this.getID());
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NodeFieldObj other = (NodeFieldObj) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public static class Factory {
		public final Field nodeField;
		public final String name;
		public final Class<?> fieldType;

		public Factory(Field nodeField, String name, Class<?> fieldType) {
			super();
			this.nodeField = nodeField;
			this.name = name;
			this.fieldType = fieldType;
		}

		public NodeFieldObj create(AbstractNode node) {
			return new NodeFieldObj(node, nodeField, name, fieldType);
		}
	}

	@Override
	public boolean isRealPort() {
		return true;
	}

	@Override
	public AbstractNode getNode() {
		return node;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return getID();
	}

	@Override
	public Class<?> getType() {
		return fieldType;
	}

	@Override
	public Object get() {
		try {
			return nodeField.get(node);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			Logger.getLogger(Logger.GLOBAL_LOGGER_NAME)
					.severe("Error getting NodeFieldObjects Value: " + e.getMessage() + "\n" + e.getStackTrace());
			return null;
		}
	}

	@Override
	public void set(Object o) {
		accept(o);
	}

	@Override
	public boolean shouldBeSerialized() {
		return !Modifier.isVolatile(nodeField.getModifiers());
	}

}
