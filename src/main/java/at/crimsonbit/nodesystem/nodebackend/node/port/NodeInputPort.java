/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.node.port;

import java.lang.reflect.Field;
import java.util.logging.Logger;

import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.util.BackendUtils;

/**
 * This class represents the Input of a Node. It can be set to an Object
 * 
 * @author Alexander Daum
 *
 */
public class NodeInputPort implements INodeInputPort {
	public final AbstractNode node;
	public final Field nodeField;
	public final String name;
	public final Class<?> fieldType;

	public NodeInputPort(AbstractNode node, Field nodeField, Class<?> fieldType, String name) {
		super();
		this.node = node;
		this.nodeField = nodeField;
		nodeField.setAccessible(true);
		this.fieldType = fieldType;
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((node == null) ? 0 : node.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NodeInputPort other = (NodeInputPort) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (node == null) {
			if (other.node != null)
				return false;
		} else if (!node.equals(other.node))
			return false;
		return true;
	}

	@Override
	public boolean isRealPort() {
		return true;
	}

	@Override
	public AbstractNode getNode() {
		return node;
	}

	@Override
	public String getName() {
		return name;
	}

	public static class Factory {
		public final Field nodeField;
		public final String name;
		public final Class<?> fieldType;

		public Factory(Field nodeField, String name, Class<?> fieldType) {
			super();
			this.nodeField = nodeField;
			this.name = name;
			this.fieldType = fieldType;
		}

		public NodeInputPort create(AbstractNode node) {
			return new NodeInputPort(node, nodeField, fieldType, name);
		}
	}

	@Override
	public String toString() {
		return getID();
	}

	@Override
	public Class<?> getType() {
		return fieldType;
	}

	@Override
	public Object get() {
		try {
			return nodeField.get(node);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			Logger.getLogger(Logger.GLOBAL_LOGGER_NAME)
					.severe("Error getting NodeInputPorts Value: " + e.getMessage() + "\n" + e.getStackTrace());
			return null;
		}
	}

	@Override
	public void set(Object o) {
		if (!BackendUtils.isClassCompatible(fieldType, o))
			throw new IllegalArgumentException("Input object of type " + o.getClass().getCanonicalName()
					+ " is not compatible with type " + fieldType.getCanonicalName());
		try {
			nodeField.set(node, BackendUtils.tryToConvert(o, fieldType));
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean shouldBeSerialized() {
		return false;
	}
}
