/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.node.port;

import java.lang.reflect.Field;
import java.util.logging.Logger;

import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.util.BackendUtils;

/**
 * A Class representing the output port of a Node
 * 
 * @author Alexander Daum
 *
 */
public class NodeOutputPort implements INodeOutputPort {
	public final Field nodeField;
	public final AbstractNode node;
	public final String name;

	public NodeOutputPort(AbstractNode node, Field nodeField, String name) {
		super();
		this.node = node;
		this.nodeField = nodeField;
		nodeField.setAccessible(true);
		this.name = name;
	}

	@Override
	public Object get() {
		try {
			return nodeField.get(node);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			Logger.getLogger(Logger.GLOBAL_LOGGER_NAME)
					.severe("Error getting NodeOutputPorts Value: " + e.getMessage() + "\n" + e.getStackTrace());
			return null;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((node == null) ? 0 : node.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NodeOutputPort other = (NodeOutputPort) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (node == null) {
			if (other.node != null)
				return false;
		} else if (!node.equals(other.node))
			return false;
		return true;
	}

	@Override
	public boolean isRealPort() {
		return true;
	}

	@Override
	public AbstractNode getNode() {
		return node;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return getID();
	}

	public static class Factory {
		public final Field nodeField;
		public final String name;

		public Factory(Field nodeField, String name) {
			super();
			this.nodeField = nodeField;
			this.name = name;
		}

		public NodeOutputPort create(AbstractNode node) {
			return new NodeOutputPort(node, nodeField, name);
		}
	}

	@Override
	public Class<?> getType() {
		return nodeField.getType();
	}

	public void set(Object obj) {
		if (BackendUtils.isClassCompatible(getType(), obj)) {
			try {
				nodeField.set(node, BackendUtils.tryToConvert(obj, getType()));
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean shouldBeSerialized() {
		return false;
	}

}
