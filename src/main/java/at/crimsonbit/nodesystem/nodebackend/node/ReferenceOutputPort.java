/*
Copyright 2019 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.node;

import at.crimsonbit.nodesystem.nodebackend.node.port.INodeOutputPort;

public class ReferenceOutputPort implements INodeOutputPort {

	public final INodeOutputPort reference;
	private final AbstractNode node;

	public ReferenceOutputPort(INodeOutputPort reference, AbstractNode realNode) {
		super();
		this.reference = reference;
		this.node = realNode;
	}

	@Override
	public boolean isRealPort() {
		return true;
	}

	@Override
	public AbstractNode getNode() {
		return node;
	}

	@Override
	public String getName() {
		return reference.getName();
	}

	@Override
	public Class<?> getType() {
		return reference.getType();
	}

	@Override
	public Object get() {
		return reference.get();
	}

	@Override
	public void set(Object o) {
		reference.set(o);
	}

	@Override
	public boolean shouldBeSerialized() {
		return reference.shouldBeSerialized();
	}

}
