package at.crimsonbit.nodesystem.nodebackend.node;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import net.sandius.rembulan.Variable;
import net.sandius.rembulan.compiler.CompiledModule;
import net.sandius.rembulan.compiler.CompilerChunkLoader;
import net.sandius.rembulan.compiler.CompilerSettings;
import net.sandius.rembulan.compiler.LuaCompiler;
import net.sandius.rembulan.load.ChunkClassLoader;
import net.sandius.rembulan.load.LoaderException;
import net.sandius.rembulan.parser.ParseException;
import net.sandius.rembulan.parser.Parser;
import net.sandius.rembulan.parser.TokenMgrError;
import net.sandius.rembulan.runtime.LuaFunction;

public class CachedCompilerChunkLoader {
	private final ChunkClassLoader chunkClassLoader;
	private final String rootClassPrefix;
	private final LuaCompiler compiler;
	private final Map<Integer, Constructor<?>> id_class_map = new HashMap<>();

	CachedCompilerChunkLoader(ClassLoader classLoader, LuaCompiler compiler, String rootClassPrefix) {
		this.chunkClassLoader = new ChunkClassLoader(Objects.requireNonNull(classLoader));
		this.compiler = Objects.requireNonNull(compiler);
		this.rootClassPrefix = Objects.requireNonNull(rootClassPrefix);
	}

	/**
	 * Returns a new instance of {@code CompilerChunkLoader} that uses the specified
	 * class loader {@code classLoader} to load classes it compiles using
	 * {@code compiler}, with every main chunk class having the class name
	 * {@code rootClassPrefix} followed by a monotonically-increasing integer
	 * suffix.
	 *
	 * @param classLoader
	 *            the class loader used by this chunk loader, must not be
	 *            {@code null}
	 * @param compiler
	 *            the compiler instance used by this chunk loader, must not be
	 *            {@code null}
	 * @param rootClassPrefix
	 *            the class name prefix for compiled classes, must not be
	 *            {@code null}
	 * @return a new instance of {@code CompilerChunkLoader}
	 *
	 * @throws NullPointerException
	 *             if {@code classLoader}, {@code compiler} or
	 *             {@code rootClassPrefix} is {@code null}
	 */
	public static CachedCompilerChunkLoader of(ClassLoader classLoader, LuaCompiler compiler, String rootClassPrefix) {
		return new CachedCompilerChunkLoader(classLoader, compiler, rootClassPrefix);
	}

	/**
	 * Returns a new instance of {@code CompilerChunkLoader} that uses the specified
	 * class loader {@code classLoader} to load classes it compiles using a new
	 * instance of the Lua compiler with the settings {@code compilerSettings}, with
	 * every main chunk class having the class name {@code rootClassPrefix} followed
	 * by a monotonically-increasing integer suffix.
	 *
	 * @param classLoader
	 *            the class loader used by this chunk loader, must not be
	 *            {@code null}
	 * @param compilerSettings
	 *            the compiler settings used to instantiate the compiler, must not
	 *            be {@code null}
	 * @param rootClassPrefix
	 *            the class name prefix for compiled classes, must not be
	 *            {@code null}
	 * @return a new instance of {@code CompilerChunkLoader}
	 *
	 * @throws NullPointerException
	 *             if {@code classLoader}, {@code compilerSettings} or
	 *             {@code rootClassPrefix} is {@code null}
	 */
	public static CachedCompilerChunkLoader of(ClassLoader classLoader, CompilerSettings compilerSettings,
			String rootClassPrefix) {
		return new CachedCompilerChunkLoader(classLoader, new LuaCompiler(compilerSettings), rootClassPrefix);
	}

	/**
	 * Returns a new instance of {@code CompilerChunkLoader} that uses the specified
	 * class loader {@code classLoader} to load classes it compiles using a compiler
	 * instantiated with {@linkplain CompilerSettings#defaultSettings() default
	 * settings}, with every main chunk class having the class name
	 * {@code rootClassPrefix} followed by a monotonically-increasing integer
	 * suffix.
	 *
	 * @param classLoader
	 *            the class loader used by this chunk loader, must not be
	 *            {@code null}
	 * @param rootClassPrefix
	 *            the class name prefix for compiled classes, must not be
	 *            {@code null}
	 * @return a new instance of {@code CompilerChunkLoader}
	 *
	 * @throws NullPointerException
	 *             if {@code classLoader} or {@code rootClassPrefix} is {@code null}
	 */
	public static CachedCompilerChunkLoader of(ClassLoader classLoader, String rootClassPrefix) {
		return of(classLoader, CompilerSettings.defaultSettings(), rootClassPrefix);
	}

	/**
	 * Returns a new instance of {@code CompilerChunkLoader} that uses the class
	 * loader that loaded the {@code CompilerChunkLoader} class to load classes it
	 * compiles using {@code compiler}, with every main chunk class having the class
	 * name {@code rootClassPrefix} followed by a monotonically-increasing integer
	 * suffix.
	 *
	 * @param compiler
	 *            the compiler instance used by this chunk loader, must not be
	 *            {@code null}
	 * @param rootClassPrefix
	 *            the class name prefix for compiled classes, must not be
	 *            {@code null}
	 * @return a new instance of {@code CompilerChunkLoader}
	 *
	 * @throws NullPointerException
	 *             {@code compiler} or {@code rootClassPrefix} is {@code null}
	 */
	public static CachedCompilerChunkLoader of(LuaCompiler compiler, String rootClassPrefix) {
		return of(CompilerChunkLoader.class.getClassLoader(), compiler, rootClassPrefix);
	}

	/**
	 * Returns a new instance of {@code CompilerChunkLoader} that uses the class
	 * loader that loaded the {@code CompilerChunkLoader} class to load classes it
	 * compiles using a new instance of the Lua compiler with the settings
	 * {@code compilerSettings}, with every main chunk class having the class name
	 * {@code rootClassPrefix} followed by a monotonically-increasing integer
	 * suffix.
	 *
	 * @param compilerSettings
	 *            the compiler settings used to instantiate the compiler, must not
	 *            be {@code null}
	 * @param rootClassPrefix
	 *            the class name prefix for compiled classes, must not be
	 *            {@code null}
	 * @return a new instance of {@code CompilerChunkLoader}
	 *
	 * @throws NullPointerException
	 *             if {@code compilerSettings} or {@code rootClassPrefix} is
	 *             {@code null}
	 */
	public static CachedCompilerChunkLoader of(CompilerSettings compilerSettings, String rootClassPrefix) {
		return of(new LuaCompiler(compilerSettings), rootClassPrefix);
	}

	/**
	 * Returns a new instance of {@code CompilerChunkLoader} that uses the class
	 * loader that loaded the {@code CompilerChunkLoader} class to load classes it
	 * compiles using a compiler instantiated with
	 * {@linkplain CompilerSettings#defaultSettings() default settings}, with every
	 * main chunk class having the class name {@code rootClassPrefix} followed by a
	 * monotonically-increasing integer suffix.
	 *
	 * @param rootClassPrefix
	 *            the class name prefix for compiled classes, must not be
	 *            {@code null}
	 * @return a new instance of {@code CompilerChunkLoader}
	 *
	 * @throws NullPointerException
	 *             if {@code rootClassPrefix} is {@code null}
	 */
	public static CachedCompilerChunkLoader of(String rootClassPrefix) {
		return of(CompilerSettings.defaultSettings(), rootClassPrefix);
	}

	public ChunkClassLoader getChunkClassLoader() {
		return chunkClassLoader;
	}

	public LuaFunction loadTextChunk(Variable env, String chunkName, String sourceText, int id) throws LoaderException {
		Objects.requireNonNull(env);
		Objects.requireNonNull(chunkName);
		Objects.requireNonNull(sourceText);

		synchronized (this) {
			try {
				if (id_class_map.containsKey(id)) {
					Constructor<?> constructor = id_class_map.get(id);
					return (LuaFunction) constructor.newInstance(env);
				}
				String rootClassName = rootClassPrefix + id;
				CompiledModule result = compiler.compile(sourceText, chunkName, rootClassName);

				String mainClassName = chunkClassLoader.install(result);
				Class<?> clazz = chunkClassLoader.loadClass(mainClassName);
				Constructor<?> constructor = clazz.getConstructor(Variable.class);
				id_class_map.put(id, constructor);
				return (LuaFunction) constructor.newInstance(env);
			} catch (TokenMgrError ex) {
				String msg = ex.getMessage();
				int line = 0; // TODO
				boolean partial = msg != null && msg.contains("Encountered: <EOF>"); // TODO: is there really no better
																						// way?
				throw new LoaderException(ex, chunkName, line, partial);
			} catch (ParseException ex) {
				boolean partial = ex.currentToken != null && ex.currentToken.next != null
						&& ex.currentToken.next.kind == Parser.EOF;
				int line = ex.currentToken != null ? ex.currentToken.beginLine : 0;
				throw new LoaderException(ex, chunkName, line, partial);
			} catch (RuntimeException | LinkageError | ReflectiveOperationException ex) {
				throw new LoaderException(ex, chunkName, 0, false);
			}
		}
	}

}
