/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.node;

import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import at.crimsonbit.nodesystem.nodebackend.node.port.INodeField;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeOutputPort;

public class DynamicPortNode extends AbstractNode {

	private BiFunction<Map<String, Object>, Map<String, Class<?>>, Map<String, Object>> computeFunction;

	/**
	 * Create a new DynamicPortNode. <br>
	 * The Node will have the inputs, outputs and fields specified in this
	 * constructor and will also have the specified compute method. The compute
	 * method will be called when the Node is evaluated and gets all Inputs as an
	 * Object[] as well as an Array of Class<?> which specifies the types of the
	 * outputs. The outputs should then be returned again as an Object[]. This Array
	 * should be of the same size as the Class[] and have the parameters in the same
	 * Order
	 * 
	 * @param inputs
	 *            An Array of InputPorts, that will be used by this Node
	 * @param outputs
	 *            An Array of OutputPorts, that will be used by this Node
	 * @param fields
	 *            An Array of NodeFields, that will be used by this Node
	 * @param computeFun
	 *            The method, that will be called whenever this Node is computed.
	 *            The Parameters to this Function are an Object[] with all the
	 *            Inputs and a Class<?>[] with the types of the outputs. The
	 *            computed outputs should be returned as an Object[] with the same
	 *            length and compatible Types as the Class<?>[]
	 */
	public DynamicPortNode(BiFunction<Map<String, Object>, Map<String, Class<?>>, Map<String, Object>> computeFun) {
		this.computeFunction = computeFun;
	}

	public void initialize(Map<String, INodeInputPort> inputs, Map<String, INodeOutputPort> outputs,
			Map<String, INodeField> fields) {
		setInputs(inputs);
		setOutputs(outputs);
		setFields(fields);
	}

	@Override
	public void compute() {
		// TODO Auto-generated method stub
		Map<String, Class<?>> outputTypeMap = getOutputs().entrySet().stream()
				.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue().getType()));
		Map<String, Object> inputMap = getInputs().entrySet().stream()
				.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue().get()));
		getFields().entrySet().stream().forEach(e -> inputMap.put(e.getKey(), e.getValue().get()));

		Map<String, Object> objs = computeFunction.apply(inputMap, outputTypeMap);
		for (Map.Entry<String, Object> out : objs.entrySet()) {
			INodeOutputPort p = getOutputs().get(out.getKey());
			p.set(out.getValue());
		}
	}

}
