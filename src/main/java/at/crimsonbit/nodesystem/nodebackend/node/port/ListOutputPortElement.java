/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.node.port;

import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public class ListOutputPortElement implements INodeOutputPort {

	private OutputVector vector;
	private Object val;
	private int index;

	public ListOutputPortElement(OutputVector port, int index) {
		this.vector = port;
		this.index = index;
	}

	@Override
	public boolean isRealPort() {
		return vector.isRealPort();
	}

	@Override
	public AbstractNode getNode() {
		return vector.getNode();
	}

	@Override
	public String getName() {
		return vector.getName() + "[" + index + "]";
	}

	@Override
	public Class<?> getType() {
		return vector.getType();
	}

	@Override
	public Object get() {
		return val;
	}

	@Override
	public void set(Object o) {
		this.val = o;
	}

	@Override
	public boolean shouldBeSerialized() {
		return false;
	}

}
