/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.node.port;

import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public interface INodePort {

	/**
	 * Checks if this is a "real" Port, that means, that it is a port of a Node. A
	 * not real port would be e.g. a Signal
	 * 
	 * @return
	 */
	boolean isRealPort();

	/**
	 * Get the Node this Input belongs to, if this is not a real port
	 * ({@link INodeInputPort#isRealPort()} returns false) this must return null
	 * 
	 * @return
	 */
	AbstractNode getNode();

	/**
	 * Returns the name of the Input Port
	 * 
	 * @return
	 */
	String getName();

	/**
	 * Get the ID of this InputPort. If it is a real port, then is returns the node
	 * ID, then a ., then the id, otherwise just the name
	 * 
	 * @return
	 */
	default String getID() {
		return (isRealPort() ? getNode().getId() + "." : "") + getName();
	}

	Class<?> getType();

	Object get();

	void set(Object o);
	
	boolean shouldBeSerialized();

}