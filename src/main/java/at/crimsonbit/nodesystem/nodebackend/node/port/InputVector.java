/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.node.port;

import java.util.List;

import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public class InputVector implements VarLenInputPort {
	private AbstractNode node;
	public final String name;
	private INodeInputPort[] ports;

	@Override
	public boolean isRealPort() {
		return true;
	}

	@Override
	public AbstractNode getNode() {
		return node;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Class<?> getType() {
		return List.class;
	}

	@Override
	public INodeInputPort getIn(int index) {
		if (index >= ports.length)
			return null;
		return ports[index];
	}

	public void setLength(int len) {
		INodeInputPort[] newPorts = new INodeInputPort[len];
		int i = 0;
		if (ports != null) {
			for (; i < Math.min(ports.length, len); i++) {
				newPorts[i] = ports[i];
			}
		}
		while (i < len) {
			newPorts[i] = new ListInputPortElement(this, i);
			i++;
		}
		ports = newPorts;
	}

	public InputVector(AbstractNode node, String name) {
		super();
		this.node = node;
		this.name = name;

	}

	public InputVector(AbstractNode node, String name, List<Object> values) {
		this(node, name);
		for (int i = 0; i < values.size(); i++) {
			INodeInputPort port = new ListInputPortElement(this, i);
			port.set(values.get(i));
			ports[i] = port;
		}
	}

	public InputVector(AbstractNode node, String name, Object[] values) {
		this(node, name);
		for (int i = 0; i < values.length; i++) {
			INodeInputPort port = new ListInputPortElement(this, i);
			port.set(values[i]);
			ports[i] = port;
		}
	}

	public void set(Object[] objects) {
		// TODO
	}

	@Override
	public int getLength() {
		return ports.length;
	}

	@Override
	public boolean shouldBeSerialized() {
		return false;
	}

}
