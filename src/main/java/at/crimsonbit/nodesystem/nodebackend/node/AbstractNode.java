/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.node;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.crimsonbit.nodesystem.nodebackend.INodeType;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeField;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeInput;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeType;
import at.crimsonbit.nodesystem.nodebackend.exception.RuntimeNodeException;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import at.crimsonbit.nodesystem.nodebackend.misc.NoNodeType;
import at.crimsonbit.nodesystem.nodebackend.module.ModuleLoader;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeField;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeOutputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodePort;
import at.crimsonbit.nodesystem.nodebackend.node.port.InputVector;
import at.crimsonbit.nodesystem.nodebackend.node.port.NodeFieldObj;
import at.crimsonbit.nodesystem.nodebackend.node.port.NodeInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.NodeOutputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.OutputVector;
import at.crimsonbit.nodesystem.nodebackend.util.BackendUtils;

/**
 * This is the superclass for all Nodes in the NodeSystem. If you want to create
 * a new Node, create a subclass of this class. These Nodes should be in their
 * own jars compiled with this project on their classpath and will be loaded by
 * the {@link ModuleLoader} when needed.
 * 
 * <h1>Ports</h1> A node can have inputs, outputs and fields. Inputs and outputs
 * can be connected to other nodes to form a {@link NodeGraph}. Fields can be
 * used to set values, that are not dependent on any other nodes, e.g.
 * constants. Fields may either be just fields or be fields and outputs, but
 * never inputs. Doing so will still compile but have no effect, since inputs
 * are set immediately before calling the {@link AbstractNode#compute()} method,
 * fields have to be set before that and are overwritten. <br>
 * To create Ports you annotate fields with {@link NodeOutput},
 * {@link NodeInput} or {@link NodeField}. Inputs, Outputs and Fields may be of
 * any type and can be cast to each other when connecting if
 * {@link BackendUtils#canBeConverted(Class, Class)} is true for the class of
 * the output to the class of the input. This means, a String can be converted
 * to any Type that has a <code>valueOf(String)</code> method
 * 
 * <h2>Port Types</h2> In Order to create Ports in a Node there are 2 different
 * approaches, wrapped ports or direct ports. For both you have to annotate a
 * Field with the correct annotation as described above
 * <h3>Wrapped Ports</h3> Wrapped Ports are created by annotating a Field of any
 * Type, that does <b>not</b> implement {@link INodePort}. When creating the
 * Node, for each wrappedPort a Wrapper Object is created, that accesses the
 * Field with Reflection. A Wrapper Type can be used in any way you want, assign
 * new values, read it like a normal object etc.
 * <h3>Direct Ports</h3> Direct Ports are created by annotating a Field of a
 * Type, that <b>does</b> implement {@link INodePort}. Direct Ports are, as the
 * name suggests, directly used by the graph, not wrapped. This imposes
 * restrictions on what you may do with a direct port.
 * <ul>
 * <li>You can instantiate the field in the constructor, but don't have to. If
 * it is null it will be instantiated when the node's ports are created</li>
 * <li>You <b>must not</b> re-create the object object or changing the reference
 * later on in the code! This is because the Object is referenced to directly,
 * and when the object is re-created, the graph still has a reference to the old
 * Object and cannot detect changes</li>
 * <li>Most {@link INodePort} implementations will use the
 * {@link INodePort#set(Object)} and {@link INodePort#get()} methods to modify
 * the value. But some (like {@link InputVector} or {@link OutputVector}) will
 * not implement those and offer their own manipulation methods.
 * </ul>
 * 
 * 
 * @author Alexander Daum
 */
@SuppressWarnings("deprecation")
public abstract class AbstractNode {

	@NodeType
	private static final INodeType ntype = NoNodeType.ABSTRACT;

	/**
	 * The id is set by the NodeMaster when registering the Node Instance. DO NOT
	 * CHANGE
	 */
	private String id = null;

	private INodeType type = null;
	/**
	 * The List of all Input Ports in this Node
	 */
	private Map<String, INodeInputPort> inputs = new HashMap<>();
	/**
	 * The List of all Output Ports in this Node
	 */
	private Map<String, INodeOutputPort> outputs = new HashMap<>();
	/**
	 * The List of all Fields in this Node
	 */
	private Map<String, INodeField> fields = new HashMap<>();

	public abstract void compute();

	/**
	 * 
	 * @return The ID of the node
	 */
	public final String getId() {
		return id;
	}

	/**
	 * Set the ID of this node, for internal use only
	 * 
	 * @param id
	 */
	final void setId(String id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The Type of the Node
	 */
	public final INodeType getType() {
		return type;
	}

	/**
	 * Sets the type of the Node, for internal use only
	 * 
	 * @param type
	 */
	final void setType(INodeType type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractNode other = (AbstractNode) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	/**
	 * Returns all Inputs of this Node as unmodifiable Map from its name to the port
	 * 
	 * @return
	 */
	public Map<String, INodeInputPort> getInputs() {
		return Collections.unmodifiableMap(inputs);
	}

	/**
	 * Returns all Outputs of this Node as unmodifiable Map from its name to the
	 * port
	 * 
	 * @return
	 */
	public Map<String, INodeOutputPort> getOutputs() {
		return Collections.unmodifiableMap(outputs);
	}

	/**
	 * Returns all Fields of this Node as unmodifiable Map from its name to the port
	 * 
	 * @return
	 */
	public Map<String, INodeField> getFields() {
		return Collections.unmodifiableMap(fields);
	}

	/**
	 * Adds an Input, for internal use only
	 * 
	 * @param input
	 */
	protected final void addInput(INodeInputPort input) {
		inputs.put(input.getName(), input);
	}

	/**
	 * Adds an output, for internal use only
	 * 
	 * @param output
	 */
	protected final void addOutput(INodeOutputPort output) {
		outputs.put(output.getName(), output);
	}

	/**
	 * Adds a Field, for internal use only
	 * 
	 * @param field
	 */
	protected final void addField(INodeField field) {
		fields.put(field.getName(), field);
	}

	/**
	 * Sets the input list, for internal use only, or when overriding
	 * {@link AbstractNode#createAnnotatedPorts()}
	 * 
	 * @param inputs
	 */
	protected final void setInputs(Map<String, INodeInputPort> inputs) {
		this.inputs = inputs;
	}

	/**
	 * Sets the output list, for internal use only, or when overriding
	 * {@link AbstractNode#createAnnotatedPorts()}
	 * 
	 * @param outputs
	 */
	protected final void setOutputs(Map<String, INodeOutputPort> outputs) {
		this.outputs = outputs;
	}

	/**
	 * Sets the field list, for internal use only, or when overriding
	 * {@link AbstractNode#createAnnotatedPorts()}
	 * 
	 * @param fields
	 */
	protected final void setFields(Map<String, INodeField> fields) {
		this.fields = fields;
	}

	/**
	 * Try to create a new Instance of a Node Port. This is used to instantiate
	 * direct Ports if they are null
	 * 
	 * @param clazz
	 * @return
	 * @throws SecurityException
	 * @throws InvocationTargetException
	 * @throws @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws NoSuchMethodException
	 */
	private <T extends INodePort> T tryToCreateNewPort(Class<T> clazz, String id)
			throws SecurityException, InstantiationException, IllegalAccessException, InvocationTargetException {
		try {
			Constructor<T> con = clazz.getConstructor(AbstractNode.class, String.class); // this should exist
			T obj = con.newInstance(this, id);
			return obj;
		} catch (NoSuchMethodException e) {
			throw new RuntimeNodeException("No fitting constructor in class " + clazz, e);
		}
	}

	/**
	 * Create the internal Port lists of inputPorts, outputPorts and Fields from the
	 * annotated fields. The Default implementation uses all Fields annotated with
	 * {@link NodeInput} as Inputs, all Fields annotated with {@link NodeOutput} as
	 * outputs and all Fields annotated with {@link NodeField} as fields. <br>
	 * If you override this Method, you have to set the inputs, outputs and fields
	 * using {@link AbstractNode#setInputs(List)},
	 * {@link AbstractNode#setOutputs(List)} and
	 * {@link AbstractNode#setFields(List)} respectively. You can also use the add
	 * 
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws InvocationTargetException
	 * @throws SecurityException
	 */
	
	protected void createAnnotatedPorts()
			throws IllegalAccessException, InstantiationException, SecurityException, InvocationTargetException {
		Field[] fields = getClass().getDeclaredFields();
		for (Field f : fields) {
			f.setAccessible(true);
			if (f.isAnnotationPresent(NodeInput.class)) {
				INodeInputPort nip;
				Class<?> clazz = f.getType();
				if (INodeInputPort.class.isAssignableFrom(clazz)) {
					nip = createDirectPort(f, clazz);
				} else {
					nip = new NodeInputPort(this, f, f.getType(), f.getName());
				}
				addInput(nip);
			} else if (f.isAnnotationPresent(NodeOutput.class)) {
				INodeOutputPort nop;
				Class<?> clazz = f.getType();
				if (INodeOutputPort.class.isAssignableFrom(f.getType())) {
					nop = createDirectPort(f, clazz);
				} else {
					nop = new NodeOutputPort(this, f, f.getName());
				}
				addOutput(nop);
			}

			if (f.isAnnotationPresent(NodeField.class)) {
				NodeFieldObj nfo = new NodeFieldObj(this, f, f.getName(), f.getType());
				addField(nfo);
			}

		}
	}

	@SuppressWarnings("unchecked")
	private <T extends INodePort> T createDirectPort(Field f, Class<?> clazz)
			throws IllegalAccessException, InstantiationException, InvocationTargetException {
		T nip;
		nip = (T) f.get(this);
		if (nip == null) {
			nip = tryToCreateNewPort((Class<T>) clazz, f.getName());
		}
		f.set(this, nip);
		return nip;
	}

	@Override
	public String toString() {
		return id;
	}
}
