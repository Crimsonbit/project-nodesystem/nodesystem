/*
Copyright 2019 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.node;

import java.util.Optional;

import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeOutputPort;
import at.crimsonbit.nodesystem.nodebackend.util.NodePortId;

/**
 * A GraphNode is a Node, that represents a whole {@link NodeGraph}. It uses the
 * globalInputs and globalOutputs of the graph as its inputs and outputs. The
 * compute method of this node computes the whole internal nodeGraph
 * 
 * @author Alexander Daum
 *
 */
public class GraphNode extends AbstractNode {

	private final NodeGraph graph;

	public GraphNode(NodeGraph internalGraph) {
		graph = internalGraph;
		for (NodePortId id : graph.getGlobalInputs()) {
			Optional<INodeInputPort> port = graph.getInputPort(id.node, id.port);
			port.ifPresent(p -> addInput(new ReferenceInputPort(p, this)));
		}
		for (NodePortId id : graph.getGlobalOutputs()) {
			Optional<INodeOutputPort> port = graph.getOutputPort(id.node, id.port);
			port.ifPresent(p -> addOutput(new ReferenceOutputPort(p, this)));
		}
	}

	@Override
	public void compute() {
		graph.executeGraph();
	}

}
