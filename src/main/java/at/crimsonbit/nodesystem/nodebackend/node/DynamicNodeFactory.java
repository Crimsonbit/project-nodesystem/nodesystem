/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.node;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import at.crimsonbit.nodesystem.nodebackend.INodeType;
import at.crimsonbit.nodesystem.nodebackend.node.port.DynamicNodeField;
import at.crimsonbit.nodesystem.nodebackend.node.port.DynamicNodeInput;
import at.crimsonbit.nodesystem.nodebackend.node.port.DynamicNodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeField;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeOutputPort;
import htl.gtm.nodeLangParser.signal.Signal;
import htl.gtm.nodeLangParser.util.Tuple;

public class DynamicNodeFactory extends NodeFactory {

	public final INodeType type;
	private final NamedType[] inputs;
	private final NamedType[] outputs;
	private final NamedType[] fields;
	public final BiFunction<Map<String, Object>, Map<String, Class<?>>, Map<String, Object>> computeFunction;

	public DynamicNodeFactory(INodeType type, NamedType[] inputs, NamedType[] outputs, NamedType[] fields,
			BiFunction<Map<String, Object>, Map<String, Class<?>>, Map<String, Object>> computeFunction) {
		this.type = type;
		this.inputs = inputs;
		this.outputs = outputs;
		this.fields = fields;
		this.computeFunction = computeFunction;
	}
	
	@Override
	public AbstractNode create(String id) {
		DynamicPortNode node = new DynamicPortNode(computeFunction);
		Map<String, INodeInputPort> inputMap = new HashMap<>();
		Map<String, INodeOutputPort> outputMap = new HashMap<>();
		Map<String, INodeField> fieldMap = new HashMap<>();
		for (NamedType in : inputs) {
			DynamicNodeInput port = new DynamicNodeInput(node, in.name, in.type);
			inputMap.put(in.name, port);
		}
		for (NamedType out : outputs) {
			DynamicNodeOutput port = new DynamicNodeOutput(node, out.name, out.type);
			outputMap.put(out.name, port);
		}
		for (NamedType fi : fields) {
			DynamicNodeField field = new DynamicNodeField(node, fi.name, fi.type);
			fieldMap.put(fi.name, field);
		}
		node.initialize(inputMap, outputMap, fieldMap);
		node.setId(id);
		node.setType(type);
		return node;
	}

	@Override
	public INodeType getType() {
		return type;
	}

	public static class NamedType {
		public final String name;
		public final Class<?> type;

		public NamedType(String name, Class<?> type) {
			this.name = name;
			this.type = type;
		}

		public static NamedType fromTuple(Tuple<String, Signal.Type> t) {
			return new NamedType(t.a, t.b.getType());
		}
	}

}
