/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.module;

public class ModuleID implements Comparable<ModuleID> {
	private static final String LATEST_VERSION = "\uFFFF";
	public final String moduleID;
	public final String version;

	public ModuleID(String moduleID, String version) {
		super();
		this.moduleID = moduleID;
		this.version = version;
	}

	public String getModuleID() {
		return moduleID;
	}

	public String getVersion() {
		return version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((moduleID == null) ? 0 : moduleID.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModuleID other = (ModuleID) obj;
		if (moduleID == null) {
			if (other.moduleID != null)
				return false;
		} else if (!moduleID.equals(other.moduleID))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	static ModuleID getLatest(String id) {
		return new ModuleID(id, LATEST_VERSION);
	}

	@Override
	public int compareTo(ModuleID o) {
		if(o == null)
			return 1;
		int scomp = this.moduleID.compareTo(o.moduleID);
		if (scomp == 0)
			return this.version.compareTo(o.version);
		else
			return scomp;
	}

}
