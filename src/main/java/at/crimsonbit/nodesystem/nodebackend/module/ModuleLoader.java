/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.module;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TreeMap;
import java.util.logging.Logger;
import java.util.stream.Stream;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;

import at.crimsonbit.nodesystem.nodebackend.exception.RuntimeNodeException;
import at.crimsonbit.nodesystem.nodebackend.master.NodeMaster;

/**
 * A Class for loading modules. You will not really need to use this class
 * directly, as {@link NodeMaster} already handles calls to the loader
 * 
 * @author Alexander Daum
 *
 */
public class ModuleLoader {
	private static final String MODULEDEFS_FILENAME = "/moduledefs.json";
	private static final String MODULEDEFS_ID_KEY = "id";
	private static final String MODULEDEFS_VERSION_KEY = "version";
	/**
	 * A List of all Search Paths
	 */
	private List<Path> searchPaths = new ArrayList<>();

	private TreeMap<ModuleID, Path> index = null;
	private boolean indexed = true;
	private ClassLoader classLoader = getClass().getClassLoader();

	/**
	 * Add a search Path for modules
	 * 
	 * @param path
	 */
	public void addSearchPath(Path path) {
		searchPaths.add(path);
		indexed = false;
	}

	/**
	 * Add a search Path for modules
	 * 
	 * @param path
	 */
	public void addSearchPath(String path) {
		addSearchPath(Paths.get(path));
	}

	/**
	 * Add a search Path for modules
	 * 
	 * @param path
	 */
	public void addSearchPath(URI path) {
		addSearchPath(Paths.get(path));
	}

	/**
	 * Add a search Path for modules
	 * 
	 * @param path
	 */
	public void addSearchPath(File path) {
		addSearchPath(path.toPath());
	}

	public List<Path> getSearchPaths() {
		return Collections.unmodifiableList(searchPaths);
	}

	/**
	 * Load a Moduledef and put it into the index map
	 * 
	 * @param p
	 * @throws IOException
	 */
	private void index(Path p) throws IOException {
		Files.walk(p, FileVisitOption.FOLLOW_LINKS).filter(f -> Files.isRegularFile(f))
				.filter(f -> f.getFileName().toString().endsWith(".jar")).forEach((f) -> {
					JsonObject jsonObj = null;
					try (FileSystem fs = FileSystems.newFileSystem(f, null)) {
						Path moduledefs = fs.getPath(MODULEDEFS_FILENAME);
						if (!Files.exists(moduledefs)) {
							Logger.getGlobal().info("No moduledefs File in " + f.toString());
							return;
						}
						try (BufferedReader in = Files.newBufferedReader(moduledefs)) {
							Object ucJson = Json.parse(in);
							if (ucJson instanceof JsonObject) {
								jsonObj = (JsonObject) ucJson;
							} else {
								throw new RuntimeNodeException(
										"Moduledefs in " + p.toAbsolutePath().toString() + " is not a json object");
							}
						}
					} catch (IOException e) {
						throw new RuntimeNodeException(e);
					}
					String id = jsonObj.getString(MODULEDEFS_ID_KEY, "null");
					String version = jsonObj.getString(MODULEDEFS_VERSION_KEY, "null");
					ModuleID mid = new ModuleID(id, version);
					index.put(mid, f);
				});
	}

	/**
	 * Index the module folders. This method loads the moduledef from each Module
	 * jar file and maps the modules to their IDs.
	 */
	public void index() {
		index = new TreeMap<ModuleID, Path>();
		for (Path p : searchPaths) {
			try {
				index(p);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	// TODO implement!
	public NodeModule loadModuleVersion() {
		return null;
	}

	/**
	 * Load a NodeModule object from a module jar. This method will load the latest
	 * version, if you need an older or specific version, use
	 * {@link ModuleLoader#loadModuleVersion()}. <br>
	 * This method also indexes the module paths, if they are not yet registered
	 * with {@link ModuleLoader#index()}. All classes from the module are then
	 * loaded and the NodeModule is instantiated and returned. Also all .strings
	 * Files will be registered with {@link LanguageSetup}
	 * 
	 * @param moduleID The ID of the module to load.
	 * @return A NodeModule to access the loaded module
	 */
	public NodeModule loadModule(String moduleID) {
		if (!indexed)
			index();
		Entry<ModuleID, Path> moduleEntry = index.floorEntry(ModuleID.getLatest(moduleID));
		if (moduleEntry == null) {
			return null;
		}
		if (!moduleEntry.getKey().moduleID.equals(moduleID)) {
			return null;
		}
		ClassLoader cl = null;
		try {
			cl = new URLClassLoader(new URL[] { moduleEntry.getValue().toUri().toURL() }, this.classLoader);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		this.classLoader = cl;
		// Walk all Files in the jar and register classes
		NodeModule module = loadJar(moduleEntry.getValue(), moduleID);
		return module;
	}

	public Path getModulePath(String moduleID) {
		if (!indexed)
			index();

		Entry<ModuleID, Path> moduleEntry = index.floorEntry(ModuleID.getLatest(moduleID));
		return moduleEntry.getValue();
	}

	/**
	 * Load all Modules that are located in any of the search folders. If the
	 * folders are not yet indexed, then {@link ModuleLoader#index()} is called. If
	 * you don't want to load all Modules, but just specific ones, use
	 * {@link ModuleLoader#loadModule()} instead.
	 * 
	 * @return A List of all Modules that have been loaded
	 */
	public Map<String, NodeModule> loadAllModules() {
		if (!indexed)
			index();

		if (index == null) {
			System.err.println("Index was null " + searchPaths.size() + " Paths are in the searchPath");
			return new HashMap<>();
		}

		ClassLoader cl = null;
		URL[] urls = new URL[index.values().size()];
		int i = 0;
		for (Path p : index.values()) {
			try {
				urls[i] = p.toUri().toURL();
				i++;
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
		cl = new URLClassLoader(urls, this.classLoader);
		this.classLoader = cl;

		Map<String, NodeModule> modules = new HashMap<>();
		for (Entry<ModuleID, Path> entry : index.entrySet()) {
			NodeModule mod = loadJar(entry.getValue(), entry.getKey().moduleID);
			modules.put(entry.getKey().moduleID, mod);
		}
		return modules;
	}

	/**
	 * Load a ModuleJar. This loads all Classes with the classloader and if the
	 * class extends {@link NodeModule}, then it is instantiated using the default
	 * constructor and returned after all other classes have been loaded. <br>
	 * This also loads all the language files, these end with .strings
	 * 
	 * @param modulePath
	 * @return
	 */
	private NodeModule loadJar(Path modulePath, String moduleName) {
		Optional<NodeModule> module = Optional.empty();
		try (FileSystem fs = FileSystems.newFileSystem(modulePath, null)) {
			Stream<Path> paths = Files.walk(fs.getPath("/")).filter(p -> p != null).filter(p -> {
				return p.toString().endsWith(".class");
			});
			Stream<String> classNames = paths.map(p -> p.toString().replace(fs.getSeparator(), "."))
					.map(n -> n.substring(1, n.length() - ".class".length()));
			Stream<Class<?>> classes = classNames.map(n -> {
				try {
					return this.classLoader.loadClass(n);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
					return null;
				}
			});
			Optional<Class<?>> clazz = classes.filter(c -> NodeModule.class.isAssignableFrom(c)).findAny();
			if (clazz.isPresent()) {
				NodeModule m = (NodeModule) clazz.get().newInstance();
				m.setName(moduleName);
				module = Optional.of(m);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		if (!module.isPresent()) {
			throw new RuntimeNodeException("Could not load module with Path" + modulePath);
		}
		return module.get();
	}
}
