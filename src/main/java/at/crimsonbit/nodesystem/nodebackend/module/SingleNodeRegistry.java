package at.crimsonbit.nodesystem.nodebackend.module;

import at.crimsonbit.nodesystem.nodebackend.node.NodeFactory;

public class SingleNodeRegistry extends NodeRegistry {

	public SingleNodeRegistry(String name, NodeFactory factory) {
		super(name);
		super.registerCustomFactory(factory.getType(), factory);
	}

}
