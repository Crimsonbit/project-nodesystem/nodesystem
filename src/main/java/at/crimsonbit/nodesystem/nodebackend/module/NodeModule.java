/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.module;

import java.util.function.Predicate;

import at.crimsonbit.nodesystem.nodebackend.INodeType;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

/**
 * A Class extending this Class represents the definition Class of a NodeModule.
 * Each NodeModule must specify exactly one class extending NodeModule <br>
 * Each Module also needs a module.json File directly in its main folder. This
 * File specifies the id and the version of the module. <br>
 * Objects of this class are typically very long lived, as they get instantiated
 * when the model is loaded and removed when either the model is unloaded, or
 * the program is closed
 * 
 * @author Alexander Daum
 *
 */
public abstract class NodeModule {
	/**
	 * Registers all the Nodes this module defines with the registry. All Nodes that
	 * are registered must extend {@link AbstractNode}. For Each Node there is also
	 * an instance of {@link INodeType} required. Typically you will create an enum
	 * that implements INodeType and use the enum constants for the different Nodes
	 * 
	 * @param registry
	 */
	public abstract void registerNodes(NodeRegistry registry);

	private String name;

	/**
	 * Set the name of the NodeModule, is used internally
	 * 
	 * @param name
	 */
	protected void setName(String name) {
		this.name = name;
	}

	public final String getName() {
		return name;
	}

}
