/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.module;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import at.crimsonbit.nodesystem.nodebackend.INodeType;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.node.DefaultNodeFactory;
import at.crimsonbit.nodesystem.nodebackend.node.NodeFactory;
import at.crimsonbit.nodesystem.nodebackend.util.BackendUtils;

public class NodeRegistry {

	private final Map<INodeType, NodeRegistryObject> registryMap = new HashMap<>();
	private final String name;
	private final boolean allowDuplicates;

	public static class NodeRegistryObject {
		public final NodeFactory factory;

		public NodeRegistryObject(NodeFactory factory) {
			super();
			this.factory = factory;
		}
	}

	public NodeRegistry(String name) {
		this(name, false);
	}

	public NodeRegistry(String name, boolean allowDuplicates) {
		this.name = name;
		this.allowDuplicates = allowDuplicates;
	}

	public void register(INodeType type, NodeRegistryObject node) {
		registryMap.put(type, node);
	}

	public void registerCustomFactory(INodeType type, NodeFactory factory) {
		register(type, new NodeRegistryObject(factory));
	}

	public void registerDefaultFactory(INodeType type, Class<? extends AbstractNode> nodeClass) {
		registerCustomFactory(type, new DefaultNodeFactory(type, nodeClass));
	}

	public Map<INodeType, NodeRegistryObject> getRegistry() {
		return Collections.unmodifiableMap(registryMap);
	}

	public <T, R> void registerCast(Class<T> typeFrom, Class<R> typeTo, Function<T, R> cast) {
		BackendUtils.addCast(typeFrom, typeTo, cast);
	}
	
	public <T> void registerSerialization(Class<T> type, Function<T, String> toString, Function<String, T> fromString) {
		BackendUtils.addStringifier(type, toString, fromString);
	}

	public String getName() {
		return name;
	}
}
