package at.crimsonbit.nodesystem.nodebackend.util;

import java.util.HashMap;
import java.util.Map;

public class SerializationUtils {

	private final Map<Class<?>, Stringifier<?>> stringifiers = new HashMap<>();

	@SuppressWarnings("unchecked")
	private <T> Stringifier<T> get(Class<T> type) {
		return (Stringifier<T>) stringifiers.get(type);
	}

	public boolean canSerialize(Class<?> type) {
		return stringifiers.containsKey(type);
	}

	public <T> void registerStringification(Class<T> type, Stringifier<T> s) {
		stringifiers.put(type, s);
	}

	public <T> T deserialize(String s, Class<T> type) {
		Stringifier<T> str = this.get(type);
		if (str != null)
			return str.fromString.apply(s);
		return null;
	}

	public <T> String serialize(T t) {
		@SuppressWarnings("unchecked")
		Stringifier<T> str = this.get((Class<T>) t.getClass());
		if (str != null)
			return str.toString.apply(t);
		return null;
	}
}
