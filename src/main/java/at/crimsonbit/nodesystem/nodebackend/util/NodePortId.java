/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NodePortId {
	public final String node;
	public final String port;
	public final int index;

	private static final Pattern nodePattern = Pattern
			.compile("((?<node>\\w+)\\.)?(?<port>\\w+)(\\[(?<index>\\d+)\\])?");

	public NodePortId(String wholestring) {
		Matcher m = nodePattern.matcher(wholestring);
		if (!m.matches()) {
			throw new IllegalArgumentException("Node name " + wholestring + " is not a valid Node Port Identifier");
		}
		node = m.group("node");
		port = m.group("port");
		String idx = m.group("index");
		if (idx != null)
			index = Integer.parseInt(idx);
		else
			index = 0;
	}

	public NodePortId(String node, String port) {
		this(node + "." + port);
	}
}
