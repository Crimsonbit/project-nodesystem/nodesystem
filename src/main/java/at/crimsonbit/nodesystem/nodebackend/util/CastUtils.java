/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

public class CastUtils {
	private List<CastFunction> castList = new ArrayList<>();

	private Cache<TypeKey, Function<Object, Object>> castCache = CacheBuilder.newBuilder().maximumSize(4096).build();

	public void addCast(CastFunction cast) {
		castList.add(cast);
	}

	public boolean isCastPossible(TypeKey castTypes) {
		return castCache.getIfPresent(castTypes) != null || findBestCast(castTypes) != null;
	}

	@SuppressWarnings("unchecked")
	public <T, R> R cast(T t, Class<R> clazzTo) {
		Class<?> clazzFrom = t.getClass();
		TypeKey key = new TypeKey(clazzFrom, clazzTo);
		if (isCastPossible(key)) {
			Function<T, R> cast = (Function<T, R>) findBestCast(key);
			if (cast != null)
				return cast.apply(t);
		}
		return null;
	}

	/**
	 * Finds the best cast for the given TypeKey in the castList and puts it into
	 * the cache if not null. If there is already an entry for this key in the
	 * cache, it is returned
	 * 
	 * @param key
	 * @return
	 */
	private Function<Object, Object> findBestCast(TypeKey key) {
		Function<Object, Object> inCache = castCache.getIfPresent(key);
		if (inCache != null)
			return inCache;
		CastFunction bestFit = null;
		Class<?> bestFrom = Object.class;
		for (CastFunction cf : castList) {
			// check if the cast can take the correct class and outputs a fitting type
			if (cf.typeFrom.isAssignableFrom(key.typeFrom) && key.typeTo.isAssignableFrom(cf.typeTo)) {
				// check if the current functions cast is more specific than the current best
				if (bestFrom.isAssignableFrom(cf.typeFrom)) {
					bestFit = cf;
					bestFrom = cf.typeFrom;
				}
			}
		}
		if (bestFit != null) {
			castCache.put(key, bestFit.cast);
			return bestFit.cast;
		}
		return null;
	}

}
