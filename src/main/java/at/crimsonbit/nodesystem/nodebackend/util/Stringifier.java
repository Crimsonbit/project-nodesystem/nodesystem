package at.crimsonbit.nodesystem.nodebackend.util;

import java.util.function.Function;

public class Stringifier<T> {
	public final Function<T, String> toString;
	public final Function<String, T> fromString;

	public Stringifier(Function<T, String> toString, Function<String, T> fromString) {
		super();
		this.toString = toString;
		this.fromString = fromString;
	}

}
