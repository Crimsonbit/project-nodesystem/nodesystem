/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.util;

public final class TypeKey {
	public final Class<?> typeFrom, typeTo;

	public TypeKey(Class<?> typeFrom, Class<?> typeTo) {
		this.typeFrom = typeFrom;
		this.typeTo = typeTo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((typeFrom == null) ? 0 : typeFrom.hashCode());
		result = prime * result + ((typeTo == null) ? 0 : typeTo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TypeKey other = (TypeKey) obj;
		if (typeFrom == null) {
			if (other.typeFrom != null)
				return false;
		} else if (!typeFrom.equals(other.typeFrom))
			return false;
		if (typeTo == null) {
			if (other.typeTo != null)
				return false;
		} else if (!typeTo.equals(other.typeTo))
			return false;
		return true;
	}
}
