/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.util;

import java.util.function.Function;

public class CastFunction {
	public final Class<?> typeFrom;
	public final Class<?> typeTo;
	public final Function<Object, Object> cast;

	public CastFunction(Class<?> typeFrom, Class<?> typeTo, Function<Object, Object> cast) {
		super();
		this.typeTo = typeTo;
		this.typeFrom = typeFrom;
		this.cast = cast;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cast == null) ? 0 : cast.hashCode());
		result = prime * result + ((typeFrom == null) ? 0 : typeFrom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CastFunction other = (CastFunction) obj;
		if (cast == null) {
			if (other.cast != null)
				return false;
		} else if (!cast.equals(other.cast))
			return false;
		if (typeFrom == null) {
			if (other.typeFrom != null)
				return false;
		} else if (!typeFrom.equals(other.typeFrom))
			return false;
		return true;
	}
}
