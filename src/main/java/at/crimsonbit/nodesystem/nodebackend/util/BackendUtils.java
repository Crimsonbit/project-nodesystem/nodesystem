/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.nio.file.ProviderNotFoundException;
import java.nio.file.spi.FileSystemProvider;
import java.util.Map;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Logger;

import com.google.common.primitives.Primitives;

public class BackendUtils {
	private static CastUtils castUtils = new CastUtils();
	private static SerializationUtils serUtils = new SerializationUtils();

	@SuppressWarnings("unchecked")
	public static <T, R> void addCast(Class<T> typeFrom, Class<R> typeTo, Function<T, R> cast) {
		castUtils.addCast(new CastFunction(typeFrom, typeTo, (Function<Object, Object>) cast));
	}

	public static byte[] intToByteArray(int i) {
		byte[] b = new byte[4];
		b[0] = (byte) (i >> 24);
		b[1] = (byte) (i >> 16);
		b[2] = (byte) (i >> 8);
		b[3] = (byte) i;
		return b;
	}

	public static <T> Object tryToConvert(Object obj, Class<T> target) {
		if (obj == null)
			return null;
		if (target.isPrimitive()) {
			Class<T> wrapper = Primitives.wrap(target);
			if (Number.class.isAssignableFrom(wrapper) && obj instanceof Number) {
				Number num = (Number) obj;
				if (target == byte.class)
					return num.byteValue();
				if (target == short.class)
					return num.shortValue();
				if (target == int.class)
					return num.intValue();
				if (target == float.class)
					return num.floatValue();
				if (target == double.class)
					return num.doubleValue();
			}

			if (target == byte.class)
				return Byte.parseByte(obj.toString());
			if (target == short.class)
				return Short.parseShort(obj.toString());
			if (target == int.class)
				return Integer.parseInt(obj.toString());
			if (target == long.class)
				return Long.parseLong(obj.toString());
			if (target == float.class)
				return Float.parseFloat(obj.toString());
			if (target == double.class)
				return Double.parseDouble(obj.toString());
			if (target == char.class)
				return obj.toString().toCharArray()[0];
			if (target == boolean.class)
				return Boolean.parseBoolean(obj.toString());
		} else if (target.isInstance(obj))
			return target.cast(obj);
		else {
			return castUtils.cast(obj, target);
		}
		return null;
	}

	public static int byteArrayToInt(byte[] b) {
		if (b.length < 4) {
			throw new IllegalArgumentException("Expected at least 4 bytes for an int");
		}
		int i = 0;
		i |= b[3];
		i |= b[2] << 8;
		i |= b[1] << 16;
		i |= b[0] << 24;
		return i;
	}

	public static boolean canBeConverted(Class<?> clazzFrom, Class<?> clazzTo) {
		if (clazzFrom == clazzTo)
			return true;
		if (clazzFrom.isPrimitive()) {
			clazzFrom = Primitives.wrap(clazzFrom);
		}
		if (clazzTo.isPrimitive()) {
			clazzTo = Primitives.wrap(clazzTo);
		}
		if (clazzTo.isAssignableFrom(clazzFrom)) {
			return true;
		}
		if (Number.class.isAssignableFrom(clazzFrom) && Number.class.isAssignableFrom(clazzTo)) {
			return true;
		}

		if (clazzTo == Character.class && clazzFrom == String.class) {
			// special case for char
			return true;
		}

		if (Number.class.isAssignableFrom(clazzFrom) && Number.class.isAssignableFrom(clazzTo)
				&& Primitives.isWrapperType(clazzTo)) {
			return true;
		}
		return castUtils.isCastPossible(new TypeKey(clazzFrom, clazzTo));

	}

	public static boolean isClassCompatible(Class<?> clazz, Object obj) {
		if (obj == null)
			return true;
		return canBeConverted(obj.getClass(), clazz);
	}

	/**
	 * Constructs a new {@code FileSystem} to access the contents of a file as a
	 * file system.
	 *
	 * <p>
	 * This method makes use of specialized providers that create pseudo file
	 * systems where the contents of one or more files is treated as a file system.
	 *
	 * <p>
	 * This method iterates over the {@link FileSystemProvider#installedProviders()
	 * installed} providers. It invokes, in turn, each provider's
	 * {@link FileSystemProvider#newFileSystem(Path,Map) newFileSystem(Path,Map)}
	 * method with an empty map. If a provider returns a file system then the
	 * iteration terminates and the file system is returned. If none of the
	 * installed providers return a {@code FileSystem} then an attempt is made to
	 * locate the provider using the given class loader. If a provider returns a
	 * file system then the lookup terminates and the file system is returned.
	 *
	 * @param path
	 *            the path to the file
	 * @param loader
	 *            the class loader to locate the provider or {@code null} to only
	 *            attempt to locate an installed provider
	 *
	 * @return a new file system
	 *
	 * @throws ProviderNotFoundException
	 *             if a provider supporting this file type cannot be located
	 * @throws ServiceConfigurationError
	 *             when an error occurs while loading a service provider
	 * @throws IOException
	 *             if an I/O error occurs
	 * @throws SecurityException
	 *             if a security manager is installed and it denies an unspecified
	 *             permission
	 */
	public static FileSystem newFileSystem(Path path, Map<String, ?> env, ClassLoader loader) throws IOException {
		if (path == null)
			throw new NullPointerException();

		// check installed providers
		for (FileSystemProvider provider : FileSystemProvider.installedProviders()) {
			try {
				return provider.newFileSystem(path, env);
			} catch (UnsupportedOperationException uoe) {
			}
		}

		// if not found, use service-provider loading facility
		if (loader != null) {
			ServiceLoader<FileSystemProvider> sl = ServiceLoader.load(FileSystemProvider.class, loader);
			for (FileSystemProvider provider : sl) {
				try {
					return provider.newFileSystem(path, env);
				} catch (UnsupportedOperationException uoe) {
				}
			}
		}

		throw new ProviderNotFoundException("Provider not found");
	}

	/**
	 * Try to convert the given String to the Type given by its class. It looks if a
	 * conversion from String is registered, if not then it checks if the Class
	 * defines a constructor (public or private) or a valueOf method (public or
	 * private) and invokes it. THIS IS NOT A WELL WRITTEN METHOD!
	 * 
	 * @param input
	 * @return
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	@SuppressWarnings("unchecked")
	public static <T> T tryToConvertFromString(String input, Class<T> type)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException {
		if (canBeConverted(String.class, type)) {
			return (T) tryToConvert(input, type);
		}
		Method valueOf = null;
		Constructor<T> constructor = null;
		try {
			valueOf = type.getMethod("valueOf", String.class);
		} catch (NoSuchMethodException e) {
			try {
				valueOf = type.getDeclaredMethod("valueOf", String.class);
			} catch (NoSuchMethodException e1) {
				try {
					constructor = type.getConstructor(String.class);
				} catch (NoSuchMethodException e2) {
					try {
						constructor = type.getDeclaredConstructor(String.class);
					} catch (NoSuchMethodException e3) {
						return null;
					} catch (SecurityException e3) {
						e3.printStackTrace();
					}
				} catch (SecurityException e2) {
					e2.printStackTrace();
				}
			} catch (SecurityException e1) {
				e1.printStackTrace();
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		if (valueOf != null) {
			valueOf.setAccessible(true);
			if (!Modifier.isStatic(valueOf.getModifiers()))
				return null;
			return (T) valueOf.invoke(null, input);
		} else if (constructor != null) {
			constructor.setAccessible(true);
			return constructor.newInstance(input);
		}
		return null;

	}

	public static void logError(String extraMessage, Exception e) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
		e.printStackTrace(new PrintStream(stream));
		String stackTrace = stream.toString();
		try {
			stream.close();
		} catch (IOException e1) {
			// should never actually be able to throw an IOException
			logger.severe("Caught an IOException closing ByteArrayOutputStream " + e1.getMessage()
					+ ", something is broken, error only logged to console");
			System.err.println("=====Caught Exception in Error Logging routine======");
			e1.printStackTrace();
			System.err.println("=======Original Error==============");
			e.printStackTrace();
			return;
		}
		logger.severe(extraMessage + "\n" + stackTrace);
	}

	/**
	 * Checks if a Type can be stored in a NodeLang file, specifically this returns
	 * true if the type is either a primitive type or a String can be converted to
	 * that type with all custom casting Rules
	 * 
	 * @param type
	 * @return
	 */
	public static boolean canBeStoredInNodeLang(Class<?> type) {
		if (type.isPrimitive())
			return true;
		else
			return canBeConverted(String.class, type);
	}

	public static <T> Predicate<T> consumerPredicate(Predicate<T> p, Consumer<T> c) {
		return t -> {
			boolean b = p.test(t);
			if (b)
				c.accept(t);
			return b;
		};
	}

	public static <T> void addStringifier(Class<T> type, Function<T, String> toString, Function<String, T> fromString) {
		serUtils.registerStringification(type, new Stringifier<T>(toString, fromString));
	}

	public static Object fromString(String s, Class<?> type) {
		if (canBeConverted(String.class, type))
			return tryToConvert(s, type);
		else
			return serUtils.deserialize(s, type);
	}

	public static String serializeToString(Object o) {
		if (canBeConverted(String.class, o.getClass())) {
			if (canBeConverted(o.getClass(), String.class))
				return (String) tryToConvert(o, String.class);
			else
				return o.toString();
		} else if (serUtils.canSerialize(o.getClass())) {
			return serUtils.serialize(o);
		}
		throw new IllegalArgumentException("Cannot serialize object of class " + o.getClass() + " to a String");

	}

	public static boolean canSerialize(Class<?> type) {
		boolean convertToString = canBeConverted(type, String.class);
		if (!convertToString)
			return serUtils.canSerialize(type);
		return convertToString;
	}

}
