package at.crimsonbit.nodesystem.nodebackend.nodelang;

import java.util.List;

import com.google.common.primitives.Primitives;

import at.crimsonbit.nodesystem.nodebackend.master.NodeMaster;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.node.GraphNodeFactory;
import at.crimsonbit.nodesystem.nodebackend.node.ReferenceInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.ReferenceOutputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodePort;

public class NLPartGraphType extends NLPartJavaType {

	public NLPartGraphType(AbstractNode node, NodeMaster master) {
		super(node, master);
	}

	@Override
	protected void writeSource(List<String> l) {
		StringBuilder sb = new StringBuilder();
		sb.append("src=");
		GraphNodeFactory fac = (GraphNodeFactory) master.getFactoryFromType(node.getType());
		sb.append(fac.graph.id);
		sb.append(".nl");
		l.add(sb.toString());
	}

	// TODO do this better
	@Override
	protected void writePort(List<String> l, INodePort port, String type) {
		INodePort referenced;
		if (port instanceof ReferenceInputPort) {
			referenced = ((ReferenceInputPort) port).reference;
		} else if (port instanceof ReferenceOutputPort) {
			referenced = ((ReferenceOutputPort) port).reference;
		} else {
			throw new IllegalStateException("GraphNode Type with Port " + port + ", that is not a ReferencePort");
		}
		
		StringBuilder b = new StringBuilder();
		b.append("\t");
		b.append(type);
		b.append(' ');
		Class<?> clazz = referenced.getType();
		if (clazz.isPrimitive() && Number.class.isAssignableFrom(Primitives.wrap(clazz)))
			b.append("number ");
		else if (clazz == boolean.class)
			b.append("boolean ");
		b.append(referenced.getID());
		l.add(b.toString());
	}

}
