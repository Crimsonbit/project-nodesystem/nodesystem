package at.crimsonbit.nodesystem.nodebackend.nodelang;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class SerializationFieldHandler implements NLSpecialFieldHandler {

	private Map<String, Object> oMap = new HashMap<>();

	@Override
	public String addSpecial(Object o) {
		if (o == null)
			throw new IllegalArgumentException("Cannot serialize null");
		if (!(o instanceof Serializable))
			throw new IllegalArgumentException("Object of Type " + o.getClass() + " cannot be serialized");
		String baseName = o.getClass().getSimpleName() + '.';
		for (int i = 0; i < Integer.MAX_VALUE; i++) {
			String key = baseName + i;
			if (!(oMap.containsKey(key))) {
				oMap.put(key, o);
				return key;
			}
		}
		throw new RuntimeException("No key could be found for Object " + o);
	}

	public void serialize(ObjectOutputStream oos) throws IOException {
		oos.writeObject(oMap);
	}

}
