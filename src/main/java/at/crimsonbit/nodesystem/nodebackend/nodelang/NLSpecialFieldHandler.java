package at.crimsonbit.nodesystem.nodebackend.nodelang;

import at.crimsonbit.nodesystem.nodebackend.node.port.INodeField;
import at.crimsonbit.nodesystem.nodebackend.util.BackendUtils;

public interface NLSpecialFieldHandler {

	String addSpecial(Object o);

	default String add(INodeField field) {
		if (BackendUtils.canSerialize(field.getType()) || field.getType().isPrimitive()) {
			return field.get().toString();
		}
		if (field.get() == null)
			return "null";
		return addSpecial(field.get());
	}
}
