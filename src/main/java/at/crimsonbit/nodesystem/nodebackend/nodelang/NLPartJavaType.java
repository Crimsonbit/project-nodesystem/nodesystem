package at.crimsonbit.nodesystem.nodebackend.nodelang;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import com.google.common.primitives.Primitives;

import at.crimsonbit.nodesystem.nodebackend.INodeType;
import at.crimsonbit.nodesystem.nodebackend.master.NodeMaster;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeField;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeOutputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodePort;

public class NLPartJavaType implements INLPart {

	protected final AbstractNode node;
	protected final NodeMaster master;

	public NLPartJavaType(AbstractNode node, NodeMaster master) {
		this.node = node;
		this.master = master;
	}

	/**
	 * Write the beginning, by default "node "
	 * 
	 * @param b
	 */
	protected void begin(StringBuilder b) {
		b.append("node ");
	}

	/**
	 * Write the name of the node type, uses {@link INodeType#regName()}
	 * 
	 * @param b
	 */
	protected void writeName(StringBuilder b) {
		b.append(node.getType().regName());
		b.append(' ');
	}

	/**
	 * Writes the source module of the type. Uses
	 * {@link NodeMaster#getModuleOfNode(INodeType)} to find out in which module the
	 * node is. The String is then added to the list.
	 * 
	 * <br>
	 * This method can be overwritten by subclasses that have no type to not add
	 * anything in the map.
	 * 
	 */
	protected void writeSource(List<String> l) {
		StringBuilder sb = new StringBuilder();
		sb.append("src=");
		String moduleName = master.getModuleOfNode(node.getType());
		sb.append(moduleName);
		sb.append(':');
		sb.append(node.getType().regName());
		l.add(sb.toString());
	}

	/**
	 * Write the field tag for a field entry in the tag. The resulting String is
	 * written to the List l.
	 * 
	 * @param l
	 * @param key
	 * @param f
	 */
	protected void writeField(List<String> l, String key, INodeField f) {
		if (!f.shouldBeSerialized())
			return;
		Class<?> type = f.getType();
		StringBuilder sb = new StringBuilder();
		if (type.isPrimitive() && Number.class.isAssignableFrom(Primitives.wrap(type)))
			sb.append("number ");
		if (type == boolean.class)
			sb.append("boolean ");
		sb.append(key);
		sb.append("=");
		sb.append(f.get());
		l.add(sb.toString());
	}

	/**
	 * Write all Fields of the Node by using
	 * {@link NLPartJavaType#writeField(List, String, INodeField)}
	 * 
	 * @param l
	 *            The List to collect all field strings
	 */
	protected void writeFields(List<String> l) {
		for (Entry<String, INodeField> field : node.getFields().entrySet()) {
			writeField(l, field.getKey(), field.getValue());
		}
	}

	/**
	 * Writes the Tag of the Node. The Tag is the part in NodeLang that is between
	 * angled braces. This first writes the source of the node, then writes the
	 * fields and collects all in a List. Then each entry in the List is written to
	 * the Tag.
	 * 
	 * @param b
	 */
	protected void writeTag(StringBuilder b) {
		List<String> l = new ArrayList<>();
		writeSource(l);
		writeFields(l);

		if (l.isEmpty())
			return;
		b.append('<');
		for (int i = 0; i < l.size(); i++) {
			if (i != 0)
				b.append(',');
			b.append(l.get(i));
		}
		b.append('>');
	}

	/**
	 * Writes the start section for ports, this is a ( followed by a newline.
	 * 
	 * @param b
	 */
	protected void startPorts(StringBuilder b) {
		b.append(" (\n");
	}

	/**
	 * Write a Port. The parameter type specifies if it is an output or input port.
	 * This method declares fields with primitive number types as "number" and
	 * boolean types as boolean. The reason why non-primitive number types are
	 * stored as objects is, that a number type is stored as a double value and for
	 * some implementations of Number, like {@link BigDecimal} there could be a loss
	 * of accuracy if stored as a double value. <br>
	 * The Port NodeLang text is then appended to List l
	 * 
	 * @param l
	 *            A list where all ports are collected
	 * @param port
	 *            The port
	 * @param type
	 *            a String representing the type, either "out" or "in"
	 */
	protected void writePort(List<String> l, INodePort port, String type) {
		StringBuilder b = new StringBuilder();
		b.append("\t");
		b.append(type);
		b.append(' ');
		Class<?> clazz = port.getType();
		if (clazz.isPrimitive() && Number.class.isAssignableFrom(Primitives.wrap(clazz)))
			b.append("number ");
		else if (clazz == boolean.class)
			b.append("boolean ");
		b.append(port.getName());
		l.add(b.toString());
	}

	/**
	 * Write all Ports by using
	 * {@link NLPartJavaType#writePort(List, INodePort, String)}
	 * 
	 * @param b
	 */
	protected void writePorts(StringBuilder b) {
		List<String> l = new ArrayList<>();
		for (Entry<String, INodeInputPort> inPort : node.getInputs().entrySet()) {
			writePort(l, inPort.getValue(), "in");
		}

		for (Entry<String, INodeOutputPort> outPort : node.getOutputs().entrySet()) {
			writePort(l, outPort.getValue(), "out");
		}
		for (int i = 0; i < l.size(); i++) {
			if (i != 0) {
				b.append(",\n");
			}
			b.append(l.get(i));
		}
	}

	protected void end(StringBuilder b) {
		b.append("\n);\n");
	}

	@Override
	public void write(StringBuilder b) {
		if (node == null)
			throw new IllegalStateException("The node is null");
		begin(b);
		writeName(b);
		writeTag(b);
		startPorts(b);
		writePorts(b);
		end(b);
	}

}
