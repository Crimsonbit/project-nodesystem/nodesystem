package at.crimsonbit.nodesystem.nodebackend.nodelang;

import java.util.Collection;

import at.crimsonbit.nodesystem.nodebackend.graph.NodeConnection;

public class NLPartConnection implements INLPart {

	protected final Collection<NodeConnection> connections;

	public NLPartConnection(Collection<NodeConnection> con) {
		this.connections = con;
	}

	protected void begin(StringBuilder b) {
		b.append("connect (\n");
	}

	protected void writeFrom(StringBuilder b, NodeConnection conn) {
		b.append('\t');
		b.append(conn.target.getID());
	}

	protected void writeTo(StringBuilder b, NodeConnection conn) {
		b.append('(');
		b.append(conn.driver.getID());
		b.append(')');
	}

	protected void writeConnections(StringBuilder b) {
		int cnt = 0;
		for (NodeConnection conn : connections) {
			writeFrom(b, conn);
			writeTo(b, conn);
			if (cnt++ != connections.size() - 1)
				b.append(',');
			b.append('\n');
		}
	}

	protected void end(StringBuilder b) {
		b.append(");");
	}

	@Override
	public void write(StringBuilder b) {
		begin(b);
		writeConnections(b);
		end(b);
	}

}
