package at.crimsonbit.nodesystem.nodebackend.nodelang;

import at.crimsonbit.nodesystem.nodebackend.master.NodeMaster;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.node.DynamicNodeFactory;
import at.crimsonbit.nodesystem.nodebackend.node.DynamicNodeType;
import at.crimsonbit.nodesystem.nodebackend.node.GraphNodeFactory;
import at.crimsonbit.nodesystem.nodebackend.node.GraphNodeType;

public class NLTypePartFactory {

	private static NLPartJavaType createDynamicType(AbstractNode node, NodeMaster master) {
		DynamicNodeFactory fac = (DynamicNodeFactory) master.getFactoryFromType(node.getType());
		if (fac.computeFunction instanceof LuaComputeFunction) {
			return new NLPartLuaType(node, master);
		} else
			throw new IllegalArgumentException("Non lua dynamic Nodes are not implemented yet");
	}

	public static NLPartJavaType createPart(AbstractNode node, NodeMaster master) {
		if (node.getType() instanceof DynamicNodeType) {
			return createDynamicType(node, master);
		}
		if(node.getType() instanceof GraphNodeType) {
			return new NLPartGraphType(node, master);
		}
		return new NLPartJavaType(node, master);
	}
}
