package at.crimsonbit.nodesystem.nodebackend.nodelang;

import java.util.Map.Entry;

import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeField;

public class NLPartJavaNode implements INLPart {

	private final AbstractNode node;
	private final NLSpecialFieldHandler fieldHandler;

	public NLPartJavaNode(AbstractNode node, NLSpecialFieldHandler fieldHandler) {
		super();
		this.node = node;
		this.fieldHandler = fieldHandler;
	}

	protected void begin(StringBuilder b) {
		b.append("create ");
	}

	protected void writeType(StringBuilder b) {
		b.append(node.getType().regName());
		b.append(' ');
	}

	/**
	 * Write the value of the field. This is {@link Object#toString()} by default.
	 * Implementations with different storage method of fields can override this
	 * method
	 * 
	 * @param o
	 * @return
	 */
	protected String writeFieldValue(INodeField o) {
		return fieldHandler.add(o);
	}

	protected void writeField(StringBuilder b, Entry<String, INodeField> entry, boolean first) {
		if (!first)
			b.append(',');
		b.append(entry.getKey());
		b.append('=');
		b.append(writeFieldValue(entry.getValue()));
	}

	protected void writeTag(StringBuilder b) {
		if (node.getFields().entrySet().isEmpty())
			return;
		b.append('<');
		boolean first = true;
		for (Entry<String, INodeField> entry : node.getFields().entrySet()) {
			writeField(b, entry, first);
			first = false;
		}
		b.append("> ");
	}

	protected void writeName(StringBuilder b) {
		b.append(node.getId());
	}

	/**
	 * Write the connection segment of the Node Section. This is (); by default, so
	 * no connections are created in the node, because all connections are created
	 * separately.
	 * 
	 * @param b
	 */
	protected void writeConnSection(StringBuilder b) {
		b.append("();\n");
	}

	protected void writeEnd(StringBuilder b) {
		b.append('\n');
	}

	@Override
	public void write(StringBuilder b) {
		begin(b);
		writeType(b);
		writeTag(b);
		writeName(b);
		writeConnSection(b);
		writeEnd(b);
	}

}
