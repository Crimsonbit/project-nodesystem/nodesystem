package at.crimsonbit.nodesystem.nodebackend.nodelang;

public class NodeLangBuilder {

	private StringBuilder text = new StringBuilder();

	public void addPart(INLPart part) {
		part.write(text);
	}

	public String toString() {
		return text.toString();
	}
}
