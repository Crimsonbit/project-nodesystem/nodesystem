package at.crimsonbit.nodesystem.nodebackend.nodelang;

public interface INLPart {
	/**
	 * Write a String representation of this NodeLang section to the StringBuilder
	 * 
	 * @param b
	 */
	void write(StringBuilder b);
}
