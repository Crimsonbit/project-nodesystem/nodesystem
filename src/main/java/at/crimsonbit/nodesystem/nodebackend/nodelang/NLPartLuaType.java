package at.crimsonbit.nodesystem.nodebackend.nodelang;

import java.util.List;

import at.crimsonbit.nodesystem.nodebackend.master.NodeMaster;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.node.DynamicNodeFactory;
import at.crimsonbit.nodesystem.nodebackend.node.NodeFactory;

public class NLPartLuaType extends NLPartJavaType {

	public NLPartLuaType(AbstractNode node, NodeMaster master) {
		super(node, master);
	}

	@Override
	protected void writeSource(List<String> l) {
		// Do nothing here, no source
	}

	protected void writeLuaFunction(StringBuilder b) {
		NodeFactory fac = master.getFactoryFromType(node.getType());
		if (fac instanceof DynamicNodeFactory) {
			DynamicNodeFactory dfac = (DynamicNodeFactory) fac;
			if (!(dfac.computeFunction instanceof LuaComputeFunction))
				throw new IllegalArgumentException("Tried to create NLPartLuaType with Node Type "
						+ node.getType().regName() + " which is a dynamic type, but does use a non-lua comput method");
			LuaComputeFunction fun = (LuaComputeFunction) dfac.computeFunction;
			String program = fun.program;
			b.append(program);
		} else {
			throw new IllegalArgumentException("Tried to create NLPartLuaType with Node Type "
					+ node.getType().regName() + " which is no Dynamic Type");
		}
	}

	@Override
	protected void end(StringBuilder b) {
		b.append("\n){");
		writeLuaFunction(b);
		b.append("};\n");
	}

}
