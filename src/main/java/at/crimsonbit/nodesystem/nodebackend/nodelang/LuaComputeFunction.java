/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.nodelang;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.logging.Logger;

import at.crimsonbit.nodesystem.nodebackend.node.CachedCompilerChunkLoader;
import at.crimsonbit.nodesystem.nodebackend.util.BackendUtils;
import net.sandius.rembulan.StateContext;
import net.sandius.rembulan.Table;
import net.sandius.rembulan.Variable;
import net.sandius.rembulan.compiler.CompilerChunkLoader;
import net.sandius.rembulan.env.RuntimeEnvironments;
import net.sandius.rembulan.exec.CallException;
import net.sandius.rembulan.exec.CallPausedException;
import net.sandius.rembulan.exec.DirectCallExecutor;
import net.sandius.rembulan.impl.StateContexts;
import net.sandius.rembulan.lib.StandardLibrary;
import net.sandius.rembulan.load.ChunkLoader;
import net.sandius.rembulan.load.LoaderException;
import net.sandius.rembulan.runtime.LuaFunction;

public class LuaComputeFunction implements BiFunction<Map<String, Object>, Map<String, Class<?>>, Map<String, Object>> {

	private static final CachedCompilerChunkLoader chunkLoader = CachedCompilerChunkLoader.of("nodesyste_lua_");
	private static int next_id = 0;

	private final int id;
	public final String program;

	public LuaComputeFunction(String program) {
		this.program = program;
		id = next_id++;
	}

	@Override
	public Map<String, Object> apply(Map<String, Object> ins, Map<String, Class<?>> outTypes) {
		HashMap<String, Object> returnMap = new HashMap<>();
		StateContext state = StateContexts.newDefaultInstance();

		Table env = StandardLibrary.in(RuntimeEnvironments.system()).installInto(state);

		for (Map.Entry<String, Object> e : ins.entrySet()) {
			env.rawset(e.getKey(), e.getValue());
		}

		LuaFunction main;
		try {
			main = chunkLoader.loadTextChunk(new Variable(env), "lua_compute_function", program, id);
			DirectCallExecutor.newExecutor().call(state, main);

		} catch (LoaderException | CallException | CallPausedException | InterruptedException e1) {
			BackendUtils.logError("Exception in executing LuaComputeFunction", e1);
		}
		for (Map.Entry<String, Class<?>> e : outTypes.entrySet()) {
			Object o = env.rawget(e.getKey());
			if (!BackendUtils.isClassCompatible(e.getValue(), o)) {
				Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).severe(
						"Return of LuaFunction of Type " + o.getClass() + " is not compatible with " + e.getValue());
			} else
				returnMap.put(e.getKey(), o);
		}
		return returnMap;
	}

}
