/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend;

/**
 * Each Node requires a type, that is an Object that implements INodeType.
 * Typically you will create an enum implementing this interface and use the
 * enum constants as NodeTypes
 * 
 * @author Alexander Daum
 *
 */
public interface INodeType {
	/**
	 * Get the name of this INodeType. For enums, this is already implemented
	 * 
	 * @return
	 */
	String name();

	/**
	 * Get the registry Name of this NodeType. By default this returns
	 * {@link INodeType#name()} in all lower case
	 * 
	 * @return
	 */
	default String regName() {
		return name().toLowerCase();
	}

	@Override
	int hashCode();
}
