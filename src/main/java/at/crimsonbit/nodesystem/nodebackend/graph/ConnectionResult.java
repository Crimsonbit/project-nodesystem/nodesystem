/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.graph;

public enum ConnectionResult {
	/**
	 * Indicates a successful connection
	 */
	SUCCESS,
	/**
	 * The input port is already connected to some output
	 */
	ALREADY_REGISTERED,
	/**
	 * There is a circular dependency that can't be resolved
	 */
	DEPENDENCY_FAIL,
	/**
	 * The input and output are the same signal
	 */
	IO_SAME,
	/**
	 * Any of the ports is null
	 */
	PORT_NULL,
	/*
	 * The Signals are not of compatible types
	 */
	TYPE_FAIL
}
