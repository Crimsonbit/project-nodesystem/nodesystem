/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.graph;

import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeOutputPort;

/**
 * A Signal connects on output to one or more Inputs.
 * 
 * @author Alexander Daum
 *
 */
public class Signal implements INodeInputPort, INodeOutputPort {

	private Object signal;
	public final String id;

	public Signal(String id) {
		super();
		this.id = id;
	}


	@Override
	public Object get() {
		return signal;
	}

	@Override
	public boolean isRealPort() {
		return false;
	}

	@Override
	public AbstractNode getNode() {
		return null;
	}

	@Override
	public String getName() {
		return id;
	}

	@Override
	public String toString() {
		return getID();
	}

	@Override
	public Class<?> getType() {
		return Object.class;
	}

	@Override
	public void set(Object o) {
		signal = o;
	}


	@Override
	public boolean shouldBeSerialized() {
		return false;
	}

}
