/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.graph;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A Class for counting instances of Nodes with the default name, so that the
 * number can be incremented
 * 
 * @author Alexander Daum
 *
 */
public class NodeCounter {
	private Map<String, Integer> nameToCount = new HashMap<>();

	private static final Pattern numberedNamePattern = Pattern.compile("(\\D+)(\\d+)");

	/**
	 * Get the current count
	 * 
	 * @param id
	 * @return
	 */
	public int getCount(String id) {
		return nameToCount.getOrDefault(id, 0);
	}

	/**
	 * Get the current count and increment it by 1
	 * 
	 * @param id
	 * @return
	 */
	public int getAndIncrement(String id) {
		int current = nameToCount.getOrDefault(id, 0);
		nameToCount.put(id, current + 1);
		return current;
	}

	/**
	 * Get the next ID for the given Basename
	 * 
	 * @param baseName
	 * @return
	 */
	public String getNextID(String baseName) {
		int suffix = getAndIncrement(baseName);
		return baseName + suffix;
	}

	/**
	 * Check if the given Name matches the typical pattern used for numbering Nodes.
	 * If it does and has the number m, and the current number associated with the
	 * base name is n, then the stored number will be set to
	 * <code>max(n, m+1)</code>
	 * 
	 * @param id
	 *            The ID of a new Node
	 * @return the next id a Node of that base Name can have, or -1 if the String
	 *         does not match the pattern
	 */
	public int handlePossibleNumberedID(String id) {
		Matcher m = numberedNamePattern.matcher(id);
		if (!m.matches())
			return -1;
		String baseName = m.group(1);
		String sNumber = m.group(2);
		int number = Integer.parseInt(sNumber);
		Integer currNum = nameToCount.getOrDefault(baseName, 0);
		int nextSave = Math.max(currNum, number + 1);
		nameToCount.put(baseName, nextSave);
		return nextSave;
	}

}
