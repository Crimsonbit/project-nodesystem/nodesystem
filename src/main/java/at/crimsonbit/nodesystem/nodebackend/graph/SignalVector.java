/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.graph;

import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeOutputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.VarLenInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.VarLenOutputPort;

public class SignalVector implements VarLenInputPort, VarLenOutputPort {
	private Signal[] sigS;
	private final String name;

	@Override
	public INodeOutputPort getOut(int index) {
		if (index > sigS.length)
			return null;
		return sigS[index];
	}

	@Override
	public INodeInputPort getIn(int index) {
		if (index > sigS.length)
			return null;
		return sigS[index];
	}

	@Override
	public int getLength() {
		return sigS.length;
	}

	public SignalVector(int len, String name) {
		sigS = new Signal[len];
		for (int i = 0; i < sigS.length; i++) {
			sigS[i] = new Signal(name + "[" + i + "]");
		}
		this.name = name;
	}

	@Override
	public boolean isRealPort() {
		return false;
	}

	@Override
	public AbstractNode getNode() {
		return null;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Class<?> getType() {
		return Object.class;
	}

	@Override
	public Object get() {
		return null;
	}

	@Override
	public void set(Object o) {
	}

	@Override
	public boolean shouldBeSerialized() {
		return false;
	}
}
