/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.graph;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableMap;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import at.crimsonbit.nodesystem.event.GlobalInputAddedEvent;
import at.crimsonbit.nodesystem.event.GlobalInputRemovedEvent;
import at.crimsonbit.nodesystem.event.GlobalOutputAddedEvent;
import at.crimsonbit.nodesystem.event.GlobalOutputRemovedEvent;
import at.crimsonbit.nodesystem.event.NodeAddedEvent;
import at.crimsonbit.nodesystem.event.NodeComputationErrorEvent;
import at.crimsonbit.nodesystem.event.NodeComputationFinishedEvent;
import at.crimsonbit.nodesystem.event.NodeComputationStartEvent;
import at.crimsonbit.nodesystem.event.NodeConnectedEvent;
import at.crimsonbit.nodesystem.event.NodeDeletedEvent;
import at.crimsonbit.nodesystem.event.NodeDisconnectedEvent;
import at.crimsonbit.nodesystem.event.PortValueChangedEvent;
import at.crimsonbit.nodesystem.event.SignalToggleMuteEvent;
import at.crimsonbit.nodesystem.nodebackend.INodeType;
import at.crimsonbit.nodesystem.nodebackend.exception.NoSuchNode;
import at.crimsonbit.nodesystem.nodebackend.exception.RuntimeNodeException;
import at.crimsonbit.nodesystem.nodebackend.master.NodeMaster;
import at.crimsonbit.nodesystem.nodebackend.module.SingleNodeRegistry;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.node.DynamicNodeType;
import at.crimsonbit.nodesystem.nodebackend.node.GraphNodeFactory;
import at.crimsonbit.nodesystem.nodebackend.node.GraphNodeType;
import at.crimsonbit.nodesystem.nodebackend.node.NodeFactory;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeField;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeOutputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodePort;
import at.crimsonbit.nodesystem.nodebackend.node.port.VarLenInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.VarLenOutputPort;
import at.crimsonbit.nodesystem.nodebackend.util.BackendUtils;
import at.crimsonbit.nodesystem.nodebackend.util.NodePortId;
import at.crimsonbit.nodesystem.nodebackend.util.Tuple;

/**
 * A Class representing a Node Graph. This specifies all instances and
 * connections of Nodes. A Node graph can also be loaded and stored to a
 * nodelang file
 * 
 * @author Alexander Daum
 *
 */
public class NodeGraph {
	private final Map<String, AbstractNode> nodeByID;
	private final Map<INodeInputPort, NodeConnection> connections;
	private final Map<String, Signal> signals;

	private final List<String> endNodes;
	/**
	 * List of Outputs of this Graph. The String is the ID of the OutputPort that
	 * should be treated as a global output
	 */
	private final List<NodePortId> globalOutputs;
	/**
	 * List of Inputs of this Graph. The String is the ID of the InputPort that
	 * should be treated as a global output
	 */
	private final List<NodePortId> globalInputs;
	private final NodeCounter nodeCount;

	/**
	 * The ID of this NodeGraph. It can be used to get this NodeGraph from the
	 * {@link NodeMaster}, so you don't need to reference this Object the whole
	 * time. It is also necessary to use this, if you want to remove this Graph from
	 * the master.
	 */
	public final String id;
	/**
	 * Not final because it has to be set to null when the Graph should be deleted,
	 * otherwise it can't be garbage collected
	 */
	private NodeMaster master;

	public final EventBus eventBus;

	private void fireDisconnect(INodeInputPort input, INodeOutputPort output) {
		eventBus.post(new NodeDisconnectedEvent(input.getID(), output.getID()));
	}

	public NodeGraph(NodeMaster master, String id, EventBus bus) {
		nodeByID = new HashMap<>();
		connections = new HashMap<>();
		signals = new HashMap<>();
		nodeCount = new NodeCounter();
		globalInputs = new ArrayList<>();
		globalOutputs = new ArrayList<>();
		this.master = master;
		this.id = id;
		this.endNodes = new ArrayList<>();
		eventBus = bus;
		eventBus.register(this);
	}

	private void registerNode(String id, AbstractNode node) {
		nodeCount.handlePossibleNumberedID(id);
		if (nodeByID.containsKey(id)) {
			// Add with another number, but log warning
			if (id.matches(node.getType().regName() + "\\d+")) {
				String newID = addNode(node.getType());
				Logger.getGlobal().warning("Attempted to register Node: " + id
						+ ", that was already used, but could form new numbered id: " + newID);
				return;
			}
			throw new RuntimeNodeException("Node with id " + id + " is already registered!");
		}
		nodeByID.put(id, node);
		endNodes.add(id);
		eventBus.post(new NodeAddedEvent(id));
	}

	/**
	 * Adds a new Node to the graph with auto generated id. The id is in the form
	 * "type[autoincrement_number]".
	 * 
	 * @param type The type of the node to be registered
	 * @return The id of the instance
	 */
	public String addNode(INodeType type) {
		NodeFactory factory = master.getFactoryFromType(type);
		if (factory == null) {
			throw new NoSuchNode("No Node with type " + type);
		}
		String id = nodeCount.getNextID(type.regName());
		AbstractNode newNode = master.createNode(type, id);
		registerNode(id, newNode);
		return id;
	}

	/**
	 * Adds a new Node to the graph with auto generated id. The id is in the form
	 * "type[autoincrement_number]".
	 * 
	 * @param type The type of the node to be registered as a String. The string is
	 *             typically the node type name in all lower case letters.
	 * @return The id of the instance
	 */
	public String addNode(String type) {
		return addNode(master.getTypeByName(type));
	}

	/**
	 * Adds a new Node to the graph with specified id. id must not be in the form
	 * "type[some_number]", else the auto generated Nodes will have id conflicts and
	 * the program will not work
	 * 
	 * @param type The Type of Node to be registered
	 * @param id   The id (name) of the node instance
	 * @return The id of the instance (the same as the parameter id)
	 */
	public String addNode(INodeType type, String id) {
		AbstractNode newNode = master.createNode(type, id);
		registerNode(id, newNode);
		return id;
	}

	/**
	 * Removes a Node and all its connections
	 * 
	 * @param id The id of the node to be removed
	 * @return true if there was a node with the id and it was removed, false if
	 *         there was none
	 */
	public boolean removeNode(String id) {
		Objects.requireNonNull(id);
		if (!nodeByID.containsKey(id))
			return false;
		Predicate<Entry<INodeInputPort, NodeConnection>> filter = e -> e.getValue().oneOfNode(id);
		connections.entrySet()
				.removeIf(BackendUtils.consumerPredicate(filter, e -> fireDisconnect(e.getKey(), e.getValue().driver)));
		endNodes.remove(id);
		globalInputs.removeIf(p -> id.equals(p.node));
		globalOutputs.removeIf(p -> id.equals(p.node));
		nodeByID.remove(id);
		eventBus.post(new NodeDeletedEvent(id));
		return true;
	}

	public Map<String, AbstractNode> getNodeByIDMap() {
		return Collections.unmodifiableMap(nodeByID);
	}

	public Map<String, Signal> getSignals() {
		return Collections.unmodifiableMap(signals);
	}

	// TODO should this even be there?
	public Map<INodeInputPort, NodeConnection> getConnections() {
		return Collections.unmodifiableMap(connections);
	}

	public List<NodePortId> getGlobalOutputs() {
		return Collections.unmodifiableList(globalOutputs);
	}

	public List<NodePortId> getGlobalInputs() {
		return Collections.unmodifiableList(globalInputs);
	}

	/**
	 * 
	 * @param input
	 * @param output
	 * @return true if possible, else if not
	 */
	private boolean dependencyCheck(INodeInputPort input, INodeOutputPort output) {
		// TODO

		if (input.isRealPort() && output.isRealPort()) {
			if (input.getNode().equals(output.getNode()))
				return false;
		}
		return true;
	}

	public ConnectionResult checkConnection(INodeInputPort input, INodeOutputPort output) {
		if (input == null || output == null)
			return ConnectionResult.PORT_NULL;
		if (connections.containsKey(input)) {
			return ConnectionResult.ALREADY_REGISTERED;
		}
		if (input == output) {
			return ConnectionResult.IO_SAME;
		}
		if (!BackendUtils.canBeConverted(output.getType(), input.getType()))
			return ConnectionResult.TYPE_FAIL;
		if (!dependencyCheck(input, output))
			return ConnectionResult.DEPENDENCY_FAIL;

		return ConnectionResult.SUCCESS;
	}

	/**
	 * Adds a connection between an Input and an Output. An Input can either be the
	 * input of a node or a signal, the output can be the output of a node or a
	 * signal.
	 * 
	 * @param input
	 * @param output
	 * @return
	 */
	public ConnectionResult addConnection(INodeInputPort input, INodeOutputPort output) {
		ConnectionResult result = checkConnection(input, output);
		NodeConnection con = new NodeConnection(output, input);
		if (result == ConnectionResult.SUCCESS) {
			connections.put(input, con);
			if (output.isRealPort())
				endNodes.remove(output.getNode().getId());
			eventBus.post(new NodeConnectedEvent(input.getID(), output.getID()));
		}
		return result;
	}

	/**
	 * Adds a connection between an Input and an Output. An Input can either be the
	 * input of a node or a signal, the output can be the output of a node or a
	 * signal.
	 * 
	 * @param input
	 * @param output
	 * @return
	 */
	public ConnectionResult addConnection(String input, String output) {
		INodeInputPort in = getInputPort(input)
				.orElseThrow(() -> new RuntimeNodeException(input + " is neither input port nor signal"));
		INodeOutputPort out = getOutputPort(output)
				.orElseThrow(() -> new RuntimeNodeException(output + " is neither input port nor signal"));
		return addConnection(in, out);
	}

	/**
	 * Removes the connection to an input or a signal if there is any.
	 * 
	 * @param input The input to which the connection should be removed
	 * @return true if there was a connection that was removed, false if there was
	 *         none
	 */
	public boolean removeConnectionTo(INodeInputPort input) {
		NodeConnection output = connections.remove(input);
		if (output != null)
			eventBus.post(new NodeDisconnectedEvent(input.getID(), output.driver.getID()));
		return output != null;
	}

	/**
	 * Remove all Connections from an output or a signal if there are any
	 * 
	 * @param output
	 */
	public void removeConnectionsFrom(INodeOutputPort output) {
		connections.entrySet().removeIf(BackendUtils.consumerPredicate(e -> e.getValue().driver == output,
				e -> fireDisconnect(e.getKey(), output)));
	}

	/**
	 * Remove all Connections from an output or a signal if there are any
	 * 
	 * @param output
	 */
	public void removeConnectionsFrom(String output) {
		Optional<? extends INodeOutputPort> out = getOutputPort(output);
		out.ifPresent(o -> removeConnectionsFrom(o));
	}

	/**
	 * Removes all connections from and to the signal, so that the signal can be
	 * deleted afterwards
	 * 
	 * @param signal
	 */
	public void removeConnectionsFromAndTo(Signal signal) {
		removeConnectionTo(signal);
		removeConnectionsFrom(signal);
	}

	public void muteConnection() {

	}

	/**
	 * Add a global output Port, that will be an output if this graph is used in a
	 * graphNode
	 */
	public void addGlobalOutput(String port) {
		getOutputPort(port).orElseThrow(() -> new NoSuchNode("OutputPort " + port + " does not exist"));
		globalOutputs.add(new NodePortId(port));
		eventBus.post(new GlobalOutputAddedEvent(port));
	}

	/**
	 * Add a global output Port, that will be an output if this graph is used in a
	 * graphNode
	 */
	public void addGlobalInput(String port) {
		getInputPort(port).orElseThrow(() -> new NoSuchNode("OutputPort " + port + " does not exist"));
		globalInputs.add(new NodePortId(port));
		eventBus.post(new GlobalInputAddedEvent(port));
	}

	/**
	 * Remove an output from the global outputs.
	 * 
	 * @param port
	 * @return true when port was a global output and was removed, false otherwise
	 */
	public boolean removeGlobalOutput(String port) {
		eventBus.post(new GlobalOutputRemovedEvent(port));
		return globalOutputs.remove(new NodePortId(port));
	}

	/**
	 * Remove an input from the global inputs.
	 * 
	 * @param port
	 * @return true when port was a global input and was removed, false otherwise
	 */
	public boolean removeGlobalInput(String port) {
		eventBus.post(new GlobalInputRemovedEvent(port));
		return globalInputs.remove(new NodePortId(port));
	}

	/**
	 * Create a new Signal, if a Signal with given name already exists, return it
	 * instead
	 * 
	 * @param name The name of the signal
	 */
	public Signal createSignal(String name) {
		if (signals.containsKey(name)) {
			return signals.get(name);
		}
		Signal sig = new Signal(name);
		signals.put(name, sig);
		return sig;
	}

	/**
	 * Create a new Signal and connect it to specified output
	 * 
	 * @param signame A signal
	 * @param output  An output
	 */
	public Tuple<Signal, ConnectionResult> createSignalFromOutput(String signame, INodeOutputPort output) {
		Signal sig = createSignal(signame);
		ConnectionResult res = addConnection(sig, output);
		return new Tuple<>(sig, res);
	}

	/**
	 * Get a Signal by its name
	 * 
	 * @param name
	 * @return an optional with the signal, or an empty optional if there is no
	 *         signal with that name
	 */
	public Optional<Signal> getSignal(String name) {
		return Optional.ofNullable(signals.get(name));
	}

	/**
	 * Removes a Signal with the specified name and all Connections from and to that
	 * Signal. If there was no signal with that name, this method returns false.
	 * 
	 * @param signal The Signal to be removed
	 * @return true if there was a signal with the name and it was removed, false
	 *         otherwise
	 */
	public boolean removeSignal(String signal) {
		if (!signals.containsKey(signal))
			return false;
		removeConnectionsFromAndTo(signals.get(signal));
		signals.remove(signal);
		return true;
	}

	public void createNewGraphNode(Collection<String> nodes, String name) {
		List<String> notExisting = nodes.parallelStream().filter(o -> !getNodeByID(o).isPresent())
				.collect(Collectors.toList());
		if (!notExisting.isEmpty()) {
			throw new NoSuchNode("Some nodes could not be found", notExisting);
		}
		NodeGraph newGraph = master.createNewGraph(name);
		for (String id : nodes) {
			AbstractNode node = getNodeByID(id).get(); // can assume all nodes exist
			newGraph.addNode(node.getType(), id);
			for (INodeField f : node.getFields().values()) {
				newGraph.getEventBus().post(new PortValueChangedEvent(name, f.get()));
			}
		}
		Map<String, String> newConnections = new HashMap<>(); // from driver to target
		for (NodeConnection con : getConnections().values()) {
			String driverNodeID = con.driver.isRealPort() ? con.driver.getNode().getId() : "";
			String targetNodeID = con.target.isRealPort() ? con.target.getNode().getId() : "";

			boolean driverIn = nodes.contains(driverNodeID);
			boolean targetIn = nodes.contains(targetNodeID);

			if (driverIn && targetIn) {
				// internal node
				newGraph.addConnection(con.target, con.driver);
			} else if (driverIn && !targetIn) {
				// output of new graph
				newGraph.addGlobalOutput(con.driver.getID());
				newConnections.put(name + "." + con.driver.getName(), con.target.getID());
			} else if (!driverIn && targetIn) {
				// input of new graph
				newGraph.addGlobalInput(con.target.getID());
				newConnections.put(con.driver.getID(), name + "." + con.target.getName());
			}
		}
		nodes.forEach(this::removeNode);
		GraphNodeFactory fac = new GraphNodeFactory(newGraph, new GraphNodeType(name));
		master.registerNodes(new SingleNodeRegistry(name, fac));
		addNode(fac.getType(), name);
		newConnections.forEach((driver, target) -> addConnection(target, driver));
	}

	public void computeNodeNoOutputs(String id) {
		computeNode(id);
	}

	public Map<String, Object> computeNode(String id) {
		return computeNode(id, new HashMap<>());
	}

	/**
	 * 
	 * @param id
	 * @param computedNodes A Map of nodes that were already computed and the values
	 *                      are already present in them to the Values
	 * @return
	 */
	private Map<String, Object> computeNode(String id, Map<String, Object> nodeCache) {
		AbstractNode node = nodeByID.get(id);
		if (node == null)
			throw new NoSuchNode("No Node with id: " + id);

		if (nodeCache.containsKey(node.getId())) {
			throw new RuntimeNodeException("Error, Node " + node.getId()
					+ " is already computed, but has been requested again. This is an internal Error");
		}
		for (INodeInputPort port : node.getInputs().values()) {
			computeNodePort(id, nodeCache, port);
		}
		eventBus.post(new NodeComputationStartEvent(id));
		// try {
		node.compute();
		// } catch (Exception e) {
		// eventBus.post(new NodeComputationErrorEvent(id));
		// }
		Map<String, Object> cache = new HashMap<>();
		for (INodeOutputPort out : node.getOutputs().values()) {
			cache.put(out.getID(), out.get());
		}
		nodeCache.putAll(cache);
		eventBus.post(new NodeComputationFinishedEvent(id));

		return cache;
	}

	private void computeNodePort(String nodeID, Map<String, Object> nodeCache, INodeInputPort port) {
		NodeConnection conn = connections.get(port);
		if (conn == null) {
			eventBus.post(new NodeComputationErrorEvent(nodeID));
			return;
		}
		if (!conn.isEnabled())
			return;
		// correctly handle signals
		// TODO check if this can be done better
		while (!conn.driver.isRealPort()) {
			conn = connections.get((INodePort) conn.driver);
			if (conn == null) {
				eventBus.post(new NodeComputationErrorEvent(nodeID));
				return;
			}
		}
		AbstractNode driverNode = conn.driver.getNode();
		if (driverNode == null) {
			throw new RuntimeNodeException("INodeOutputPort " + conn + " is a real port, but has node null");
		}
		String driverID = driverNode.getId();
		if (!nodeCache.containsKey(conn.driver.getID())) {
			computeNode(driverID, nodeCache);
		}
		String outName = conn.driver.getID();
		Object signal = nodeCache.get(outName);
		port.set(signal);
	}

	/**
	 * Deletes this NodeGraph, it clears all of the internal Maps, as well as
	 * setting the master reference to null, so that this NodeGraph Object can be
	 * GarbageCollected
	 */
	public void clear() {
		nodeByID.clear();
		connections.clear();
		signals.clear();
		master = null;
	}

	public void delete() {
		master.deleteNodeGraph(id);
	}

	public void save(String path, boolean b) throws IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * Creates a new Node with the same Type as the given Node
	 * 
	 * @param nodeID
	 * @return The ID of the new node
	 */
	public String copyOfNode(String nodeID) {
		return addNode(nodeByID.get(nodeID).getType());
	}

	/**
	 * Get the AbstractNode from its ID. For most reasons the ID should be stored
	 * and when you really need the AbstractNode instance, then invoke this method
	 * instead of storing the AbstractNode instance for longer times
	 * 
	 * @param id
	 * @return
	 */
	public Optional<AbstractNode> getNodeByID(String id) {
		return Optional.ofNullable(nodeByID.get(id));
	}

	public Map<String, INodeInputPort> getInputsOfNode(String node) {
		Optional<AbstractNode> absNode = getNodeByID(node);
		if (!absNode.isPresent()) {
			return ImmutableMap.of();
		}
		return absNode.get().getInputs();
	}

	public Map<String, INodeOutputPort> getOutputsOfNode(String node) {
		Optional<AbstractNode> absNode = getNodeByID(node);
		if (!absNode.isPresent()) {
			return ImmutableMap.of();
		}
		return absNode.get().getOutputs();
	}

	public Map<String, INodeField> getFieldsOfNode(String node) {
		Optional<AbstractNode> absNode = getNodeByID(node);
		if (!absNode.isPresent()) {
			return ImmutableMap.of();
		}
		return absNode.get().getFields();
	}

	public Tuple<String, String> splitPortName(String portname) {
		String[] parts = portname.split("\\.");
		if (parts.length != 2)
			throw new RuntimeNodeException("Portname " + portname + " is invalid, has multiple dots");
		return new Tuple<>(parts[0], parts[1]);
	}

	public Optional<INodeInputPort> getInputPort(String node, String inputPort) {
		return Optional.ofNullable(getInputsOfNode(node).get(inputPort));
	}

	public Optional<? extends INodeInputPort> getInputPort(String portname) {
		NodePortId id = new NodePortId(portname);
		if (id.node != null) {
			Map<String, INodeInputPort> inMap = getInputsOfNode(id.node);
			if (inMap == null)
				return Optional.empty();
			INodeInputPort in = inMap.get(id.port);
			if (in instanceof VarLenInputPort)
				in = ((VarLenInputPort) in).getIn(id.index);
			return Optional.ofNullable(in);
		} else {
			return getSignal(id.port);
		}
	}

	public Optional<? extends INodeOutputPort> getOutputPort(String portname) {
		NodePortId id = new NodePortId(portname);
		if (id.node != null) {
			Map<String, INodeOutputPort> outMap = getOutputsOfNode(id.node);
			if (outMap == null)
				return Optional.empty();
			INodeOutputPort out = outMap.get(id.port);
			if (out instanceof VarLenOutputPort)
				out = ((VarLenOutputPort) out).getOut(id.index);
			return Optional.ofNullable(out);
		} else {
			return getSignal(portname);
		}
	}

	public Optional<INodeField> getField(String field) {
		if (field.contains(".")) {
			Tuple<String, String> split = splitPortName(field);
			return getField(split.a, split.b);
		} else {
			return Optional.empty();
		}
	}

	public Optional<INodeOutputPort> getOutputPort(String node, String outputPort) {
		return Optional.ofNullable(getOutputsOfNode(node).get(outputPort));
	}

	public Optional<INodeField> getField(String node, String field) {
		return Optional.ofNullable(getFieldsOfNode(node).get(field));
	}

	public boolean setField(String field, Object obj) {
		if (field.contains(".")) {
			Tuple<String, String> split = splitPortName(field);
			return setField(split.a, split.b, obj);
		} else {
			return false;
		}
	}

	public boolean setField(String node, String field, Object obj) {
		Optional<INodeField> f = getField(node, field);
		if (!f.isPresent())
			return false;
		INodeField nodeField = f.get();
		if (BackendUtils.canBeConverted(obj.getClass(), nodeField.getType())) {
			nodeField.set(BackendUtils.tryToConvert(obj, nodeField.getType()));
			return true;
		}
		return false;
	}

	/**
	 * Checks if the input port is already connected
	 * 
	 * @param port
	 * @return
	 */
	public boolean isConnected(INodeInputPort port) {
		return connections.containsKey(port);
	}

	/**
	 * Checks if an input port is connected to some output port
	 * 
	 * @param inport
	 * @return
	 */
	public boolean isConnected(String inport) {
		if (inport.contains(".")) {
			// is port
			Optional<? extends INodeInputPort> port = getInputPort(inport);
			if (!port.isPresent())
				return false;
			return connections.containsKey(port.get());

		} else {
			// is signal
			Optional<Signal> sig = getSignal(inport);
			if (!sig.isPresent())
				return false;
			return connections.containsKey(sig.get());
		}

	}

	/**
	 * Gets the type of a node by its id.
	 * 
	 * @param id
	 * @return The type of the node
	 */
	public INodeType getTypeOfNode(String id) {
		return getNodeByID(id).orElseThrow(() -> new NoSuchNode("No node with ID " + id)).getType();
	}

	/**
	 * Compute all End Nodes of the Graph. This only makes sense, if you have a
	 * Graph with Nodes, that do something when computing, other than setting some
	 * values. For example a Graph, that defines the execution of a Program, then
	 * you can put one or more end node at the end and call this Method
	 * 
	 * @return
	 */
	public Map<String, Object> executeGraph() {
		Map<String, Object> cache = new HashMap<>();
		for (String node : endNodes) {
			computeNode(node, cache);
		}
		return cache;
	}

	@Subscribe
	public void onFieldSet(PortValueChangedEvent e) {
		Optional<INodeField> oField = getField(e.id);
		if (oField.isPresent()) {
			INodeField f = oField.get();
			f.set(e.value);
		} else {
			Optional<? extends INodeInputPort> oIn = getInputPort(e.id);
			oIn.ifPresent(n -> n.set(e.value));

		}

	}

	@Subscribe
	public void onToggleMute(SignalToggleMuteEvent e) {
		Optional<? extends INodeInputPort> port = getInputPort(e.inPortID);
		if (port.isPresent()) {
			NodeConnection con = connections.get(port.get());
			con.setEnabled(!e.mute);
		}
	}

	public void subscribeToEventBus(Object o) {
		eventBus.register(o);
	}

	public EventBus getEventBus() {
		return eventBus;
	}

	/**
	 * Saves this NodeGraph in a File
	 * 
	 * @param file The file to save this NodeGraph to
	 * @throws IOException
	 * @see NodeMaster#saveGraphToFile(NodeGraph, Path)
	 */
	public void saveToFile(Path file) throws IOException {
		master.saveGraphToFile(this, file);
	}

	public NodeMaster getMaster() {
		return master;
	}

}
