/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.graph;

import at.crimsonbit.nodesystem.nodebackend.node.port.INodeInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeOutputPort;

/**
 * This is a Helper Class for managing connections in the {@link NodeGraph}
 * 
 * @author Alexander Daum
 *
 */
public class NodeConnection {
	private boolean enabled;
	public final INodeOutputPort driver;
	public final INodeInputPort target;

	public NodeConnection(INodeOutputPort driver, INodeInputPort target) {
		super();
		this.driver = driver;
		this.target = target;
		enabled = true;
	}

	public void disable() {
		enabled = false;
	}

	public void enable() {
		enabled = true;
	}

	public void toggle() {
		enabled = !enabled;
	}
	
	public void setEnabled(boolean ena) {
		enabled = ena;
	}

	/**
	 * @return if the Connection is currently enabled and should be used
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Propagate the Connection. This means, that the target is set to the output of
	 * the driver
	 */
	public void propagate() {
		if (enabled)
			target.set(driver.get());
	}

	/**
	 * Checks if one of the Ports is a Port of the given Node. This is useful for
	 * checking if this Connection should be removed when deleting a Node
	 * 
	 * @param nodeID
	 * @return
	 */
	public boolean oneOfNode(String nodeID) {
		return (driver.isRealPort() && driver.getNode().getId().equals(nodeID))
				|| (target.isRealPort() && target.getNode().getId().equals(nodeID));
	}

}
