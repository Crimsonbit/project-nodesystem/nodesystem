/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.master;

import java.io.IOException;
import java.nio.file.Path;

import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;

public interface NodeGraphSaver {
	/**
	 * Save the given NodeGraph to the Path specified by target. Target should be
	 * either the path of a directory where all files should be stored or a path to
	 * the file if only one is needed, depending on the implementation. How the
	 * graph should be stored is implementation dependent, a specific saver could
	 * e.g. store the graph in a nodelang file.
	 */
	void save(Path targetDir, NodeGraph graph) throws IOException;

	/**
	 * Load a Node Graph previously saved by a call to
	 * {@link NodeGraphSaver#save(Path, NodeGraph)}. This method should check if a
	 * saved graph exists in that directory before trying to load the graph
	 * 
	 * @param targetDir
	 * @return
	 */
	NodeGraph load(Path targetDir) throws IOException;

	/**
	 * Load a NodeGraph, but do
	 * 
	 * @param targetDir
	 * @param existing
	 * @return
	 */
	NodeGraph loadIntoExisting(Path targetDir, NodeGraph existing) throws IOException;
}
