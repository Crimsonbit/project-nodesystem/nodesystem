/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.master;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;

import com.google.common.collect.ImmutableMap;
import com.google.common.eventbus.EventBus;
import com.google.common.primitives.Primitives;

import at.crimsonbit.nodesystem.event.LoadEvent;
import at.crimsonbit.nodesystem.event.NodeGraphCreatedEvent;
import at.crimsonbit.nodesystem.event.SaveEvent;
import at.crimsonbit.nodesystem.event.ValueLoadedEvent;
import at.crimsonbit.nodesystem.nodebackend.INodeType;
import at.crimsonbit.nodesystem.nodebackend.config.NGFile;
import at.crimsonbit.nodesystem.nodebackend.exception.NoSuchModule;
import at.crimsonbit.nodesystem.nodebackend.exception.NoSuchNode;
import at.crimsonbit.nodesystem.nodebackend.exception.RuntimeNodeException;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import at.crimsonbit.nodesystem.nodebackend.module.DynamicNodeModule;
import at.crimsonbit.nodesystem.nodebackend.module.ModuleLoader;
import at.crimsonbit.nodesystem.nodebackend.module.NodeModule;
import at.crimsonbit.nodesystem.nodebackend.module.NodeRegistry;
import at.crimsonbit.nodesystem.nodebackend.module.NodeRegistry.NodeRegistryObject;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.node.DynamicNodeFactory;
import at.crimsonbit.nodesystem.nodebackend.node.DynamicNodeFactory.NamedType;
import at.crimsonbit.nodesystem.nodebackend.node.DynamicNodeType;
import at.crimsonbit.nodesystem.nodebackend.node.NodeFactory;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeField;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeOutputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.NodeFieldObj;
import at.crimsonbit.nodesystem.nodebackend.nodelang.LuaComputeFunction;
import htl.gtm.nodeLangParser.Instance;
import htl.gtm.nodeLangParser.Node;
import htl.gtm.nodeLangParser.NodeLangParser;
import htl.gtm.nodeLangParser.exception.NodeLangException;
import htl.gtm.nodeLangParser.signal.Consumer;
import htl.gtm.nodeLangParser.signal.LuaVariable;
import htl.gtm.nodeLangParser.signal.Port;
import htl.gtm.nodeLangParser.signal.Producer;
import htl.gtm.nodeLangParser.signal.Signal;
import htl.gtm.nodeLangParser.signal.Wire;
import htl.gtm.nodeLangParser.util.Tuple;

/**
 * A Node Master is used to Manage registration and connection of Node, which
 * are represented by Classes, that extend {@link AbstractNode} <br>
 * All Node creation and connection management has to happen through an instance
 * of this class
 * 
 * 
 * @author Alexander Daum
 *
 */
public class NodeMaster {

	private final Map<INodeType, NodeFactory> factories;
	/**
	 * A list of all loaded modules
	 */
	private final Map<String, NodeModule> loadedModules = new HashMap<>();

	// map to enable creating Nodes by using strings
	private final Map<String, INodeType> stringToType;
	// Map the NodeType to the name of their source Module
	private final Map<INodeType, String> nodeSources;

	private final Map<String, NodeGraph> graphs;

	protected final ModuleLoader moduleLoader = new ModuleLoader();
	private final EventBus eventBus;

	public NodeMaster(EventBus bus) {
		this.eventBus = bus;
		stringToType = new HashMap<>();
		factories = new HashMap<>();
		graphs = new HashMap<>();
		nodeSources = new HashMap<>();
	}
	
	public NodeMaster() {
		this(new EventBus());
	}

	/**
	 * Gets a Map of Loaded Module ids to their modules as an unmodifiable List
	 * 
	 * @return
	 */
	public Map<String, NodeModule> getLoadedModules() {
		return Collections.unmodifiableMap(loadedModules);
	}

	/**
	 * Gets the Map of INodeTypes to their Factories as an unmodifiable Map
	 * 
	 * @return
	 */
	public Map<INodeType, NodeFactory> getFactories() {
		return Collections.unmodifiableMap(factories);
	}

	/**
	 * Gets the Map of String ids to INodeTypes as an unmodifiable Map
	 * 
	 * @return
	 */
	public Map<String, INodeType> getStrToType() {
		return Collections.unmodifiableMap(stringToType);
	}

	/**
	 * Get a NodeType by its Name. Returns null if there is no type associated to
	 * that name
	 * 
	 * @param unlocalizedName
	 * @return
	 */
	public INodeType getTypeByName(String unlocalizedName) {
		return stringToType.get(unlocalizedName);
	}

	/**
	 * Gets a NodeFactory from the NodeType as INodeType. If you want the Factory of
	 * a type by its String, use {@link NodeMaster#getFactoryFromType(String)}
	 * 
	 * @param type
	 * @return
	 */
	public NodeFactory getFactoryFromType(INodeType type) {
		return factories.get(type);
	}

	/**
	 * Gets a NodeFactory from the NodeType as String. If you want the Factory of a
	 * type by its INodeType, use {@link NodeMaster#getFactoryFromType(INodeType)}
	 * 
	 * @param type
	 * @return
	 */
	public NodeFactory getFactoryFromType(String type) {
		return factories.get(stringToType.get(type));
	}

	public Optional<NodeFactory> getOptionalFactoryFromType(INodeType type) {
		return Optional.ofNullable(getFactoryFromType(type));
	}

	public Optional<NodeFactory> getOptionalFactoryFromType(String type) {
		return Optional.ofNullable(getFactoryFromType(type));
	}

	/**
	 * Gets the Module a NodeType was registered in, or null if the type was not
	 * registered in any loaded module
	 * 
	 * @param type
	 * @return
	 */
	public String getModuleOfNode(INodeType type) {
		return nodeSources.get(type);
	}

	/**
	 * Get the Path the Module, in which a NodeType was registered in. If the Type
	 * was not registered in any loaded module, then a {@link RuntimeNodeException}
	 * is thrown.
	 * 
	 * @param type
	 * @return
	 */
	public Path getModulePathOfNode(INodeType type) {
		String moduleName = nodeSources.get(type);
		if (moduleName == null)
			throw new RuntimeNodeException("The type " + type + " was not registered in any loaded module");
		return moduleLoader.getModulePath(moduleName);
	}

	/**
	 * Get a Set of all registered INodeTypes as an unmodifiable Set
	 * 
	 * @return
	 */
	public Set<INodeType> getRegisteredNodes() {
		return Collections.unmodifiableSet(factories.keySet());
	}

	/**
	 * Register all Nodes from a NodeModule when its registry is given. If you want
	 * to register the nodes from a standard jar Module, use
	 * {@link NodeMaster#loadModule(String)} with its ID instead. <br>
	 * This can also be used to register Nodes without a Module by just putting them
	 * into a NodeRegistry and passing it to this method
	 * 
	 * @param reg
	 */
	public void registerNodes(NodeRegistry reg) {
		Map<INodeType, NodeRegistryObject> regMap = reg.getRegistry();
		for (Entry<INodeType, NodeRegistryObject> entry : regMap.entrySet()) {
			if (factories.containsKey(entry.getKey())) {
				throw new RuntimeNodeException(
						"Node " + entry.getKey().regName() + " is already registered, but is in registry again");
			}
			factories.put(entry.getKey(), entry.getValue().factory);
			stringToType.put(entry.getKey().regName(), entry.getKey());
			nodeSources.put(entry.getKey(), reg.getName());
		}
	}

	public void createDummyNode() {
		DynamicNodeModule dummyModule = new DynamicNodeModule("dummy");
		DynamicNodeType type = new DynamicNodeType("dummy_node");
		NamedType[] inputs = new NamedType[] { new NamedType("input", Double.class) };
		NamedType[] outputs = new NamedType[] { new NamedType("output", Double.class) };
		NamedType[] fields = new NamedType[] {};
		BiFunction<Map<String, Object>, Map<String, Class<?>>, Map<String, Object>> fn = new BiFunction<Map<String, Object>, Map<String, Class<?>>, Map<String, Object>>() {
			@Override
			public Map<String, Object> apply(Map<String, Object> t, Map<String, Class<?>> u) {
				Map<String, Object> ret = new HashMap<>();
				Double in1 = (Double) t.get("input");
				ret.put("output", in1.doubleValue() * 2);
				return ret;
			}
		};
		DynamicNodeFactory dummyFactory = new DynamicNodeFactory(type, inputs, outputs, fields, fn);
		dummyModule.addNode(type, dummyFactory);
		registerDynamicNodes(dummyModule);
	}

	/**
	 * Registers all Nodes from a {@link NodeModule}. This is only used internally
	 * and for testing, use {@link NodeMaster#loadModule(String)} and have the
	 * module as a jar file in the modules folder. Otherwise loading and saving <b>
	 * will not </b> work
	 * 
	 * @param module
	 */
	protected void registerNodes(NodeModule module) {
		if (module == null)
			throw new IllegalArgumentException("Tried to register null Module");
		NodeRegistry reg = new NodeRegistry(module.getName());
		module.registerNodes(reg);
		registerNodes(reg);
	}

	/**
	 * Registers the nodes in the module and adds the module to the loaded Modules
	 * folder. This should only be used with dynamic NodeModules, for normal jar
	 * Modules use {@link NodeMaster#loadModule(String)}
	 * 
	 * @param module
	 */
	protected void registerDynamicNodes(DynamicNodeModule module) {
		if (module == null)
			throw new IllegalArgumentException("Tried to register null Module");
		NodeRegistry reg = new NodeRegistry(module.getName());
		module.registerNodes(reg, t -> !factories.containsKey(t));
		registerNodes(reg);
		loadedModules.put(module.getName(), module);
	}

	/**
	 * Adds a folder as String to the module search path
	 * 
	 * @param moduleFolder
	 * @see NodeMaster#addModuleFolder(String)
	 */
	public void addModuleFolder(String moduleFolder) {
		addModuleFolder(Paths.get(moduleFolder));
	}

	/**
	 * Adds a folder as Path to the module search path
	 * 
	 * @param moduleFolder
	 * @see NodeMaster#addModuleFolder(String)
	 */
	public void addModuleFolder(Path moduleFolder) {
		// System.out.println("Module:" + moduleFolder.toAbsolutePath());
		moduleLoader.addSearchPath(moduleFolder);
	}

	/**
	 * Load a NodeModule from its ID
	 * 
	 * @param moduleID
	 * @see ModuleLoader#loadModule(String)
	 * @return
	 */
	public NodeModule loadModule(String moduleID) {
		if (!loadedModules.containsKey(moduleID)) {
			NodeModule mod = moduleLoader.loadModule(moduleID);
			loadedModules.put(moduleID, mod);
			if (mod == null)
				throw new NoSuchModule("No Module with name: " + moduleID + " exists in module Path");
			registerNodes(mod);
			return mod;
		} else
			return loadedModules.get(moduleID);
	}

	/**
	 * Loads all Modules and registers their nodes
	 */
	public void loadAllNodesFromModules() {
		for (Entry<String, NodeModule> mod : moduleLoader.loadAllModules().entrySet()) {
			if (!loadedModules.containsKey(mod.getKey())) {
				loadedModules.put(mod.getKey(), mod.getValue());
				registerNodes(mod.getValue());
			}
		}
	}

	/**
	 * Create a new AbstractNode with specified type and id. This method creates a
	 * new Instance of the node type by invoking {@link NodeFactory#create(String)}
	 * on its corresponding NodeFactory. Afterwards the NodeType and id as well as
	 * fields, inputs and outputs are set. <br>
	 * This Method will rarely have to be invoked, if you want to add a new Node to
	 * the Graph, just call {@link NodeGraph#addNode(INodeType)} or
	 * {@link NodeGraph#addNode(INodeType, String)}
	 * 
	 * @param type The Type of the Node to be created
	 * @param id   The id of the Node
	 * @return A new Abstract Node of the given Type
	 */
	public AbstractNode createNode(INodeType type, String id) {
		NodeFactory factory = factories.get(type);
		if (factory == null) {
			throw new RuntimeNodeException("No Factory defined for INodeType " + type);
		}
		AbstractNode node = factory.create(id);
		return node;
	}

	/**
	 * Create a new {@link NodeGraph} with the next empty ID and add it to the
	 * internal map.
	 * 
	 * @param name
	 * 
	 * @return the new NodeGraph
	 */
	public NodeGraph createNewGraph(String name) {
		return createNewGraph(new EventBus(), name);
	}

	public NodeGraph createNewGraph(EventBus bus, String id) {
		NodeGraph graph = new NodeGraph(this, id, bus);
		graphs.put(id, graph);
		eventBus.post(new NodeGraphCreatedEvent(graph));
		return graph;
	}

	/**
	 * Delete a NodeGraph by its ID
	 * 
	 * @param id
	 */
	public void deleteNodeGraph(String id) {
		deleteNodeGraph(graphs.get(id));
	}

	/**
	 * Delete a NodeGraph
	 */
	public void deleteNodeGraph(NodeGraph graph) {
		graphs.remove(graph.id);
		graph.clear();
	}

	/**
	 * Load a node Graph from a file. The file should be a nsys format file, for
	 * more info on the format refer to
	 * {@link NodeMaster#saveGraphToFile(NodeGraph, Path)} <br>
	 * For this to work, all used modules should already be in the module path by
	 * {@link NodeMaster#addModuleFolder(Path)} and should be loaded.
	 * 
	 * @param fileToLoad
	 * @return
	 * @throws IOException
	 * @throws NoSuchNode
	 * @throws NodeLangException
	 */
	public NodeGraph loadGraphFromFile(Path parentFolder, NodeGraph g)
			throws IOException, NoSuchNode, NodeLangException {
		NodeGraphSaver saver;
		if (isZip(parentFolder))
			saver = new ZipGraphSaver(this);
		else
			saver = new NodeLangGraphSaver(this);
		return saver.loadIntoExisting(parentFolder, g);
	}

	/**
	 * Load a node Graph from a file. The file should be a nsys format file, for
	 * more info on the format refer to
	 * {@link NodeMaster#saveGraphToFile(NodeGraph, Path)} <br>
	 * For this to work, all used modules should already be in the module path by
	 * {@link NodeMaster#addModuleFolder(Path)} and should be loaded.
	 * 
	 * @param fileToLoad
	 * @return
	 * @throws IOException
	 * @throws NoSuchNode
	 * @throws NodeLangException
	 */
	public NodeGraph loadGraphFromFile(Path parentFolder) throws IOException, NoSuchNode, NodeLangException {
		return loadGraphFromFile(parentFolder, this.createNewGraph("root"));
	}

	/**
	 * Load a node Graph from a nodelang file. This is a plain-text file, which can
	 * be written by hand and describes the connections. For more information visit
	 * TODO insert link for doc <br>
	 * 
	 * For this to work, all used modules should already be in the module path by
	 * {@link NodeMaster#addModuleFolder(Path)}. The modules are loaded
	 * automatically when an external node is declared in the nodelang file.
	 * 
	 * @param fileToLoad
	 * @return
	 * @throws IOException
	 * @throws NoSuchNode
	 * @throws NodeLangException
	 */
	public NodeGraph importGraphFromNodelang(Path fileToLoad, NodeGraph g)
			throws IOException, NoSuchNode, NodeLangException {
		NodeLangGraphSaver saver;
		if (isZip(fileToLoad))
			saver = new ZipGraphSaver(this);
		else
			saver = new NodeLangGraphSaver(this);
		saver.importGraphFromNodelang(fileToLoad, g, ImmutableMap.of());
		return g;
	}

	/**
	 * Load a node Graph from a nodelang file. This is a plain-text file, which can
	 * be written by hand and describes the connections. For more information visit
	 * TODO insert link for doc <br>
	 * 
	 * For this to work, all used modules should already be in the module path by
	 * {@link NodeMaster#addModuleFolder(Path)}. The modules are loaded
	 * automatically when an external node is declared in the nodelang file.
	 * 
	 * @param fileToLoad
	 * @return
	 * @throws IOException
	 * @throws NoSuchNode
	 * @throws NodeLangException
	 */
	public NodeGraph importGraphFromNodelang(Path fileToLoad) throws IOException, NoSuchNode, NodeLangException {
		return importGraphFromNodelang(fileToLoad, this.createNewGraph("root"));
	}

	/**
	 * 
	 * @param graph
	 * @param parentFolder
	 * @throws IOException
	 */
	public void saveGraphToFile(NodeGraph graph, Path parentFolder) throws IOException {
		NodeGraphSaver saver;
		if (isZip(parentFolder))
			saver = new ZipGraphSaver(this);
		else
			saver = new NodeLangGraphSaver(this);
		saver.save(parentFolder, graph);
	}

	private boolean isZip(Path parentFolder) {
		boolean zip = parentFolder.getFileName().toString().endsWith(".zip");
		boolean existingFolder = Files.exists(parentFolder) && Files.isDirectory(parentFolder);
		zip = zip && !existingFolder;
		return zip;
	}

}
