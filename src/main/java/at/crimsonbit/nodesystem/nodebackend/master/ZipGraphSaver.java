package at.crimsonbit.nodesystem.nodebackend.master;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import at.crimsonbit.nodesystem.nodebackend.util.BackendUtils;

public class ZipGraphSaver extends NodeLangGraphSaver {

	public ZipGraphSaver(NodeMaster master) {
		super(master);
	}

	@Override
	public void save(Path targetDir, NodeGraph graph) throws IOException {
		Map<String, String> env = new HashMap<>();
		env.put("create", String.valueOf(Files.notExists(targetDir)));

		try (FileSystem fs = BackendUtils.newFileSystem(targetDir, env, null)) {
			super.save(fs.getPath("/"), graph);
		}
	}

	@Override
	public NodeGraph load(Path targetDir) throws IOException {
		try (FileSystem fs = FileSystems.newFileSystem(targetDir, null)) {
			return super.load(fs.getPath("/"));
		}
	}

	@Override
	public NodeGraph loadIntoExisting(Path targetDir, NodeGraph existing) throws IOException {
		try (FileSystem fs = FileSystems.newFileSystem(targetDir, null)) {
			return super.loadIntoExisting(fs.getPath("/"), existing);
		}
	}

}
