/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.master;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;

import com.google.common.collect.ImmutableMap;

import at.crimsonbit.nodesystem.event.LoadEvent;
import at.crimsonbit.nodesystem.event.SaveEvent;
import at.crimsonbit.nodesystem.event.ValueLoadedEvent;
import at.crimsonbit.nodesystem.nodebackend.INodeType;
import at.crimsonbit.nodesystem.nodebackend.config.NGFile;
import at.crimsonbit.nodesystem.nodebackend.exception.NoSuchNode;
import at.crimsonbit.nodesystem.nodebackend.exception.RuntimeNodeException;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeConnection;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import at.crimsonbit.nodesystem.nodebackend.module.DynamicNodeModule;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.node.DynamicNodeFactory;
import at.crimsonbit.nodesystem.nodebackend.node.DynamicNodeFactory.NamedType;
import at.crimsonbit.nodesystem.nodebackend.node.DynamicNodeType;
import at.crimsonbit.nodesystem.nodebackend.node.GraphNodeFactory;
import at.crimsonbit.nodesystem.nodebackend.node.GraphNodeType;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeField;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeOutputPort;
import at.crimsonbit.nodesystem.nodebackend.nodelang.LuaComputeFunction;
import at.crimsonbit.nodesystem.nodebackend.nodelang.NLPartConnection;
import at.crimsonbit.nodesystem.nodebackend.nodelang.NLPartJavaNode;
import at.crimsonbit.nodesystem.nodebackend.nodelang.NLPartJavaType;
import at.crimsonbit.nodesystem.nodebackend.nodelang.NLTypePartFactory;
import at.crimsonbit.nodesystem.nodebackend.nodelang.NodeLangBuilder;
import at.crimsonbit.nodesystem.nodebackend.nodelang.SerializationFieldHandler;
import at.crimsonbit.nodesystem.nodebackend.util.BackendUtils;
import htl.gtm.nodeLangParser.Instance;
import htl.gtm.nodeLangParser.Node;
import htl.gtm.nodeLangParser.NodeLangParser;
import htl.gtm.nodeLangParser.exception.NodeLangException;
import htl.gtm.nodeLangParser.signal.Consumer;
import htl.gtm.nodeLangParser.signal.LuaVariable;
import htl.gtm.nodeLangParser.signal.Port;
import htl.gtm.nodeLangParser.signal.Producer;
import htl.gtm.nodeLangParser.signal.Signal;
import htl.gtm.nodeLangParser.signal.Wire;
import htl.gtm.nodeLangParser.util.Tuple;

/**
 * An Implementation of {@link NodeGraphSaver}, that saves a graph as a nodelang
 * file and stores object fields in an additional serialized map. <br>
 * Normally all necessary data can be stored in the nodelang file in a human
 * readable form, except Objects that have no simple String representation.
 * Objects are considered having a simple String representation if they are
 * either a String, a primitive or a String can be cast to it according to the
 * Rules of {@link BackendUtils#canBeConverted(Class, Class)}. <br>
 * 
 * 
 * @author Alexander Daum
 *
 */
public class NodeLangGraphSaver implements NodeGraphSaver {

	private final NodeMaster master;
	private final SerializationFieldHandler handler;

	public NodeLangGraphSaver(NodeMaster master) {
		super();
		this.master = master;
		handler = new SerializationFieldHandler();
	}

	@Override
	public void save(Path targetDir, NodeGraph graph) throws IOException {
		NGFile ngFile = NGFile.getFromOrCreateJson(targetDir);
		ngFile.setModulesFolder(master.moduleLoader.getSearchPaths());
		if (Files.exists(ngFile.root_nl))
			Files.delete(ngFile.root_nl);

		saveOneGraph(graph, ngFile.root_nl);

		Files.deleteIfExists(ngFile.objects);
		try (ObjectOutputStream oos = new ObjectOutputStream(Files.newOutputStream(ngFile.objects))) {
			handler.serialize(oos);
		}
		graph.eventBus.post(new SaveEvent(targetDir));
	}

	private void saveOneGraph(NodeGraph graph, Path nlFile) throws IOException {
		if (Files.exists(nlFile))
			Files.delete(nlFile);
		try (BufferedWriter writer = Files.newBufferedWriter(nlFile, StandardOpenOption.CREATE)) {
			final NodeLangBuilder nlBuilder = new NodeLangBuilder();

			List<NLPartJavaType> types = new ArrayList<>();
			List<NLPartJavaNode> nodes = new ArrayList<>();
			List<GraphNodeFactory> extraGraphs = new ArrayList<>();

			writeNodes(graph, types, nodes, extraGraphs);
			types.forEach(nlBuilder::addPart);
			nodes.forEach(nlBuilder::addPart);

			writeConnections(graph, nlBuilder);

			writer.write(nlBuilder.toString());
			writer.close();

			for (GraphNodeFactory fac : extraGraphs) {
				saveOneGraph(fac.graph, nlFile.getParent().resolve(fac.getType().name() + ".nl"));
			}
		}
	}

	@Override
	public NodeGraph load(Path targetDir) throws IOException {
		NodeGraph g = master.createNewGraph("root");
		return loadIntoExisting(targetDir, g);
	}

	@SuppressWarnings("unchecked")
	@Override
	public NodeGraph loadIntoExisting(Path targetDir, NodeGraph existing) throws IOException {
		Path ngFile = targetDir.resolve(".ng");
		NGFile ng = null;
		if (Files.exists(ngFile)) {
			ng = NGFile.getFromJson(targetDir);
		} else {
			throw new NoSuchFileException("The file .ng does not exist in the NodeSystem Folder "
					+ targetDir.toAbsolutePath().toString() + ", but is required to load the graph");
		}
		ng.modules.forEach(master::addModuleFolder);
		Map<String, Object> specialFields = null;
		if (Files.exists(ng.objects)) {
			try (ObjectInputStream ois = new ObjectInputStream(Files.newInputStream(ng.objects))) {
				specialFields = (Map<String, Object>) ois.readObject();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		} else {
			specialFields = ImmutableMap.of();
		}
		try {
			importGraphFromNodelang(ng.root_nl, existing, specialFields);
		} catch (NodeLangException e) {
			e.printStackTrace();
			Logger.getGlobal().severe("NodeLang File " + ng.root_nl + " could not be loaded correctly");
		}
		existing.eventBus.post(new LoadEvent(targetDir));
		return existing;
	}

	public void importGraphFromNodelang(Path root_nl, NodeGraph g, Map<String, Object> specialFields)
			throws IOException, NodeLangException {
		NodeLangParser nlp = new NodeLangParser();
		nlp.setTargetPath(root_nl.toAbsolutePath());
		nlp.parse();
		Map<String, INodeType> regTypes = loadNodes(nlp, root_nl, specialFields);
		loadInstances(nlp, g, regTypes, specialFields);
		loadConnections(nlp, g);
	}

	private Map<String, INodeType> loadNodes(NodeLangParser nlp, Path root_nl, Map<String, Object> specialFields)
			throws IOException, NodeLangException {
		Map<String, Node> nodes = nlp.getNodes();
		Map<String, INodeType> regTypes = new HashMap<>();
		DynamicNodeModule module = new DynamicNodeModule("lua_nodes");
		for (Entry<String, Node> entry : nodes.entrySet()) {
			Node node = entry.getValue();
			if (node.getFields().containsKey("src")) {
				String src = node.getFields().get("src").toString();
				if (src.contains(":")) {
					String[] parts = src.split(":");
					master.loadModule(parts[0]);
					INodeType type = master.getTypeByName(parts[1]);
					if (type == null) {
						throw new RuntimeNodeException("Error, node was specified with source " + src
								+ " but no type with that name could be found");
					}
					regTypes.put(node.getId(), type);
				} else {
					// this is a graph type
					NodeGraph newGraph = master.createNewGraph(node.getId());
					importGraphFromNodelang(root_nl.getParent().resolve(src), newGraph, specialFields);

					List<Tuple<String, Signal.Type>> inputs = node.getInputs();
					List<Tuple<String, Signal.Type>> outputs = node.getOutputs();

					for (Tuple<String, Signal.Type> in : inputs) {
						newGraph.addGlobalInput(in.a);
					}

					for (Tuple<String, Signal.Type> out : outputs) {
						newGraph.addGlobalOutput(out.a);
					}

					GraphNodeType gntype = new GraphNodeType(src);
					GraphNodeFactory gnfac = new GraphNodeFactory(newGraph, gntype);
					regTypes.put(node.getId(), gntype);
					module.addNode(gntype, gnfac);
				}
			} else {
				/*
				 * Logger.getLogger("NodeSystem")
				 * .warning("Loading a graph with non-external Nodes, this is not supported yet"
				 * );
				 */
				List<Tuple<String, Signal.Type>> inputs = node.getInputs();
				List<Tuple<String, Signal.Type>> outputs = node.getOutputs();
				Map<String, LuaVariable> fields = node.getFields();
				NamedType[] inputArray = inputs.stream().map(t -> NamedType.fromTuple(t)).toArray(NamedType[]::new);
				NamedType[] outputArray = outputs.stream().map(t -> NamedType.fromTuple(t)).toArray(NamedType[]::new);
				NamedType[] fieldArray = fields.entrySet().stream()
						.map(e -> new NamedType(e.getKey(), e.getValue().getType().getType()))
						.toArray(NamedType[]::new);

				INodeType type = new DynamicNodeType(node.getId());
				LuaComputeFunction fun = new LuaComputeFunction(node.getBehaviour());

				DynamicNodeFactory factory = new DynamicNodeFactory(type, inputArray, outputArray, fieldArray, fun);
				module.addNode(type, factory);
				regTypes.put(node.getId(), type);
			}
		}
		master.registerDynamicNodes(module);
		return regTypes;
	}

	private void loadInstances(NodeLangParser nlp, NodeGraph g, Map<String, INodeType> typeLookup,
			Map<String, Object> specialFields) {
		Map<String, Instance> instances = nlp.getInstances();

		for (Entry<String, Instance> entry : instances.entrySet()) {
			Instance inst = entry.getValue();
			Node parent = inst.getParent();
			// String src = parent.getFields().get("src").toString();
			// String[] parts = src.split(":");
			// INodeType type = stringToType.get(parts[1]);
			INodeType type = typeLookup.get(parent.getId());
			if (type == null) {
				throw new NoSuchNode("No node with type " + parent.getId() + " was registered");
			}
			g.addNode(type, inst.getName());

			for (Entry<String, LuaVariable> fieldEntry : inst.getFields().entrySet()) {
				Optional<INodeField> field = g.getField(inst.getName(), fieldEntry.getKey());

				field.ifPresent(f -> {
					Object o = fieldEntry.getValue().getVar();
					if ("null".equals(o))
						o = null;
					if (o instanceof String && specialFields.containsKey(o))
						o = specialFields.get(o);
					if (o instanceof String && BackendUtils.canSerialize(f.getType()))
						o = BackendUtils.fromString((String) o, f.getType());
					f.accept(o);
					g.eventBus.post(new ValueLoadedEvent(f.getID(), f.get()));
				});
			}
		}
	}

	private void loadConnections(NodeLangParser nlp, NodeGraph g) {
		Map<String, Signal> signals = nlp.getSignalMap();
		for (Entry<String, Signal> entry : signals.entrySet()) {
			Signal sig = entry.getValue();
			if (sig instanceof Consumer) {
				Producer driver = ((Consumer) sig).getProducer();
				if (driver == null)
					continue;

				INodeInputPort connectTo = null;
				INodeOutputPort connectFrom = null;
				// TODO check if this is a good way to do
				if (sig instanceof Port) {
					connectTo = g.getInputPort(sig.getName()).get();
				} else if (sig instanceof Wire) {
					connectTo = g.createSignal(sig.getName());
				} else {
					throw new IllegalArgumentException(
							"Consumer " + sig + " is not instance of either Port or Wire, this should not be possible");
				}
				// toConnect is now the Consumer to connect to
				if (driver instanceof Port) {
					// this is an output port
					Optional<? extends INodeOutputPort> oOutput = g.getOutputPort(((Port) driver).getName());
					connectFrom = oOutput
							.orElseThrow(() -> new NoSuchNode("Port " + ((Port) driver).getName() + " is not present"));

				} else if (driver instanceof Wire) {
					connectFrom = g.createSignal(driver.toString());
				} else {
					throw new IllegalArgumentException("Producer " + driver
							+ " is not instance of either Port or Wire, this should not be possible");
				}
				g.addConnection(connectTo, connectFrom);
			}
		}
	}

	private void writeConnections(NodeGraph graph, NodeLangBuilder nlb) throws IOException {
		if (graph.getConnections().isEmpty())
			return;
		nlb.addPart(new NLPartConnection(graph.getConnections().values()));
	}

	/**
	 * Write the Nodes to the nl File
	 * 
	 * @param graph
	 * @param writer
	 * @throws IOException
	 */
	private void writeNodes(NodeGraph graph, List<NLPartJavaType> types, List<NLPartJavaNode> nodes,
			List<GraphNodeFactory> extraGraphs) throws IOException {
		Set<INodeType> writtenTypes = new HashSet<>();
		for (Entry<String, AbstractNode> instance : graph.getNodeByIDMap().entrySet()) {
			if (!writtenTypes.contains(instance.getValue().getType())) {
				NLPartJavaType type = NLTypePartFactory.createPart(instance.getValue(), master);
				types.add(type);
				if (instance.getValue().getType() instanceof GraphNodeType) {
					GraphNodeFactory fac = (GraphNodeFactory) master.getFactoryFromType(instance.getValue().getType());
					extraGraphs.add(fac);
				}
				writtenTypes.add(instance.getValue().getType());
			}
			NLPartJavaNode node = new NLPartJavaNode(instance.getValue(), handler);
			nodes.add(node);
		}
	}

}
