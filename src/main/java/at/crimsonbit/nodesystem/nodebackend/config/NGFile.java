/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.config;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import com.eclipsesource.json.WriterConfig;
import com.google.common.collect.ImmutableList;

/**
 * A Class for creating and reading the .ng json File
 * 
 * @author Alexander Daum
 *
 */
public class NGFile {

	public static final String NG_FILE_ROOT_NL = "root_nl";
	public static final String NG_FILE_POSFILE = "posfile";
	public static final String NG_FILE_MODULES = "modules";
	public static final String NG_FILE_OBJS = "objects";

	public static final String NG_FILE_ROOT_NL_DEFAULT = "root.nl";
	public static final String NG_FILE_POSFILE_DEFAULT = ".pos";
	public static final String NG_FILE_MODULES_DEFAULT = "../Modules";
	public static final String NG_FILE_OBJS_DEFAULT = "obj.dat";

	/**
	 * A Path to the root nodelang (.nl) File All sub nl-files can just be included
	 * in the nl File with a Path relative to the working directory
	 */
	public final Path root_nl;
	/**
	 * A Path to the posfile where the positions of the GNodes are Stored
	 */
	public final Path posfile;
	/**
	 * A Path to the Module Folder used by this NodeGraph
	 */
	public final List<Path> modules;

	/**
	 * A Path the object file, warning may not exist
	 */
	public final Path objects;

	public NGFile(Path root_nl, Path posfile, List<Path> modules, Path objects) {
		super();
		this.root_nl = root_nl;
		this.posfile = posfile;
		this.modules = modules;
		this.objects = objects;
	}

	public static NGFile getDefault(Path parent) {
		List<Path> modules = new ArrayList<>();
		return new NGFile(parent.resolve(NG_FILE_ROOT_NL_DEFAULT), parent.resolve(NG_FILE_POSFILE_DEFAULT), modules,
				parent.resolve(NG_FILE_OBJS_DEFAULT));
	}

	public static NGFile getFromJson(Path parent) throws IOException {
		Path ngFile = parent.resolve(".ng");
		if (Files.exists(ngFile)) {
			try (Reader reader = Files.newBufferedReader(ngFile)) {
				JsonObject obj = Json.parse(reader).asObject();
				Path nl = parent.resolve(obj.getString(NG_FILE_ROOT_NL, NG_FILE_ROOT_NL_DEFAULT));
				Path pos = parent.resolve(obj.getString(NG_FILE_POSFILE, NG_FILE_POSFILE_DEFAULT));
				Path objs = parent.resolve(obj.getString(NG_FILE_OBJS, NG_FILE_OBJS_DEFAULT));
				JsonArray jsonModules = obj.get(NG_FILE_MODULES).asArray();
				List<Path> modules = new ArrayList<>();
				for (JsonValue s : jsonModules) {
					modules.add(parent.resolve(s.asString()));
				}
				return new NGFile(nl, pos, Collections.unmodifiableList(modules), objs);
			}
		} else {
			return getDefault(parent);
		}
	}

	public JsonObject toJson(Path parent) {
		JsonObject obj = new JsonObject();
		obj.add(NG_FILE_ROOT_NL, parent.relativize(root_nl).toString());
		JsonArray jsonModules = new JsonArray();
		for (Path p : modules) {
			jsonModules.add(parent.relativize(p).toString());
		}
		obj.add(NG_FILE_MODULES, jsonModules);
		obj.add(NG_FILE_POSFILE, parent.relativize(posfile).toString());
		obj.add(NG_FILE_OBJS, parent.relativize(objects).toString());
		return obj;
	}

	public void write(Path parent) throws IOException {
		Path ngFile = parent.resolve(".ng");
		if (Files.exists(ngFile))
			Files.delete(ngFile);
		try (BufferedWriter writer = Files.newBufferedWriter(ngFile, StandardOpenOption.CREATE)) {
			writer.write(toJson(parent).toString(WriterConfig.PRETTY_PRINT));
		}
	}

	public static NGFile getFromOrCreateJson(Path parentFolder) throws IOException {
		NGFile file = getFromJson(parentFolder);
		if (!Files.exists(parentFolder.resolve(".ng"))) {
			file.write(parentFolder);
		}
		return file;
	}

	public NGFile setModulesFolder(List<Path> modules) {
		return new NGFile(this.root_nl, this.posfile, ImmutableList.copyOf(modules), objects);
	}

	public NGFile setRootNL(Path root_nl) {
		return new NGFile(root_nl, this.posfile, this.modules, objects);
	}

	public NGFile setPosFile(Path posfile) {
		return new NGFile(this.root_nl, posfile, this.modules, objects);
	}

}
