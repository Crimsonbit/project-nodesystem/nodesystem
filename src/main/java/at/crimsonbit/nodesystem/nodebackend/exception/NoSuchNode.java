/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.exception;

import java.util.Collection;

import com.google.common.collect.ImmutableList;

/**
 * An Exception indicating, that a Node Does not exist
 * 
 * @author Alexander Daum
 *
 */
public class NoSuchNode extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6509586411824369320L;

	public final Collection<String> notExistingNodes;

	public NoSuchNode(Collection<String> notExistingNodes) {
		super();
		this.notExistingNodes = notExistingNodes;
	}

	public NoSuchNode(String message) {
		this(message, ImmutableList.of());
	}

	public NoSuchNode(String message, Throwable cause, Collection<String> notExistingNodes) {
		super(message, cause);
		this.notExistingNodes = notExistingNodes;
	}

	public NoSuchNode(String message, Collection<String> notExistingNodes) {
		super(message);
		this.notExistingNodes = notExistingNodes;
	}

	public NoSuchNode(Throwable cause, Collection<String> notExistingNodes) {
		super(cause);
		this.notExistingNodes = notExistingNodes;
	}

}
