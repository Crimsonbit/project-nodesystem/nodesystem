package at.crimsonbit.nodesystem;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class GraphConfig {

	private BooleanProperty runOnMobileProperty = new SimpleBooleanProperty(false);
	private BooleanProperty touchEnabledProperty = new SimpleBooleanProperty(false);
	private BooleanProperty menuLineEnabledProperty = new SimpleBooleanProperty(true);
	private BooleanProperty enableQuickMenuProperty = new SimpleBooleanProperty(true);
	private DoubleProperty quickMenuPositionProperty = new SimpleDoubleProperty(0.7D);
	private BooleanProperty useNodeAreaWidgetProperty = new SimpleBooleanProperty(false);
	private BooleanProperty useSelectionWidgetProperty = new SimpleBooleanProperty(false);
	private BooleanProperty useSearchBarWidgetProperty = new SimpleBooleanProperty(true);

	public GraphConfig() {

	}

	public BooleanProperty runOnMobileProperty() {
		return runOnMobileProperty;
	}

	public boolean runOnMobile() {
		return runOnMobileProperty().get();
	}

	public void setRunOnMobile(boolean runOnMobile) {
		runOnMobileProperty().set(runOnMobile);
	}

	public void setRunOnMobileProperty(BooleanProperty runOnMobileProperty) {
		this.runOnMobileProperty = runOnMobileProperty;
	}

	public BooleanProperty touchEnabledProperty() {
		return touchEnabledProperty;
	}

	public boolean touchEnabled() {
		return touchEnabledProperty().get();
	}

	public void setTouchEnabled(boolean touchEnabled) {
		touchEnabledProperty().set(touchEnabled);
	}

	public void setTouchEnabledProperty(BooleanProperty touchEnabledProperty) {
		this.touchEnabledProperty = touchEnabledProperty;
	}

	public BooleanProperty menuLineEnabledProperty() {
		return menuLineEnabledProperty;
	}

	public boolean menuLineEnabled() {
		return menuLineEnabledProperty().get();
	}

	public void setMenuLineEnabled(boolean menuLineEnabled) {
		menuLineEnabledProperty().set(menuLineEnabled);
	}

	public void setMenuLineEnabledProperty(BooleanProperty menuLineEnabledProperty) {
		this.menuLineEnabledProperty = menuLineEnabledProperty;
	}

	public BooleanProperty enableQuickMenuProperty() {
		return enableQuickMenuProperty;
	}

	public boolean enableQuickMenu() {
		return enableQuickMenuProperty().get();
	}

	public void setEnableQuickMenu(boolean enableQuickMenu) {
		enableQuickMenuProperty().set(enableQuickMenu);
	}

	public void setEnableQuickMenuProperty(BooleanProperty enableQuickMenuProperty) {
		this.enableQuickMenuProperty = enableQuickMenuProperty;
	}

	public DoubleProperty quickMenuPositionProperty() {
		return quickMenuPositionProperty;
	}

	public double quickMenuPosition() {
		return quickMenuPositionProperty().get();
	}

	public void setQuickMenuPosition(double position) {
		quickMenuPositionProperty().set(position);
	}

	public void setQuickMenuPositionProperty(DoubleProperty quickMenuPositionProperty) {
		this.quickMenuPositionProperty = quickMenuPositionProperty;
	}

	public BooleanProperty useNodeAreaWidgetProperty() {
		return useNodeAreaWidgetProperty;
	}

	public boolean useNodeAreaWidget() {
		return useNodeAreaWidgetProperty().get();
	}

	public void setUseNodeAreaWidget(boolean useNodeAreaWidget) {
		useNodeAreaWidgetProperty().set(useNodeAreaWidget);
	}

	public void setUseNodeAreaWidgetProperty(BooleanProperty useNodeAreaWidgetProperty) {
		this.useNodeAreaWidgetProperty = useNodeAreaWidgetProperty;
	}

	public BooleanProperty useSelectionWidgetProperty() {
		return useSelectionWidgetProperty;
	}

	public boolean useSelectionWidget() {
		return useSelectionWidgetProperty().get();
	}

	public void setUseSelectionWidget(boolean useSelectionWidget) {
		useSelectionWidgetProperty().set(useSelectionWidget);
	}

	public void setUseSelectionWidgetProperty(BooleanProperty useSelectionWidgetProperty) {
		this.useSelectionWidgetProperty = useSelectionWidgetProperty;
	}

	public BooleanProperty useSearchBarWidgetProperty() {
		return useSearchBarWidgetProperty;
	}

	public boolean useSearchBarWidget() {
		return useSearchBarWidgetProperty().get();
	}

	public void setUseSearchBarWidget(boolean useSearchBarWidget) {
		useSearchBarWidgetProperty().set(useSearchBarWidget);
	}

	public void setUseSearchBarWidgetProperty(BooleanProperty useSearchBarWidgetProperty) {
		this.useSearchBarWidgetProperty = useSearchBarWidgetProperty;
	}

}
