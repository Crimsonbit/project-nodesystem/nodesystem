/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.graph;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import at.crimsonbit.nodesystem.nodebackend.exception.NoSuchNode;
import at.crimsonbit.nodesystem.nodebackend.master.NodeMaster;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import htl.gtm.nodeLangParser.exception.NodeLangException;

class TestGraphSaving {

	static NodeMaster master;

	@BeforeAll
	static void loadModuleAndInitMaster() {
		master = new NodeMaster();
		master.addModuleFolder("Modules");
		master.loadModule("base_nodes");
	}

	@Test
	void testSaving() throws IOException {
		NodeGraph graph = master.createNewGraph("save");
		String constNode = graph.addNode("integer_constant");
		String outNode = graph.addNode("output");
		String in = outNode + ".input";
		String out = constNode + ".constant";
		graph.addConnection(in, out);
		Files.createDirectories(Paths.get("/", "tmp", "nodegraph-test-savebasic"));
		master.saveGraphToFile(graph, Paths.get("/", "tmp", "nodegraph-test-savebasic"));
	}

	@Test
	void testSavingAndLoading() throws NoSuchNode, IOException, NodeLangException {
		testSaving();
		NodeGraph loaded = master.loadGraphFromFile(Paths.get("/", "tmp", "nodegraph-test-savebasic"));
		Map<String, AbstractNode> nodes = loaded.getNodeByIDMap();
		assertEquals(2, nodes.size());
		assertTrue(loaded.isConnected("output0.input"));
	}

}
