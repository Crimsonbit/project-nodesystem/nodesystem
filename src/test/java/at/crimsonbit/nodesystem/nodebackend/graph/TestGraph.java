/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.graph;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import at.crimsonbit.nodesystem.nodebackend.master.NodeMaster;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeInputPort;
import at.crimsonbit.nodesystem.nodebackend.node.port.INodeOutputPort;

class TestGraph {
	static NodeMaster master;

	@BeforeAll
	static void loadModuleAndInitMaster() {
		master = new NodeMaster();
		master.addModuleFolder("Modules");
		master.loadModule("base_nodes");
	}

	@Test
	void testAddNodeToGraph() {
		NodeGraph graph = master.createNewGraph("add");
		String nodeId = graph.addNode("integer_constant");
		assertNotNull(nodeId);
		if (!nodeId.startsWith("integer_constant"))
			fail("NodeId is not in correct format");
		graph.clear();
	}

	@Test
	void testAddAndRemoveNodeFromGraph() {
		NodeGraph graph = master.createNewGraph("addAndRemove");
		String nodeId = graph.addNode("integer_constant");
		assertNotNull(nodeId);
		assertTrue(graph.removeNode(nodeId));
		assertFalse(graph.getNodeByID(nodeId).isPresent());
		graph.clear();
	}

	@Test
	void testConnectNodes() {
		NodeGraph graph = master.createNewGraph("connect");
		String constNode = graph.addNode("integer_constant");
		String outNode = graph.addNode("output");
		assertNotNull(constNode);
		assertNotNull(outNode);
		INodeInputPort in = graph.getInputsOfNode(outNode).get("input");
		INodeOutputPort out = graph.getOutputsOfNode(constNode).get("constant");
		graph.addConnection(in, out);
		assertTrue(graph.isConnected(in));
		graph.removeConnectionsFrom(out);
		assertFalse(graph.isConnected(in));
		graph.clear();
	}

	@Test
	void testConnectNodesByName() {
		NodeGraph graph = master.createNewGraph("connectByName");
		String constNode = graph.addNode("integer_constant");
		String outNode = graph.addNode("output");
		assertNotNull(constNode);
		assertNotNull(outNode);
		String in = outNode + ".input";
		String out = constNode + ".constant";
		graph.addConnection(in, out);
		assertTrue(graph.isConnected(in));
		graph.removeConnectionsFrom(out);
		assertFalse(graph.isConnected(in));
		graph.clear();
	}

	@Test
	void testComputeNode() {
		NodeGraph graph = master.createNewGraph("compute");
		String constNode = graph.addNode("integer_constant");
		String outNode = graph.addNode("output");
		String in = outNode + ".input";
		String out = constNode + ".constant";
		graph.addConnection(in, out);
		graph.setField(out, 20);
		Map<String, Object> results = graph.computeNode(outNode);
		assertEquals(20, results.get(outNode + ".output"));
	}

}
