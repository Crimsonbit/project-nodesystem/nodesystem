/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.testnodes;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeInput;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.node.port.InputVector;
import at.crimsonbit.nodesystem.nodebackend.node.port.OutputVector;

public class VectorTestNode extends AbstractNode {

	@NodeInput
	InputVector input;

	@NodeOutput
	OutputVector output;

	public VectorTestNode() {
		input = new InputVector(this, "input");
		input.setLength(2);
	}

	@Override
	public void compute() {
		output.setLength(input.getLength());
		Number i1 = (Number) input.getIn(0).get();
		Number i2 = (Number) input.getIn(1).get();
		output.getOut(0).set(10 + i1.intValue());
		output.getOut(1).set(100 + i2.intValue());
	}

}
