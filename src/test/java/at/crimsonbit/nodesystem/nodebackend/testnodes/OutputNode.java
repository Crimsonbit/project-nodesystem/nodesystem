package at.crimsonbit.nodesystem.nodebackend.testnodes;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeInput;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public class OutputNode extends AbstractNode {
	@NodeInput
	private Object in;

	@NodeOutput
	private Object out;

	@Override
	public void compute() {
		out = in;
	}

}
