/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.testnodes;

import at.crimsonbit.nodesystem.nodebackend.module.NodeModule;
import at.crimsonbit.nodesystem.nodebackend.module.NodeRegistry;

public class TestNodeModule extends NodeModule {

	@Override
	public void registerNodes(NodeRegistry registry) {
		registry.registerDefaultFactory(MyNodeTypes.NUM_CONSTANT, NumConstantNode.class);
		registry.registerDefaultFactory(MyNodeTypes.SIMPLE, SimpleNode.class);
		registry.registerDefaultFactory(MyNodeTypes.STR_CONSTANT, StrConstantNode.class);
		registry.registerDefaultFactory(MyNodeTypes.OUTPUT, OutputNode.class);
	}

}
