/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.node;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import at.crimsonbit.nodesystem.nodebackend.graph.ConnectionResult;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import at.crimsonbit.nodesystem.nodebackend.master.NodeMaster;
import at.crimsonbit.nodesystem.nodebackend.module.NodeRegistry;
import at.crimsonbit.nodesystem.nodebackend.module.NodeRegistry.NodeRegistryObject;
import at.crimsonbit.nodesystem.nodebackend.node.DynamicNodeFactory.NamedType;
import at.crimsonbit.nodesystem.nodebackend.testnodes.DynamicTestNodeTypes;

class TestDynamicPortNode {

	@Test
	void testCreation() {
		NamedType[] inputs = new NamedType[] { new NamedType("input", Object.class) };
		NamedType[] outputs = new NamedType[] { new NamedType("output", Object.class) };
		NamedType[] fields = new NamedType[] {};
		DynamicNodeFactory dnf = new DynamicNodeFactory(DynamicTestNodeTypes.PASSTHROUGH, inputs, outputs, fields,
				(ins, outType) -> {
					HashMap<String, Object> returnMap = new HashMap<>();
					returnMap.put("output", ins.get("input"));
					return returnMap;
				});
		AbstractNode node = dnf.create("dynamic");
		assertNotNull(node);
	}

	private NodeGraph createGraphWithRegisteredDynamicNodes() {
		NodeMaster master = new NodeMaster();
		NodeRegistry reg = new NodeRegistry("dynamics");
		reg.register(DynamicTestNodeTypes.PASSTHROUGH, new NodeRegistryObject(createPassthroughNode()));
		reg.register(DynamicTestNodeTypes.ADDITION, new NodeRegistryObject(createAdditionNode()));
		reg.register(DynamicTestNodeTypes.CONSTANT, new NodeRegistryObject(createConstantNode()));
		master.registerNodes(reg);
		return master.createNewGraph("graph");
	}

	@Test
	void testSimple() {
		NodeGraph g = createGraphWithRegisteredDynamicNodes();
		String constant = g.addNode(DynamicTestNodeTypes.CONSTANT, "constant42");
		String pass = g.addNode(DynamicTestNodeTypes.PASSTHROUGH, "pass");
		assertEquals(constant, "constant42");
		assertEquals(pass, "pass");
		boolean set = g.setField(constant, "constIn", 42);
		assertTrue(set);
		ConnectionResult con = g.addConnection(pass + ".input", constant + ".constant");
		assertEquals(ConnectionResult.SUCCESS, con);
		Map<String, Object> results = g.executeGraph();
		assertEquals(42, results.get(pass + ".output"));
	}

	private NodeFactory createPassthroughNode() {
		NamedType[] inputs = new NamedType[] { new NamedType("input", Object.class) };
		NamedType[] outputs = new NamedType[] { new NamedType("output", Object.class) };
		NamedType[] fields = new NamedType[] {};
		DynamicNodeFactory dnf = new DynamicNodeFactory(DynamicTestNodeTypes.PASSTHROUGH, inputs, outputs, fields,
				(ins, outType) -> {
					HashMap<String, Object> returnMap = new HashMap<>();
					returnMap.put("output", ins.get("input"));
					return returnMap;
				});
		return dnf;
	}

	private NodeFactory createConstantNode() {
		NamedType[] inputs = new NamedType[] {};
		NamedType[] outputs = new NamedType[] { new NamedType("constant", Number.class) };
		NamedType[] fields = new NamedType[] { new NamedType("constIn", Number.class) };
		DynamicNodeFactory dnf = new DynamicNodeFactory(DynamicTestNodeTypes.CONSTANT, inputs, outputs, fields,
				(ins, outType) -> {
					HashMap<String, Object> returnMap = new HashMap<>();
					returnMap.put("constant", ins.get("constIn"));
					return returnMap;
				});
		return dnf;
	}

	private NodeFactory createAdditionNode() {
		NamedType[] inputs = new NamedType[] { new NamedType("a", Number.class), new NamedType("b", Number.class) };
		NamedType[] outputs = new NamedType[] { new NamedType("sum", Number.class) };
		NamedType[] fields = new NamedType[] {};
		DynamicNodeFactory dnf = new DynamicNodeFactory(DynamicTestNodeTypes.ADDITION, inputs, outputs, fields,
				(ins, outType) -> {
					HashMap<String, Object> returnMap = new HashMap<>();
					Number a = (Number) ins.get("a");
					Number b = (Number) ins.get("b");
					Number sum = a.doubleValue() + b.doubleValue();
					returnMap.put("sum", sum);
					return returnMap;
				});
		return dnf;
	}

}
