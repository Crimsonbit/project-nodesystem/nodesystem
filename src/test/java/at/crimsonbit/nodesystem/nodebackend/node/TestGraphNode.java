package at.crimsonbit.nodesystem.nodebackend.node;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;

import org.junit.jupiter.api.Test;

import com.google.common.collect.ImmutableList;

import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import at.crimsonbit.nodesystem.nodebackend.master.NodeMaster;
import at.crimsonbit.nodesystem.nodebackend.module.DynamicNodeModule;
import at.crimsonbit.nodesystem.nodebackend.module.NodeRegistry;
import at.crimsonbit.nodesystem.nodebackend.testnodes.MyNodeTypes;
import at.crimsonbit.nodesystem.nodebackend.testnodes.TestNodeModule;

class TestGraphNode {

	private NodeMaster createMaster() {
		NodeMaster master = new NodeMaster();
		NodeRegistry registry = new NodeRegistry("simpleTypes");
		new TestNodeModule().registerNodes(registry);
		master.registerNodes(registry);
		return master;
	}

	@Test
	void test() {
		NodeMaster master = createMaster();

		NodeGraph mainGraph = master.createNewGraph("main");
		NodeGraph subGraph = master.createNewGraph("sub");
		subGraph.addNode(MyNodeTypes.SIMPLE, "concat");
		mainGraph.addNode(MyNodeTypes.NUM_CONSTANT, "number");
		mainGraph.addNode(MyNodeTypes.STR_CONSTANT, "string");
		subGraph.addGlobalInput("concat.val1");
		subGraph.addGlobalInput("concat.s1");
		subGraph.addGlobalOutput("concat.concat");
		NodeFactory graphFac = new GraphNodeFactory(subGraph, new DynamicNodeType("subGraph"));
		DynamicNodeModule module = new DynamicNodeModule("graphModule");
		module.addNode(graphFac.getType(), graphFac);
		NodeRegistry reg = new NodeRegistry("graphNodes");
		module.registerNodes(reg);
		master.registerNodes(reg);
		mainGraph.addNode(graphFac.getType(), "subGraph");

		mainGraph.addConnection("subGraph.val1", "number.constant");
		mainGraph.addConnection("subGraph.s1", "string.constant");
		mainGraph.setField("number.constant", 20);
		mainGraph.setField("string.constant", "HelloWorld");
		Map<String, Object> results = mainGraph.executeGraph();
		Object out = results.get("subGraph.concat");

		assertEquals("HelloWorld20", out);
	}

	@Test
	void testCreateNewGraph() {
		NodeMaster master = createMaster();
		NodeGraph mainGraph = master.createNewGraph("main");

		mainGraph.addNode(MyNodeTypes.SIMPLE, "concat");
		mainGraph.addNode(MyNodeTypes.NUM_CONSTANT, "number");
		mainGraph.addNode(MyNodeTypes.STR_CONSTANT, "string");
		mainGraph.addNode(MyNodeTypes.OUTPUT, "out");
		
		mainGraph.addConnection("concat.val1", "number.constant");
		mainGraph.addConnection("concat.s1", "string.constant");
		mainGraph.addConnection("out.in", "concat.concat");
		
		mainGraph.setField("number.constant", 20);
		mainGraph.setField("string.constant", "HelloWorld");
		

		mainGraph.createNewGraphNode(ImmutableList.of("concat"), "subGraph");

		Map<String, Object> results = mainGraph.executeGraph();
		Object out = results.get("subGraph.concat");
		assertEquals("HelloWorld20", out);
	}

}
