/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.node;

import static org.junit.Assume.assumeTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.*;

import at.crimsonbit.nodesystem.nodebackend.graph.ConnectionResult;
import at.crimsonbit.nodesystem.nodebackend.graph.NodeGraph;
import at.crimsonbit.nodesystem.nodebackend.master.NodeMaster;
import at.crimsonbit.nodesystem.nodebackend.module.NodeRegistry;
import at.crimsonbit.nodesystem.nodebackend.module.NodeRegistry.NodeRegistryObject;
import at.crimsonbit.nodesystem.nodebackend.testnodes.NumConstantNode;
import at.crimsonbit.nodesystem.nodebackend.testnodes.MyNodeTypes;
import at.crimsonbit.nodesystem.nodebackend.testnodes.VectorTestNode;

public class TestVarlenPort {
	static NodeMaster master;

	@BeforeAll
	static void loadModuleAndInitMaster() {
		master = new NodeMaster();
		master.addModuleFolder("Modules");
	}

	private NodeGraph createGraphWithRegisteredDynamicNodes() {
		NodeMaster master = new NodeMaster();
		NodeRegistry reg = new NodeRegistry("dynamics");
		reg.register(MyNodeTypes.VECTOR_TEST,
				new NodeRegistryObject(new DefaultNodeFactory(MyNodeTypes.VECTOR_TEST, VectorTestNode.class)));
		reg.register(MyNodeTypes.NUM_CONSTANT,
				new NodeRegistryObject(new DefaultNodeFactory(MyNodeTypes.NUM_CONSTANT, NumConstantNode.class)));
		master.registerNodes(reg);
		return master.createNewGraph("root");
	}

	@Test
	void testNodeCreation() {
		NodeGraph g = createGraphWithRegisteredDynamicNodes();
		g.addNode(MyNodeTypes.VECTOR_TEST);
		g.clear();
	}

	@Test
	void testOutput() {
		if(true) return; //TODO fix that
		NodeGraph g = createGraphWithRegisteredDynamicNodes();
		g.addNode(MyNodeTypes.VECTOR_TEST, "vec1");
		g.addNode(MyNodeTypes.NUM_CONSTANT, "const1");
		g.addNode(MyNodeTypes.NUM_CONSTANT, "const2");
		g.setField("const1.constant", 10);
		g.setField("const2.constant", 20);
		ConnectionResult con1 = g.addConnection("vec1.input[0]", "const1.constant");
		System.out.println("Connection returned: " + con1);
		ConnectionResult con2 = g.addConnection("vec1.input[1]", "const2.constant");
		System.out.println("Connection returned: " + con2);
		Map<String, Object> outs = g.computeNode("vec1");
		outs.entrySet().forEach(System.out::println);
	}
}
