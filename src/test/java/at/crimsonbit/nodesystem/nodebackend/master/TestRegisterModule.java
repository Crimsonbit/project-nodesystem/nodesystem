/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.master;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;

import at.crimsonbit.nodesystem.nodebackend.INodeType;
import at.crimsonbit.nodesystem.nodebackend.testnodes.MyNodeTypes;
import at.crimsonbit.nodesystem.nodebackend.testnodes.TestNodeModule;

class TestRegisterModule {

	@Test
	void test() {
		NodeMaster master = new NodeMaster();
		master.registerNodes(new TestNodeModule());
		Set<INodeType> nodes = master.getRegisteredNodes();
		List<INodeType> expected = new ArrayList<>();
		expected.add(MyNodeTypes.NUM_CONSTANT);
		expected.add(MyNodeTypes.SIMPLE);
		expected.add(MyNodeTypes.STR_CONSTANT);
		expected.add(MyNodeTypes.OUTPUT);
		assertTrue(CollectionUtils.isEqualCollection(expected, nodes));
	}

}
