/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package at.crimsonbit.nodesystem.nodebackend.module;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class TestModuleID {

	@Test
	void testLatestID() {
		ModuleID v1 = new ModuleID("sample", "1.0");
		ModuleID v2_1 = new ModuleID("sample", "2.1");
		ModuleID v2_1_a = new ModuleID("sample", "2.1.a");
		// also check strange versions
		ModuleID strange1 = new ModuleID("sample", "VERSION 5");
		ModuleID strange2 = new ModuleID("sample", "\t startign with a tab");
		ModuleID otherModule = new ModuleID("zample", "1.0");

		ModuleID latest = ModuleID.getLatest("sample");
		assertTrue(latest.compareTo(v1) > 0);
		assertTrue(latest.compareTo(v2_1) > 0);
		assertTrue(latest.compareTo(v2_1_a) > 0);
		assertTrue(latest.compareTo(strange1) > 0);
		assertTrue(latest.compareTo(strange2) > 0);
		assertTrue(latest.compareTo(otherModule) < 0);
	}

}
