node int <src = base_nodes:integer_constant, number constant=0>(
	out number constant
);

node add(
	in number a,
	in number b,
	out number sum
)
{
	sum = a + b;
};

node out(
	in input,
	out output
)
{
	output = input;
};

create out output();

create add addition();

create int <constant = 20> const20(
	.constant(addition.a)
);
create int <constant = 42> const42(
	.constant(addition.b)
);

connect(
	output.input(addition.sum)
);