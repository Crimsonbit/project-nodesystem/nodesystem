node int <src = base_nodes:integer_constant, number constant=0>(
	out constant
);

node out <src = base_nodes:output>(
	in input,
	out output
);

create out output();

create int <constant = 20> const(
	.constant(output.input)
);